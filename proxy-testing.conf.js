module.exports = {
  // Redirect to fake server
  '/api': {
    target: `https://dev-1.ryax.io`,
    secure: false,
    changeOrigin: true,
    bypass: function(req) {
      req.headers["X-Forwarded-Prefix"] = "/api";
    }
  },
  '/studio': {
    target: `https://dev-1.ryax.io`,
    secure: false,
    changeOrigin: true
  },
  '/repository': {
    target: `https://dev-1.ryax.io`,
    secure: false,
    changeOrigin: true
  },
  '/authorization': {
    target: `https://dev-1.ryax.io`,
    secure: false,
    changeOrigin: true
  },
  '/functions': {
    target: `https://dev-1.ryax.io`,
    secure: false,
    changeOrigin: true,
    bypass: function(req) {
      req.headers["X-Forwarded-Prefix"] = "/functions";
    }
  }
};
