// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Route } from '@angular/router';
// tslint:disable-next-line:nx-enforce-module-boundaries
import { AuthAccessGuard, AuthLoginComponent, AuthPrivateComponent } from '@ryax/auth/shell';
import { WorkflowPortalAccessGuard, WorkflowPortalComponent } from '@ryax/workflow/feature-portal';
import { TutorialComponent } from '@ryax/tutorial/feature-portal';

const indexRoute: Route = {
  path: '',
  pathMatch: 'full',
  redirectTo: 'studio',
};

const loginRoute: Route = {
  path: 'login',
  component: AuthLoginComponent
};

const portalFormRoute: Route = {
  path: 'studio/workflows/:workflowId/portals/:workflowPortalId',
  canActivate: [WorkflowPortalAccessGuard],
  component: WorkflowPortalComponent
};

const portalTutorialRoute: Route = {
  path: 'tutorial/:id',
  component: TutorialComponent
};

const studioRoute: Route = {
  path: 'studio',
  data: {
    sidebar: {
      title: 'Studio',
      icon: 'experiment'
    },
    breadcrumb: 'Studio'
  },
  component: AuthPrivateComponent,
  canActivate: [AuthAccessGuard],
  loadChildren: () => import('@ryax/workflow/shell').then(m => m.WorkflowShellModule)
};

const monitoringRoute: Route = {
  path: 'workflows/monitoring',
  data: {
    sidebar: {
      title: 'Monitoring',
      icon: 'monitor'
    },
    breadcrumb: "Monitoring"
  },
  component: AuthPrivateComponent,
  canActivate: [AuthAccessGuard],
  loadChildren: () => import('@ryax/monitoring/shell').then(m => m.MonitoringShellModule),
};

// TODO : Remove this alias after navigation refactoring
const modulesStoreRoute: Route = {
  path: 'modules/store',
  pathMatch: 'full',
  redirectTo: 'store',
};

const storeRoute: Route = {
  path: 'store',
  data: {
    title: "Module store",
    sidebar: {
      title: 'Module store',
      icon: 'appstore'
    },
    breadcrumb: 'Module store'
  },
  component: AuthPrivateComponent,
  canActivate: [AuthAccessGuard],
  loadChildren: () => import('@ryax/store/shell').then(m => m.StoreShellModule)
};

const usersRoute: Route = {
  path: 'users',
  data: {
    breadcrumb: "Users"
  },
  component: AuthPrivateComponent,
  canActivate: [AuthAccessGuard],
  loadChildren: () => import('@ryax/user/shell').then(m => m.UserShellModule),
};

const repositoriesRoute: Route = {
  path: 'repositories',
  data: {
    breadcrumb: "Module builder"
  },
  component: AuthPrivateComponent,
  canActivate: [AuthAccessGuard],
  loadChildren: () => import('@ryax/repository/shell').then(m => m.RepositoryShellModule),
};

const authorizationRoute: Route = {
  path: 'projects',
  data: {
    breadcrumb: "Projects"
  },
  component: AuthPrivateComponent,
  canActivate: [AuthAccessGuard],
  loadChildren: () => import('@ryax/authorization/shell').then(m => m.AuthorizationShellModule),
};

const errorRoute: Route = {
  path: 'error',
  data: { title: 'Error' },
  component: AuthPrivateComponent,
  canActivate: [AuthAccessGuard],
  loadChildren: () => import('@ryax/error-ui').then(m => m.ErrorUiModule),
};

const wildcardRoute: Route = {
  path: '**',
  redirectTo: 'error',
};

export const appRoutes = [
  indexRoute,
  loginRoute,
  portalTutorialRoute,
  studioRoute,
  portalFormRoute,
  monitoringRoute,
  modulesStoreRoute,
  storeRoute,
  usersRoute,
  repositoriesRoute,
  authorizationRoute,
  errorRoute,
  wildcardRoute,
];
