// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, OnInit } from '@angular/core';
import { ProjectDetailsFacade } from '../../../../domain/src/lib/application/facades';
import { ProjectDetailsActions } from '../../../../domain/src/lib/application/actions';

@Component({
  selector: 'ryax-project-details',
  templateUrl: './project-details.component.pug'
})
export class ProjectDetailsComponent implements OnInit {
  project$ = this.facade.project$;
  users$ = this.facade.users$;
  visibility$ = this.facade.visibility$;

  constructor(
    private readonly facade: ProjectDetailsFacade
  ) {}

  ngOnInit(): void {
    this.facade.init();
  }

  refresh() {
    this.facade.refresh();
  }

  editProject(projectId: string) {
    this.facade.editProject(projectId);
  }

  addUserToProject() {
    this.facade.openUserAssignModal();
  }

  closeUserAssignModal() {
    this.facade.closeUserAssignModal();
  }

  addUser(userId: string) {
    this.facade.addUserToProject(userId);
  }

  removeUser(userId: string) {
    this.facade.removeUserFromProject(userId);
  }
}
