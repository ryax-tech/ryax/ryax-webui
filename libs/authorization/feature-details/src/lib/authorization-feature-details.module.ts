// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedUiCommonModule } from '@ryax/shared/ui-common';
import { ProjectDetailsComponent } from './project-details/project-details.component';
import { ProjectDetailsHeaderComponent } from './project-details-header/project-details-header.component';
import { ProjectDetailsBodyComponent } from './project-details-body/project-details-body.component';
import { AuthorizationDomainModule } from '@ryax/authorization/domain';
import { AuthorizationFeatureEditModule } from '@ryax/authorization/feature-edit';
import { AuthorizationUiModule } from '@ryax/authorization/ui';

@NgModule({
  imports: [
    CommonModule,
    SharedUiCommonModule,
    AuthorizationDomainModule,
    AuthorizationFeatureEditModule,
    AuthorizationUiModule,
  ],
  declarations: [
    ProjectDetailsComponent,
    ProjectDetailsHeaderComponent,
    ProjectDetailsBodyComponent
  ],
  exports: [
    ProjectDetailsComponent
  ]
})
export class AuthorizationFeatureDetailsModule {}
