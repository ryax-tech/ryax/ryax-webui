// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Project } from '../../../../domain/src/lib/entities';

@Component({
  selector: 'ryax-project-details-header',
  templateUrl: './project-details-header.component.pug'
})
export class ProjectDetailsHeaderComponent {
  @Input() project: Project;
  @Output() refresh = new EventEmitter<void>();
  @Output() editProject = new EventEmitter<string>();
  @Output() addUserToProject = new EventEmitter<void>();

  onRefresh() {
    this.refresh.emit();
  }

  onEdit(projectId: string) {
    this.editProject.emit(projectId);
  }

  onAddUserToProject() {
    this.addUserToProject.emit();
  }

}
