// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ProjectDetails } from '../../../../domain/src/lib/entities';

@Component({
  selector: 'ryax-project-details-body',
  templateUrl: './project-details-body.component.pug'
})
export class ProjectDetailsBodyComponent {
  @Input() loading: boolean;
  @Input() project: ProjectDetails;
  @Output() removeUser = new EventEmitter<string>();

  trackIByItemId(index: number, item: any) {
    return item.id;
  }

  onRemoveUser(userId: string) {
    this.removeUser.emit(userId)
  }
}
