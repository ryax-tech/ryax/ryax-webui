// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthorizationFeatureProjectSelectModule } from '@ryax/authorization/feature-project-select';

@NgModule({
  imports: [
    CommonModule,
    AuthorizationFeatureProjectSelectModule
  ],
  exports: [
    AuthorizationFeatureProjectSelectModule
  ]
})
export class AuthorizationApiModule {}
