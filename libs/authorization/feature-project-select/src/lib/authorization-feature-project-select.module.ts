// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedUiCommonModule } from '@ryax/shared/ui-common';
import { AuthorizationDomainModule } from '@ryax/authorization/domain';
import { ProjectSelectComponent } from './project-select/project-select.component';
import { ProjectDropdownComponent } from './project-dropdown/project-dropdown.component';
import { AuthorizationUiModule } from '@ryax/authorization/ui';

@NgModule({
  imports: [
    CommonModule,
    SharedUiCommonModule,
    AuthorizationDomainModule,
    AuthorizationUiModule
  ],
  declarations: [
    ProjectSelectComponent,
    ProjectDropdownComponent
  ],
  exports: [
    ProjectSelectComponent,
  ]
})
export class AuthorizationFeatureProjectSelectModule {}
