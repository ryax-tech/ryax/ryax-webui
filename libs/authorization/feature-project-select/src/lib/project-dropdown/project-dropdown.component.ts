// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Project } from '../../../../domain/src/lib/entities';
import { Subject } from 'rxjs';
import { FormControl, FormGroup } from '@angular/forms';
import { filter, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'ryax-project-dropdown',
  templateUrl: './project-dropdown.component.pug',
  styleUrls: ['./project-dropdown.component.scss'],
})
export class ProjectDropdownComponent implements OnInit, OnDestroy {
  private _currentProject: Project;

  form = new FormGroup({
    project: new FormControl(null),
  }, { updateOn: 'change' });
  destroy$ = new Subject<void>();

  @Input() projects: Project[];
  @Input() set currentProject(value: Project) {
    this._currentProject = value;
    this.initForm();
  }
  @Output() filterChange = new EventEmitter<string>();

  private initForm() {
    const formValue = {
      project: this._currentProject ? this._currentProject.id : null,
    };
    this.form.reset(formValue);
  }

  ngOnInit(): void {
    this.initForm();
    this.form.valueChanges.pipe(
      filter(() => this.form.valid),
      takeUntil(this.destroy$),
    ).subscribe((formData) => {
      const { project } = formData;
      if(this._currentProject.id) { // prevent the project id to be set with a null value
        if(formData.project === this._currentProject.id) {
          // console.log('same project')
        } else {
          this.filterChange.emit(project);
        }
      }
    });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
