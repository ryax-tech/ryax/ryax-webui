// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, OnInit } from '@angular/core';
import { ProjectSelectFacade } from '../../../../domain/src/lib/application/facades';

@Component({
  selector: 'ryax-project-select',
  templateUrl: './project-select.component.pug'
})
export class ProjectSelectComponent implements OnInit {
  projects$ = this.facade.projects$;
  currentProject$ = this.facade.currentProject$;
  notFoundModalVisibility$ = this.facade.notFoundModalVisibility$;

  constructor(
    private readonly facade: ProjectSelectFacade
  ) {}

  ngOnInit(): void {
    this.facade.init();
  }

  changeProject(projectId: string) {
    this.facade.changeProject(projectId);
  }

  closeModal() {
    this.facade.closeModal();
  }
}
