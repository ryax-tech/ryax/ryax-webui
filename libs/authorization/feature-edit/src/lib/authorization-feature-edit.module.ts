// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedUiCommonModule } from '@ryax/shared/ui-common';
import { AuthorizationDomainModule } from '@ryax/authorization/domain';
import { ProjectEditComponent } from './project-edit/project-edit.component';
import { ProjectEditFormComponent } from './project-edit-form/project-edit-form.component';

@NgModule({
  imports: [
    CommonModule,
    AuthorizationDomainModule,
    SharedUiCommonModule
  ],
  declarations: [
    ProjectEditComponent,
    ProjectEditFormComponent,
  ],
  exports: [
    ProjectEditComponent,
  ]
})
export class AuthorizationFeatureEditModule {}
