// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component } from '@angular/core';
import { ProjectEditFacade } from '../../../../domain/src/lib/application/facades';
import { UpdateProjectData } from '../../../../domain/src/lib/entities';

@Component({
  selector: 'ryax-project-edit',
  templateUrl: './project-edit.component.pug'
})
export class ProjectEditComponent {
  project$ = this.facade.project$;
  editDrawerVisible$ = this.facade.editDrawerVisible$;

  constructor(
    private readonly facade: ProjectEditFacade
  ) {}

  onSave(data: UpdateProjectData) {
    this.facade.save(data);
  }

  onClose() {
    this.facade.closeEditDrawer();
  }
}
