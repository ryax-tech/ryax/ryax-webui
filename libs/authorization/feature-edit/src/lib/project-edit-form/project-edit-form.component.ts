// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Project, UpdateProjectData } from '../../../../domain/src/lib/entities';

@Component({
  selector: 'ryax-project-edit-form',
  templateUrl: './project-edit-form.component.pug'
})
export class ProjectEditFormComponent {
  private _project: Project;

  form = new FormGroup({
    name: new FormControl(null, Validators.required),
  });

  @Input() set project(value: Project) {
    this._project = value;
    this.initForm();
  }
  @Output() save = new EventEmitter<UpdateProjectData>();
  @Output() cancel = new EventEmitter<void>();


  private initForm() {
    const formValue = {
      name: this._project ? this._project.name : null,
    };
    this.form.reset(formValue);
  }

  onSave() {
    const formView = this.form.value;
    this.save.emit(formView);
  }

  onCancel() {
    this.cancel.emit();
  }
}
