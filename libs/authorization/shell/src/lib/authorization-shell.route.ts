// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { ProjectListComponent } from '../../../feature-list/src/lib/project-list/project-list.component';
import { ProjectDetailsComponent } from '../../../feature-details/src/lib/project-details/project-details.component';

export const routes = [
  {
    path: '',
    component: ProjectListComponent
  },
  {
    path: ':projectId',
    component: ProjectDetailsComponent,
    data: {
      breadcrumb: "Details"
    }
  },
];
