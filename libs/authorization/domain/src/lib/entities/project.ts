// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
export interface Project {
  id: string;
  name: string;
  creationDate: Date
}

export interface ProjectDetails extends Project{
  users: ProjectUser[]
}

export interface ProjectUser {
  id: string;
  username: string;
}

export interface AddProjectData {
  name: string;
}

export interface UpdateProjectData {
  name: string;
}
