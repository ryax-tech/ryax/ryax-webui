// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
export interface ProjectDto {
  id: string;
  name: string;
  creation_date: Date
}

export interface ProjectDetailsDto extends ProjectDto{
  users: ProjectUserDto[]
}

export interface ProjectUserDto {
  id: string;
  username: string;
}

export interface AddProjectDataDto {
  name: string;
}

export interface UpdateProjectDataDto {
  name: string;
}

export interface AddProjectResponseDto {
  project_id: string;
}
