// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { map, tap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import {
  AddProjectDataDto,
  AddProjectResponseDto,
  ProjectDetailsDto,
  ProjectDto,
  UpdateProjectDataDto,
} from '../dtos';
import { ProjectAdapter } from '../adapters';
import { AddProjectData, UpdateProjectData } from '../../entities';

@Injectable()
export class ProjectApi {
  private baseUrl = "/api/authorization/projects";
  private userUrl = "/api/authorization/users";

  constructor(
    private readonly http: HttpClient,
    private readonly projectAdapter: ProjectAdapter,
  ) {}

  loadAll() {
    return this.http.get<ProjectDto[]>(this.baseUrl, { responseType: "json" }).pipe(
      map(dtos => dtos.map(item => this.projectAdapter.adapt(item)))
    );
  }

  loadOne(projectId: string) {
    const url = [this.baseUrl, projectId].join('/');
    return this.http.get<ProjectDetailsDto>(url, { responseType: "json" }).pipe(
      map(dto => this.projectAdapter.adaptDetails(dto))
    );
  }

  createProject(data: AddProjectData) {
    const createDto: AddProjectDataDto = {
      name: data.name ? data.name : undefined,
    };
    return this.http.post<AddProjectResponseDto>(this.baseUrl, createDto, { responseType: "json" }).pipe(
      map(dto => dto.project_id)
    );
  }

  updateProject(projectId: string, data: UpdateProjectData) {
    const url = [this.baseUrl, projectId].join('/');
    const updateDto: UpdateProjectDataDto = {
      name: data.name ? data.name : undefined,
    };
    return this.http.put<ProjectDto>(url, updateDto, { responseType: "json" }).pipe(
      map(dto => this.projectAdapter.adapt(dto))
    );
  }

  deleteProject(projectId: string) {
    const url = [this.baseUrl, projectId].join('/');
    return this.http.delete<void>(url, { responseType: 'json' });
  }

  addUser(projectId: string, userId: string) {
    const url = [this.baseUrl, projectId, 'user'].join('/');
    const data = {
      user_id: userId
    };
    return this.http.post<void>(url, data,{ responseType: 'json' });
  }

  removeUser(projectId: string, userId: string) {
    const url = [this.baseUrl, projectId, 'user', userId].join('/');
    return this.http.delete<void>(url,{ responseType: 'json' });
  }

  getCurrentUserId() {
    const url = ['/api/authorization', 'me'].join('/');
    return this.http.get<any>(url, { responseType: "json" }).pipe(
      map(dto => dto.id)
    );
  }

  loadAssignedProjects(userId: string) {
    const url = [this.baseUrl, 'users', userId, 'projects'].join('/');
    return this.http.get<ProjectDto[]>(url, { responseType: "json" }).pipe(
      map(dtos => dtos.map(item => this.projectAdapter.adapt(item)))
    );
  }

  loadCurrentProject(userId: string) {
    const url = [this.baseUrl, 'users', userId, 'current'].join('/');
    return this.http.get<ProjectDto>(url, { responseType: "json" }).pipe(
      map(dto => this.projectAdapter.adapt(dto))
    );
  }

  changeCurrentProject(userId: string, projectId: string) {
    const url = [this.baseUrl, 'users', userId, 'current'].join('/');
    const newProjectId = {
      project_id: projectId
    };
    return this.http.post<void>(url, newProjectId, { responseType: "json" }).pipe(
      map(dto => dto)
    );
  }
}
