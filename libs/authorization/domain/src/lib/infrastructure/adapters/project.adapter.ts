// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { Project, ProjectDetails, ProjectUser } from '../../entities';
import { ProjectDetailsDto, ProjectDto, ProjectUserDto } from '../dtos';

@Injectable()
export class ProjectAdapter {
  adapt(dto: ProjectDto): Project {
    return {
      id: dto ? dto.id : null,
      name: dto ? dto.name : null,
      creationDate: dto ? dto.creation_date : null,
    };
  }

  adaptDetails(dto: ProjectDetailsDto): ProjectDetails {
    return {
      id: dto ? dto.id : null,
      name: dto ? dto.name : null,
      creationDate: dto ? dto.creation_date : null,
      users: dto ? dto.users.map(item => this.adaptProjectUser(item)) : [],
    };
  }

  adaptProjectUser(dto: ProjectUserDto): ProjectUser {
    return {
      id: dto.id,
      username: dto.username
    };
  }

}
