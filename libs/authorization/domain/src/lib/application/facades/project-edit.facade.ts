// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { ProjectEditActions } from '../actions';
import { ProjectState, selectEditDrawerVisible, selectEditProject } from '../reducers';
import { Store } from '@ngrx/store';
import { UpdateProjectData } from '../../entities';

@Injectable()
export class ProjectEditFacade {
  editDrawerVisible$ = this.store.select(selectEditDrawerVisible);
  project$ = this.store.select(selectEditProject);

  constructor(
    private readonly store: Store<ProjectState>,
  ) {}

  save(data: UpdateProjectData) {
    this.store.dispatch(ProjectEditActions.save(data));
  }

  closeEditDrawer() {
    this.store.dispatch(ProjectEditActions.closeEditDrawer())
  }
}
