// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { ProjectSelectActions } from '../actions';
import {
  ProjectState,
  selectCurrentAssignedProject,
  selectNotFoundModalVisibility,
  selectUserAssignedProjects,
} from '../reducers';
import { Store } from '@ngrx/store';

@Injectable()
export class ProjectSelectFacade {
  projects$ = this.store.select(selectUserAssignedProjects);
  currentProject$ = this.store.select(selectCurrentAssignedProject);
  notFoundModalVisibility$ = this.store.select(selectNotFoundModalVisibility);

  constructor(
    private readonly store: Store<ProjectState>,
  ) {}

  init() {
    this.store.dispatch(ProjectSelectActions.init())
  }

  changeProject(projectId: string) {
    this.store.dispatch(ProjectSelectActions.changeProject(projectId))
  }

  closeModal() {
    this.store.dispatch(ProjectSelectActions.closeProjectNotFoundModal())
  }
}
