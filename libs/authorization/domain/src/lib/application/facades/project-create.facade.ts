// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { ProjectCreateActions } from '../actions';
import { ProjectState, selectCreateDrawerVisible } from '../reducers';
import { Store } from '@ngrx/store';
import { AddProjectData } from '../../entities';

@Injectable()
export class ProjectCreateFacade {
  createDrawerVisible$ = this.store.select(selectCreateDrawerVisible);

  constructor(
    private readonly store: Store<ProjectState>,
  ) {}

  save(data: AddProjectData) {
    this.store.dispatch(ProjectCreateActions.save(data));
  }

  closeCreateDrawer() {
    this.store.dispatch(ProjectCreateActions.closeCreateDrawer())
  }
}
