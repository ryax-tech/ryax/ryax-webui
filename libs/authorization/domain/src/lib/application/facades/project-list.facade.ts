// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { ProjectListActions } from '../actions';
import { ProjectState, selectListProjects } from '../reducers';
import { Store } from '@ngrx/store';

@Injectable()
export class ProjectListFacade {
  projects$ = this.store.select(selectListProjects);

  constructor(
    private readonly store: Store<ProjectState>,
  ) {}

  init() {
    this.store.dispatch(ProjectListActions.init())
  }

  refresh() {
    this.store.dispatch(ProjectListActions.refresh())
  }

  addProject() {
    this.store.dispatch(ProjectListActions.addProject())
  }

  editProject(projectId: string) {
    this.store.dispatch(ProjectListActions.editProject(projectId))
  }

  deleteProject(projectId: string) {
    this.store.dispatch(ProjectListActions.deleteProject(projectId))
  }

  // navigateToImportedWorkflow(workflowId: string) {
  //   this.router.navigate(['/', 'studio', 'workflows', workflowId]);
  //   this.closeImportSuccessModal();
  // }
}
