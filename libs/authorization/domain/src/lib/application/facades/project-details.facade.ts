// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { ProjectDetailsActions } from '../actions';
import { Store } from '@ngrx/store';
import {
  ProjectState,
  selectAddUserDrawerVisibility,
  selectCurrentProject,
  selectDetailsProject,
  selectDetailsUsers
} from '../reducers';

@Injectable()
export class ProjectDetailsFacade {
  project$ = this.store.select(selectDetailsProject);
  users$ = this.store.select(selectDetailsUsers);
  visibility$ = this.store.select(selectAddUserDrawerVisibility);

  constructor(
    private readonly store: Store<ProjectState>,
  ) {}

  init() {
    this.store.dispatch(ProjectDetailsActions.init())
  }

  refresh() {
    this.store.dispatch(ProjectDetailsActions.refresh())
  }

  editProject(projectId: string) {
    this.store.dispatch(ProjectDetailsActions.editProject(projectId))
  }

  openUserAssignModal() {
    this.store.dispatch(ProjectDetailsActions.openUserAddDrawer())
  }

  closeUserAssignModal() {
    this.store.dispatch(ProjectDetailsActions.closeUserAddDrawer())
  }

  addUserToProject(userId: string) {
    this.store.dispatch(ProjectDetailsActions.addUserToProject(userId))
  }

  removeUserFromProject(userId: string) {
    this.store.dispatch(ProjectDetailsActions.removeUserFromProject(userId))
  }
}
