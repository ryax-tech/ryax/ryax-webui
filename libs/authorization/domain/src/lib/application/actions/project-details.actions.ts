// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createAction } from '@ngrx/store';
import { Project, ProjectDetails, ProjectUser } from '../../entities';

export const init = createAction(
  '[Project Details] Init',
);

export const initSuccess = createAction(
  '[Project Details] Init success',
  (project: ProjectDetails) => ({
    payload: project
  })
);

export const initError = createAction(
  '[Project Details] Init error',
);

export const refresh = createAction(
  '[Project Details] Refresh',
);

export const refreshSuccess = createAction(
  '[Project Details] Refresh success',
  (project: ProjectDetails) => ({
    payload: project
  })
);

export const refreshError = createAction(
  '[Project Details] Refresh error',
);

export const editProject = createAction(
  '[Project Details] Edit project',
  (projectId: string) => ({
    payload: projectId
  })
);

export const editProjectSuccess = createAction(
  '[Project Details] Edit project successful',
  () => ({
    notification: "Project updated successfully"
  })
);

export const editProjectError = createAction(
  '[Project Details] Edit project error',
  () => ({
    notification: "Project update error"
  })
);

export const openUserAddDrawer = createAction(
  '[Project Details] Open drawer to add user to project',
);

export const loadUsersSuccess = createAction(
  '[Project Details] Load users success',
  (users: ProjectUser[]) => ({
    payload: users
  })
);

export const loadUsersError = createAction(
  '[Project Details] Load users error',
);

export const closeUserAddDrawer = createAction(
  '[Project Details] Close drawer to add user to project',
);

export const addUserToProject = createAction(
  '[Project Details] Add user to project',
    (userId: string) => ({
    payload: userId
  })
);

export const addUserToProjectSuccess = createAction(
  '[Project Details] Add user to project success',
    () => ({
      notification: "User assigned successfully"
  })
);

export const addUserToProjectError = createAction(
  '[Project Details] Add user to project error',
    () => ({
      notification: "User assignment error"
  })
);

export const removeUserFromProject = createAction(
  '[Project Details] Remove user from project',
    (userId: string) => ({
      payload: userId
  })
);

export const removeUserFromProjectSuccess = createAction(
  '[Project Details] Remove user from project success',
  (userId: string) => ({
    payload: userId,
    notification: "User removed successfully"
  })
);

export const removeUserFromProjectError = createAction(
  '[Project Details] Remove user from project error',
  () => ({
    notification: "User removal error"
  })
);
