// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createAction } from '@ngrx/store';
import { Project } from '../../entities';
import { HttpErrorResponse } from '@angular/common/http';

export const init = createAction(
  '[Project List] Init',
);

export const initSuccess = createAction(
  '[Project List] Init success',
  (projects: Project[]) => ({
    payload: projects
  })
);

export const initError = createAction(
  '[Project List] Init error',
  (error?: HttpErrorResponse) => ({
    payload: error
  })
);

export const refresh = createAction(
  '[Project List] Refresh',
);

export const refreshSuccess = createAction(
  '[Project List] Refresh success',
  (projects: Project[]) => ({
    payload: projects
  })
);

export const refreshError = createAction(
  '[Project List] Refresh error',
);

export const addProject = createAction(
  '[Project List] Open create modal',
);

export const addProjectSuccess = createAction(
  '[Project List] Add project successful',
  () => ({
    notification: "Project created successfully"
  })
);

export const addProjectError = createAction(
  '[Project List] Add project error',
  () => ({
    notification: "Project creation error"
  })
);

export const editProject = createAction(
  '[Project List] Edit project',
  (projectId: string) => ({
    payload: projectId
  })
);

export const editProjectSuccess = createAction(
  '[Project List] Edit project successful',
  () => ({
    notification: "Project updated successfully"
  })
);

export const editProjectError = createAction(
  '[Project List] Edit project error',
  () => ({
    notification: "Project update error"
  })
);

export const deleteProject = createAction(
  '[Project List] Delete project',
  (projectId: string) => ({
    payload: projectId
  })
);

export const deleteProjectSuccess = createAction(
  '[Project List] Delete project successful',
() => ({
    notification: "Project deleted successfully"
  })
);

export const deleteProjectError = createAction(
  '[Project List] Delete project error',
() => ({
    notification: "Project delete error"
  })
);
