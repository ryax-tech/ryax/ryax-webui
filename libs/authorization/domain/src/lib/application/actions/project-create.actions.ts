// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createAction } from '@ngrx/store';
import { AddProjectData } from '../../entities';

export const closeCreateDrawer = createAction(
  '[Project Create] Close create drawer',
);

export const save = createAction(
  '[Project Create] Save',
  (data: AddProjectData) => ({
    payload: data
  })
);

export const createProjectSuccess = createAction(
  '[Project Create] Create project successful',
  (projectId: string) => ({
    payload: projectId,
    notification: "Project created successfully"
  })
);

export const createProjectError = createAction(
  '[Project Create] Create project error',
  () => ({
    notification: "Project creation error"
  })
);
