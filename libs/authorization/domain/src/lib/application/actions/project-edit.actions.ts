// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createAction } from '@ngrx/store';
import { Project, UpdateProjectData } from '../../entities';

export const save = createAction(
  '[Project Edit] Save',
  (data: UpdateProjectData) => ({
    payload: data
  })
);

export const updateSuccess = createAction(
  '[Project Edit] Update success',
  (project: Project) => ({
    payload: project,
    notification: "Project updated successfully"
  })
);

export const updateError = createAction(
  '[Project Edit] Update error',
  () => ({
    notification: "Project update failed"
  })
);

export const closeEditDrawer = createAction(
  '[Project Edit] Close edit drawer',
);
