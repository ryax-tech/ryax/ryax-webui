// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createAction } from '@ngrx/store';
import { Project } from '../../entities';

export const init = createAction(
  '[Project Select] Init',
);

export const initSuccess = createAction(
  '[Project Select] Init success',
  (currentUserId: string) => ({
    payload: currentUserId
  })
);

export const initError = createAction(
  '[Project Select] Init error',
);

export const loadAssignedProjects = createAction(
  '[Project Select] Load assigned projects',
);

export const loadAssignedProjectsSuccess = createAction(
  '[Project Select] Load assigned projects success',
  (projects: Project[]) => ({
    payload: projects
  })
);

export const loadAssignedProjectsError = createAction(
  '[Project Select] Load assigned projects error',
);

export const loadCurrentProject = createAction(
  '[Project Select] Load current project',
);

export const loadCurrentProjectSuccess = createAction(
  '[Project Select] Load current project success',
  (project: Project) => ({
    payload: project
  })
);

export const loadCurrentProjectError = createAction(
  '[Project Select] Load current projects error',
);

export const changeProject = createAction(
  '[Project Select] Change current project',
  (projectId: string) => ({
    payload: projectId
  })
);

export const changeProjectSuccess = createAction(
  '[Project Select] Change current project success',
  () => ({
    notification: "Switched project successfully"
  })
);

export const changeProjectError = createAction(
  '[Project Select] Change current project error',
  () => ({
    notification: "Switched project error"
  })
);

export const openProjectNotFoundModal = createAction(
  '[Project Select] Open project not found modal',
);

export const closeProjectNotFoundModal = createAction(
  '[Project Select] Close project not found modal',
);
