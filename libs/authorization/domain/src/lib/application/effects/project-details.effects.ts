// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { ProjectDetailsActions } from '../actions';
import { catchError, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';
import { of } from 'rxjs';
import { Store } from '@ngrx/store';
import { ProjectState, selectCurrentProjectId } from '../reducers';
import { ProjectApi } from '../../infrastructure/services';
import { MessageService } from '@ryax/shared/ui-common';
import { UserApi } from '../../../../../../user/domain/src/lib/infrastructure/services';

@Injectable()
export class ProjectDetailsEffects {
  projectId$ = this.store.select(selectCurrentProjectId);

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store<ProjectState>,
    private readonly projectApi: ProjectApi,
    private readonly userApi: UserApi,
    private readonly messageService: MessageService,
  ) {}

  init$ = createEffect(() => this.actions$.pipe(
    ofType(ProjectDetailsActions.init),
    withLatestFrom(this.projectId$, (_, projectId) => projectId),
    switchMap(projectId => this.projectApi.loadOne(projectId).pipe(
      map(project => ProjectDetailsActions.initSuccess(project)),
      catchError(() => of(ProjectDetailsActions.initError()))
    ))
  ));

  loadUsers$ = createEffect(() => this.actions$.pipe(
    ofType(ProjectDetailsActions.openUserAddDrawer),
    switchMap(() => this.userApi.loadAll().pipe(
      map(users => ProjectDetailsActions.loadUsersSuccess(users)),
      catchError(() => of(ProjectDetailsActions.loadUsersError()))
    ))
  ));

  addUserToProject$ = createEffect(() => this.actions$.pipe(
    ofType(ProjectDetailsActions.addUserToProject),
    withLatestFrom(this.projectId$, ({ payload }, projectId) => ({ payload, projectId })),
    switchMap(({ payload, projectId }) => this.projectApi.addUser(projectId, payload).pipe(
      map(() => ProjectDetailsActions.addUserToProjectSuccess()),
      catchError(() => of(ProjectDetailsActions.addUserToProjectError()))
    ))
  ));

  removeUserFromProject$ = createEffect(() => this.actions$.pipe(
    ofType(ProjectDetailsActions.removeUserFromProject),
    withLatestFrom(this.projectId$, ({ payload }, projectId) => ({ payload, projectId })),
    switchMap(({ payload, projectId }) => this.projectApi.removeUser(projectId, payload).pipe(
      map(() => ProjectDetailsActions.removeUserFromProjectSuccess(payload)),
      catchError(() => of(ProjectDetailsActions.removeUserFromProjectError()))
    ))
  ));

  refresh$ = createEffect(() => this.actions$.pipe(
    ofType(
      ProjectDetailsActions.refresh,
      ProjectDetailsActions.editProjectSuccess,
      ProjectDetailsActions.addUserToProjectSuccess,
      ProjectDetailsActions.removeUserFromProjectSuccess,
    ),
    withLatestFrom(this.projectId$, (_, projectId) => projectId),
    switchMap(projectId => this.projectApi.loadOne(projectId).pipe(
      map(project => ProjectDetailsActions.refreshSuccess(project)),
      catchError(() => of(ProjectDetailsActions.refreshError()))
    ))
  ));

  notifySuccess$ = createEffect(() => this.actions$.pipe(
    ofType(
      ProjectDetailsActions.editProjectSuccess,
      ProjectDetailsActions.addUserToProjectSuccess,
      ProjectDetailsActions.removeUserFromProjectSuccess,
    ),
    tap(({ notification }) => this.messageService.displaySuccessMessage(notification))
  ), { dispatch: false });

  notifyError$ = createEffect(() => this.actions$.pipe(
    ofType(
      ProjectDetailsActions.editProjectError,
      ProjectDetailsActions.addUserToProjectError,
      ProjectDetailsActions.removeUserFromProjectError,
    ),
    tap(({ notification }) => this.messageService.displayErrorMessage(notification))
  ), { dispatch: false })
}
