// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, filter, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';
import { of } from 'rxjs';
import { ProjectEditActions } from '../actions';
import { ProjectApi } from '../../infrastructure/services';
import { Store } from '@ngrx/store';
import { ProjectState, selectEditProjectId } from '../reducers';
import { MessageService } from '@ryax/shared/ui-common';

@Injectable()
export class ProjectEditEffects {
  private projectId$ = this.store.select(selectEditProjectId);

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store<ProjectState>,
    private readonly projectApi: ProjectApi,
    private readonly messageService: MessageService,
  ) {}

  updateProject$ = createEffect(() => this.actions$.pipe(
    ofType(ProjectEditActions.save),
    withLatestFrom(this.projectId$, ({ payload }, projectId) => ({ payload, projectId })),
    switchMap(({ projectId, payload }) => this.projectApi.updateProject(projectId, payload).pipe(
      map(project => ProjectEditActions.updateSuccess(project)),
      catchError(() => of(ProjectEditActions.updateError()))
    ))
  ));

  notifySuccess$ = createEffect(() => this.actions$.pipe(
    ofType(
      ProjectEditActions.updateSuccess,
    ),
    tap(({ notification }) => this.messageService.displaySuccessMessage(notification))
  ), { dispatch: false });

  notifyError$ = createEffect(() => this.actions$.pipe(
    ofType(
      ProjectEditActions.updateError,
    ),
    tap(({ notification }) => this.messageService.displayErrorMessage(notification))
  ), { dispatch: false });
}
