// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { catchError, debounceTime, flatMap, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { ProjectState, selectCurrentUserId } from '../reducers';
import { ProjectDetailsActions, ProjectSelectActions } from '../actions';
import { ProjectApi } from '../../infrastructure/services';
import { MessageService } from '@ryax/shared/ui-common';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable()
export class ProjectSelectEffects {
  userId$ = this.store.select(selectCurrentUserId);

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store<ProjectState>,
    private readonly projectApi: ProjectApi,
    private readonly messageService: MessageService,
    private readonly router: Router
  ) {}

  init$ = createEffect(() => this.actions$.pipe(
    ofType(ProjectSelectActions.init),
    switchMap(() => this.projectApi.getCurrentUserId().pipe(
      flatMap(userId => [
        ProjectSelectActions.initSuccess(userId),
        ProjectSelectActions.loadCurrentProject(),
        ProjectSelectActions.loadAssignedProjects(),
      ]),
      catchError(() => of(ProjectSelectActions.initError()))
    ))
  ));

  loadCurrentProject$ = createEffect(() => this.actions$.pipe(
    ofType(
      ProjectSelectActions.loadCurrentProject
    ),
    withLatestFrom(this.userId$, (_, userId) => userId),
    switchMap((userId) => this.projectApi.loadCurrentProject(userId).pipe(
      map(project => {
        if(project.id || this.router.url === '/projects') {
          return ProjectSelectActions.loadCurrentProjectSuccess(project)
        } else {
          this.router.navigate(['/', 'error', 'unassigned']);
        }
      }),
      catchError(() => of(ProjectSelectActions.loadCurrentProjectError()))
    ))
  ));

  loadAssignedProjects$ = createEffect(() => this.actions$.pipe(
    ofType(
      ProjectDetailsActions.addUserToProjectSuccess,
      ProjectDetailsActions.removeUserFromProjectSuccess,
      ProjectSelectActions.loadAssignedProjects
    ),
    withLatestFrom(this.userId$, (_, userId) => userId),
    switchMap((userId) => this.projectApi.loadAssignedProjects(userId).pipe(
      map(projects => ProjectSelectActions.loadAssignedProjectsSuccess(projects)),
      catchError(() => of(ProjectSelectActions.loadAssignedProjectsError()))
    ))
  ));

  changeProject$ = createEffect(() => this.actions$.pipe(
    ofType(ProjectSelectActions.changeProject),
    withLatestFrom(this.userId$, ({ payload }, userId) => ({ payload, userId })),
    switchMap(({ payload, userId }) => this.projectApi.changeCurrentProject(userId, payload).pipe(
      map(() => ProjectSelectActions.changeProjectSuccess()),
      catchError((err: HttpErrorResponse) => {
        if(err.status === 404) {
          return of(ProjectSelectActions.openProjectNotFoundModal())
        }
        return of(ProjectSelectActions.changeProjectError())
      })
    ))
  ));

  reloadPageAfterProjectSwitch$ = createEffect(() => this.actions$.pipe(
    debounceTime(400),
    ofType(
      ProjectSelectActions.changeProjectSuccess,
    ),
    tap(() => {
      if(this.router.url === "/error/unassigned") {
        this.router.navigate(['/', 'studio'])
      } else {
        document.location.reload()
      }
    })
  ), { dispatch: false });

  notifySuccess$ = createEffect(() => this.actions$.pipe(
    ofType(
      ProjectSelectActions.changeProjectSuccess,
    ),
    tap(({ notification }) => this.messageService.displaySuccessMessage(notification))
  ), { dispatch: false });

  notifyError$ = createEffect(() => this.actions$.pipe(
    ofType(
      ProjectSelectActions.changeProjectError,
    ),
    tap(({ notification }) => this.messageService.displayErrorMessage(notification))
  ), { dispatch: false });
}
