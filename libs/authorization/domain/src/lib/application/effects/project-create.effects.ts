// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, filter, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';
import { of } from 'rxjs';
import { ProjectCreateActions } from '../actions';
import { ProjectApi } from '../../infrastructure/services';
import { Store } from '@ngrx/store';
import { ProjectState } from '../reducers';
import { MessageService } from '@ryax/shared/ui-common';
import { Router } from '@angular/router';

@Injectable()
export class ProjectCreateEffects {


  constructor(
    private readonly actions$: Actions,
    private readonly store: Store<ProjectState>,
    private readonly projectApi: ProjectApi,
    private readonly messageService: MessageService,
    private readonly router: Router,
  ) {}

  createProject$ = createEffect(() => this.actions$.pipe(
    ofType(ProjectCreateActions.save),
    switchMap(({ payload }) => this.projectApi.createProject(payload).pipe(
      map(newProjectId => ProjectCreateActions.createProjectSuccess(newProjectId)),
      catchError(() => of(ProjectCreateActions.createProjectError()))
    ))
  ));

  notifySuccess$ = createEffect(() => this.actions$.pipe(
    ofType(
      ProjectCreateActions.createProjectSuccess,
    ),
    tap(({ notification }) => this.messageService.displaySuccessMessage(notification))
  ), { dispatch: false });

  notifyError$ = createEffect(() => this.actions$.pipe(
    ofType(
      ProjectCreateActions.createProjectError,
    ),
    tap(({ notification }) => this.messageService.displayErrorMessage(notification))
  ), { dispatch: false });

  goToProjectDetails$ = createEffect(() => this.actions$.pipe(
    ofType(ProjectCreateActions.createProjectSuccess),
    tap(({ payload }) => this.router.navigate(['/', 'projects', payload]))
  ), { dispatch: false });
}
