// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { catchError, map, switchMap, tap } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { MessageService } from '@ryax/shared/ui-common';
import { ProjectState } from '../reducers';
import { ProjectListActions } from '../actions';
import { ProjectApi } from '../../infrastructure/services';
import { Router } from '@angular/router';

@Injectable()
export class ProjectListEffects {

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store<ProjectState>,
    private readonly projectApi: ProjectApi,
    private readonly messageService: MessageService,
    private readonly router: Router
  ) {}

  init$ = createEffect(() => this.actions$.pipe(
    ofType(ProjectListActions.init),
    switchMap(() => this.projectApi.loadAll().pipe(
      map(projects => ProjectListActions.initSuccess(projects)),
      catchError((err) => {
        if(err.status === 401) {
          this.router.navigate(['/', 'error', 'unauthorized']);
          return of(ProjectListActions.initError(err))
        } else {
          return of(ProjectListActions.initError())
        }
      }),
    ))
  ));

  deleteProject$ = createEffect(() => this.actions$.pipe(
    ofType(ProjectListActions.deleteProject),
    switchMap(({ payload }) => this.projectApi.deleteProject(payload).pipe(
      map(() => ProjectListActions.deleteProjectSuccess()),
      catchError(() => of(ProjectListActions.deleteProjectError()))
    ))
  ));

  refresh$ = createEffect(() => this.actions$.pipe(
    ofType(
      ProjectListActions.refresh,
      ProjectListActions.addProjectSuccess,
      ProjectListActions.editProjectSuccess,
      ProjectListActions.deleteProjectSuccess,
    ),
    switchMap(() => this.projectApi.loadAll().pipe(
      map(projects => ProjectListActions.refreshSuccess(projects)),
      catchError(() => of(ProjectListActions.refreshError()))
    ))
  ));

  notifySuccess$ = createEffect(() => this.actions$.pipe(
    ofType(
      ProjectListActions.addProjectSuccess,
      ProjectListActions.editProjectSuccess,
      ProjectListActions.deleteProjectSuccess,
    ),
    tap(({ notification }) => this.messageService.displaySuccessMessage(notification))
  ), { dispatch: false });

  notifyError$ = createEffect(() => this.actions$.pipe(
    ofType(
      ProjectListActions.addProjectError,
      ProjectListActions.editProjectError,
      ProjectListActions.deleteProjectError,
    ),
    tap(({ notification }) => this.messageService.displayErrorMessage(notification))
  ), { dispatch: false })
}
