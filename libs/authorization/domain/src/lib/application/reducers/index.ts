// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { InjectionToken } from '@angular/core';
import { ActionReducerMap, createFeatureSelector, createSelector } from '@ngrx/store';
import { NavigationQueries } from '@ryax/domain/navigation';
import * as fromProject from './project.reducer';
import * as fromProjectDetails from './project-details.reducer';
import * as fromProjectList from './project-list.reducer';

export const ProjectFeatureKey = 'authorizationDomain';

export interface ProjectState {
  projects: fromProject.State
  projectDetails: fromProjectDetails.State
  projectList: fromProjectList.State
}

export const ProjectReducerToken = new InjectionToken<ActionReducerMap<ProjectState>>(ProjectFeatureKey);

export const ProjectReducerProvider = {
  provide: ProjectReducerToken,
  useValue: {
    projects: fromProject.reducer,
    projectDetails: fromProjectDetails.reducer,
    projectList: fromProjectList.reducer,
  },
};

export const selectFeatureState = createFeatureSelector<ProjectState>(ProjectFeatureKey);

export const selectProjectsState = createSelector(selectFeatureState, state => state.projects);
export const selectProjectsDetailsState = createSelector(selectFeatureState, state => state.projectDetails);
export const selectProjectsListState = createSelector(selectFeatureState, state => state.projectList);

export const selectListProjects = createSelector(selectProjectsState, fromProject.selectProjects);

export const selectCurrentProjectId = createSelector(NavigationQueries.selectParams, navParams => navParams && navParams['projectId']);
export const selectDetailsProject = createSelector(selectProjectsState, selectCurrentProjectId, fromProject.selectProjectById);

export const selectCurrentProject = createSelector(selectProjectsDetailsState, fromProjectDetails.selectCurrentProject);

export const selectLoading = createSelector(selectProjectsState, fromProject.selectLoading);
export const selectCreateDrawerVisible = createSelector(selectProjectsState, fromProject.selectCreateDrawerVisible);
export const selectEditDrawerVisible = createSelector(selectProjectsState, fromProject.selectEditDrawerVisible);

//Edit
export const selectEditProjectId = createSelector(selectProjectsState, fromProject.selectEditProjectId);
export const selectEditProject = createSelector(selectProjectsState, selectEditProjectId, fromProject.selectEditProject);

//Drawer
export const selectAddUserDrawerVisibility = createSelector(selectProjectsDetailsState, fromProjectDetails.selectAddUserDrawerVisibility);
export const selectDetailsUsers = createSelector(selectProjectsDetailsState, fromProjectDetails.selectDetailsUsers);

//Select project dropdown
export const selectUserAssignedProjects  = createSelector(selectProjectsState, fromProject.selectUserAssignedProjects);
export const selectCurrentAssignedProject  = createSelector(selectProjectsState, fromProject.selectCurrentAssignedProject);
export const selectCurrentUserId  = createSelector(selectProjectsState, fromProject.selectCurrentUserId);
export const selectNotFoundModalVisibility  = createSelector(selectProjectsState, fromProject.selectNotFoundModalVisibility);

