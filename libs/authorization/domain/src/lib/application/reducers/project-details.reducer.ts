// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createReducer, on } from '@ngrx/store';
import { ProjectDetailsActions } from '../actions';
import { Project, ProjectDetails, ProjectUser } from '../../entities';

export interface State {
  currentProject: ProjectDetails;
  addUserDrawerVisibility: boolean;
  users: ProjectUser[];
}

export const initialState: State = {
  currentProject: null,
  addUserDrawerVisibility: false,
  users: null,
};

export const reducer = createReducer<State>(
  initialState,
  on(ProjectDetailsActions.init, () => initialState),
  on(ProjectDetailsActions.initSuccess, (state, { payload }) => ({
    ...state,
    currentProject: payload
  })),
  on(ProjectDetailsActions.openUserAddDrawer, (state) => ({
    ...state,
    addUserDrawerVisibility: true,
  })),
  on(ProjectDetailsActions.loadUsersSuccess, (state, { payload }) => ({
    ...state,
    users: payload
  })),
  on(ProjectDetailsActions.closeUserAddDrawer, (state) => ({
    ...state,
    addUserDrawerVisibility: false
  })),
);

export const selectCurrentProject = (state: State) => state.currentProject;
export const selectAddUserDrawerVisibility = (state: State) => state.addUserDrawerVisibility;
export const selectDetailsUsers = (state: State) => state.users;

