// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import { Project } from '../../entities';

export interface State extends EntityState<Project>{
  selectedWorkflowId: string,
}

export const adapter = createEntityAdapter<Project>({
  selectId: project => project.id
});

export const initialState: State = adapter.getInitialState({
  selectedWorkflowId: null,
});

export const reducer = createReducer<State>(
  initialState,
);

// Define state selectors
export const selectSelectedWorkflowId = (state: State) => state.selectedWorkflowId;
