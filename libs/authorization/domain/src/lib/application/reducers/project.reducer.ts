// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import { Project } from '../../entities';
import {
  ProjectCreateActions,
  ProjectDetailsActions,
  ProjectEditActions,
  ProjectListActions,
  ProjectSelectActions,
} from '../actions';

export interface State extends EntityState<Project>{
  loading: boolean;
  createDrawerVisible: boolean;
  editDrawerVisible: boolean;
  editProjectId: string;
  currentUserId: string;
  assignedProjects: Project[];
  currentAssignedProject: Project;
  notFoundModalVisibility: boolean;
}

export const adapter = createEntityAdapter<Project>({
  selectId: project => project.id
});

export const initialState: State = adapter.getInitialState({
  loading: null,
  createDrawerVisible: false,
  editDrawerVisible: false,
  editProjectId: null,
  currentUserId: null,
  assignedProjects: null,
  currentAssignedProject: null,
  notFoundModalVisibility: false,
});

export const reducer = createReducer<State>(
  initialState,
  on(ProjectListActions.init, (state) => ({
    ...state,
    loading: true
  })),
  on(ProjectListActions.initSuccess, (state, { payload }) => adapter.setAll(payload, {
    ...state,
    loading: false
  })),
  on(ProjectDetailsActions.initSuccess, (state, { payload }) => adapter.upsertOne(payload, {
    ...state,
    loading: false
  })),
  on(ProjectListActions.initError, (state) => adapter.removeAll(state)),
  on(ProjectListActions.refresh,
    ProjectDetailsActions.refresh, (state) => ({
    ...state,
    loading: true
  })),
  on(ProjectListActions.refreshSuccess, (state, { payload }) => adapter.setAll(payload, {
    ...state,
    loading: false
  })),
  on(ProjectDetailsActions.refreshSuccess, (state, { payload }) => adapter.upsertOne(payload, {
    ...state,
    loading: false
  })),
  on(ProjectListActions.refreshError, (state) => adapter.removeAll(state)),
  on(ProjectListActions.addProject, (state) => ({
    ...state,
    createDrawerVisible: true,
  })),
  on(ProjectCreateActions.closeCreateDrawer, ProjectCreateActions.createProjectSuccess, (state) => ({
    ...state,
    createDrawerVisible: false,
  })),
  on(ProjectListActions.editProject,
    ProjectDetailsActions.editProject, (state) => ({
    ...state,
    editDrawerVisible: true,
  })),
  on(ProjectListActions.editProject,
    ProjectDetailsActions.editProject, (state, {payload}) => ({
    ...state,
    editProjectId: payload,
  })),
  on(ProjectEditActions.updateSuccess, (state, { payload }) => adapter.upsertOne(payload, state)),
  on(ProjectEditActions.closeEditDrawer, ProjectEditActions.updateSuccess, (state) => ({
    ...state,
    editDrawerVisible: false,
  })),
  on(ProjectDetailsActions.removeUserFromProjectSuccess, (state, { payload }) => adapter.removeOne(payload, state)),
  on(ProjectSelectActions.initSuccess, (state, { payload }) => ({
    ...state,
    currentUserId: payload,
  })),
  on(ProjectSelectActions.loadAssignedProjectsSuccess, (state, { payload }) => ({
    ...state,
    assignedProjects: payload,
  })),
  on(ProjectSelectActions.loadCurrentProjectSuccess, (state, { payload }) => ({
    ...state,
    currentAssignedProject: payload,
  })),
  on(ProjectSelectActions.openProjectNotFoundModal, (state) => ({
    ...state,
    notFoundModalVisibility: true,
  })),
  on(ProjectSelectActions.closeProjectNotFoundModal, (state) => ({
    ...state,
    notFoundModalVisibility: false,
  })),
);

const {
  selectAll,
  selectEntities
} = adapter.getSelectors();

// Define state selectors
export const selectProjects = selectAll;
// export const selectProjectById = (state: State, projectId: string) => state.entities[projectId];
export const selectProjectById = (state: State, projectId: string) => selectEntities(state)[projectId];
export const selectLoading = (state: State) => state.loading;
export const selectCreateDrawerVisible = (state: State) => state.createDrawerVisible;
export const selectEditDrawerVisible = (state: State) => state.editDrawerVisible;
export const selectEditProjectId = (state: State) => state.editProjectId;
export const selectEditProject = (state: State, projectId: string) => state.entities[projectId];
export const selectCurrentUserId = (state: State) => state.currentUserId;
export const selectUserAssignedProjects = (state: State) => state.assignedProjects;
export const selectCurrentAssignedProject = (state: State) => state.currentAssignedProject;
export const selectNotFoundModalVisibility = (state: State) => state.notFoundModalVisibility;
