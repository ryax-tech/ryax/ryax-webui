// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectAdapter } from './infrastructure/adapters';
import { StoreModule } from '@ngrx/store';
import { ProjectFeatureKey, ProjectReducerProvider, ProjectReducerToken } from './application/reducers';
import { EffectsModule } from '@ngrx/effects';
import {
  ProjectCreateEffects,
  ProjectDetailsEffects,
  ProjectListEffects,
  ProjectSelectEffects,
} from './application/effects';
import {
  ProjectCreateFacade,
  ProjectDetailsFacade,
  ProjectEditFacade,
  ProjectListFacade,
  ProjectSelectFacade,
} from './application/facades';
import { ProjectApi } from './infrastructure/services';
import { ProjectEditEffects } from './application/effects/project-edit.effects';
import { UserApi } from '../../../../user/domain/src/lib/infrastructure/services';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature(ProjectFeatureKey, ProjectReducerToken),
    EffectsModule.forFeature([
      ProjectListEffects,
      ProjectDetailsEffects,
      ProjectCreateEffects,
      ProjectEditEffects,
      ProjectSelectEffects,
    ])
  ],
  providers: [
    ProjectReducerProvider,
    ProjectListFacade,
    ProjectDetailsFacade,
    ProjectCreateFacade,
    ProjectEditFacade,
    ProjectSelectFacade,
    ProjectAdapter,
    ProjectApi,
    UserApi
  ]
})
export class AuthorizationDomainModule {}
