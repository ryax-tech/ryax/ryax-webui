// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'ryax-project-list-header',
  templateUrl: './project-list-header.component.pug'
})
export class ProjectListHeaderComponent {
  @Output() refresh = new EventEmitter<void>();
  @Output() addProject = new EventEmitter<void>();

  onAddProject() {
    this.addProject.emit();
  }

  onRefresh() {
    this.refresh.emit();
  }
}
