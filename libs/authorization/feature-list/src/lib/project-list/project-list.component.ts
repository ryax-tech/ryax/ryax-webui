// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, OnInit } from '@angular/core';
import { ProjectListFacade } from '../../../../domain/src/lib/application/facades';
import { Project } from '../../../../domain/src/lib/entities';

@Component({
  selector: 'ryax-project-list',
  templateUrl: './project-list.component.pug'
})
export class ProjectListComponent implements OnInit {
  projects$ = this.facade.projects$;

  constructor(
    private readonly facade: ProjectListFacade
  ) {}

  ngOnInit(): void {
    this.facade.init();
  }

  refresh() {
    this.facade.refresh();
  }

  addProject() {
    this.facade.addProject();
  }

  editProject(projectId: string) {
    this.facade.editProject(projectId);
  }

  deleteProject(projectId: string) {
    this.facade.deleteProject(projectId);
  }
}
