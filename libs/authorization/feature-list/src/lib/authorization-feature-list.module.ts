// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectListComponent } from './project-list/project-list.component';
import { ProjectListHeaderComponent } from './project-list-header/project-list-header.component';
import { ProjectListTableComponent } from './project-list-table/project-list-table.component';
import { SharedUiCommonModule } from '@ryax/shared/ui-common';
import { AuthorizationDomainModule } from '@ryax/authorization/domain';
import { ProjectListTableRowComponent } from './project-list-table-row/project-list-table-row.component';
import { AuthorizationFeatureCreateModule } from '@ryax/authorization/feature-create';
import { AuthorizationFeatureEditModule } from '@ryax/authorization/feature-edit';

@NgModule({
  imports: [
    CommonModule,
    SharedUiCommonModule,
    AuthorizationDomainModule,
    AuthorizationFeatureCreateModule,
    AuthorizationFeatureEditModule,
  ],
  declarations: [
    ProjectListComponent,
    ProjectListHeaderComponent,
    ProjectListTableComponent,
    ProjectListTableRowComponent,
  ],
  exports: [
    ProjectListComponent
  ]
})
export class AuthorizationFeatureListModule {}
