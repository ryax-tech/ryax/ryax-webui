// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Project } from '../../../../domain/src/lib/entities';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'tr[ryax-project-list-table-row]',
  templateUrl: './project-list-table-row.component.pug',
  styleUrls: ['./project-list-table-row.component.scss']
})
export class ProjectListTableRowComponent {
  @Input() project: Project;
  @Output() editProject = new EventEmitter<string>();
  @Output() delete = new EventEmitter<string>();

  onEditProject(projectId: string, e: MouseEvent) {
    e.preventDefault();
    e.stopPropagation();
    this.editProject.emit(projectId);
  }

  preventNavigation(e:MouseEvent) {
    e.preventDefault();
    e.stopPropagation();
  }

  onDelete(projectId: string) {
    this.delete.emit(projectId);
  }
}
