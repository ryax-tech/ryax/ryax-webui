// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, EventEmitter, Input, Output, TemplateRef } from '@angular/core';
import { Project } from '../../../../domain/src/lib/entities';

@Component({
  selector: 'ryax-project-list-table',
  templateUrl: './project-list-table.component.pug'
})
export class ProjectListTableComponent {
  @Input() loading: boolean;
  @Input() projects: Project[];
  @Input() itemTemplate: TemplateRef<any>;

  trackIByItemId(index: number, item: Project) {
    return item.id;
  }
}
