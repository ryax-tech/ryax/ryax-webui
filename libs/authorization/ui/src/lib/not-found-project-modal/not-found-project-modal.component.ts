// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'ryax-not-found-project-modal',
  templateUrl: './not-found-project-modal.component.pug'
})
export class NotFoundProjectModalComponent {
  @Input() visible: boolean;
  @Output() cancel = new EventEmitter<void>();

  handleCancel() {
    this.cancel.emit();
  }
}
