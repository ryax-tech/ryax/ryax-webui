// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserAssignModalComponent } from './user-assign-modal/user-assign-modal.component';
import { SharedUiCommonModule } from '@ryax/shared/ui-common';
import { NotFoundProjectModalComponent } from './not-found-project-modal/not-found-project-modal.component';

@NgModule({
  imports: [
    CommonModule,
    SharedUiCommonModule,
  ],
  declarations: [
    UserAssignModalComponent,
    NotFoundProjectModalComponent
  ],
  exports: [
    UserAssignModalComponent,
    NotFoundProjectModalComponent
  ]
})
export class AuthorizationUiModule {}
