// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Project, ProjectDetails, ProjectUser } from '../../../../domain/src/lib/entities';

@Component({
  selector: 'ryax-user-assign-modal',
  templateUrl: './user-assign-modal.component.pug'
})
export class UserAssignModalComponent {
  @Input() visible: boolean;
  @Input() users: ProjectUser[];
  @Input() project: ProjectDetails;
  @Output() cancel = new EventEmitter<void>();
  @Output() assignUser = new EventEmitter<string>();

  userIsAssigned(userId: string) {
    const array = this.project.users;
    return array.find(i => i.id === userId);
  }

  handleCancel() {
    this.cancel.emit();
  }

  addUserToProject(userId: string) {
    this.assignUser.emit(userId);
  }
}
