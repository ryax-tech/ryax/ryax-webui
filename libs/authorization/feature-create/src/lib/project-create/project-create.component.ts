// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component } from '@angular/core';
import { ProjectCreateFacade } from '../../../../domain/src/lib/application/facades';
import { AddProjectData } from '../../../../domain/src/lib/entities';

@Component({
  selector: 'ryax-project-create',
  templateUrl: './project-create.component.pug'
})
export class ProjectCreateComponent {
  createDrawerVisible$ = this.facade.createDrawerVisible$;

  constructor(
    private readonly facade: ProjectCreateFacade
  ) {}

  onSave(data: AddProjectData) {
    this.facade.save(data);
  }

  onClose() {
    this.facade.closeCreateDrawer();
  }
}
