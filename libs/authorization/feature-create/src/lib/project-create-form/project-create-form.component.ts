// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AddProjectData, Project } from '../../../../domain/src/lib/entities';

@Component({
  selector: 'ryax-project-create-form',
  templateUrl: './project-create-form.component.pug'
})
export class ProjectCreateFormComponent {
  private _project: Project;

  form = new FormGroup({
    name: new FormControl(null, Validators.required),
  });

  @Input() set project(value: Project) {
    this._project = value;
    this.initForm();
  }
  @Output() save = new EventEmitter<AddProjectData>();
  @Output() cancel = new EventEmitter<void>();


  private initForm() {
    const formValue = {
      name: this._project ? this._project.name : null,
    };
    this.form.reset(formValue);
  }

  onSave() {
    const formView = this.form.value;
    this.save.emit(formView);
  }

  onCancel() {
    this.cancel.emit();
  }
}
