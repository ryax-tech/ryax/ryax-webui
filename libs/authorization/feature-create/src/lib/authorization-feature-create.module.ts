// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthorizationDomainModule } from '@ryax/authorization/domain';
import { SharedUiCommonModule } from '@ryax/shared/ui-common';
import { ProjectCreateComponent } from './project-create/project-create.component';
import { ProjectCreateFormComponent } from './project-create-form/project-create-form.component';

@NgModule({
  imports: [
    CommonModule,
    AuthorizationDomainModule,
    SharedUiCommonModule
  ],
  declarations: [
    ProjectCreateComponent,
    ProjectCreateFormComponent
  ],
  exports: [
    ProjectCreateComponent,
  ]
})
export class AuthorizationFeatureCreateModule {}
