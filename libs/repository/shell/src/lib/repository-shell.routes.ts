// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { RepositoryFeatureListComponent } from '@ryax/repository/feature-list';
import { RepositoryFeatureScanComponent } from '@ryax/repository/feature-scan';
import { RepositoryFeatureModuleListComponent } from '@ryax/repository/feature-module-list';


export const routes = [
  {
    path: '',
    patchMatch: "full",
    redirectTo: "sources"
  },
  {
    path: 'sources',
    component: RepositoryFeatureListComponent,
    data: {
      breadcrumb: null
    }
  },
  {
    path: 'sources/:sourceId/scan',
    component: RepositoryFeatureScanComponent,
    data: {
      breadcrumb: null
    }
  },
  {
    path: 'modules',
    component: RepositoryFeatureModuleListComponent,
    data: {
      breadcrumb: null
    }
  }
];
