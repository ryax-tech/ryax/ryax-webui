// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RepositoryFeatureListModule } from '@ryax/repository/feature-list';
import { RouterModule } from '@angular/router';
import { routes } from './repository-shell.routes';
import { RepositoryFeatureScanModule } from '@ryax/repository/feature-scan';
import { RepositoryFeatureModuleListModule } from '@ryax/repository/feature-module-list';

@NgModule({
  imports: [
    CommonModule,
    RepositoryFeatureListModule,
    RepositoryFeatureScanModule,
    RepositoryFeatureModuleListModule,
    RouterModule.forChild(routes)
  ],
})
export class RepositoryShellModule {}
