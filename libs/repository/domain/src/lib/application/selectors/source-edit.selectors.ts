// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { RepositoryAdapters, RepositoryFeatureKey, RepositoryState } from '../reducers';


const selectFeatureState = createFeatureSelector<RepositoryState>(RepositoryFeatureKey);
const selectSourcesState = createSelector(selectFeatureState, state => state.sources);
const selectSourceEditState = createSelector(selectFeatureState, state => state.sourceEdit);

const {
  selectEntities: selectSources,
} = RepositoryAdapters.sources.getSelectors(selectSourcesState);

export const selectVisible = createSelector(selectSourceEditState,
  state => state.mode !== null);

export const selectMode = createSelector(selectSourceEditState,
  state => state.mode);

export const selectRepositoryId = createSelector(selectSourceEditState,
  (state) => state.source);

export const selectRepository = createSelector(selectSources, selectRepositoryId,
  (repositories, repositoryId) => repositories[repositoryId]);

