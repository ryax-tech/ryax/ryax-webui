// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { RepositoryAdapters, RepositoryFeatureKey, RepositoryState } from '../reducers';
import { getModuleView } from '../../entities/utils';

const selectFeatureState = createFeatureSelector<RepositoryState>(RepositoryFeatureKey);
const selectSourcesState = createSelector(selectFeatureState, state => state.sources);
const selectModulesState = createSelector(selectFeatureState, state => state.modules);

const {
  selectAll: selectAllModules,
} = RepositoryAdapters.modules.getSelectors(selectModulesState);

const {
  selectEntities: selectSourceEntities,
} = RepositoryAdapters.sources.getSelectors(selectSourcesState);

export const selectLoading = createSelector(selectModulesState,
  state => state.loading);

export const selectErrorModalVisible = createSelector(selectModulesState,
  state => state.errorModalVisible);

export const selectSelectedModule = createSelector(selectModulesState,
  state => state.selectedModule);

export const selectModules = createSelector(selectAllModules, selectSourceEntities,
  (modules, sources) => {
    const array = modules.map(item => getModuleView(item, sources[item.repositoryId]));
    return array.sort((a,b) => new Date(b.creationDate).getTime() - new Date(a.creationDate).getTime());
});
