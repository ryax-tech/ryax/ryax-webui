// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createFeatureSelector, createSelector, Store } from '@ngrx/store';
import { RepositoryAdapters, RepositoryFeatureKey, RepositoryState } from '../reducers';
import { NavigationQueries } from '@ryax/domain/navigation';
import { getSourceScanModuleView } from '../../entities/utils';

const selectFeatureState = createFeatureSelector<RepositoryState>(RepositoryFeatureKey);
const selectSourcesState = createSelector(selectFeatureState, state => state.sources);
const selectSourceScanState = createSelector(selectFeatureState, state => state.sourceScan);
const selectSourceScanModulesState = createSelector(selectFeatureState, state => state.sourceScanModules);

const {
  selectEntities: selectRepositoryEntities,
} = RepositoryAdapters.sources.getSelectors(selectSourcesState);

const {
  selectAll: selectAllSourceScanModules,
} = RepositoryAdapters.sourceScanModules.getSelectors(selectSourceScanModulesState);


export const selectSourceId = createSelector(NavigationQueries.selectParams,
  (navParams) => navParams && navParams['sourceId']);

export const selectSource = createSelector(selectRepositoryEntities, selectSourceId,
  (repositories, repositoryId) => repositories[repositoryId]);

export const selectSourceScanProcessing = createSelector(selectSourceScanState,
  (repositoryScanState) => repositoryScanState.loading);

export const selectSourceScanResult = createSelector(selectSourceScanState,
  (repositoryScanState) => repositoryScanState.result);

export const selectDeleteDuplicateModalVisible = createSelector(selectSourceScanState,
  (repositoryScanState) => repositoryScanState.deleteDuplicateModalVisible);

export const selectScanErrorModalVisible = createSelector(selectSourceScanState,
  (repositoryScanState) => repositoryScanState.scanErrorModalVisible);

export const selectSelectedSourceScanModule = createSelector(selectSourceScanState,
  (repositoryScanState) => {
  return repositoryScanState.selectedSourceScanModule
});

export const selectSourceScanModules = createSelector(selectAllSourceScanModules,
  (sourceScanModules) => sourceScanModules.map(item => getSourceScanModuleView(item)));

export const selectLastScanModules = createSelector(selectSourcesState,
  (sourcesState) => sourcesState.lastScan);

export const selectLastScanSha = createSelector(selectSourcesState,
  (sourcesState) => sourcesState.lastScanSha);

export const selectRepositorySort = createSelector(selectSourceScanState,
  (repositoryScanState) => repositoryScanState.sort);

export const selectBuiltModules = createSelector(selectSourcesState,
  (sourcesState) => sourcesState.builtModules);

export const selectScanModalBranchVisibility = createSelector(selectSourceScanState,
  (repositoryScanState) => repositoryScanState.scanModalBranchVisibility);
