// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { from, of } from 'rxjs';
import { catchError, flatMap, map, switchMap, tap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { MessageService } from '@ryax/shared/ui-common';
import { SourceApi } from '../../infrastructure/services';
import { SourceListActions } from '../actions';

@Injectable()
export class SourceListEffects {
  constructor(
    private readonly actions$: Actions,
    private readonly sourceApi: SourceApi,
    private readonly messageService: MessageService,
  ) {}

  loadRepositories$ = createEffect(() => this.actions$.pipe(
    ofType(SourceListActions.init, SourceListActions.refresh),
    switchMap(() => this.sourceApi.loadAll().pipe(
      map(repositories => SourceListActions.loadAllSuccess(repositories)),
      catchError(() => of(SourceListActions.loadAllError()))
    ))
  ));

  deleteRepository$ = createEffect(() => this.actions$.pipe(
    ofType(SourceListActions.deleteSource),
    switchMap(({ payload }) => this.sourceApi.delete(payload).pipe(
      flatMap(() => [
        SourceListActions.deleteSuccess(payload),
        SourceListActions.notifySuccess('Repository deleted successfully')
      ]),
      catchError(() => from([
        SourceListActions.deleteError(),
        SourceListActions.notifyError('Repository deletion failed')
      ]))
    ))
  ));

  notifySuccess$ = createEffect(() => this.actions$.pipe(
    ofType(SourceListActions.notifySuccess),
    tap(({ payload }) => this.messageService.displaySuccessMessage(payload))
  ), { dispatch: false });

  notifyError$ = createEffect(() => this.actions$.pipe(
    ofType(SourceListActions.notifyError),
    tap(({ payload }) => this.messageService.displayErrorMessage(payload))
  ), { dispatch: false });
}
