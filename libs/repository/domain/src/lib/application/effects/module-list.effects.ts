// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { from, of } from 'rxjs';
import { catchError, flatMap, map, switchMap, tap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { ModuleApi } from '../../infrastructure/services';
import { MessageService } from '@ryax/shared/ui-common';
import { ModuleListActions } from '../actions';

@Injectable()
export class ModuleListEffects {
  constructor(
    private readonly actions$: Actions,
    private readonly moduleApi: ModuleApi,
    private readonly messageService: MessageService,
  ) {}

  loadModules$ = createEffect(() => this.actions$.pipe(
    ofType(ModuleListActions.init, ModuleListActions.refresh),
    switchMap(() => this.moduleApi.loadAll().pipe(
      map(({ modules, repositories }) => ModuleListActions.loadAllSuccess(modules, repositories)),
      catchError(() => of(ModuleListActions.loadAllError()))
    ))
  ));

  buildModule$ = createEffect(() => this.actions$.pipe(
    ofType(ModuleListActions.build),
    switchMap(({ payload }) => this.moduleApi.build(payload).pipe(
      flatMap(result => [
        ModuleListActions.buildSuccess(result),
        ModuleListActions.notifySuccess('Module built successfully')
      ]),
      catchError(() => from([
        ModuleListActions.buildError(payload),
        ModuleListActions.notifyError('Module build failed')
      ]))
    ))
  ));


  deleteModule$ = createEffect(() => this.actions$.pipe(
    ofType(ModuleListActions.deleteModule),
    switchMap(({ payload }) => this.moduleApi.deleteModule(payload).pipe(
      flatMap(() => [
        ModuleListActions.deleteSuccess(payload),
        ModuleListActions.notifySuccess('Module deleted successfully'),
      ]),
      catchError(() => from([
        ModuleListActions.deleteError(),
        ModuleListActions.notifyError('Module delete failed'),
      ]))
    ))
  ));

  notifySuccess$ = createEffect(() => this.actions$.pipe(
    ofType(ModuleListActions.notifySuccess),
    tap(({ payload }) => this.messageService.displaySuccessMessage(payload))
  ), { dispatch: false });

  notifyError$ = createEffect(() => this.actions$.pipe(
    ofType(ModuleListActions.notifyError),
    tap(({ payload }) => this.messageService.displayErrorMessage(payload))
  ), { dispatch: false });
}
