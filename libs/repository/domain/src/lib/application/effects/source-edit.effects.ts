// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { from } from 'rxjs';
import { catchError, filter, flatMap, switchMap, tap, withLatestFrom } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { MessageService } from '@ryax/shared/ui-common';
import { SourceApi } from '../../infrastructure/services';
import { SourceEditMode } from '../../entities/models';
import { SourceEditSelectors } from '../selectors';
import { RepositoryState } from '../reducers';
import { SourceEditActions } from '../actions';

@Injectable()
export class SourceEditEffects {
  mode$ = this.store.select(SourceEditSelectors.selectMode);
  repositoryId$ = this.store.select(SourceEditSelectors.selectRepositoryId);

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store<RepositoryState>,
    private readonly sourceApi: SourceApi,
    private readonly messageService: MessageService,
  ) {}

  createRepository$ = createEffect(() => this.actions$.pipe(
    ofType(SourceEditActions.save),
    withLatestFrom(this.mode$, ({ payload }, mode) => ({ payload, mode })),
    filter(({ mode }) => mode === SourceEditMode.NewSource),
    switchMap(({ payload }) => this.sourceApi.create(payload).pipe(
      flatMap(repository => [
        SourceEditActions.createSuccess(repository),
        SourceEditActions.close(),
        SourceEditActions.notifySuccess('Repository created successfully')
      ]),
      catchError(() => from([
        SourceEditActions.createError(),
        SourceEditActions.notifyError('Repository creation failed')
      ]))
    ))
  ));

  updateRepository$ = createEffect(() => this.actions$.pipe(
    ofType(SourceEditActions.save),
    withLatestFrom(this.mode$, this.repositoryId$, ({ payload }, mode, repositoryId) => ({ payload, mode, repositoryId })),
    filter(({ mode, repositoryId }) => repositoryId && mode === SourceEditMode.EditSourceInfos),
    switchMap(({ repositoryId, payload }) => this.sourceApi.update(repositoryId, payload).pipe(
      flatMap(repository => [
        SourceEditActions.updateSuccess(repository),
        SourceEditActions.close(),
        SourceEditActions.notifySuccess('Repository updated successfully'),
      ]),
      catchError(() => from([
        SourceEditActions.updateError(),
        SourceEditActions.notifyError('Repository update failed')
      ]))
    ))
  ));

  notifySuccess$ = createEffect(() => this.actions$.pipe(
    ofType(SourceEditActions.notifySuccess),
    tap(({ payload }) => this.messageService.displaySuccessMessage(payload))
  ), { dispatch: false });

  notifyError$ = createEffect(() => this.actions$.pipe(
    ofType(SourceEditActions.notifyError),
    tap(({ payload }) => this.messageService.displayErrorMessage(payload))
  ), { dispatch: false });
}
