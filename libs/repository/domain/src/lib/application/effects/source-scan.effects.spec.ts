// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Store } from '@ngrx/store';
import { TestBed } from '@angular/core/testing';
import { MessageService } from '@ryax/shared/ui-common';
import { ModuleApi, SourceApi } from '../../infrastructure/services';
import { provideMockActions } from '@ngrx/effects/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { RepositoryState } from '../reducers';
import { Observable } from 'rxjs';
import {
  Module, ModuleStatus,
  Source,
  SourceScanModule, SourceScanModuleBuildState,
  SourceScanRequest,
  SourceScanResult,
} from '@ryax/repository/domain';
import { cold, hot } from 'jest-marbles';
import { ModuleListActions, SourceScanActions } from '../actions';
import { SourceScanEffects } from './source-scan.effects';
import { selectSourceId } from '../selectors/source-scan.selectors';

function provideMockSourceApi() {
  return {
    provide: SourceApi,
    useValue: {
      loadAll: jest.fn(),
      loadOne: jest.fn(),
      create: jest.fn(),
      update: jest.fn(),
      delete: jest.fn(),
      scan: jest.fn(),
    }
  }
}

function provideMockModuleApi() {
  return {
    provide: ModuleApi,
    useValue: {
      loadAll: jest.fn(),
      build: jest.fn(),
      deleteModule: jest.fn(),
    }
  }
}

function provideMockMessageService() {
  return {
    provide: MessageService,
    useValue: {
      displaySuccessMessage: jest.fn(),
      displayErrorMessage: jest.fn()
    }
  }
}

describe('SourceScanEffects', () => {
  let actions$: Observable<any>;
  let effect: SourceScanEffects;
  let sourceApi: SourceApi;
  let moduleApi: ModuleApi;
  let store: MockStore<RepositoryState>;
  let messageService: MessageService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [],
      providers: [
        SourceScanEffects,
        provideMockStore({}),
        provideMockActions(() => actions$),
        provideMockSourceApi(),
        provideMockModuleApi(),
        provideMockMessageService(),
      ]
    });

    effect = TestBed.get(SourceScanEffects);
    sourceApi = TestBed.get(SourceApi);
    moduleApi = TestBed.get(ModuleApi);
    messageService = TestBed.get(MessageService);
    store = TestBed.get(Store);
  });

  describe('init$', () => {
    let repository: Source;

    beforeEach(() => {
      repository = {
        id: "id",
        name: "name",
        url: "url",
      };
    });

    it('when init success', () => {
      (sourceApi.loadOne as jest.Mock)
        .mockImplementation(() => cold('-a|', { a : repository }));

      store.overrideSelector(selectSourceId, "id");

      actions$ = hot('-a', {
        a: SourceScanActions.init(),
      });

      const expected$ = hot('--a', {
        a: SourceScanActions.loadOneSuccess(repository),
      });

      expect(effect.init$).toBeObservable(expected$)
    });

    it('when init fails', () => {
      (sourceApi.loadOne as jest.Mock)
        .mockImplementation(() => cold('-#', { a : repository }));

      store.overrideSelector(selectSourceId, "id");

      actions$ = hot('-a', {
        a: SourceScanActions.init(),
      });

      const expected$ = hot('--a', {
        a: SourceScanActions.scanError(),
      });
      expect(effect.init$).toBeObservable(expected$)
    });
  });

  describe('submit$', () => {
    let data: SourceScanRequest;
    let result: SourceScanResult;
    let modules: SourceScanModule[];

    beforeEach(() => {
      data = {
        username: "data_username",
        password: "data_password",
        branch: "data_branch",
      };
      result = {
        error: "result_error",
        branch: "result_branch",
      };
      modules = [{
        id: "module_id",
        name: "module_name",
        version: "module_version",
        description: "module_description",
        error: "module_error",
        buildState: SourceScanModuleBuildState.Ready,
      }];
    });

    xit('when submit success', () => {
      (sourceApi.scan as jest.Mock)
        .mockImplementation(() => cold('-a|', { a : data }));

      store.overrideSelector(selectSourceId, "id");

      actions$ = hot('-a', {
        a: SourceScanActions.submit(data),
      });

      const expected$ = hot('--a', {
        a: SourceScanActions.scanSuccess(result, modules),
      });

      expect(effect.submit$).toBeObservable(expected$)
    });

    it('when submit fails', () => {
      (sourceApi.scan as jest.Mock)
        .mockImplementation(() => cold('-#', { a : data }));

      store.overrideSelector(selectSourceId, "id");

      actions$ = hot('-a', {
        a: SourceScanActions.submit(data),
      });

      const expected$ = hot('--a', {
        a: SourceScanActions.scanError(),
      });
      expect(effect.submit$).toBeObservable(expected$)
    });
  });

  describe('buildModule$', () => {
    const moduleId = "moduleId";
    let module: Module;

    beforeEach(() => {
      module = {
        id: "moduleId",
        name: "module_name",
        version: "module_version",
        description: "module_description",
        status: ModuleStatus.Scanned,
        error: "module_error",
        repositoryId: "module_repositoryId",
        creationDate: new Date("February 5, 2001 18:15:00"),
      };
    });

    it('when build success', () => {
      (moduleApi.build as jest.Mock)
        .mockImplementation(() => cold('-a|', { a : module }));

      actions$ = hot('-a', {
        a: SourceScanActions.build(moduleId),
      });

      const expected$ = hot('--(ab)', {
        a: ModuleListActions.buildSuccess(module),
        b: ModuleListActions.notifySuccess('Module built successfully')
      });

      expect(effect.buildModule$).toBeObservable(expected$)
    });

    it('when build fails', () => {
      (moduleApi.build as jest.Mock)
        .mockImplementation(() => cold('-#', { a : module }));

      actions$ = hot('-a', {
        a: SourceScanActions.build(moduleId),
      });

      const expected$ = hot('--(ab)', {
        a: ModuleListActions.buildError(moduleId),
        b: ModuleListActions.notifyError('Module build failed')
      });
      expect(effect.buildModule$).toBeObservable(expected$)
    });
  });

  test('notifySuccess$', () => {
    const message = "message";
    actions$ = hot('-a', {
      a: SourceScanActions.notifySuccess(message),
    });
    expect(effect.notifySuccess$).toSatisfyOnFlush(() => {
      expect(messageService.displaySuccessMessage).toHaveBeenCalledWith(message)
    });
  });

  test('notifyError$', () => {
    const message = "message";
    actions$ = hot('-a', {
      a: SourceScanActions.notifyError(message),
    });
    expect(effect.notifyError$).toSatisfyOnFlush(() => {
      expect(messageService.displayErrorMessage).toHaveBeenCalledWith(message)
    });
  });

});
