// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Store } from '@ngrx/store';
import { TestBed } from '@angular/core/testing';
import { MessageService } from '@ryax/shared/ui-common';
import { SourceApi } from '../../infrastructure/services';
import { provideMockActions } from '@ngrx/effects/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { RepositoryState } from '../reducers';
import { Observable } from 'rxjs';
import { Source } from '@ryax/repository/domain';
import { cold, hot } from 'jest-marbles';
import { SourceListActions } from '../actions';
import { SourceListEffects } from './source-list.effects';

function provideMockSourceApi() {
  return {
    provide: SourceApi,
    useValue: {
      loadAll: jest.fn(),
      loadOne: jest.fn(),
      create: jest.fn(),
      update: jest.fn(),
      delete: jest.fn(),
      scan: jest.fn(),
    }
  }
}

function provideMockMessageService() {
  return {
    provide: MessageService,
    useValue: {
      displaySuccessMessage: jest.fn(),
      displayErrorMessage: jest.fn()
    }
  }
}

describe('SourceListEffects', () => {
  let actions$: Observable<any>;
  let effect: SourceListEffects;
  let sourceApi: SourceApi;
  let store: MockStore<RepositoryState>;
  let messageService: MessageService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [],
      providers: [
        SourceListEffects,
        provideMockStore({}),
        provideMockActions(() => actions$),
        provideMockSourceApi(),
        provideMockMessageService(),
      ]
    });

    effect = TestBed.get(SourceListEffects);
    sourceApi = TestBed.get(SourceApi);
    messageService = TestBed.get(MessageService);
    store = TestBed.get(Store);
  });

  describe('loadRepositories$', () => {
    let sources: Source[];

    beforeEach(() => {
      sources = [{
        id: "source_id",
        name: "source_name",
        url: "source_url",
      }];
    });

    it.each([
      SourceListActions.init(),
      SourceListActions.refresh(),
    ])('when load success', (action) => {
      (sourceApi.loadAll as jest.Mock)
        .mockImplementation(() => cold('-a|', { a : sources }));

      actions$ = hot('-a', {
        a: action,
      });

      const expected$ = hot('--a', {
        a: SourceListActions.loadAllSuccess(sources)
      });

      expect(effect.loadRepositories$).toBeObservable(expected$)
    });

    it.each([
      SourceListActions.init(),
      SourceListActions.refresh(),
    ])('when load fails', (action) => {
      (sourceApi.loadAll as jest.Mock)
        .mockImplementation(() => cold('-#', { a : sources }));

      actions$ = hot('-a', {
        a: action,
      });

      const expected$ = hot('--a', {
        a: SourceListActions.loadAllError()
      });
      expect(effect.loadRepositories$).toBeObservable(expected$)
    });
  });

  describe('deleteRepository$', () => {
    const repositoryId = "repositoryId";

    it('when delete success', () => {
      (sourceApi.delete as jest.Mock)
        .mockImplementation(() => cold('-a|', { a : repositoryId }));

      actions$ = hot('-a', {
        a: SourceListActions.deleteSource(repositoryId)
      });

      const expected$ = hot('--(ab)', {
        a: SourceListActions.deleteSuccess(repositoryId),
        b: SourceListActions.notifySuccess('Repository deleted successfully')
      });

      expect(effect.deleteRepository$).toBeObservable(expected$)
    });

    it('when delete fails', () => {
      (sourceApi.delete as jest.Mock)
        .mockImplementation(() => cold('-#', { a : repositoryId }));

      actions$ = hot('-a', {
        a: SourceListActions.deleteSource(repositoryId)
      });

      const expected$ = hot('--(ab)', {
        a: SourceListActions.deleteError(),
        b: SourceListActions.notifyError('Repository deletion failed')
      });
      expect(effect.deleteRepository$).toBeObservable(expected$)
    });
  });

  test('notifySuccess$', () => {
    const message = "message";
    actions$ = hot('-a', {
      a: SourceListActions.notifySuccess(message),
    });
    expect(effect.notifySuccess$).toSatisfyOnFlush(() => {
      expect(messageService.displaySuccessMessage).toHaveBeenCalledWith(message)
    });
  });

  test('notifyError$', () => {
    const message = "message";
    actions$ = hot('-a', {
      a: SourceListActions.notifyError(message),
    });
    expect(effect.notifyError$).toSatisfyOnFlush(() => {
      expect(messageService.displayErrorMessage).toHaveBeenCalledWith(message)
    });
  });

});
