// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Store } from '@ngrx/store';
import { TestBed } from '@angular/core/testing';
import { MessageService } from '@ryax/shared/ui-common';
import { SourceApi } from '../../infrastructure/services';
import { provideMockActions } from '@ngrx/effects/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { SourceEditEffects } from './source-edit.effects';
import { RepositoryState } from '../reducers';
import { Observable } from 'rxjs';
import { Source, SourceEditData, SourceEditMode } from '@ryax/repository/domain';
import { selectMode, selectRepositoryId } from '../selectors/source-edit.selectors';
import { cold, hot } from 'jest-marbles';
import { SourceEditActions } from '../actions';

function provideMockSourceApi() {
  return {
    provide: SourceApi,
    useValue: {
      loadAll: jest.fn(),
      loadOne: jest.fn(),
      create: jest.fn(),
      update: jest.fn(),
      delete: jest.fn(),
      scan: jest.fn(),
    }
  }
}

function provideMockMessageService() {
  return {
    provide: MessageService,
    useValue: {
      displaySuccessMessage: jest.fn(),
      displayErrorMessage: jest.fn()
    }
  }
}

describe('SourceEditEffects', () => {
  let actions$: Observable<any>;
  let effect: SourceEditEffects;
  let sourceApi: SourceApi;
  let store: MockStore<RepositoryState>;
  let messageService: MessageService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [],
      providers: [
        SourceEditEffects,
        provideMockStore({}),
        provideMockActions(() => actions$),
        provideMockSourceApi(),
        provideMockMessageService(),
      ]
    });

    effect = TestBed.get(SourceEditEffects);
    sourceApi = TestBed.get(SourceApi);
    messageService = TestBed.get(MessageService);
    store = TestBed.get(Store);
  });

  describe('createRepository$', () => {
    let value: SourceEditData;
    let repository: Source;

    beforeEach(() => {
      value = {
        name: "name",
        url: "url",
      };
      repository = {
        id: "id",
        name: "name",
        url: "url",
      };
    });

    xit('when create save success', () => {
      (sourceApi.create as jest.Mock)
        .mockImplementation(() => cold('-a|', { a : value }));

      store.overrideSelector(selectMode, SourceEditMode.NewSource);

      actions$ = hot('-a', {
        a: SourceEditActions.save(value),
      });

      const expected$ = hot('--(abc)', {
        a: SourceEditActions.createSuccess(repository),
        b: SourceEditActions.close(),
        c: SourceEditActions.notifySuccess('Repository created successfully')
      });

      expect(effect.createRepository$).toBeObservable(expected$)
    });

    it('when create save fails', () => {
      (sourceApi.create as jest.Mock)
        .mockImplementation(() => cold('-#', { a : value }));

      store.overrideSelector(selectMode, SourceEditMode.NewSource);

      actions$ = hot('-a', {
        a: SourceEditActions.save(value),
      });

      const expected$ = hot('--(ab)', {
        a: SourceEditActions.createError(),
        b: SourceEditActions.notifyError('Repository creation failed')
      });
      expect(effect.createRepository$).toBeObservable(expected$)
    });
  });

  describe('updateRepository$', () => {
    let value: SourceEditData;
    let repository: Source;

    beforeEach(() => {
      value = {
        name: "name",
        url: "url",
      };
      repository = {
        id: "id",
        name: "name",
        url: "url",
      };
    });

    xit('when update save success', () => {
      (sourceApi.update as jest.Mock)
        .mockImplementation(() => cold('-a|', { a : value }));

      store.overrideSelector(selectMode, SourceEditMode.EditSourceInfos);
      store.overrideSelector(selectRepositoryId, "id");

      actions$ = hot('-a', {
        a: SourceEditActions.save(value),
      });

      const expected$ = hot('--(abc)', {
        a: SourceEditActions.updateSuccess(repository),
        b: SourceEditActions.close(),
        c: SourceEditActions.notifySuccess('Repository updated successfully')
      });

      expect(effect.updateRepository$).toBeObservable(expected$)
    });

    it('when update save fails', () => {
      (sourceApi.update as jest.Mock)
        .mockImplementation(() => cold('-#', { a : value }));

      store.overrideSelector(selectMode, SourceEditMode.EditSourceInfos);
      store.overrideSelector(selectRepositoryId, "id");

      actions$ = hot('-a', {
        a: SourceEditActions.save(value),
      });

      const expected$ = hot('--(ab)', {
        a: SourceEditActions.updateError(),
        b: SourceEditActions.notifyError('Repository update failed')
      });
      expect(effect.updateRepository$).toBeObservable(expected$)
    });
  });

  test('notifySuccess$', () => {
    const message = "message";
    actions$ = hot('-a', {
      a: SourceEditActions.notifySuccess(message),
    });
    expect(effect.notifySuccess$).toSatisfyOnFlush(() => {
      expect(messageService.displaySuccessMessage).toHaveBeenCalledWith(message)
    });
  });

  test('notifyError$', () => {
    const message = "message";
    actions$ = hot('-a', {
      a: SourceEditActions.notifyError(message),
    });
    expect(effect.notifyError$).toSatisfyOnFlush(() => {
      expect(messageService.displayErrorMessage).toHaveBeenCalledWith(message)
    });
  });

});
