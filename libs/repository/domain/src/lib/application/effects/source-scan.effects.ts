// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { ModuleApi, SourceApi } from '../../infrastructure/services';
import { MessageService } from '@ryax/shared/ui-common';
import { ModuleListActions, SourceScanActions } from '../actions';
import { catchError, flatMap, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';
import { from, of } from 'rxjs';
import { Store } from '@ngrx/store';
import { RepositoryState } from '../reducers';
import { SourceScanSelectors } from '../selectors';

@Injectable()
export class SourceScanEffects {
  repositoryId$ = this.store.select(SourceScanSelectors.selectSourceId);
  sort$ = this.store.select(SourceScanSelectors.selectRepositorySort);

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store<RepositoryState>,
    private readonly sourceApi: SourceApi,
    private readonly moduleApi: ModuleApi,
    private readonly messageService: MessageService,
  ) {}

  loadOne$ = createEffect(() => this.actions$.pipe(
    ofType(SourceScanActions.init, SourceScanActions.refresh, SourceScanActions.updateSort),
    withLatestFrom(this.repositoryId$, this.sort$,(_, repositoryId, sort) => ({repositoryId, sort})),
    switchMap(({repositoryId, sort}) => this.sourceApi.loadOne(repositoryId, sort.key, sort.value).pipe(
      map(repository => SourceScanActions.loadOneSuccess(repository)),
      catchError(() => of(SourceScanActions.loadOneError()))
    ))
  ));

  submit$ = createEffect(() => this.actions$.pipe(
    ofType(SourceScanActions.submit),
    withLatestFrom(this.repositoryId$, ({ payload }, repositoryId) => ({ repositoryId, payload })),
    switchMap(({ repositoryId, payload }) => this.sourceApi.scan(repositoryId, payload).pipe(
      map(({ result, modules }) => SourceScanActions.scanSuccess(result,modules)),
      catchError(() => from([
        SourceScanActions.scanError(),
        ModuleListActions.notifyError('Scan failed')
      ]))
    ))
  ));

  buildModule$ = createEffect(() => this.actions$.pipe(
    ofType(SourceScanActions.build),
    switchMap(({ payload }) => this.moduleApi.build(payload).pipe(
      flatMap(result => [
        ModuleListActions.buildSuccess(result),
        ModuleListActions.notifySuccess('Module build started successfully')
      ]),
      catchError(() => from([
        ModuleListActions.buildError(payload),
        ModuleListActions.notifyError('Module build failed')
      ]))
    ))
  ));


  notifySuccess$ = createEffect(() => this.actions$.pipe(
    ofType(SourceScanActions.notifySuccess),
    tap(({ payload }) => this.messageService.displaySuccessMessage(payload))
  ), { dispatch: false });

  notifyError$ = createEffect(() => this.actions$.pipe(
    ofType(SourceScanActions.notifyError),
    tap(({ payload }) => this.messageService.displayErrorMessage(payload))
  ), { dispatch: false });
}
