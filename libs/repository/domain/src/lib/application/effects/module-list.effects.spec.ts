// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { ModuleApi } from '../../infrastructure/services';
import { Observable } from 'rxjs';
import { ModuleListEffects } from './module-list.effects';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Store } from '@ngrx/store';
import { RepositoryState } from '../reducers';
import { Module, ModuleStatus, Source } from '@ryax/repository/domain';
import { ModuleListActions } from '../actions';
import { cold, hot } from 'jest-marbles';
import { MessageService } from '@ryax/shared/ui-common';


function provideMockModuleApi() {
  return {
    provide: ModuleApi,
    useValue: {
      loadAll: jest.fn(),
      build: jest.fn(),
      deleteModule: jest.fn(),
    }
  }
}

function provideMockMessageService() {
  return {
    provide: MessageService,
    useValue: {
      displaySuccessMessage: jest.fn(),
      displayErrorMessage: jest.fn()
    }
  }
}

describe('ModuleListEffects', () => {
  let actions$: Observable<any>;
  let effect: ModuleListEffects;
  let moduleApi: ModuleApi;
  let store: MockStore<RepositoryState>;
  let messageService: MessageService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [],
      providers: [
        ModuleListEffects,
        provideMockStore({}),
        provideMockActions(() => actions$),
        provideMockModuleApi(),
        provideMockMessageService(),
      ]
    });

    effect = TestBed.get(ModuleListEffects);
    moduleApi = TestBed.get(ModuleApi);
    messageService = TestBed.get(MessageService);
    store = TestBed.get(Store);
  });

  describe('loadModules$', () => {
    let modules: Module[];
    let sources: Source[];

    beforeEach(() => {
      modules = [{
        id: "module_id",
        name: "module_name",
        version: "module_version",
        description: "module_description",
        status: ModuleStatus.Scanned,
        error: "module_error",
        repositoryId: "module_repositoryId",
        creationDate: new Date("February 5, 2001 18:15:00"),
      }];
      sources = [{
        id: "source_id",
        name: "source_name",
        url: "source_url",
      }];
    });

    xit.each([
      ModuleListActions.init(),
      ModuleListActions.refresh(),
    ])('when load success', (action) => {
      (moduleApi.loadAll as jest.Mock)
        .mockImplementation(() => cold('-a|', { a : modules }));

      actions$ = hot('-a', {
        a: action,
      });

      const expected$ = hot('--a', {
        a: ModuleListActions.loadAllSuccess(modules, sources)
      });

      expect(effect.loadModules$).toBeObservable(expected$)
    });

    it.each([
      ModuleListActions.init(),
      ModuleListActions.refresh(),
    ])('when load fails', (action) => {
      (moduleApi.loadAll as jest.Mock)
        .mockImplementation(() => cold('-#', { a : modules }));

      actions$ = hot('-a', {
        a: action,
      });

      const expected$ = hot('--a', {
        a: ModuleListActions.loadAllError()
      });
      expect(effect.loadModules$).toBeObservable(expected$)
    });
  });

  describe('buildModule$', () => {
    let module: Module;

    beforeEach(() => {
      module = {
        id: "module_id",
        name: "module_name",
        version: "module_version",
        description: "module_description",
        status: ModuleStatus.Scanned,
        error: "module_error",
        repositoryId: "module_repositoryId",
        creationDate: new Date("February 5, 2001 18:15:00"),
      };
    });

    it('when build success', () => {
      (moduleApi.build as jest.Mock)
        .mockImplementation(() => cold('-a|', { a : module }));

      actions$ = hot('-a', {
        a: ModuleListActions.build(module.id),
      });

      const expected$ = hot('--(ab)', {
        a: ModuleListActions.buildSuccess(module),
        b: ModuleListActions.notifySuccess('Module built successfully')
      });

      expect(effect.buildModule$).toBeObservable(expected$)
    });

    it('when build fails', () => {
      (moduleApi.build as jest.Mock)
        .mockImplementation(() => cold('-#', { a : module }));

      actions$ = hot('-a', {
        a: ModuleListActions.build(module.id),
      });

      const expected$ = hot('--(ab)', {
        a: ModuleListActions.buildError(module.id),
        b: ModuleListActions.notifyError('Module build failed')
      });
      expect(effect.buildModule$).toBeObservable(expected$)
    });
  });

  describe('deleteModule$', () => {
    const moduleId = "moduleId";

    it('when delete success', () => {
      (moduleApi.deleteModule as jest.Mock)
        .mockImplementation(() => cold('-a|', { a : moduleId }));

      actions$ = hot('-a', {
        a: ModuleListActions.deleteModule(moduleId),
      });

      const expected$ = hot('--(ab)', {
        a: ModuleListActions.deleteSuccess(moduleId),
        b: ModuleListActions.notifySuccess('Module deleted successfully'),
      });

      expect(effect.deleteModule$).toBeObservable(expected$)
    });

    it('when delete fails', () => {
      (moduleApi.deleteModule as jest.Mock)
        .mockImplementation(() => cold('-#', { a : moduleId }));

      actions$ = hot('-a', {
        a: ModuleListActions.deleteModule(moduleId),
      });

      const expected$ = hot('--(ab)', {
        a: ModuleListActions.deleteError(),
        b: ModuleListActions.notifyError('Module delete failed')
      });
      expect(effect.deleteModule$).toBeObservable(expected$)
    });
  });

  test('notifySuccess$', () => {
    const message = "message";
    actions$ = hot('-a', {
      a: ModuleListActions.notifySuccess(message),
    });
    expect(effect.notifySuccess$).toSatisfyOnFlush(() => {
      expect(messageService.displaySuccessMessage).toHaveBeenCalledWith(message)
    });
  });

  test('notifyError$', () => {
    const message = "message";
    actions$ = hot('-a', {
      a: ModuleListActions.notifyError(message),
    });
    expect(effect.notifyError$).toSatisfyOnFlush(() => {
      expect(messageService.displayErrorMessage).toHaveBeenCalledWith(message)
    });
  });


});
