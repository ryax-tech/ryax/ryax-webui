// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import * as fromSourceScan from './source-scan.reducer';
import { SourceScanActions } from '../actions';
import {
  SourceScanModule,
  SourceScanModuleBuildState,
  SourceScanRequest,
  SourceScanResult,
} from '@ryax/repository/domain';

describe('SourceScanReducer', () => {
  test('on source scan init', () => {
    const state: fromSourceScan.State = fromSourceScan.initialState;
    const action = SourceScanActions.init();
    expect(fromSourceScan.reducer(state, action)).toEqual({
      ...state,
      loading: false,
      result: null
    });
  });

  test('on source scan submit', () => {
    const data: SourceScanRequest = {
      username: "username",
      password: "password",
      branch: "branch",
    };
    const state: fromSourceScan.State = fromSourceScan.initialState;
    const action = SourceScanActions.submit(data);
    expect(fromSourceScan.reducer(state, action)).toEqual({
      ...state,
      loading: true,
    });
  });

  test('on source scan success', () => {
    const result: SourceScanResult = {
      error: "error",
      branch: "branch",
    };
    const modules: SourceScanModule[] = [{
      id: "id",
      name: "name",
      version: "version",
      description: "description",
      error: "error",
      buildState: SourceScanModuleBuildState.Ready,
    }];
    const state: fromSourceScan.State = fromSourceScan.initialState;
    const action = SourceScanActions.scanSuccess(result, modules);
    expect(fromSourceScan.reducer(state, action)).toEqual({
      ...state,
      loading: false,
      result: result
    });
  });

  test('on source scan error', () => {
    const state: fromSourceScan.State = fromSourceScan.initialState;
    const action = SourceScanActions.scanError();
    expect(fromSourceScan.reducer(state, action)).toEqual({
      ...state,
      loading: false,
      result: null
    });
  });

});
