// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import { Source, SourceScanModule } from '../../entities/models';
import {
  ModuleListActions,
  SourceEditActions,
  SourceListActions,
  SourceScanActions,
} from '../actions';

export interface State extends EntityState<Source> {
  loading: boolean;
  lastScan: SourceScanModule[];
  builtModules: SourceScanModule[];
  lastScanSha: string;
}

export const adapter = createEntityAdapter<Source>({
  selectId: repository => repository.id
});

export const initialState: State = adapter.getInitialState({
  loading: false,
  lastScan: null,
  builtModules: null,
  lastScanSha: null,
});

export const reducer = createReducer<State>(
  initialState,
  on(SourceListActions.init, SourceListActions.refresh, (state) => ({
    ...state,
    loading: true
  })),
  on(SourceScanActions.init, (state) => ({
    ...state,
    loading: true
  })),
  on(SourceListActions.loadAllSuccess, (state, { payload }) => adapter.setAll(payload, {
    ...state,
    loading: false
  })),
  on(SourceListActions.loadAllError, (state ) => adapter.removeAll({
    ...state,
    loading: false
  })),
  //TODO line 48, delete payload as any
  on(SourceScanActions.loadOneSuccess, (state, { payload }) => adapter.upsertOne(payload as any, {
    ...state,
    loading: false,
    lastScan: payload.lastScan.noneBuiltModules,
    builtModules: payload.lastScan.builtModules,
    lastScanSha: payload.lastScan.sha
  })),
  on(SourceScanActions.loadOneError, (state ) => ({
    ...state,
    loading: false
  })),
  on(SourceEditActions.createSuccess, (state, { payload }) => adapter.addOne(payload, state)),
  on(SourceEditActions.updateSuccess, (state, { payload }) => adapter.upsertOne(payload, state)),
  on(SourceListActions.deleteSuccess, (state, { payload }) => adapter.removeOne(payload, state)),
  on(ModuleListActions.loadAllSuccess, (state, { payload }) => adapter.upsertMany(payload.sources, {
    ...state,
    loading: false
  })),
);
