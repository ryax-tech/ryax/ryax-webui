// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Module } from '@ryax/repository/domain';
import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import { ModuleListActions } from '../actions';

export interface State extends EntityState<Module> {
  loading: boolean;
  errorModalVisible: boolean;
  selectedModule: Module;
}

export const adapter = createEntityAdapter<Module>({
  selectId: module => module.id
});

export const initialState: State = adapter.getInitialState({
  loading: false,
  errorModalVisible: false,
  selectedModule: null,
});

export const reducer = createReducer<State>(
  initialState,
  on(ModuleListActions.init, ModuleListActions.refresh, (state) => ({
    ...state,
    loading: true
  })),
  on(ModuleListActions.loadAllSuccess, (state, { payload }) => adapter.setAll(payload.modules, {
    ...state,
    loading: false
  })),
  on(ModuleListActions.loadAllError, (state ) => adapter.removeAll({
    ...state,
    loading: false
  })),
  on(ModuleListActions.buildSuccess, (state, { payload }) => adapter.upsertOne(payload, state)),
  on(ModuleListActions.deleteSuccess, (state, { payload }) => adapter.removeOne(payload, state)),
  on(ModuleListActions.openErrorModal, (state, { payload }) => ({
    ...state,
    selectedModule: payload,
    errorModalVisible: true,
  })),
  on(ModuleListActions.closeErrorModal, (state) => ({
    ...state,
    errorModalVisible: false,
  })),
);
