// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { SourceScanModule, SourceScanModuleBuildState } from '../../entities/models';
import { createReducer, on } from '@ngrx/store';
import { ModuleListActions, SourceScanActions } from '../actions';

export type State = EntityState<SourceScanModule>

export const adapter = createEntityAdapter<SourceScanModule>({
  selectId: sourceScanModule => sourceScanModule.id
});

export const initialState: State = adapter.getInitialState();

export const reducer = createReducer<State>(
  initialState,
  on(SourceScanActions.init, (state) => adapter.removeAll(state)),
  on(SourceScanActions.scanSuccess, (state, { payload }) => adapter.setAll(payload.modules, state)),
  on(SourceScanActions.loadOneSuccess, (state, { payload }) => adapter.setAll(payload.lastScan.noneBuiltModules, state)),
  on(SourceScanActions.scanError, (state ) => adapter.removeAll(state)),
  on(SourceScanActions.build, (state, { payload }) => adapter.updateOne({
    id: payload,
    changes: {
      buildState: SourceScanModuleBuildState.Pending
    }
  }, state)),
  on(ModuleListActions.buildSuccess, (state, { payload }) => adapter.updateOne({
    id: payload.id,
    changes: {
      buildState: SourceScanModuleBuildState.Triggered
    }
  }, state)),
  on(ModuleListActions.buildError, (state, { payload }) => adapter.updateOne({
    id: payload,
    changes: {
      buildState: SourceScanModuleBuildState.Ready
    }
  }, state))
);
