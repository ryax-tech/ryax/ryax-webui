// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import * as fromModule from './module.reducer';
import { ModuleListActions } from '../actions';
import { Module, ModuleStatus, Source } from '@ryax/repository/domain';

describe('ModuleReducer', () => {

  test('on module list init', () => {
    const state: fromModule.State = fromModule.initialState;
    const action = ModuleListActions.init();
    expect(fromModule.reducer(state, action)).toEqual({
      ...state,
      loading: true,
    });
  });

  test('on module list refresh', () => {
    const state: fromModule.State = fromModule.initialState;
    const action = ModuleListActions.refresh();
    expect(fromModule.reducer(state, action)).toEqual({
      ...state,
      loading: true,
    });
  });

  test('on module list load all success', () => {
    const module1: Module = {
      id: "module_id_1",
      name: "module_name_1",
      version: "module_version_1",
      description: "module_description_1",
      status: ModuleStatus.Scanned,
      error: "module_error_1",
      repositoryId: "module_repositoryId_1",
      creationDate: new Date("February 5, 2001 18:15:00"),
    };
    const module2: Module = {
      id: "module_id_2",
      name: "module_name_2",
      version: "module_version_2",
      description: "module_description_2",
      status: ModuleStatus.Scanned,
      error: "module_error_2",
      repositoryId: "module_repositoryId_2",
      creationDate: new Date("February 5, 2001 18:15:00"),
    };
    const source1: Source = {
      id: "source_id_1",
      name: "source_name_1",
      url: "source_url_1",
    };
    const source2: Source = {
      id: "source_id_2",
      name: "source_name_2",
      url: "source_url_2",
    };
    const state: fromModule.State = fromModule.initialState;
    const action = ModuleListActions.loadAllSuccess([module1, module2], [source1, source2]);
    expect(fromModule.reducer(state, action)).toEqual({
      ...state,
      ids: [module1.id, module2.id],
      entities: {
        [module1.id]: module1,
        [module2.id]: module2,
      },
      loading: false,
    });
  });

  test('on module list load all error', () => {
    const state: fromModule.State = fromModule.initialState;
    const action = ModuleListActions.loadAllError();
    expect(fromModule.reducer(state, action)).toEqual({
      ...state,
      loading: false,
    });
  });

  test('on module list build success', () => {
    const module: Module = {
      id: "module_id",
      name: "module_name",
      version: "module_version",
      description: "module_description",
      status: ModuleStatus.Built,
      error: "module_error",
      repositoryId: "module_repositoryId",
      creationDate: new Date("February 5, 2001 18:15:00"),
    };
    const state: fromModule.State = fromModule.initialState;
    const action = ModuleListActions.buildSuccess(module);
    expect(fromModule.reducer(state, action)).toEqual({
      ...state,
      ids: [module.id],
      entities: {
        [module.id]: module
      },
      loading: false,
    });
  });

  test('on module list delete success', () => {
    const moduleId = "moduleId";
    const state: fromModule.State = fromModule.initialState;
    const action = ModuleListActions.deleteSuccess(moduleId);
    expect(fromModule.reducer(state, action)).toEqual({
      ...state,
    });
  });
});
