// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { RepositorySortKey, RepositorySortValue, SourceScanModule, SourceScanResult } from '@ryax/repository/domain';
import { createReducer, on } from '@ngrx/store';
import { SourceScanActions } from '../actions';

export interface State {
  loading: boolean;
  result: SourceScanResult;
  deleteDuplicateModalVisible: boolean;
  scanErrorModalVisible: boolean;
  selectedSourceScanModule: SourceScanModule;
  sort: {
    key: RepositorySortKey,
    value: RepositorySortValue
  },
  scanModalBranchVisibility: boolean
}

export const initialState: State = {
  loading: false,
  result: null,
  deleteDuplicateModalVisible: false,
  scanErrorModalVisible: false,
  selectedSourceScanModule: null,
  sort: {
    key: null,
    value: null
  },
  scanModalBranchVisibility: false
};

export const reducer = createReducer<State>(
  initialState,
  on(SourceScanActions.init, (state) => ({
    ...state,
    loading: false,
    result: null
  })),
  on(SourceScanActions.submit, (state) => ({
    ...state,
    loading: true,
  })),
  on(SourceScanActions.openDuplicateModal, (state) => ({
    ...state,
    deleteDuplicateModalVisible: true,
  })),
  on(SourceScanActions.closeDuplicateModal, (state) => ({
    ...state,
    deleteDuplicateModalVisible: false,
  })),
  on(SourceScanActions.scanSuccess, (state, { payload }) => ({
    ...state,
    loading: false,
    result: payload.result,
    scanModalBranchVisibility: false,
  })),
  on(SourceScanActions.scanError, (state ) => ({
    ...state,
    loading: false,
    result: null
  })),
  on(SourceScanActions.openScanErrorModal, (state, { payload }) => ({
    ...state,
    selectedSourceScanModule: payload,
    scanErrorModalVisible: true,
  })),
  on(SourceScanActions.closeScanErrorModal, (state) => ({
    ...state,
    scanErrorModalVisible: false,
  })),
  on(SourceScanActions.updateSort, (state, { payload }) => ({
    ...state,
    sort: payload.sort,
  })),
  on(SourceScanActions.openScanModal, (state) => ({
    ...state,
    scanModalBranchVisibility: true,
  })),
  on(SourceScanActions.closeScanModal, (state) => ({
    ...state,
    scanModalBranchVisibility: false,
  })),
);
