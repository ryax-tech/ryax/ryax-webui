// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import * as fromSource from './source.reducer';
import { ModuleListActions, SourceEditActions, SourceListActions, SourceScanActions } from '../actions';
import { Module, ModuleStatus, Source } from '@ryax/repository/domain';
import * as fromModule from './module.reducer';

describe('SourceReducer', () => {

  test('on source list init', () => {
    const state: fromSource.State = fromSource.initialState;
    const action = SourceListActions.init();
    expect(fromSource.reducer(state, action)).toEqual({
      ...state,
      loading: true,
    });
  });

  test('on source list refresh', () => {
    const state: fromSource.State = fromSource.initialState;
    const action = SourceListActions.refresh();
    expect(fromSource.reducer(state, action)).toEqual({
      ...state,
      loading: true,
    });
  });

  test('on source scan init', () => {
    const state: fromSource.State = fromSource.initialState;
    const action = SourceScanActions.init();
    expect(fromSource.reducer(state, action)).toEqual({
      ...state,
      loading: true,
    });
  });

  test('on source list load all success', () => {
    const source1: Source = {
      id: "source_id_1",
      name: "source_name_1",
      url: "source_url_1",
    };
    const source2: Source = {
      id: "source_id_2",
      name: "source_name_2",
      url: "source_url_2",
    };
    const state: fromSource.State = fromSource.initialState;
    const action = SourceListActions.loadAllSuccess([source1, source2]);
    expect(fromSource.reducer(state, action)).toEqual({
      ...state,
      ids: [source1.id, source2.id],
      entities: {
        [source1.id]: source1,
        [source2.id]: source2,
      },
      loading: false,
    });
  });

  test('on source list load all error', () => {
    const state: fromSource.State = fromSource.initialState;
    const action = SourceListActions.loadAllError();
    expect(fromSource.reducer(state, action)).toEqual({
      ...state,
      loading: false,
    });
  });

  test('on source scan load one success', () => {
    const source: Source = {
      id: "source_id",
      name: "source_name",
      url: "source_url",
    };
    const state: fromSource.State = fromSource.initialState;
    const action = SourceScanActions.loadOneSuccess(source);
    expect(fromSource.reducer(state, action)).toEqual({
      ...state,
      ids: [source.id],
      entities: {
        [source.id]: source
      },
      loading: false,
    });
  });

  test('on source scan load one error', () => {
    const state: fromSource.State = fromSource.initialState;
    const action = SourceScanActions.loadOneError();
    expect(fromSource.reducer(state, action)).toEqual({
      ...state,
      loading: false,
    });
  });

  test('on source edit create success', () => {
    const source: Source = {
      id: "source_id",
      name: "source_name",
      url: "source_url",
    };
    const state: fromSource.State = fromSource.initialState;
    const action = SourceEditActions.createSuccess(source);
    expect(fromSource.reducer(state, action)).toEqual({
      ...state,
      ids: [source.id],
      entities: {
        [source.id]: source
      },
      loading: false,
    });
  });

  test('on source edit update success', () => {
    const source: Source = {
      id: "source_id",
      name: "source_name",
      url: "source_url",
    };
    const state: fromSource.State = fromSource.initialState;
    const action = SourceEditActions.updateSuccess(source);
    expect(fromSource.reducer(state, action)).toEqual({
      ...state,
      ids: [source.id],
      entities: {
        [source.id]: source
      },
      loading: false,
    });
  });

  test('on source list delete success', () => {
    const repositoryId = "repositoryId";
    const state: fromSource.State = fromSource.initialState;
    const action = SourceListActions.deleteSuccess(repositoryId);
    expect(fromSource.reducer(state, action)).toEqual({
      ...state,
    });
  });

  test('on module list load success', () => {
    const repositoryId = "repositoryId";
    const state: fromSource.State = fromSource.initialState;
    const action = SourceListActions.deleteSuccess(repositoryId);
    expect(fromSource.reducer(state, action)).toEqual({
      ...state,
    });
  });

  test('on module list load all success', () => {
    const module1: Module = {
      id: "module_id_1",
      name: "module_name_1",
      version: "module_version_1",
      description: "module_description_1",
      status: ModuleStatus.Scanned,
      error: "module_error_1",
      repositoryId: "module_repositoryId_1",
      creationDate: new Date("February 5, 2001 18:15:00"),
    };
    const module2: Module = {
      id: "module_id_2",
      name: "module_name_2",
      version: "module_version_2",
      description: "module_description_2",
      status: ModuleStatus.Scanned,
      error: "module_error_2",
      repositoryId: "module_repositoryId_2",
      creationDate: new Date("February 5, 2001 18:15:00"),
    };
    const source1: Source = {
      id: "source_id_1",
      name: "source_name_1",
      url: "source_url_1",
    };
    const source2: Source = {
      id: "source_id_2",
      name: "source_name_2",
      url: "source_url_2",
    };
    const state: fromModule.State = fromModule.initialState;
    const action = ModuleListActions.loadAllSuccess([module1, module2], [source1, source2]);
    expect(fromModule.reducer(state, action)).toEqual({
      ...state,
      ids: [module1.id, module2.id],
      entities: {
        [module1.id]: module1,
        [module2.id]: module2,
      },
      loading: false,
    });
  });
});
