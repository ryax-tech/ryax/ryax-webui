// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { InjectionToken } from '@angular/core';
import { ActionReducerMap } from '@ngrx/store';
import * as fromModule from './module.reducer';
import * as fromSource from './source.reducer';
import * as fromSourceEdit from './source-edit.reducer';
import * as fromSourceScan from './source-scan.reducer';
import * as fromSourceScanModule from './source-scan-module.reducer';

export const RepositoryFeatureKey = 'repositoryDomain';

export interface RepositoryState {
  sources: fromSource.State;
  modules: fromModule.State;
  sourceEdit: fromSourceEdit.State;
  sourceScan: fromSourceScan.State;
  sourceScanModules: fromSourceScanModule.State
}

export const RepositoryAdapters = {
  sources: fromSource.adapter,
  modules: fromModule.adapter,
  sourceScanModules: fromSourceScanModule.adapter
};

export const RepositoryReducerToken = new InjectionToken<ActionReducerMap<RepositoryState>>(RepositoryFeatureKey);

export const RepositoryReducerProvider = {
  provide: RepositoryReducerToken,
  useValue: {
    sources: fromSource.reducer,
    modules: fromModule.reducer,
    sourceEdit: fromSourceEdit.reducer,
    sourceScan: fromSourceScan.reducer,
    sourceScanModules: fromSourceScanModule.reducer
  },
};
