// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createReducer, on } from '@ngrx/store';
import { SourceEditMode } from '../../entities/models';
import { SourceEditActions, SourceListActions, SourceScanActions } from '../actions';

export interface State {
  source: string;
  mode: SourceEditMode;
}

export const initialState: State = {
  source: null,
  mode: null
};

export const reducer = createReducer<State>(
  initialState,
  on(SourceListActions.addSource, (state) => ({
    ...state,
    source: null,
    mode: SourceEditMode.NewSource
  })),
  on(SourceListActions.editSourceInfos, SourceScanActions.editRepository, (state, { payload }) => ({
    ...state,
    source: payload,
    mode: SourceEditMode.EditSourceInfos
  })),
  on(SourceEditActions.close, (state) => ({
    ...state,
    source: null,
    mode: null
  }))
);
