// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import * as fromSourceEdit from './source-edit.reducer';
import { SourceEditActions, SourceListActions } from '../actions';
import { SourceEditMode } from '@ryax/repository/domain';

describe('SourceEditReducer', () => {
  test('on source list add source', () => {
    const state: fromSourceEdit.State = fromSourceEdit.initialState;
    const action = SourceListActions.addSource();
    expect(fromSourceEdit.reducer(state, action)).toEqual({
      ...state,
      source: null,
      mode: SourceEditMode.NewSource
    });
  });

  test('on source list edit source infos', () => {
    const sourceId = "sourceId";
    const state: fromSourceEdit.State = fromSourceEdit.initialState;
    const action = SourceListActions.editSourceInfos(sourceId);
    expect(fromSourceEdit.reducer(state, action)).toEqual({
      ...state,
      source: sourceId,
      mode: SourceEditMode.EditSourceInfos
    });
  });

  test('on source edit close', () => {
    const state: fromSourceEdit.State = fromSourceEdit.initialState;
    const action = SourceEditActions.close();
    expect(fromSourceEdit.reducer(state, action)).toEqual({
      ...state,
      source: null,
      mode: null
    });
  });

});
