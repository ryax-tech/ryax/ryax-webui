// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import * as fromSourceScanModule from './source-scan-module.reducer';
import { ModuleListActions, SourceScanActions } from '../actions';
import {
  Module, ModuleStatus,
  SourceScanModuleBuildState,
} from '@ryax/repository/domain';

describe('SourceScanModuleReducer', () => {
  xit('on source scan build', () => {
    const module: Module = {
      id: "module_id",
      name: "module_name",
      version: "module_version",
      description: "module_description",
      status: ModuleStatus.Scanned,
      error: "module_error",
      repositoryId: "module_repositoryId",
      creationDate: new Date("February 5, 2001 18:15:00"),
    };
    const state: fromSourceScanModule.State = fromSourceScanModule.initialState;
    const action = SourceScanActions.build(module.id);
    expect(fromSourceScanModule.reducer(state, action)).toEqual({
      ...state,
      id: module.id,
      changes: {
        buildState: SourceScanModuleBuildState.Pending
      }
    });
  });

  xit('on source scan build success', () => {
    const module: Module = {
      id: "module_id",
      name: "module_name",
      version: "module_version",
      description: "module_description",
      status: ModuleStatus.Built,
      error: "module_error",
      repositoryId: "module_repositoryId",
      creationDate: new Date("February 5, 2001 18:15:00"),
    };
    const state: fromSourceScanModule.State = fromSourceScanModule.initialState;
    const action = ModuleListActions.buildSuccess(module);
    expect(fromSourceScanModule.reducer(state, action)).toEqual({
      ...state,
      id: module.id,
      changes: {
        buildState: SourceScanModuleBuildState.Triggered
      }
    });
  });

  xit('on source scan build error', () => {
    const module: Module = {
      id: "module_id",
      name: "module_name",
      version: "module_version",
      description: "module_description",
      status: ModuleStatus.Scanned,
      error: "module_error",
      repositoryId: "module_repositoryId",
      creationDate: new Date("February 5, 2001 18:15:00"),
    };
    const state: fromSourceScanModule.State = fromSourceScanModule.initialState;
    const action = ModuleListActions.buildError(module.id);
    expect(fromSourceScanModule.reducer(state, action)).toEqual({
      ...state,
      id: module,
      changes: {
        buildState: SourceScanModuleBuildState.Ready
      }
    });
  });
});
