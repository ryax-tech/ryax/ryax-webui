// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Source } from '../../entities/models';
import { SourceListActions } from '../actions';
import { SourceListSelectors } from '../selectors';
import { RepositoryState } from '../reducers';

@Injectable()
export class SourceListFacade {
  loading$ = this.store.select(SourceListSelectors.selectLoading);
  sources$ = this.store.select(SourceListSelectors.selectAllSources);

  constructor(
    private readonly store: Store<RepositoryState>
  ) {}

  init() {
    this.store.dispatch(SourceListActions.init());
  }

  refresh() {
    this.store.dispatch(SourceListActions.refresh());
  }

  addSource() {
    this.store.dispatch(SourceListActions.addSource());
  }

  editSourceInfos(source: Source) {
    const { id } = source;
    this.store.dispatch(SourceListActions.editSourceInfos(id));
  }

  deleteSource(source: Source) {
    const { id } = source;
    this.store.dispatch(SourceListActions.deleteSource(id));
  }
}
