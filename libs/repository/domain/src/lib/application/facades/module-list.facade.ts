// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { ModuleListSelectors } from '../selectors';
import { ModuleListActions } from '../actions';
import { RepositoryState } from '../reducers';
import { Module } from '../../entities/models';

@Injectable()
export class ModuleListFacade {
  loading$ = this.store.select(ModuleListSelectors.selectLoading);
  modules$ = this.store.select(ModuleListSelectors.selectModules);
  errorModalVisible$ = this.store.select(ModuleListSelectors.selectErrorModalVisible);
  selectedModule$ = this.store.select(ModuleListSelectors.selectSelectedModule);

  constructor(
    private readonly store: Store<RepositoryState>
  ) {}

  init() {
    this.store.dispatch(ModuleListActions.init());
  }

  refresh() {
    this.store.dispatch(ModuleListActions.refresh());
  }

  build(moduleId: string) {
    this.store.dispatch(ModuleListActions.build(moduleId));
  }

  deleteModule(moduleId: string) {
    this.store.dispatch(ModuleListActions.deleteModule(moduleId));
  }

  openErrorModal(module: Module) {
    this.store.dispatch(ModuleListActions.openErrorModal(module));
  }

  closeErrorModal() {
    this.store.dispatch(ModuleListActions.closeErrorModal());
  }
}
