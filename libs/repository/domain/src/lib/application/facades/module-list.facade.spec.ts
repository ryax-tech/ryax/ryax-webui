// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import {
  ModuleListFacade,
  ModuleStatus,
  ModuleView,
} from '@ryax/repository/domain';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { RepositoryState } from '../reducers';
import { TestBed } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { cold } from 'jest-marbles';
import { ModuleListActions } from '../actions';
import { selectModules } from '../selectors/module-list.selectors';

describe('ModuleListFacade', () => {
  let facade: ModuleListFacade;
  let store: MockStore<RepositoryState>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [],
      providers: [
        ModuleListFacade,
        provideMockStore({})
      ]
    });

    facade = TestBed.get(ModuleListFacade);
    store = TestBed.get(Store);
  });

  it('modules$', () => {
    const module: ModuleView[] = [{
      id: "module_id",
      name: "module_name",
      version: "module_version",
      description: "module_description",
      status: ModuleStatus.Scanned,
      error: "module_error",
      repositoryId: "module_repositoryId",
      repositoryName: "module_repositoryName",
      creationDate: Date,
      buildEnabled: true,
      deleteEnabled: true,
      isSuccess: true,
    }];
    store.overrideSelector(selectModules, null);
    selectModules.setResult(module);
    const expected = cold('a-', { a: module });
    expect(facade.modules$).toBeObservable(expected);
  });

  it('init', () => {
    spyOn(store, 'dispatch');
    facade.init();
    expect(store.dispatch).toHaveBeenCalledWith(ModuleListActions.init())
  });

  it('refresh', () => {
    spyOn(store, 'dispatch');
    facade.refresh();
    expect(store.dispatch).toHaveBeenCalledWith(ModuleListActions.refresh())
  });

  it('build', () => {
    const moduleId = "moduleId";
    spyOn(store, 'dispatch');
    facade.build(moduleId);
    expect(store.dispatch).toHaveBeenCalledWith(ModuleListActions.build(moduleId))
  });

  it('deleteModule', () => {
    const moduleId = "moduleId";
    spyOn(store, 'dispatch');
    facade.deleteModule(moduleId);
    expect(store.dispatch).toHaveBeenCalledWith(ModuleListActions.deleteModule(moduleId))
  });
});
