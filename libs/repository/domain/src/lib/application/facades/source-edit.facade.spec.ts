// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Source, SourceEditData, SourceEditFacade, SourceEditMode } from '@ryax/repository/domain';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { RepositoryState } from '../reducers';
import { TestBed } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { cold } from 'jest-marbles';
import { SourceEditActions } from '../actions';
import { selectMode, selectRepository, selectVisible } from '../selectors/source-edit.selectors';

describe('SourceEditFacade', () => {
  let facade: SourceEditFacade;
  let store: MockStore<RepositoryState>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [],
      providers: [
        SourceEditFacade,
        provideMockStore({})
      ]
    });

    facade = TestBed.get(SourceEditFacade);
    store = TestBed.get(Store);
  });

  it('visible$', () => {
    const visible = false;
    store.overrideSelector(selectVisible, null);
    selectVisible.setResult(visible);
    const expected = cold('a-', { a: visible });
    expect(facade.visible$).toBeObservable(expected);
  });

  it('mode$', () => {
    const mode: SourceEditMode = SourceEditMode.NewSource;
    store.overrideSelector(selectMode, null);
    selectMode.setResult(mode);
    const expected = cold('a-', { a: mode });
    expect(facade.mode$).toBeObservable(expected);
  });

  it('source$', () => {
    const source: Source = {
      id: "source_id",
      name: "source_name",
      url: "source_url",
    };
    store.overrideSelector(selectRepository, null);
    selectRepository.setResult(source);
    const expected = cold('a-', { a: source });
    expect(facade.source$).toBeObservable(expected);
  });

  it('save', () => {
    const source: SourceEditData = {
      name: "source_name",
      url: "source_url",
    };
    spyOn(store, 'dispatch');
    facade.save(source);
    expect(store.dispatch).toHaveBeenCalledWith(SourceEditActions.save(source))
  });

  it('close', () => {
    spyOn(store, 'dispatch');
    facade.close();
    expect(store.dispatch).toHaveBeenCalledWith(SourceEditActions.close())
  });
});
