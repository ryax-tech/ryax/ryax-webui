// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { TestBed } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { cold } from 'jest-marbles';
import {
  Source,
  SourceScanFacade,
  SourceScanModuleView,
  SourceScanRequest,
  SourceScanResult,
} from '@ryax/repository/domain';
import { RepositoryState } from '../reducers';
import {
  selectSource,
  selectSourceScanModules,
  selectSourceScanProcessing,
  selectSourceScanResult,
} from '../selectors/source-scan.selectors';
import { SourceScanActions } from '../actions';

describe('SourceScanFacade', () => {
  let facade: SourceScanFacade;
  let store: MockStore<RepositoryState>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [],
      providers: [
        SourceScanFacade,
        provideMockStore({})
      ]
    });

    facade = TestBed.get(SourceScanFacade);
    store = TestBed.get(Store);
  });

  it('source$', () => {
    const source: Source = {
      id: "sourceId",
      name: "source name",
      url: "http://source.url",
    };
    store.overrideSelector(selectSource, null);
    selectSource.setResult(source);
    const expected = cold('a-', { a: source });
    expect(facade.source$).toBeObservable(expected);
  });

  it('sourceScanProcessing$', () => {
    const scan = true ;
    store.overrideSelector(selectSourceScanProcessing, null);
    selectSourceScanProcessing.setResult(scan);
    const expected = cold('a-', { a: scan });
    expect(facade.sourceScanProcessing$).toBeObservable(expected);
  });

  it('sourceScanResult$', () => {
    const result: SourceScanResult = {
      error: "error",
      branch: "branch",
    };
    store.overrideSelector(selectSourceScanResult, null);
    selectSourceScanResult.setResult(result);
    const expected = cold('a-', { a: result });
    expect(facade.sourceScanResult$).toBeObservable(expected);
  });

  it('sourceScanModules$', () => {
    const sourceModule: SourceScanModuleView[] = [{
      id: "source_id",
      name: "source_name",
      version: "source_version",
      description: "source_description",
      error: "source_error",
      duplicated: false,
      buildDisabled: false,
    }];
    store.overrideSelector(selectSourceScanModules, null);
    selectSourceScanModules.setResult(sourceModule);
    const expected = cold('a-', { a: sourceModule });
    expect(facade.sourceScanModules$).toBeObservable(expected);
  });

  it('init', () => {
    spyOn(store, 'dispatch');
    facade.init();
    expect(store.dispatch).toHaveBeenCalledWith(SourceScanActions.init())
  });

  it('submit', () => {
    const data: SourceScanRequest = {
      username: "username",
      password: "password",
      branch: "branch",
    };
    spyOn(store, 'dispatch');
    facade.submit(data);
    expect(store.dispatch).toHaveBeenCalledWith(SourceScanActions.submit(data))
  });

  it('build', () => {
    const moduleId = "moduleId";
    spyOn(store, 'dispatch');
    facade.build(moduleId);
    expect(store.dispatch).toHaveBeenCalledWith(SourceScanActions.build(moduleId))
  });
});
