// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { RepositorySort, Source, SourceScanModule, SourceScanRequest } from '../../entities/models';
import { RepositoryState } from '../reducers';
import { SourceScanActions } from '../actions';
import { SourceScanSelectors } from '../selectors';

@Injectable()
export class SourceScanFacade {
  source$ = this.store.select(SourceScanSelectors.selectSource);
  sourceScanProcessing$ = this.store.select(SourceScanSelectors.selectSourceScanProcessing);
  sourceScanResult$ = this.store.select(SourceScanSelectors.selectSourceScanResult);
  sourceScanModules$ = this.store.select(SourceScanSelectors.selectSourceScanModules);
  lastScanModules$ = this.store.select(SourceScanSelectors.selectLastScanModules);
  lastScanSha$ = this.store.select(SourceScanSelectors.selectLastScanSha);
  builtModules$ = this.store.select(SourceScanSelectors.selectBuiltModules);
  deleteDuplicateModalVisible$ = this.store.select(SourceScanSelectors.selectDeleteDuplicateModalVisible);
  scanErrorModalVisible$ = this.store.select(SourceScanSelectors.selectScanErrorModalVisible);
  selectedSourceScanModule$ = this.store.select(SourceScanSelectors.selectSelectedSourceScanModule);
  scanModalBranchVisibility$ = this.store.select(SourceScanSelectors.selectScanModalBranchVisibility);

  constructor(
    private readonly store: Store<RepositoryState>
  ) {}

  init() {
    this.store.dispatch(SourceScanActions.init());
  }

  submit(data: SourceScanRequest) {
    this.store.dispatch(SourceScanActions.submit(data));
  }

  openScanModal() {
    this.store.dispatch(SourceScanActions.openScanModal());
  }

  closeScanModal() {
    this.store.dispatch(SourceScanActions.closeScanModal());
  }

  build(moduleId: string) {
    this.store.dispatch(SourceScanActions.build(moduleId));
  }

  openDuplicateModal() {
    this.store.dispatch(SourceScanActions.openDuplicateModal());
  }

  closeDuplicateModal() {
    this.store.dispatch(SourceScanActions.closeDuplicateModal());
  }

  openScanErrorModal(module: SourceScanModule) {
    this.store.dispatch(SourceScanActions.openScanErrorModal(module));
  }

  closeScanErrorModal() {
    this.store.dispatch(SourceScanActions.closeScanErrorModal());
  }

  refresh() {
    this.store.dispatch(SourceScanActions.refresh())
  }

  editRepository(repository: Source) {
    const { id } = repository;
    this.store.dispatch(SourceScanActions.editRepository(id))
  }

  updateSort(data: RepositorySort) {
    this.store.dispatch(SourceScanActions.updateSort(data));
  }
}
