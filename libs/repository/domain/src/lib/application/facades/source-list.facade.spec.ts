// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Source, SourceListFacade } from '@ryax/repository/domain';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { RepositoryState } from '../reducers';
import { TestBed } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { cold } from 'jest-marbles';
import { SourceListActions } from '../actions';

describe('SourceListFacade', () => {
  let facade: SourceListFacade;
  let store: MockStore<RepositoryState>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [],
      providers: [
        SourceListFacade,
        provideMockStore({})
      ]
    });

    facade = TestBed.get(SourceListFacade);
    store = TestBed.get(Store);
  });

  xit('sources$', () => {
    const sources: Source[] = [
      {
        id: "sourceId",
        name: "source name",
        url: "http://source.url",
      },
      {
        id: "sourceId",
        name: "source name",
        url: "http://source.url",
      }
    ];
    // store.overrideSelector(selectAllSources, null);
    // selectAllSources.setResult(sources);
    const expected = cold('a-', { a: sources });
    expect(facade.sources$).toBeObservable(expected);
  });

  it('init', () => {
    spyOn(store, 'dispatch');
    facade.init();
    expect(store.dispatch).toHaveBeenCalledWith(SourceListActions.init())
  });

  it('refresh', () => {
    spyOn(store, 'dispatch');
    facade.refresh();
    expect(store.dispatch).toHaveBeenCalledWith(SourceListActions.refresh())
  });

  it('addSource', () => {
    spyOn(store, 'dispatch');
    facade.addSource();
    expect(store.dispatch).toHaveBeenCalledWith(SourceListActions.addSource())
  });

  it('editSourceInfos', () => {
    const source: Source = {
      id: "source_id",
      name: "source_name",
      url: "source_url",
    };
    const { id } = source;
    spyOn(store, 'dispatch');
    facade.editSourceInfos(source);
    expect(store.dispatch).toHaveBeenCalledWith(SourceListActions.editSourceInfos(id))
  });

  it('deleteSource', () => {
    const source: Source = {
      id: "source_id",
      name: "source_name",
      url: "source_url",
    };
    const { id } = source;
    spyOn(store, 'dispatch');
    facade.deleteSource(source);
    expect(store.dispatch).toHaveBeenCalledWith(SourceListActions.deleteSource(id))
  });
});
