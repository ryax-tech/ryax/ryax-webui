// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { SourceEditActions } from '../actions';
import { SourceEditSelectors } from '../selectors';
import { RepositoryState } from '../reducers';
import { SourceEditData } from '../../entities/models';

@Injectable()
export class SourceEditFacade {
  visible$ = this.store.select(SourceEditSelectors.selectVisible);
  mode$ = this.store.select(SourceEditSelectors.selectMode);
  source$ = this.store.select(SourceEditSelectors.selectRepository);

  constructor(
    private readonly store: Store<RepositoryState>
  ) {}

  save(value: SourceEditData) {
    this.store.dispatch(SourceEditActions.save(value));
  }

  close() {
    this.store.dispatch(SourceEditActions.close());
  }
}
