// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createAction } from '@ngrx/store';
import { RepositorySort, SourceDetails, Source, SourceScanModule, SourceScanRequest, SourceScanResult } from '@ryax/repository/domain';

export const init = createAction(
  '[Source Scan] Init',
);

export const refresh = createAction(
  '[Source Scan] Refresh',
);

export const submit = createAction(
  '[Source Scan] Submit',
  (data: SourceScanRequest) => ({
    payload: data
  })
);

export const build = createAction(
  '[Source Scan] Build module',
  (moduleId: string) => ({
    payload: moduleId
  })
);

export const openDuplicateModal = createAction(
  '[Source Scan] Open duplicate modal',
);

export const closeDuplicateModal = createAction(
  '[Source Scan] Close duplicate modal',
);

export const loadOneSuccess = createAction(
  '[Source Scan] Load one success',
  (source: SourceDetails) => ({
    payload: source
  })
);

export const loadOneError = createAction(
  '[Source Scan] Load one error'
);

export const scanSuccess = createAction(
  '[Source Scan] Scan success',
  (result: SourceScanResult, modules: SourceScanModule[]) => ({
    payload: { result, modules }
  })
);

export const scanError = createAction(
  '[Source Scan] Scan error',
);

export const notifySuccess = createAction(
  '[Source Scan] Notify success',
  (message: string) => ({
    payload: message
  })
);

export const notifyError = createAction(
  '[Source Scan] Notify error',
  (message: string) => ({
    payload: message
  })
);

export const openScanErrorModal = createAction(
  '[Source Scan] Open scan error modal',
  (module: SourceScanModule) => ({
    payload: module,
  })
);

export const closeScanErrorModal = createAction(
  '[Source Scan] Close scan error modal',
);

export const editRepository = createAction(
  '[Source Scan] Edit repository',
  (repositoryId: string) => ({
    payload: repositoryId,
  })
);

export const updateSort = createAction(
  '[Source Scan] Update sort',
  (sort: RepositorySort) => ({
    payload: { sort },
  })
);

export const openScanModal = createAction(
  '[Source Scan] Open scan modal'
);

export const closeScanModal = createAction(
  '[Source Scan] Close scan modal'
);
