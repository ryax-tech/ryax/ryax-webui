// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createAction } from '@ngrx/store';
import { Source } from '@ryax/repository/domain';

export const init = createAction(
  '[Source List] Init'
);

export const refresh = createAction(
  '[Source List] Refresh'
);

export const addSource = createAction(
  '[Source List] Add source'
);

export const editSourceInfos = createAction(
  '[Source List] Edit source infos',
  (sourceId: string) => ({
    payload: sourceId
  })
);

export const deleteSource = createAction(
  '[Source List] Delete source',
  (sourceId: string) => ({
    payload: sourceId
  })
);

export const loadAllSuccess = createAction(
  '[Source List] Load all success',
  (sources: Source[]) => ({
    payload: sources
  })
);

export const loadAllError = createAction(
  '[Source List] Load all error'
);

export const deleteSuccess = createAction(
  '[Source List] Delete success',
  (repositoryId: string) => ({
    payload: repositoryId
  })
);

export const deleteError = createAction(
  '[Source List] Delete error',
);

export const notifySuccess = createAction(
  '[Source List] Notify success',
  (message: string) => ({
    payload: message
  })
);

export const notifyError = createAction(
  '[Source List] Notify error',
  (message: string) => ({
    payload: message
  })
);
