// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createAction } from '@ngrx/store';
import { Source, SourceEditData } from '../../entities/models';

export const save = createAction(
  '[Source Edit] Save',
  (value: SourceEditData) => ({
    payload: value
  })
);

export const close = createAction(
  '[Source Edit] Close'
);

export const createSuccess = createAction(
  '[Source Edit] Create success',
  (source: Source) => ({
    payload: source
  })
);

export const createError = createAction(
  '[Source Edit] Create error'
);

export const updateSuccess = createAction(
  '[Source Edit] Update success',
  (source: Source) => ({
    payload: source
  })
);

export const updateError = createAction(
  '[Source Edit] Update error'
);

export const notifySuccess = createAction(
  '[Source Edit] Notify success',
  (message: string) => ({
    payload: message
  })
);

export const notifyError = createAction(
  '[Source Edit] Notify error',
  (message: string) => ({
    payload: message
  })
);
