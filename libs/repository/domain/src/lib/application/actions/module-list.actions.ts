// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createAction } from '@ngrx/store';
import { Module, Source } from '@ryax/repository/domain';

export const init = createAction(
  '[Module List] Init'
);

export const refresh = createAction(
  '[Module List] Refresh'
);

export const build = createAction(
  '[Module List] Build module',
  (moduleId: string) => ({
    payload: moduleId
  })
);

export const deleteModule = createAction(
  '[Module List] Delete module',
  (moduleId: string) => ({
    payload: moduleId
  })
);

export const loadAllSuccess = createAction(
  '[Module List] Load all modules success',
  (modules: Module[], sources: Source[]) => ({
    payload: { modules, sources }
  })
);

export const loadAllError = createAction(
  '[Module List] Load all modules error'
);

export const buildSuccess = createAction(
  '[Module List] Build success',
  (module: Module) => ({
    payload: module
  })
);

export const buildError = createAction(
  '[Module List] Build error',
  (moduleId: string) => ({
    payload: moduleId
  })
);

export const deleteSuccess = createAction(
  '[Module List] Delete success',
  (moduleId: string) => ({
    payload: moduleId
  })
);

export const deleteError = createAction(
  '[Module List] Delete error'
);

export const openDuplicateModal = createAction(
  '[Module List] Open duplicate modal',
);

export const closeDuplicateModal = createAction(
  '[Module List] Close duplicate modal',
);

export const notifySuccess = createAction(
  '[Module List] Notify success',
  (message: string) => ({
    payload: message
  })
);

export const notifyError = createAction(
  '[Module List] Notify error',
  (message: string) => ({
    payload: message
  })
);

export const openErrorModal = createAction(
  '[Module List] Open error modal',
  (module: Module) => ({
    payload: module,
  })
);

export const closeErrorModal = createAction(
  '[Module List] Close error modal',
);
