// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {
  RepositorySort,
  RepositorySortKey,
  RepositorySortValue,
  SourceEditData,
  SourceScanRequest,
  SourceScanResult,
} from '../../entities/models';
import {
  CreateRepositoryDto,
  RepositoryDto,
  RepositoryScanRequestDto,
  RepositoryScanResultDto, SourceDetailsDto,
  UpdateRepositoryDto,
} from '../models';
import { SourceAdapter, SourceDetailsAdapter, SourceScanAdapter, SourceScanModuleAdapter } from '../adapters';


@Injectable()
export class SourceApi {
  private baseUrl = "/api/repository/sources";

  constructor(
    private readonly http: HttpClient,
    private readonly sourceAdapter: SourceAdapter,
    private readonly sourceScanAdapter: SourceScanAdapter,
    private readonly sourceScanModuleAdapter: SourceScanModuleAdapter,
    private readonly sourceDetailsAdapter: SourceDetailsAdapter
  ) {}

  private adaptSortValue(sortValue: RepositorySortValue) {
    switch(sortValue) {
      case "ascend":
        return "asc";
      case 'descend':
        return "desc"
    }
  };

  loadAll() {
    return this.http.get<RepositoryDto[]>(this.baseUrl, { responseType: 'json' }).pipe(
      map(dtos => dtos.map(item => this.sourceAdapter.adapt(item)))
    )
  }

  loadOne(repositoryId: string, sortKey: RepositorySortKey, sortValue: RepositorySortValue) {
    const url = [this.baseUrl, repositoryId].join('/');
    const params = {
      module_sorting: sortKey,
      module_sorting_type: this.adaptSortValue(sortValue)
    };
    return this.http.get<SourceDetailsDto>(url, { params: params, responseType: 'json' }).pipe(
      map(dto => this.sourceDetailsAdapter.adapt(dto))
    );
  }

  create(data: SourceEditData) {
    const createDto: CreateRepositoryDto = {
      name: data.name,
      url: data.url,
      username: data.username ? data.username : "",
      password: data.password ? data.password : "",
    };
    return this.http.post<RepositoryDto>(this.baseUrl, createDto, { responseType: 'json'}).pipe(
      map(dto => this.sourceAdapter.adapt(dto))
    );
  }

  update(repositoryId: string, data: SourceEditData) {
    const url = [this.baseUrl, repositoryId].join('/');
    const updateDto: UpdateRepositoryDto = {
      name: data.name,
      url: data.url,
      username: data.username,
      password: data.password,
    };
    return this.http.put<RepositoryDto>(url, updateDto, { responseType: 'json'}).pipe(
      map(dto => this.sourceAdapter.adapt(dto))
    );
  }

  delete(repositoryId: string) {
    const url = [this.baseUrl, repositoryId].join('/');
    return this.http.delete<void>(url, { responseType: 'json' });
  }

  scan(repositoryId: string, data: SourceScanRequest) {
    const url = [this.baseUrl, repositoryId, 'scan'].join('/');
    const scanDto: RepositoryScanRequestDto = {
      username: data.username ? data.username : undefined,
      password: data.password ? data.password : undefined,
      branch: data.branch ? data.branch : undefined
    };
    return this.http.post<RepositoryScanResultDto>(url, scanDto, { responseType: 'json' }).pipe(
      map(dto => ({
        result: this.sourceScanAdapter.adapt(dto),
        modules: dto.modules.map(item => this.sourceScanModuleAdapter.adapt(item))
      }))
    );
  }
}
