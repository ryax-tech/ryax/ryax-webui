// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ModuleDto } from '../models/module-dto';
import { ModuleAdapter, SourceAdapter } from '../adapters';


@Injectable()
export class ModuleApi {
  private baseUrl = "/api/repository/modules";

  constructor(
    private readonly http: HttpClient,
    private readonly moduleAdapter: ModuleAdapter,
    private readonly repositoryAdapter: SourceAdapter
  ) {}

  loadAll() {
    return this.http.get<ModuleDto[]>(this.baseUrl, { responseType: 'json' }).pipe(
      map(dtos => ({
        modules: dtos.map(item => this.moduleAdapter.adapt(item)),
        repositories: dtos.map(item => this.repositoryAdapter.adapt(item.source))
      }))
    )
  }

  build(moduleId: string) {
    const url = [this.baseUrl, moduleId, 'build'].join('/');
    return this.http.post<ModuleDto>(url, undefined, { responseType: 'json'}).pipe(
      map(dto => this.moduleAdapter.adapt(dto))
    );
  }

  deleteModule(moduleId: string) {
    const url = [this.baseUrl, moduleId].join('/');
    return this.http.delete<string>(url, { responseType: 'json' });
  }
}
