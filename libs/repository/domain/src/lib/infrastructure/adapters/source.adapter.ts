// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { ScanResult, Source } from '../../entities/models';
import { RepositoryDto, ScanResultDto } from '../models';

@Injectable()
export class SourceAdapter {
  adapt(dto: Partial<RepositoryDto>): Source {
    return {
      id: dto.id,
      name: dto.name,
      url: dto.url,
      scanResult: dto.scan_result,
      username: dto.username,
      password: dto.password,
      isPasswordSet: dto.password_is_set
    }
  }
}
