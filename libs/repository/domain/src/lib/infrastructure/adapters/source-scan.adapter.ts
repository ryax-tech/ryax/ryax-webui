// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { RepositoryScanResultDto } from '../models';
import { SourceScanResult } from '../../entities/models';

@Injectable()
export class SourceScanAdapter {
  adapt(dto: RepositoryScanResultDto): SourceScanResult {
    return {
      branch: dto.branch,
      sha: dto.sha,
      error: dto.error
    };
  }
}
