// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { LastScanDto, SourceDetailsDto } from '../models';
import { LastScan, SourceDetails } from '@ryax/repository/domain';
import { SourceScanModuleAdapter } from './source-scan-module.adapter';

@Injectable()
export class SourceDetailsAdapter {

  constructor(
    private readonly sourceScanModuleAdapter : SourceScanModuleAdapter
  ) {}

  adapt(dto: Partial<SourceDetailsDto>): SourceDetails {
    return {
      id: dto.id,
      name: dto.name,
      url: dto.url,
      lastScan: this.adaptLastScan(dto.last_scan),
    }
  }

  adaptLastScan(dto: Partial<LastScanDto>): LastScan {
    return {
      sha: dto.sha,
      builtModules: dto.built_modules.map(item => this.sourceScanModuleAdapter.adapt(item)),
      noneBuiltModules: dto.none_built_modules.map(item => this.sourceScanModuleAdapter.adapt(item)),
    }
  }
}
