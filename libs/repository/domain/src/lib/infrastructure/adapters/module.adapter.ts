// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { Module, ModuleStatus } from '../../entities/models';
import { ModuleStatusDto, ModuleDto } from '../models/module-dto';

@Injectable()
export class ModuleAdapter {

  private adaptBuildStatus(dto: ModuleStatusDto): ModuleStatus {
    if(dto in ModuleStatus) {
      return ModuleStatus[dto];
    } else {
      return ModuleStatus.Scanning
    }
  }

  adapt(dto: Partial<ModuleDto>): Module {
    return {
      id: dto.id,
      name: dto.name,
      version: dto.version,
      description: dto.description,
      status: dto.status ? this.adaptBuildStatus(dto.status) : undefined,
      repositoryId: dto.source && dto.source.id ? dto.source.id : undefined,
      creationDate: new Date(dto.creation_date),
      error: dto.error ? dto.error : null,
    }
  }
}
