// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { RepositoryScanModuleDto } from '../models';
import { SourceScanModule, SourceScanModuleBuildState } from '../../entities/models';

@Injectable()
export class SourceScanModuleAdapter {
  adapt(dto: RepositoryScanModuleDto): SourceScanModule {
    return {
      id: dto.id,
      errors: dto ? dto.scan_errors : null,
      description: dto.description,
      duplicated: dto.duplicated,
      path: dto.metadata_path,
      name: dto.name,
      type: dto.type,
      dynamicOutputs: dto.dynamic_outputs,
      status: dto.status,
      version: dto.version,
      kind: dto.kind,
      technicalName: dto.technical_name,
      buildState: SourceScanModuleBuildState.Ready,
      sha: dto.sha,
    }
  }
}
