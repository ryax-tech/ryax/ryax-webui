// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { RepositoryDto } from './repository-dto';

export type ModuleStatusDto = "Scanning" | "Scanned" | "Scan Error" | "Build Pending" | "Building" | "Build Error" | "Built";

export interface ModuleDto {
  id: string;
  name: string;
  version: string;
  description: string;
  status: ModuleStatusDto;
  source: Partial<RepositoryDto>
  creation_date: string;
  error: ModuleBuildErrorDto;
}

export interface ModuleBuildErrorDto {
  logs: string;
  code: number;
}
