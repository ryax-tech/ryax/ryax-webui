// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
export interface RepositoryDto {
  id: string;
  name: string;
  url: string;
  scan_result: ScanResultDto;
  username: string,
  password: string;
  password_is_set: boolean;
}

export interface ScanResultDto {
  bar: any;
}
