// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { RepositoryScanModuleDto } from './repository-scan-module-dto';

export interface SourceDetailsDto {
  id: string;
  name: string;
  url: string,
  last_scan: LastScanDto;
}

export interface LastScanDto {
  sha: string;
  built_modules: RepositoryScanModuleDto[];
  none_built_modules: RepositoryScanModuleDto[];
}
