// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
export interface RepositoryScanModuleDto {
  id: string;
  scan_errors: string[];
  description: string;
  duplicated: boolean;
  metadata_path: any;
  name: string;
  type: string;
  dynamic_outputs: boolean;
  status: string;
  version: string;
  kind: string;
  technical_name: string;
  sha: string;
}
