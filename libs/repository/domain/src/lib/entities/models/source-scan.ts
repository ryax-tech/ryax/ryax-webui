// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
export interface SourceScanRequest {
  username: string;
  password: string;
  branch: string;
}

export interface SourceScanResult {
  branch: string;
  sha: string;
  error: string;
}

