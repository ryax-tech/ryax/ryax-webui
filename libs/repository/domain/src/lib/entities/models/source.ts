// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
export interface Source {
  id: string;
  name: string;
  url: string;
  scanResult: ScanResult;
  username: string;
  password: string;
  isPasswordSet: boolean;
}

export interface SourceEditData {
  name: string,
  url: string,
  username: string;
  password: string;
}

export interface ScanResult {
  bar: any;
}

export enum SourceEditMode {
  NewSource ,
  EditSourceInfos ,
}
