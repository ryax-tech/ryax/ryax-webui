// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
export interface Module {
  id: string;
  name: string;
  version: string;
  description: string;
  status: ModuleStatus;
  repositoryId: string;
  creationDate: Date;
  error: ModuleBuildError;
}

export interface ModuleView {
  id: string;
  name: string;
  version: string;
  description: string;
  status: ModuleStatus;
  repositoryId: string;
  creationDate: Date;
  error: ModuleBuildError;
  repositoryName: string;
}

export interface ModuleBuildError {
  logs: string;
  code: ModuleBuildErrorCode;
}

export enum ModuleStatus{
    Scanning = "Scanning",
    Scanned = "Scanned",
    ScanError = "Scan Error",
    BuildPending = "Build Pending",
    Building = "Building",
    BuildError = "Build Error",
    Built = "Built"
}

export enum ModuleBuildErrorCode{
  IMAGE_NAME_UNDEFINED = 101,
  MODULE_TYPE_NOT_SUPPORTED = 102,
  CONNECTION_FAILED = 103,
  ERROR_WHILE_RUNNING = 104,
  ERROR_WHILE_REGISTERING_MODULE = 105,
}
