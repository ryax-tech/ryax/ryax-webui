// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
export enum SourceScanModuleBuildState {
  Ready = "Ready",
  Pending = "Pending",
  Triggered = "Triggered",
}

export interface SourceScanModule {
  id: string;
  errors: string[];
  description: string;
  duplicated: boolean;
  path: string;
  name: string;
  type: string;
  dynamicOutputs: boolean;
  status: string;
  version: string;
  kind: string;
  technicalName: string;
  buildState: SourceScanModuleBuildState;
  sha: string;
}

export interface SourceScanModuleView {
  id: string;
  name: string;
  version: string;
  description: string;
  errors: string[];
  duplicated: boolean;
  buildDisabled: boolean;
}
export type RepositorySortKey = "status" | "name";

export type RepositorySortValue = "ascend" | "descend" | null;

export interface RepositorySort {
  key: RepositorySortKey;
  value: RepositorySortValue;
}


