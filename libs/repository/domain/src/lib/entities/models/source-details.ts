// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { SourceScanModule } from './source-scan-module';

export interface SourceDetails {
  id: string;
  name: string;
  url: string,
  lastScan: LastScan;
}

export interface LastScan {
  sha: string;
  builtModules: SourceScanModule[];
  noneBuiltModules: SourceScanModule[];
}
