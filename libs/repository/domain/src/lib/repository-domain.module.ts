// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { RepositoryFeatureKey, RepositoryReducerProvider, RepositoryReducerToken } from './application/reducers';
import { EffectsModule } from '@ngrx/effects';
import {
  ModuleListEffects,
  SourceEditEffects,
  SourceListEffects,
  SourceScanEffects,
} from './application/effects';
import {
  ModuleListFacade,
  SourceEditFacade,
  SourceListFacade,
  SourceScanFacade,
} from './application/facades';
import { ModuleApi, SourceApi } from './infrastructure/services';
import {
  ModuleAdapter,
  SourceAdapter,
  SourceDetailsAdapter,
  SourceScanAdapter,
  SourceScanModuleAdapter,
} from './infrastructure/adapters';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature(RepositoryFeatureKey, RepositoryReducerToken),
    EffectsModule.forFeature([
      SourceListEffects,
      SourceEditEffects,
      SourceScanEffects,
      ModuleListEffects
    ])
  ],
  providers: [
    RepositoryReducerProvider,
    SourceListFacade,
    SourceEditFacade,
    SourceScanFacade,
    ModuleListFacade,
    SourceApi,
    ModuleApi,
    ModuleAdapter,
    SourceAdapter,
    SourceScanAdapter,
    SourceScanModuleAdapter,
    SourceDetailsAdapter
  ]
})
export class RepositoryDomainModule {}
