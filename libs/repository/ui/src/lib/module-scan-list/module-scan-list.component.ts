// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Module, RepositorySort, SourceScanModule, SourceScanModuleBuildState } from '@ryax/repository/domain';

@Component({
  selector: 'ryax-repository-module-scan-list',
  templateUrl: './module-scan-list.component.pug',
  styleUrls: ['./module-scan-list.component.scss']
})
export class ModuleScanListComponent {
  @Input() modules: SourceScanModule[];
  @Output() build = new EventEmitter<string>();
  @Output() onDuplicate = new EventEmitter<void>();
  @Output() scanError = new EventEmitter<SourceScanModule>();
  @Output() updateSort = new EventEmitter<RepositorySort>();

  trackModuleByMetadata(index: number, item: Module) {
    return item.id;
  }

  buildModule(id: string) {
    this.build.emit(id);
  }

  buildDisabled(item: SourceScanModule) {
    if (item && item.buildState)
      return item.buildState === SourceScanModuleBuildState.Pending
    || item.buildState === SourceScanModuleBuildState.Triggered;
  }

  isDuplicated(item: SourceScanModule) {
    if (item && item.duplicated)
      return item.duplicated && !this.hasErrors(item);
  }

  hasErrors(item: SourceScanModule) {
    if (item && item.errors)
      return item.errors.length;
  }

  openDuplicateModal() {
    this.onDuplicate.emit();
  }

  openScanErrorModal(module: SourceScanModule) {
    this.scanError.emit(module);
  }

  onSortChange(data: { key: string, value: string }) {
    this.updateSort.emit(data as RepositorySort);
  }
}
