// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Module } from '@ryax/repository/domain';
import { ModuleErrorMessagePipe } from '../pipes/module-error-message.pipe';


@Component({
  selector: 'ryax-module-error-modal',
  templateUrl: './module-error-modal.component.pug',
  styleUrls: ['./module-error-modal.component.scss']
})
export class ModuleErrorModalComponent {
  @Input() visible: boolean;
  @Input() module: Module;
  @Output() cancel = new EventEmitter<void>();

  constructor(
    private readonly pipe: ModuleErrorMessagePipe
  ){}

  handleCancel() {
    this.cancel.emit();
  }

  get displayError() {
    if(this.module){
      const error = this.module.error;
      if(error){
        const logs = error.logs;
        const code = this.pipe.transform(error.code);
        if(logs){
          return logs
        } else {
          return code
        }
      }
    }
  }
}
