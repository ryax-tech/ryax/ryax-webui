// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Pipe, PipeTransform } from '@angular/core';
import { ModuleBuildErrorCode } from '@ryax/repository/domain';

@Pipe({
  name: 'ryaxModuleErrorMessage',
  pure: true
})
export class ModuleErrorMessagePipe implements PipeTransform {

  private config = {
    [ModuleBuildErrorCode.IMAGE_NAME_UNDEFINED]: "Image name must be defined for the Container module type",
    [ModuleBuildErrorCode.MODULE_TYPE_NOT_SUPPORTED]: "Module type is not supported",
    [ModuleBuildErrorCode.CONNECTION_FAILED]: "The connection to internal registry failed",
    [ModuleBuildErrorCode.ERROR_WHILE_RUNNING]: "Error while running process",
    [ModuleBuildErrorCode.ERROR_WHILE_REGISTERING_MODULE]: "Error while registering the module in the docker registry",
  };

  transform(value: ModuleBuildErrorCode): string {
    if(value in this.config) {
      return this.config[value];
    } else {
      return `Unknown error (${value})`;
    }
  }
}
