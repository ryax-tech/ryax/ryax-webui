// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { SourceScanModule } from '@ryax/repository/domain';

@Component({
  selector: 'ryax-module-scan-error-modal',
  templateUrl: './module-scan-error-modal.component.pug',
  styleUrls: ['./module-scan-error-modal.component.scss']
})
export class ModuleScanErrorModalComponent {
  @Input() visible: boolean;
  @Input() module: SourceScanModule;
  @Output() cancel = new EventEmitter<void>();

  handleCancel() {
    this.cancel.emit();
  }
}
