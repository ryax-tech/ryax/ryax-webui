// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedUiCommonModule } from '@ryax/shared/ui-common';
import { ModuleDeleteDuplicateModalComponent } from './module-delete-duplicate-modal/module-delete-duplicate-modal.component';
import { ModuleScanErrorModalComponent } from './module-scan-error-modal/module-scan-error-modal.component';
import { ModuleErrorModalComponent } from './module-error-modal/module-error-modal.component';
import { ModuleErrorMessagePipe } from './pipes/module-error-message.pipe';
import { ModuleScanListComponent } from './module-scan-list/module-scan-list.component';
import { ModuleScanBranchModalComponent } from './module-scan-branch-modal/module-scan-branch-modal.component';

@NgModule({
  imports: [
    CommonModule,
    SharedUiCommonModule
  ],
  declarations: [
    ModuleDeleteDuplicateModalComponent,
    ModuleScanErrorModalComponent,
    ModuleErrorModalComponent,
    ModuleErrorMessagePipe,
    ModuleScanListComponent,
    ModuleScanBranchModalComponent
  ],
  exports: [
    ModuleDeleteDuplicateModalComponent,
    ModuleScanErrorModalComponent,
    ModuleErrorModalComponent,
    ModuleErrorMessagePipe,
    ModuleScanListComponent,
    ModuleScanBranchModalComponent
  ],
  providers: [
    ModuleErrorMessagePipe
  ]
})
export class RepositoryUiModule {}
