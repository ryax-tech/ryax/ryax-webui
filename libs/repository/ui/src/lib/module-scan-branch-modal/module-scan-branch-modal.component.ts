// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { SourceScanRequest } from '@ryax/repository/domain';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'ryax-module-scan-branch-modal',
  templateUrl: './module-scan-branch-modal.component.pug'
})
export class ModuleScanBranchModalComponent {
  form = this.formBuilder.group({
    branch: this.formBuilder.control(null)
  });

  @Input() visible: boolean;
  @Output() submitScan = new EventEmitter<SourceScanRequest>();
  @Output() close = new EventEmitter<void>();

  constructor(
    private readonly formBuilder: FormBuilder
  ) {}

  triggerScan() {
    const formData = this.form.value;
    this.submitScan.emit(formData);
  }

  closeModal() {
    this.close.emit();
  }
}
