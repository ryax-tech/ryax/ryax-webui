// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Source } from '@ryax/repository/domain';

@Component({
  selector: 'ryax-repository-scan-header',
  templateUrl: './repository-scan-header.component.pug'
})
export class RepositoryScanHeaderComponent {
  @Input() repository: Source;
  @Output() refresh = new EventEmitter<void>();
  @Output() edit = new EventEmitter<Source>();

  onRefresh() {
    this.refresh.emit()
  }

  onEdit(repository: Source) {
    this.edit.emit(repository)
  }
}
