// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { RepositorySort, SourceScanModule } from '@ryax/repository/domain';

@Component({
  selector: 'ryax-repository-scan-modules',
  templateUrl: './repository-scan-modules.component.pug'
})
export class RepositoryScanModulesComponent {
  @Input() modulesScanned: SourceScanModule[];
  @Output() build = new EventEmitter<string>();
  @Output() onDuplicate = new EventEmitter<void>();
  @Output() scanError = new EventEmitter<SourceScanModule>();
  @Output() updateSort = new EventEmitter<RepositorySort>();


  buildModule(id: string) {
    this.build.emit(id);
  }

  openDuplicateModal() {
    this.onDuplicate.emit();
  }

  openScanErrorModal(module: SourceScanModule) {
    this.scanError.emit(module);
  }

  onSortChange(data: { key: string, value: string }) {
    this.updateSort.emit(data as RepositorySort);
  }
}
