// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { SourceScanRequest, SourceScanResult } from '@ryax/repository/domain';

@Component({
  selector: 'ryax-repository-scan-form',
  templateUrl: './repository-scan-form.component.pug'
})
export class RepositoryScanFormComponent {
  form = this.formBuilder.group({
    branch: this.formBuilder.control(null)
  });

  @Input() set loading(value: boolean) {
    if(value) {
      this.form.disable();
    } else {
      this.form.enable()
    }
  }
  @Output() submitScan = new EventEmitter<SourceScanRequest>();

  constructor(
    private readonly formBuilder: FormBuilder
  ) {}

  onSubmit() {
    const formData = this.form.value;
    this.submitScan.emit(formData);
  }
}
