// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { RepositorySort, SourceScanModule } from '@ryax/repository/domain';

@Component({
  selector: 'ryax-repository-scan-built-modules',
  templateUrl: './repository-scan-built-modules.component.pug',
  styleUrls: ['./repository-scan-built-modules.component.scss']
})
export class RepositoryScanBuiltModulesComponent {
  @Input() modulesBuilt: SourceScanModule[];
  @Output() build = new EventEmitter<string>();
  @Output() duplicate = new EventEmitter<void>();
  @Output() scanError = new EventEmitter<SourceScanModule>();
  @Output() updateSort = new EventEmitter<RepositorySort>();


  buildModule(id: string) {
    this.build.emit(id);
  }

  onDuplicate() {
    this.duplicate.emit();
  }

  onScanError(module: SourceScanModule) {
    this.scanError.emit(module);
  }

  onSortChange(data: { key: string, value: string }) {
    this.updateSort.emit(data as RepositorySort);
  }
}
