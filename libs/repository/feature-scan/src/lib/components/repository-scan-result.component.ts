// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, EventEmitter, Input, Output, TemplateRef } from '@angular/core';
import {
  RepositorySort,
  SourceScanModule,
  SourceScanResult,
} from '@ryax/repository/domain';

@Component({
  selector: 'ryax-repository-scan-result',
  templateUrl: './repository-scan-result.component.pug'
})
export class RepositoryScanResultComponent {
  @Input() loading: boolean;
  @Input() lastScan: SourceScanModule[];
  @Input() lastScanSha: string;
  @Input() result: SourceScanResult;
  @Input() modulesTemplate: TemplateRef<void>;
  @Output() build = new EventEmitter<string>();
  @Output() duplicate = new EventEmitter<void>();
  @Output() scanError = new EventEmitter<SourceScanModule>();
  @Output() updateSort = new EventEmitter<RepositorySort>();
  @Output() openScanModal = new EventEmitter<void>();

  buildModule(id: string) {
    this.build.emit(id);
  }

  openDuplicateModal() {
    this.duplicate.emit();
  }

  openScanErrorModal(module: SourceScanModule) {
    this.scanError.emit(module);
  }

  onSortChange(data: { key: string, value: string }) {
    this.updateSort.emit(data as RepositorySort);
  }

  onNewScan() {
    this.openScanModal.emit()
  }
}
