// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedUiCommonModule } from '@ryax/shared/ui-common';
import { RepositoryDomainModule } from '@ryax/repository/domain';
import { RepositoryFeatureScanComponent } from './repository-feature-scan.component';
import { RepositoryScanHeaderComponent } from './components/repository-scan-header.component';
import { RepositoryScanFormComponent } from './components/repository-scan-form.component';
import { RepositoryScanResultComponent } from './components/repository-scan-result.component';
import { RepositoryScanModulesComponent } from './components/repository-scan-modules.component';
import { RepositoryUiModule } from '@ryax/repository/ui';
import { RepositoryFeatureEditModule } from '@ryax/repository/feature-edit';
import { RepositoryScanBuiltModulesComponent } from './components/repository-scan-built-modules/repository-scan-built-modules.component';

@NgModule({
  imports: [
    CommonModule,
    SharedUiCommonModule,
    RepositoryDomainModule,
    RepositoryUiModule,
    RepositoryFeatureEditModule,
  ],
  declarations: [
    RepositoryFeatureScanComponent,
    RepositoryScanHeaderComponent,
    RepositoryScanFormComponent,
    RepositoryScanResultComponent,
    RepositoryScanModulesComponent,
    RepositoryScanBuiltModulesComponent
  ],
  exports: [
    RepositoryFeatureScanComponent
  ]
})
export class RepositoryFeatureScanModule {}
