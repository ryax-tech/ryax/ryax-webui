// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, OnInit } from '@angular/core';
import { RepositorySort, Source, SourceScanFacade, SourceScanModule, SourceScanRequest } from '@ryax/repository/domain';

@Component({
  selector: 'ryax-repository-feature-scan',
  templateUrl: './repository-feature-scan.component.pug'
})
export class RepositoryFeatureScanComponent implements OnInit {
  source$ = this.facade.source$;
  sourceScanProcessing$ = this.facade.sourceScanProcessing$;
  sourceScanResult$ = this.facade.sourceScanResult$;
  sourceScanModules$ = this.facade.sourceScanModules$;
  lastScanModules$ = this.facade.lastScanModules$;
  lastScanSha$ = this.facade.lastScanSha$;
  builtModules$ = this.facade.builtModules$;
  deleteDuplicateModalVisible$ = this.facade.deleteDuplicateModalVisible$;
  scanErrorModalVisible$ = this.facade.scanErrorModalVisible$;
  selectedSourceScanModule$ = this.facade.selectedSourceScanModule$;
  scanModalBranchVisibility$ = this.facade.scanModalBranchVisibility$;

  constructor(
    private readonly facade: SourceScanFacade
  ) {}

  ngOnInit(): void {
    this.facade.init();
  }

  submitScan(data: SourceScanRequest) {
    this.facade.submit(data);
  }

  openScanModal() {
    this.facade.openScanModal()
  }

  closeScanModal() {
    this.facade.closeScanModal()
  }

  onBuild(moduleId: string) {
    this.facade.build(moduleId);
  }

  openDuplicateModal() {
    this.facade.openDuplicateModal();
  }

  openScanErrorModal(module: SourceScanModule) {
    this.facade.openScanErrorModal(module);
  }

  cancelScanErrorModal() {
    this.facade.closeScanErrorModal();
  }

  validateModuleDeleteDuplicate() {
    this.facade.closeDuplicateModal();
  }

  cancelModuleDeleteDuplicate() {
    this.facade.closeDuplicateModal();
  }

  refresh() {
    this.facade.refresh();
  }

  editRepository(repository: Source) {
    this.facade.editRepository(repository);
  }

  onUpdateSort(data: RepositorySort) {
    this.facade.updateSort(data);
  }
}
