// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedUiCommonModule } from '@ryax/shared/ui-common';
import { RepositoryFeatureEditComponent } from './repository-feature-edit.component';
import { RepositoryEditFormComponent } from './repository-edit-form/repository-edit-form.component';
import { RepositoryEditTitlePipe } from './pipes/repository-edit-title.pipe';

@NgModule({
  imports: [
    CommonModule,
    SharedUiCommonModule
  ],
  declarations: [
    RepositoryFeatureEditComponent,
    RepositoryEditFormComponent,
    RepositoryEditTitlePipe,
  ],
  exports: [
    RepositoryFeatureEditComponent
  ],
})
export class RepositoryFeatureEditModule {}
