// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Pipe, PipeTransform } from '@angular/core';
import { SourceEditMode } from '@ryax/repository/domain';

@Pipe({
  name: "ryaxRepositoryEditTitle",
  pure: true
})
export class RepositoryEditTitlePipe implements PipeTransform {
  transform(value: SourceEditMode): string {
    switch (value) {
      case SourceEditMode.EditSourceInfos:
        return "Edit repository informations";
      case SourceEditMode.NewSource:
        return "New repository";
    }
  }
}
