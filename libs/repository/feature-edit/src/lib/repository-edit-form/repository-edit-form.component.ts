// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Source, SourceEditData, SourceEditMode } from '@ryax/repository/domain';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'ryax-repository-edit-form',
  templateUrl: './repository-edit-form.component.pug',
  styleUrls: ['./repository-edit-form.component.scss']
})
export class RepositoryEditFormComponent {
  private _mode: SourceEditMode;
  private _repository: Source;

  form = this.formBuilder.group({});
  nameControl = this.formBuilder.control(null, Validators.required);
  urlControl = this.formBuilder.control(null, Validators.required);
  usernameControl = this.formBuilder.control(null);
  passwordControl = this.formBuilder.control(null);

  @Input() set mode(value: SourceEditMode) {
    this._mode = value;
    this.setInfosForm();
    this.initForm(null);
  }
  @Input() set repository(value: Source) {
    this._repository = value;
    this.initForm(value);
  }
  @Output() save = new EventEmitter<SourceEditData>();

  constructor(
    private readonly formBuilder: FormBuilder
  ) {}

  get isPasswordSet() {
    if(this._repository)
      return this._repository.isPasswordSet;
  }

  private setInfosForm() {
    this.form.addControl("name", this.nameControl);
    this.form.addControl("url", this.urlControl);
    this.form.addControl("username", this.usernameControl);
    this.form.addControl("password", this.passwordControl);
  }

  private initForm(repository: Source) {
    if(repository) {
      const { id, ...formValue } = repository;
      if(this.isPasswordSet)
        this.form.patchValue({name: repository.name, url: repository.url, username: repository.username, password: "••••••••"});
      if(!this.isPasswordSet)
        this.form.patchValue(formValue);
    } else {
      this.form.reset();
    }
  }

  onSubmit() {
    const formValue = this.form.value;
    if(this.isPasswordSet && formValue.password === "••••••••") {
      this.save.emit({name: formValue.name, url: formValue.url, username: formValue.username, password: undefined});
    } else {
      this.save.emit(formValue);
    }
  }
}

