// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component } from '@angular/core';
import { SourceEditData, SourceEditFacade } from '@ryax/repository/domain';

@Component({
  selector: 'ryax-repository-feature-edit',
  templateUrl: './repository-feature-edit.component.pug'
})
export class RepositoryFeatureEditComponent {
  visible$ = this.facade.visible$;
  mode$ = this.facade.mode$;
  repository$ = this.facade.source$;

  constructor(
    private readonly facade: SourceEditFacade,
  ) {}

  onSave(value: SourceEditData) {
    this.facade.save(value);
  }

  onClose() {
    this.facade.close();
  }
}
