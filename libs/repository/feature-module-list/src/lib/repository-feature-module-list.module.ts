// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedUiCommonModule } from '@ryax/shared/ui-common';
import { RepositoryDomainModule } from '@ryax/repository/domain';
import { RepositoryFeatureModuleListComponent } from './repository-module-list/repository-feature-module-list.component';
import { RepositoryModuleListHeaderComponent } from './repository-module-list-header/repository-module-list-header.component';
import { RepositoryModuleListTableComponent } from './repository-module-list-table/repository-module-list-table.component';
import { RepositoryModuleBuildStatusColorPipe } from './pipes/repository-module-build-status-color.pipe';
import { RepositoryUiModule } from '@ryax/repository/ui';

@NgModule({
  imports: [
    CommonModule,
    SharedUiCommonModule,
    RepositoryDomainModule,
    RepositoryUiModule,
  ],
  declarations: [
    RepositoryFeatureModuleListComponent,
    RepositoryModuleListHeaderComponent,
    RepositoryModuleListTableComponent,
    RepositoryModuleBuildStatusColorPipe
  ],
  exports: [
    RepositoryFeatureModuleListComponent
  ]
})
export class RepositoryFeatureModuleListModule {}
