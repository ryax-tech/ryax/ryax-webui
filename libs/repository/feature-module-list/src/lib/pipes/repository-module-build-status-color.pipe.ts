// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Pipe, PipeTransform } from '@angular/core';
import { ModuleStatus } from '@ryax/repository/domain';

@Pipe({
  name: "ryaxRepositoryModuleBuildStatusColor",
  pure: true
})
export class RepositoryModuleBuildStatusColorPipe implements PipeTransform {
  private config = {
    [ModuleStatus.Scanned]: null,
    [ModuleStatus.BuildPending]: "orange",
    [ModuleStatus.Building]: "blue",
    [ModuleStatus.Built]: "green",
    [ModuleStatus.Scanned]: "green",
    [ModuleStatus.ScanError]: "red",
    [ModuleStatus.BuildError]: "red",
  };

  transform(value: ModuleStatus): string {
    const color = this.config[value];
    return color ? color : null;
  }
}
