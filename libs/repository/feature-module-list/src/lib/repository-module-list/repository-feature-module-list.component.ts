// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, OnInit } from '@angular/core';
import { Module, ModuleListFacade } from '@ryax/repository/domain';

@Component({
  selector: 'ryax-repository-feature-module-list',
  templateUrl: './repository-feature-module-list.component.pug'
})
export class RepositoryFeatureModuleListComponent implements OnInit{
  loading$ = this.facade.loading$;
  modules$ = this.facade.modules$;
  errorModalVisible$ = this.facade.errorModalVisible$;
  selectedModule$ = this.facade.selectedModule$;

  constructor(
    private readonly facade: ModuleListFacade
  ) {}

  ngOnInit(): void {
    this.facade.init();
  }

  onRefresh() {
    this.facade.refresh();
  }

  onBuild(id: string) {
    this.facade.build(id);
  }

  onDelete(id: string) {
    this.facade.deleteModule(id);
  }

  onModuleError(module: Module) {
    this.facade.openErrorModal(module);
  }

  cancelErrorModal() {
    this.facade.closeErrorModal();
  }
}
