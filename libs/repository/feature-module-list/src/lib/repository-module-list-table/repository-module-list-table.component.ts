// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Module, ModuleStatus, ModuleView, SourceScanModule } from '@ryax/repository/domain';

@Component({
  selector: 'ryax-repository-module-list-table',
  templateUrl: './repository-module-list-table.component.pug'
})
export class RepositoryModuleListTableComponent {
  @Input() loading: boolean;
  @Input() items: ModuleView[];
  @Output() build = new EventEmitter<string>();
  @Output() onDelete = new EventEmitter<string>();
  @Output() moduleError = new EventEmitter<ModuleView>();

  trackIByItemId(index: number, item: ModuleView) {
    return item.id;
  }

  isSuccess(item: Module) {
    return item.status === ModuleStatus.Built;
  }

  buildEnabled(item: Module) {
    return item.status === ModuleStatus.BuildError
      || item.status === ModuleStatus.Scanned;
  }

  deleteEnabled(item: Module) {
    return item.status === ModuleStatus.BuildPending
      || item.status === ModuleStatus.BuildError
      || item.status === ModuleStatus.Built
      || item.status === ModuleStatus.ScanError;
  }

  buildModule(id: string) {
    this.build.emit(id)
  }

  deleteModule(id: string) {
    this.onDelete.emit(id)
  }

  openErrorModal(module: ModuleView) {
    this.moduleError.emit(module);
  }

  hasErrors(item: ModuleView) {
    if(item.error)
      return item.error;
  }
}
