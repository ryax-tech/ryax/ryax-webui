// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Source } from '@ryax/repository/domain';

@Component({
  selector: 'ryax-repository-list-table',
  templateUrl: './repository-list-table.component.pug',
  styleUrls: ['./repository-list-table.component.scss'],
})
export class RepositoryListTableComponent {
  @Input() loading: boolean;
  @Input() items: Source[];
  @Output() editInfos = new EventEmitter<Source>();
  @Output() delete = new EventEmitter<Source>();

  isNull(source: Source) {
    if(source && source.scanResult === null) {
      return true;
    }
  }

  hasBuilt(source: Source) {
    if(source && source.scanResult && source.scanResult.bar) {
      return source.scanResult.bar.modules_success;
    }
  }

  hasBuilding(source: Source) {
    if(source && source.scanResult && source.scanResult.bar) {
      return source.scanResult.bar.modules_in_progress;
    }
  }

  hasError(source: Source) {
    if(source && source.scanResult && source.scanResult.bar) {
      return source.scanResult.bar.modules_error;
    }
  }

  hasNonBuilt(source: Source) {
    if(source && source.scanResult && source.scanResult.bar) {
      return source.scanResult.bar.modules_ready;
    }
  }

  calculateProgressWidth(source: Source, number: number) {
    if(source && source.scanResult && source.scanResult.bar) {
      const bar = source.scanResult.bar;
      const sumValues = obj => Object.values(obj).reduce((a: number, b: number) => a + b);
      const modulesTotal: any = sumValues(bar);

      const pixel = (((number*100)/modulesTotal)*240)/100;
      return `width: ${pixel}px`;
    }
  }

  tooltipText(source: Source) {
    if(source && source.scanResult && source.scanResult.bar) {
      const built = source.scanResult.bar.modules_success;
      const building = source.scanResult.bar.modules_in_progress;
      const error = source.scanResult.bar.modules_error;
      const nonBuilt = source.scanResult.bar.modules_ready;
      return `${built} built / ${building} building / ${error} error / ${nonBuilt} non built`;
    }
  }

  trackIByItemId(index: number, item: Source) {
    return item.id;
  }

  onEditInfos(item: Source) {
    this.editInfos.emit(item);
  }

  onDelete(item: Source) {
    this.delete.emit(item);
  }
}
