// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, OnInit } from '@angular/core';
import { Source, SourceListFacade } from '@ryax/repository/domain';

@Component({
  selector: 'ryax-repository-feature-list',
  templateUrl: './repository-feature-list.component.pug'
})
export class RepositoryFeatureListComponent implements OnInit {
  loading$ = this.facade.loading$;
  repositories$ = this.facade.sources$;

  constructor(
    private readonly facade: SourceListFacade
  ) {}

  ngOnInit(): void {
    this.facade.init();
  }

  onRefresh() {
    this.facade.refresh();
  }

  onAddRepository() {
    this.facade.addSource();
  }

  onEditRepositoryInfos(repository: Source) {
    this.facade.editSourceInfos(repository);
  }

  onDeleteRepository(repository: Source) {
    this.facade.deleteSource(repository);
  }
}
