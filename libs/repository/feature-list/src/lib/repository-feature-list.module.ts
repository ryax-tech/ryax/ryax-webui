// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedUiCommonModule } from '@ryax/shared/ui-common';
import { RepositoryDomainModule } from '@ryax/repository/domain';
import { RepositoryFeatureListComponent } from './repository-feature-list.component';
import { RepositoryListHeaderComponent } from './repository-list-header/repository-list-header.component';
import { RepositoryListTableComponent } from './repository-list-table/repository-list-table.component';
import { RepositoryFeatureEditModule } from '@ryax/repository/feature-edit';

@NgModule({
  imports: [
    CommonModule,
    SharedUiCommonModule,
    RepositoryDomainModule,
    RepositoryFeatureEditModule
  ],
  declarations: [
    RepositoryFeatureListComponent,
    RepositoryListHeaderComponent,
    RepositoryListTableComponent,
  ],
  exports: [
    RepositoryFeatureListComponent
  ]
})
export class RepositoryFeatureListModule {}
