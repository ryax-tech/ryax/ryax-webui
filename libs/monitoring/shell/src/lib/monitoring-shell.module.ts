// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MonitoringFeatureListModule } from '@ryax/monitoring/feature-list';
import { MonitoringFeatureDetailsModule } from '@ryax/monitoring/feature-details';
import { routes } from './monitoring-shell.routes';

@NgModule({
  imports: [
    CommonModule,
    MonitoringFeatureListModule,
    MonitoringFeatureDetailsModule,
    RouterModule.forChild(routes)
  ],
})
export class MonitoringShellModule {}
