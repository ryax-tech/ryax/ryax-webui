// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { MonitoringListComponent } from '@ryax/monitoring/feature-list';
import { CanAccessGuard, MonitoringDetailsComponent } from '@ryax/monitoring/feature-details';

export const routes = [
  {
    path: '',
    component: MonitoringListComponent
  },
  {
    path: ':executionId',
    component: MonitoringDetailsComponent,
    canActivate: [
      CanAccessGuard
    ],
    data: {
      breadcrumb: "Details"
    }
  },
];
