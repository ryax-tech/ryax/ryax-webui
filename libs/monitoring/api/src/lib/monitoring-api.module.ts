// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { NgModule } from '@angular/core';
import { MonitoringFeaturePortalModule, MonitoringPortalComponent } from '@ryax/monitoring/feature-portal';
import { MonitoringProcessingComponent } from '../../../feature-portal/src/lib/monitoring-processing/monitoring-processing.component';

@NgModule({
  imports: [
    MonitoringFeaturePortalModule
  ],
  exports: [
    MonitoringPortalComponent,
    MonitoringProcessingComponent
  ]
})
export class MonitoringApiModule {}
