// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Observable, Subject } from 'rxjs';
import * as d3 from 'd3';
import * as moment from 'moment';
import { ResizeObserver } from 'resize-observer';
import {
  AfterViewInit,
  Component,
  ElementRef,
  Input,
  OnChanges,
  OnDestroy,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import { Execution, ExecutionState } from '@ryax/monitoring/domain';


export interface TimelineItem {
  label: string;
  startDate: moment.Moment
  endDate: moment.Moment;
  barFillColor: string;
  barStrokeColor: string;
}

@Component({
  selector: 'ryax-monitoring-details-timeline',
  templateUrl: './monitoring-details-timeline.component.pug',
  styleUrls: ['./monitoring-details-timeline.component.scss']
})
export class MonitoringDetailsTimelineComponent implements AfterViewInit, OnChanges, OnDestroy {
  @Input() execution: Execution;
  @Input() previousExecutions: Execution[];
  @Input() nextExecutions: Execution[];
  @ViewChild('chart', { static: true }) chartContainer: ElementRef;

  private items: TimelineItem[] = [];
  private destroy$ = new Subject();
  private width: number;
  private height: number;
  private margin = { top: 0, bottom: 20, left: 80, right: 30 };
  private chart: any;
  private xScale: d3.ScaleTime<any, any>;
  private yScale: d3.ScaleBand<string>;
  private xAxis: any;
  private xAxisElement: any;
  private yAxis: any;
  private yAxisElement: any;

  static getExecutionStatePastelColor(state: ExecutionState) {
    switch(state) {
      case ExecutionState.Running:
        return '#e6f7ff';
      case ExecutionState.Done:
        return '#f6ffed';
      case ExecutionState.Error:
        return '#fff1f0';
      default:
        return '#fafafa';
    }
  };

  static getExecutionStateLightColor(state: ExecutionState) {
    switch (state) {
      case ExecutionState.Running:
        return '#91d5ff';
      case ExecutionState.Done:
        return '#b7eb8f';
      case ExecutionState.Error:
        return '#ffa39e';
      default:
        return '#d9d9d9';
    }
  };

  static getTimelineItem(execution: Execution): TimelineItem {
    return {
      label: execution.moduleInstanceId,
      startDate: moment(execution.submittedAt),
      endDate: execution.endedAt ? moment(execution.endedAt) : undefined,
      barFillColor: this.getExecutionStatePastelColor(execution.state),
      barStrokeColor: this.getExecutionStateLightColor(execution.state),
    };
  }

  ngAfterViewInit(): void {
    this.createChart();
    this.updateChart();
    this.observeChartSize()
      .subscribe(() => this.resizeChart());
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes.execution && changes.execution.currentValue && this.chart) {
      this.updateItems();
      this.updateChart();
    }

    if(changes.previousExecutions && changes.previousExecutions.currentValue && this.chart) {
      this.updateItems();
      this.updateChart();
    }

    if(changes.nextExecutions && changes.nextExecutions.currentValue && this.chart) {
      this.updateItems();
      this.updateChart();
    }
  }

  ngOnDestroy(): void {
    this.destroy$.complete();
  }

  private updateItems() {
    this.items = [
      ...this.previousExecutions.map(item => MonitoringDetailsTimelineComponent.getTimelineItem(item)),
      MonitoringDetailsTimelineComponent.getTimelineItem(this.execution),
      ...this.nextExecutions.map(item => MonitoringDetailsTimelineComponent.getTimelineItem(item)),
    ];
  }

  private observeChartSize() {
    const element = this.chartContainer.nativeElement;
    return new Observable(function(observer) {
      const resizeObserver = new ResizeObserver(() => observer.next(null));
      resizeObserver.observe(element);
      return () => resizeObserver.disconnect()
    });
  };

  private createChart() {
    const element = this.chartContainer.nativeElement;
    this.width = element.offsetWidth - this.margin.left - this.margin.right;
    this.height = element.offsetHeight - this.margin.top - this.margin.bottom;
    const svg = d3.select(element).append('svg')
      .attr('width', element.offsetWidth)
      .attr('height', element.offsetHeight);

    // Chart plot area
    this.chart = svg.append('g')
      .attr('class', 'bars')
      .attr('transform', `translate(${this.margin.left}, ${this.margin.top})`);

    // Domains
    const xDomain = [0, Date.now()];
    const yDomain = [];

    // Scales
    this.xScale = d3.scaleTime()
      .domain(xDomain)
      .range([0, this.width]);

    this.yScale = d3.scaleBand()
      .padding(0.1)
      .domain(yDomain)
      .rangeRound([0, this.height]);

    // Axis elements
    this.xAxis = d3.axisBottom(this.xScale)
      .tickFormat((tick: Date) => moment.utc(tick).format('HH:mm:ss'));

    this.yAxis = d3.axisLeft(this.yScale);

    // Axis elements
    this.xAxisElement = svg.append('g')
      .attr('class', 'axis axis-x')
      .attr('transform', `translate(${this.margin.left}, ${this.margin.top + this.height})`)
      .call(this.xAxis);

    this.yAxisElement = svg.append('g')
      .attr('class', 'axis axis-y')
      .attr('transform', `translate(${this.margin.left}, ${this.margin.top})`)
      .call(this.yAxis);
  }

  private updateChart() {
    this.xScale.domain([d3.min(this.items, data => data.startDate), d3.max(this.items, data => data.endDate)]);
    this.yScale.domain(this.items.map(item => item.label));
    this.xAxisElement.call(this.xAxis);
    this.yAxisElement.call(this.yAxis);

    const update = this.chart.selectAll('.bar')
      .data(this.items);

    // Remove existing bars
    update.exit().remove();

    // Update Existing bars
    this.chart.selectAll('.bar')
      .attr('x', (data: TimelineItem) => this.xScale(data.startDate))
      .attr('y', (data: TimelineItem) => this.yScale(data.label))
      .attr('height', this.yScale.bandwidth())
      .attr('fill', (data: TimelineItem) => data.barFillColor)
      .attr('stroke', (data: TimelineItem) => data.barStrokeColor)
      .attr('width', (data: TimelineItem) => this.xScale(data.endDate) - this.xScale(data.startDate));

    update
      .enter()
      .append('rect')
      .attr('class', 'bar')
      .attr('x', (data: TimelineItem) => this.xScale(data.startDate))
      .attr('y', (data: TimelineItem) => this.yScale(data.label))
      .attr("rx", 5)
      .attr("ry", 5)
      .attr('height', this.yScale.bandwidth())
      .attr('width', (data: TimelineItem) => this.xScale(data.endDate) - this.xScale(data.startDate))
      .attr('fill', (data: TimelineItem) => data.barFillColor)
      .attr('stroke', (data: TimelineItem) => data.barStrokeColor)
  }

  private resizeChart() {
    const element = this.chartContainer.nativeElement;
    this.width = element.offsetWidth - this.margin.left - this.margin.right;
    this.height = element.offsetHeight - this.margin.top - this.margin.bottom;

    d3.select(element).select('svg')
      .attr('width', element.offsetWidth)
      .attr('height', element.offsetHeight);

    // Update scales
    this.xScale.range([0, this.width]);
    this.yScale.rangeRound([0, this.height]);

    // Update chart
    this.updateChart();
  }
}
