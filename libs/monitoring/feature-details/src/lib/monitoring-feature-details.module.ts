// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedUiCommonModule } from '@ryax/shared/ui-common';
import { CanAccessGuard } from './guards/can-access.guard';
import { MonitoringDetailsComponent } from './monitoring-details/monitoring-details.component';
import { MonitoringDetailsHeaderComponent } from './monitoring-details-header/monitoring-details-header.component';
import { MonitoringDetailsLogsComponent } from './monitoring-details-logs/monitoring-details-logs.component';
import { MonitoringDetailsTimelineComponent } from './monitoring-details-timeline/monitoring-details-timeline.component';
import { MonitoringDetailsInformationsComponent } from './monitoring-details-informations/monitoring-details-informations.component';
import { MonitoringDetailsTableComponent } from './monitoring-details-table/monitoring-details-table.component';
import { MonitoringDetailsTableRowComponent } from './monitoring-details-table-row/monitoring-details-table-row.component';
import { MonitoringDetailsInputsComponent } from './monitoring-details-inputs/monitoring-details-inputs.component';
import { MonitoringDetailsOutputsComponent } from './monitoring-details-outputs/monitoring-details-outputs.component';
import { MonitoringUiModule } from '@ryax/monitoring/ui';

@NgModule({
  imports: [
    CommonModule,
    SharedUiCommonModule,
    MonitoringUiModule,
  ],
  providers: [
    CanAccessGuard
  ],
  declarations: [
    MonitoringDetailsComponent,
    MonitoringDetailsHeaderComponent,
    MonitoringDetailsLogsComponent,
    MonitoringDetailsTimelineComponent,
    MonitoringDetailsInformationsComponent,
    MonitoringDetailsTableComponent,
    MonitoringDetailsTableRowComponent,
    MonitoringDetailsInputsComponent,
    MonitoringDetailsOutputsComponent
  ],
  exports: [
    MonitoringDetailsComponent
  ]
})
export class MonitoringFeatureDetailsModule {}
