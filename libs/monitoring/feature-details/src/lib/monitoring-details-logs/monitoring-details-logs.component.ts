// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, Input } from '@angular/core';
import { Execution } from '@ryax/monitoring/domain';

@Component({
  selector: 'ryax-monitoring-details-logs',
  templateUrl: './monitoring-details-logs.component.pug'
})
export class MonitoringDetailsLogsComponent {
  bodyStyle = {
    "white-space": "pre-wrap",
    "font-size": "12px",
    "font-family": "monospace",
    "background-color": "black",
    "color": "white"
  };

  @Input() loading: boolean;
  @Input() execution: Execution;
}
