// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, Input } from '@angular/core';
import { Execution, Module, Workflow } from '@ryax/monitoring/domain';

@Component({
  selector: 'ryax-monitoring-details-informations',
  templateUrl: './monitoring-details-informations.component.pug'
})
export class MonitoringDetailsInformationsComponent {
  @Input() execution: Execution;
  @Input() workflow: Workflow;
  @Input() module: Module;

  get executionDuration() {
    return this.execution.endedAt - this.execution.startedAt;
  }
}
