// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Execution } from '@ryax/monitoring/domain';

@Component({
  selector: 'ryax-monitoring-details-outputs',
  templateUrl: 'monitoring-details-outputs.component.pug',
  styleUrls: ['monitoring-details-outputs.component.scss'],
})

export class MonitoringDetailsOutputsComponent {
  @Input() execution: Execution;
  @Input() passwordVisibility: boolean;
  @Output() showModal = new EventEmitter<void>();
  @Output() toggleVisibility = new EventEmitter<boolean>();

  get isVisible() {
    return Object.keys(this.execution.outputData).length > 0;
  }

  get displayIcon() {
    if(this.passwordVisibility) {
      return "eye-invisible"
    } else {
      return "eye"
    }
  }

  onShowModal(): void {
    this.showModal.emit();
  }

  isFile(data: string) {
    return data === 'file'
  }

  isDirectory(data: string) {
    return data === 'directory'
  }

  isPassword(data: string) {
    return data === 'password'
  }

  togglePasswordVisibility() {
    const visibility =  !this.passwordVisibility;
    this.toggleVisibility.emit(visibility)
  }
}
