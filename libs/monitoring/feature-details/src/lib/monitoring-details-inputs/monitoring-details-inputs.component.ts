// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Execution, ExecutionInputData } from '@ryax/monitoring/domain';

@Component({
  selector: 'ryax-monitoring-details-inputs',
  templateUrl: 'monitoring-details-inputs.component.pug'
})

export class MonitoringDetailsInputsComponent {
  @Input() execution: Execution;
  @Input() passwordVisibility: boolean;
  @Output() showModal = new EventEmitter<void>();
  @Output() toggleVisibility = new EventEmitter<boolean>();

  get displayIcon() {
    if(this.passwordVisibility) {
      return "eye-invisible"
    } else {
      return "eye"
    }
  }

  onShowModal(): void {
    this.showModal.emit();
  }

  isFile(data: string) {
    return data === 'file'
  }

  isDirectory(data: string) {
    return data === 'directory'
  }

  isPassword(data: string) {
    return data === 'password'
  }

  togglePasswordVisibility() {
    const visibility =  !this.passwordVisibility;
    this.toggleVisibility.emit(visibility)
  }

  checkInputData(input: ExecutionInputData) {
    if(Object.keys(input).length === 0 && input.constructor === Object){
      return false
    } else {
      return true
    }
  }
}
