// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component } from '@angular/core';
import { MonitoringDetailsFacade } from '@ryax/monitoring/domain';

@Component({
  selector: 'ryax-monitoring-details',
  templateUrl: './monitoring-details.component.pug'
})
export class MonitoringDetailsComponent {
  loading$ = this.facade.loading$;
  execution$ = this.facade.execution$;
  previousExecutions$ = this.facade.previousExecutions$;
  nextExecutions$ = this.facade.nextExecutions$;
  inputPasswordVisibility$ = this.facade.inputPasswordVisibility$;
  outputPasswordVisibility$ = this.facade.outputPasswordVisibility$;

  isVisible = false;

  constructor(
    private readonly facade: MonitoringDetailsFacade
  ) {}

  getWorkflowById$(workflowId: string) {
    return this.facade.getWorkflowById$(workflowId);
  }

  getModuleById$(moduleId: string) {
    return this.facade.getModuleById$(moduleId);
  }

  showModal(): void {
    this.isVisible = true;
  }

  handleOk(): void {
    this.isVisible = false;
  }

  handleCancel(): void {
    this.isVisible = false;
  }

  toggleInputVisibility(visibility: boolean): void {
    this.facade.toggleInputVisibility(visibility);
  }

  toggleOutputVisibility(visibility: boolean): void {
    this.facade.toggleOutputVisibility(visibility);
  }
}
