// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate } from '@angular/router';
import { MonitoringDetailsFacade } from '@ryax/monitoring/domain';
import { first, map } from 'rxjs/operators';

@Injectable()
export class CanAccessGuard implements CanActivate {

  constructor(
    private readonly facade: MonitoringDetailsFacade
  ) {}

  canActivate(route: ActivatedRouteSnapshot) {
    this.facade.init();
    return this.facade.execution$.pipe(
      map(execution => !!execution),
      first(canAccess => !!canAccess)
    );
  }
}
