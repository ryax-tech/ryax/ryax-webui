// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Pipe, PipeTransform } from '@angular/core';
import { ExecutionState } from '@ryax/monitoring/domain';

@Pipe({
  name: 'ryaxExecutionStateColor',
  pure: true
})
export class ExecutionStateColorPipe implements PipeTransform {
  private config = {
    [ExecutionState.Submitted]: "blue",
    [ExecutionState.Running]: "geekblue",
    [ExecutionState.Ran]: "lime",
    [ExecutionState.Done]: "green",
    [ExecutionState.Error]: "red",
    [ExecutionState.Created]: "gold",
    [ExecutionState.Internal_Error]: "volcano",
  };

  transform(value: ExecutionState): string {
    if(value in this.config) {
      return this.config[value];
    } else {
      return "Unknown";
    }
  }
}
