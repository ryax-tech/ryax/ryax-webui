// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Pipe, PipeTransform } from '@angular/core';
import { ExecutionState } from '@ryax/monitoring/domain';

@Pipe({
  name: 'ryaxExecutionStateLabel',
  pure: true
})
export class ExecutionStateLabelPipe implements PipeTransform {
  private config = {
    [ExecutionState.Submitted]: "Submitted",
    [ExecutionState.Running]: "Running",
    [ExecutionState.Ran]: "Ran",
    [ExecutionState.Done]: "Done",
    [ExecutionState.Error]: "Error",
    [ExecutionState.Created]: "Created",
    [ExecutionState.Internal_Error]: "Internal Error",
  };

  transform(value: ExecutionState): string {
    if(value in this.config) {
      return this.config[value];
    } else {
      return null;
    }
  }
}
