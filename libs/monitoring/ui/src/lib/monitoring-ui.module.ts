// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedUiCommonModule } from '@ryax/shared/ui-common';
import { ExecutionStateColorPipe } from './pipes/execution-state-color.pipe';
import { ExecutionStateLabelPipe } from './pipes/execution-state-label.pipe';

@NgModule({
  imports: [
    CommonModule,
    SharedUiCommonModule
  ],
  declarations: [
    ExecutionStateColorPipe,
    ExecutionStateLabelPipe
  ],
  exports: [
    SharedUiCommonModule,
    ExecutionStateColorPipe,
    ExecutionStateLabelPipe
  ]
})
export class MonitoringUiModule {}
