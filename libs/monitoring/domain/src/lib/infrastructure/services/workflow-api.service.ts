// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { WorkflowDetailsDto, WorkflowDto } from '../dtos';
import { ModuleAdapter, WorkflowAdapter } from '../adapters';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class WorkflowApiService {
  constructor(
    private readonly http: HttpClient,
    private readonly workflowAdapter: WorkflowAdapter,
    private readonly moduleAdapter: ModuleAdapter
  ) {}

  loadAll() {
    const url = `/api/studio/workflows`;
    return this.http.get<WorkflowDto[]>(url).pipe(
      map(dtos => dtos.map(item => this.workflowAdapter.adapt(item)))
    );
  }

  loadOne(workflowId: string) {
    const url = `/api/studio/workflows/${workflowId}`;
    return this.http.get<WorkflowDetailsDto>(url).pipe(
      map((dto) => ({
        workflow: this.workflowAdapter.adapt(dto),
        modules: dto.modules.map(item => this.moduleAdapter.adapt(item))
      })
    ));
  }
}
