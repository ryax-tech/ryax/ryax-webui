// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Api } from '@ryax/shared/util-api';
import { ExecutionDto, ExecutionTreeDto } from '../dtos';
import { ExecutionSortKey, ExecutionSortValue, ExecutionState } from '../../entities';
import { ExecutionAdapter, ExecutionTreeAdapter } from '../adapters';

@Injectable()
export class ExecutionApiService {
  constructor(
    private readonly api: Api,
    private readonly executionAdapter: ExecutionAdapter,
    private readonly executionTreeAdapter: ExecutionTreeAdapter
  ) {}

  private buildSortParam(sortKey: ExecutionSortKey) {
    switch(sortKey) {
      case "submittedAt":
        return "submitted_at";
      case 'endedAt':
        return "ended_at"
    }
  };

  private buildStateParam(state: ExecutionState) {
    switch(state) {
      case ExecutionState.Submitted:
        return 'SUBMITTED';
      case ExecutionState.Running:
        return 'RUNNING';
      case ExecutionState.Ran:
        return 'RAN';
      case ExecutionState.Done:
        return "DONE";
      case ExecutionState.Error:
        return 'ERROR';
      case ExecutionState.Created:
        return 'CREATED';
      case ExecutionState.Internal_Error:
        return 'INTERNAL_ERROR';
    }
  };

  private buildParams(startIndex: number, endIndex: number, sortKey: ExecutionSortKey, sortValue: ExecutionSortValue, workflowFilter: string, moduleFilter: string, stateFilter: ExecutionState) {
    return {
      range: (startIndex !== null && endIndex !== null) ? `${startIndex}-${endIndex}` : undefined,
      asc_sort_by: sortValue === "ascend" ? (this.buildSortParam(sortKey) || undefined) : undefined,
      desc_sort_by: sortValue === "descend" ? (this.buildSortParam(sortKey) || undefined) : undefined,
      workflow_deployment_id: workflowFilter ? workflowFilter : undefined,
      function_instance_id: moduleFilter ? moduleFilter : undefined,
      state: stateFilter ? this.buildStateParam(stateFilter) : undefined
    };
  }

  loadAll(startIndex: number, endIndex: number, sortKey: ExecutionSortKey, sortValue: ExecutionSortValue,
          workflowFilter: string, moduleFilter: string, stateFilter: string) {
    const url = `/executions`, params = this.buildParams(startIndex, endIndex, sortKey, sortValue,
      workflowFilter, moduleFilter, ExecutionState[stateFilter]);
    return this.api.getWithResponse<ExecutionDto[]>(url, params).pipe(
      map(response => ({
        items: response.body.map(item => this.executionAdapter.adapt(item)),
        ...this.api.parseContentRangeHeader(response.headers)
      }))
    )
  }

  loadOne(executionId: string) {
    const url = `/executions/${executionId}`;
    return this.api.get<ExecutionDto>(url).pipe(
      map(dto => this.executionAdapter.adapt(dto))
    )
  }

  loadTree(executionId: string) {
    const url = `/executions/${executionId}/tree`;
    return this.api.get<ExecutionTreeDto>(url).pipe(
      map(dto => this.executionTreeAdapter.adapt(dto))
    )
  }
}
