// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { ModuleDto } from '../dtos';
import { ModuleAdapter } from '../adapters';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ModuleApiService {
  constructor(
    private readonly http: HttpClient,
    private readonly moduleAdapter: ModuleAdapter
  ) {}

  loadAll() {
    const url = `/api/studio/workflow-modules`;
    return this.http.get<ModuleDto[]>(url).pipe(
      map(dtos => dtos.map(item => this.moduleAdapter.adapt(item)))
    );
  }
}
