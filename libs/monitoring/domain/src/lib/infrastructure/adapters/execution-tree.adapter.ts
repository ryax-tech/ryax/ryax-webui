// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { Execution } from '@ryax/monitoring/domain';
import { ExecutionTreeDto } from '../dtos';
import { ExecutionAdapter } from './execution.adapter';

@Injectable()
export class ExecutionTreeAdapter {

  constructor(
    private readonly executionAdapter: ExecutionAdapter
  ) {}

  adapt(dto: ExecutionTreeDto): Execution[] {
    const executionRoot = this.executionAdapter.adapt(dto.execution);
    return dto.children.reduce((prev, item) => [
      ...prev, ...this.adapt(item)
    ], [executionRoot]);
  }
}
