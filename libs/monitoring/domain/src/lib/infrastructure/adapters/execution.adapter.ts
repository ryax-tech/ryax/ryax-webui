// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { ExecutionDto, ExecutionStateDto } from '../dtos';
import { Execution, ExecutionState } from '../../entities';

@Injectable()
export class ExecutionAdapter {

  private adaptState(dto: ExecutionStateDto) {
    switch (dto) {
      case 'SUBMITTED':
        return ExecutionState.Submitted;
      case 'RUNNING':
        return ExecutionState.Running;
      case 'RAN':
        return ExecutionState.Ran;
      case 'DONE':
        return ExecutionState.Done;
      case 'ERROR':
        return ExecutionState.Error;
      case 'CREATED':
        return ExecutionState.Created;
      case 'INTERNAL_ERROR':
        return ExecutionState.Internal_Error;
    }
  }

  adapt(dto: ExecutionDto): Execution {
    return {
      id: dto.id,
      state: this.adaptState(dto.state),
      submittedAt: dto.submitted_at * 1000,
      startedAt: dto.started_at * 1000,
      endedAt: dto.ended_at * 1000,
      logs: dto.logs,
      previousExecutions: dto.prev_execs,
      nextExecutions: dto.next_execs,
      workflowDeploymentId: dto.workflow_deployment_id,
      moduleInstanceId: dto.function_instance_id,
      moduleDeploymentId: dto.function_deploy_id,
      inputData: dto.input_data ? dto.input_data : {},
      outputData: dto.output_data ? dto.output_data : {},
    }
  }
}
