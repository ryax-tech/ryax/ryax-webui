// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { WorkflowDetailsDto, WorkflowDto } from '../dtos';
import { Workflow } from '../../entities';

@Injectable()
export class WorkflowAdapter {
  adapt(dto: WorkflowDto | WorkflowDetailsDto): Workflow {
    return {
      id: dto.id,
      name: dto.name,
    }
  }
}
