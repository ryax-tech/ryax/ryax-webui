// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
export interface ExecutionDto {
  id: string;
  submitted_at: any;
  started_at: any;
  ended_at: any;
  logs: string;
  state: ExecutionStateDto;
  prev_execs: string[];
  next_execs: string[];
  workflow_deployment_id: string;
  function_deploy_id: string;
  function_instance_id: string;
  input_data?: ExecutionInputDto;
  output_data?: object;
}

export interface ExecutionInputDto {
  [key: string]: {
    value: any;
    type: string;
  }
}

export type ExecutionStateDto = "SUBMITTED" | "RUNNING" | "RAN" | "DONE" | "ERROR" | "CREATED" | "INTERNAL_ERROR"
