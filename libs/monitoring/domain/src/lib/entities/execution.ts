// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Module } from './module';

export enum ExecutionState {
  Submitted = "Submitted",
  Running = "Running",
  Ran = "Ran",
  Done = "Done",
  Error = "Error",
  Created = "Created",
  Internal_Error = "Internal_Error"
}

export interface Execution {
  id: string;
  workflowDeploymentId: string;
  moduleInstanceId: string;
  moduleDeploymentId: string;
  submittedAt: any;
  startedAt: any;
  endedAt: any;
  logs: string;
  state: ExecutionState;
  previousExecutions: string[];
  nextExecutions: string[];
  inputData: ExecutionInputData;
  outputData: object;
}

export interface ExecutionInputData {
  [key: string]: {
    value: any;
    type: string;
  }
}

export interface ExecutionView extends Execution {
  module: Module;
}

export type ExecutionSortKey = "submittedAt" | "endedAt";

export type ExecutionSortValue = "ascend" | "descend" | null;

export interface ExecutionSort {
  key: ExecutionSortKey;
  value: ExecutionSortValue;
}

export interface ExecutionPagination {
  pageIndex: number;
  pageSize: number;
  total: number;
}

export interface ExecutionSearch {
  workflow: string;
  state: ExecutionState;
  module: string;
}
