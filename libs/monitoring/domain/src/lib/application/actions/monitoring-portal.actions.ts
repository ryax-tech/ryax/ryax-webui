// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createAction } from '@ngrx/store';
import { Execution } from '@ryax/monitoring/domain';

export const init = createAction(
  '[Monitoring Portal] Init',
  (executionId: string) => ({
    payload: { executionId }
  })
);

export const exit = createAction(
  '[Monitoring Portal] Exit',
  (executionId: string) => ({
    payload: { executionId }
  })
);

export const loadExecutionSuccess = createAction(
  '[Monitoring Portal] Load execution success',
  (executions: Execution[]) => ({
    payload: { executions }
  })
);

export const loadExecutionError = createAction(
  '[Monitoring Portal] Load execution error',
  (executionId: string) => ({
    payload: { executionId }
  })
);

export const workflowExecutionFinishedSuccess = createAction(
  '[Monitoring Portal] Workflow execution finished success',
);

export const workflowExecutionFinishedError = createAction(
  '[Monitoring Portal] Workflow execution finished error',
);

export const showModuleExecutionsList = createAction(
  '[Monitoring Portal] Show module executions list',
);
