// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createAction } from '@ngrx/store';
import { Execution, Module, Workflow } from '@ryax/monitoring/domain';

export const init = createAction(
  '[Monitoring Details] Init'
);

export const loadExecutionSuccess = createAction(
  '[Monitoring Details] Load execution success',
  (execution: Execution) => ({
    payload: { execution }
  })
);

export const loadExecutionError = createAction(
  '[Monitoring Details] Load execution error',
  (executionId: string) => ({
    payload: { executionId }
  })
);

export const loadWorkflowSuccess = createAction(
  '[Monitoring Details] Load workflow success',
  (workflow: Workflow, modules: Module[]) => ({
    payload: { workflow, modules }
  })
);

export const loadWorkflowError = createAction(
  '[Monitoring Details] Load workflow error',
  (workflowId: string) => ({
    payload: { workflowId }
  })
);

export const loadPreviousExecutionsSuccess = createAction(
  '[Monitoring Details] Load previous executions success',
  (executions: Execution[]) => ({
    payload: { executions }
  })
);

export const loadPreviousExecutionsError = createAction(
  '[Monitoring Details] Load previous executions error',
  (executionId: string) => ({
    payload: { executionId }
  })
);

export const loadNextExecutionsSuccess = createAction(
  '[Monitoring Details] Load next executions success',
  (executions: Execution[]) => ({
    payload: { executions }
  })
);

export const loadNextExecutionsError = createAction(
  '[Monitoring Details] Load next executions error',
  (executionId: string) => ({
    payload: { executionId }
  })
);

export const toggleInputVisibility = createAction(
  '[Monitoring Details] Toggle input password visibility',
  (visibility: boolean) => ({
    payload: visibility
  })
);

export const toggleOutputVisibility = createAction(
  '[Monitoring Details] Toggle output password visibility',
  (visibility: boolean) => ({
    payload: visibility
  })
);
