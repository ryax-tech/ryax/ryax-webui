// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createAction } from '@ngrx/store';
import {
  Execution, ExecutionSearch,
  ExecutionSort,
  ExecutionSortKey,
  ExecutionSortValue,
  ExecutionState, Module,
  Workflow,
} from '../../entities';

export const init = createAction(
  '[Monitoring List] Init'
);

export const refresh = createAction(
  '[Monitoring List] Refresh'
);

export const updatePageIndex = createAction(
  '[Monitoring List] Update page index',
  (pageIndex: number) => ({
    payload: { pageIndex }
  })
);

export const updateSort = createAction(
  '[Monitoring List] Update sort',
  (sort: ExecutionSort) => ({
    payload: { sort }
  })
);

export const updateFilter = createAction(
  '[Monitoring List] Update filter',
  (search: ExecutionSearch) => ({
    payload: { search }
  })
);

export const loadExecutionsSuccess = createAction(
  '[Monitoring List] Load executions success',
  (executions: Execution[], total: number) => ({
    payload: { executions, total }
  })
);

export const loadExecutionsError = createAction(
  '[Monitoring List] Load executions error'
);

export const loadWorkflowsSuccess = createAction(
  '[Monitoring List] Load workflows success',
  (workflows: Workflow[]) => ({
    payload: { workflows }
  })
);

export const loadWorkflowsError = createAction(
  '[Monitoring List] Load workflows error'
);

export const loadModulesSuccess = createAction(
  '[Monitoring List] Load modules success',
  (modules: Module[]) => ({
    payload: { modules }
  })
);

export const loadModulesError = createAction(
  '[Monitoring List] Load modules error'
);
