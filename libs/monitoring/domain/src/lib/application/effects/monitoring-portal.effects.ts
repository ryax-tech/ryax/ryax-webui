// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { ExecutionApiService } from '../../infrastructure/services';
import { MonitoringPortalActions } from '../actions';
import { catchError, filter, flatMap, mapTo, switchMap, takeUntil } from 'rxjs/operators';
import { interval, of } from 'rxjs';

@Injectable()
export class MonitoringPortalEffects {
  constructor(
    private readonly actions$: Actions,
    private readonly executionApi: ExecutionApiService,
  ) {}

  loadExecution$ = createEffect(() => this.actions$.pipe(
    ofType(MonitoringPortalActions.init),
    filter(({ payload }) => !!payload.executionId),
    switchMap((actions$) => interval(1000).pipe(
      mapTo(actions$),
      takeUntil(this.actions$.pipe(ofType(MonitoringPortalActions.exit))),
    )),
    switchMap(({ payload }) => this.executionApi.loadTree(payload.executionId).pipe(
      flatMap(executions => {
        const failedExecution = executions.find(exec => {
          return exec.state === "Error"
        });
        const lastExecution = executions[executions.length - 1];
        if(lastExecution.state === "Done" && lastExecution.nextExecutions.length === 0) {
          return [
            MonitoringPortalActions.loadExecutionSuccess(executions),
            MonitoringPortalActions.workflowExecutionFinishedSuccess()
          ]
        } else if(failedExecution) {
          return [
            MonitoringPortalActions.loadExecutionSuccess(executions),
            MonitoringPortalActions.workflowExecutionFinishedError(),
          ]
        } else {
          return [
            MonitoringPortalActions.loadExecutionSuccess(executions),
            MonitoringPortalActions.showModuleExecutionsList(),
          ]
        }
      }),
      catchError(() => of(MonitoringPortalActions.loadExecutionError(payload.executionId)))
    )),
  ));
}
