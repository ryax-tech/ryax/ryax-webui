// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { forkJoin, of } from 'rxjs';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { MonitoringState, selectCurrentExecutionId } from '../reducers';
import { MonitoringDetailsActions } from '../actions';
import { catchError, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';
import { ExecutionApiService, ModuleApiService, WorkflowApiService } from '../../infrastructure/services';

@Injectable()
export class MonitoringDetailsEffects {
  executionId$ = this.store.select(selectCurrentExecutionId);

  constructor(
    private readonly store: Store<MonitoringState>,
    private readonly actions$: Actions,
    private readonly router: Router,
    private readonly executionApi: ExecutionApiService,
    private readonly workflowApi: WorkflowApiService,
    private readonly moduleApi: ModuleApiService
  ) {}

  loadExecution$ = createEffect(() => this.actions$.pipe(
    ofType(MonitoringDetailsActions.init),
    withLatestFrom(this.executionId$, (_, executionId) => ({ executionId })),
    switchMap(({ executionId }) => this.executionApi.loadOne(executionId).pipe(
      map(execution => MonitoringDetailsActions.loadExecutionSuccess(execution)),
      catchError(() => of(MonitoringDetailsActions.loadExecutionError(executionId)))
    ))
  ));

  loadWorkflow$ = createEffect(() => this.actions$.pipe(
    ofType(MonitoringDetailsActions.loadExecutionSuccess),
    switchMap(({ payload }) => this.workflowApi.loadOne(payload.execution.workflowDeploymentId).pipe(
      map(({ workflow, modules }) => MonitoringDetailsActions.loadWorkflowSuccess(workflow, modules)),
      catchError(() => of(MonitoringDetailsActions.loadWorkflowError(payload.execution.workflowDeploymentId)))
    ))
  ))

  loadPreviousExecutions$ = createEffect(() => this.actions$.pipe(
    ofType(MonitoringDetailsActions.loadExecutionSuccess),
    map(({ payload }) => ({
      executionId: payload.execution.id,
      requests: payload.execution.previousExecutions.map(item => this.executionApi.loadOne(item))
    })),
    switchMap(({ executionId, requests }) => forkJoin(requests).pipe(
      map(executions => MonitoringDetailsActions.loadPreviousExecutionsSuccess(executions)),
      catchError(() => of(MonitoringDetailsActions.loadPreviousExecutionsError(executionId)))
    ))
  ));

  loadNextExecutions$ = createEffect(() => this.actions$.pipe(
    ofType(MonitoringDetailsActions.loadExecutionSuccess),
    map(({ payload }) => ({
      executionId: payload.execution.id,
      requests: payload.execution.nextExecutions.map(item => this.executionApi.loadOne(item))
    })),
    switchMap(({ executionId, requests }) => forkJoin(requests).pipe(
      map(executions => MonitoringDetailsActions.loadNextExecutionsSuccess(executions)),
      catchError(() => of(MonitoringDetailsActions.loadNextExecutionsError(executionId)))
    ))
  ));

  goToNotFoundError$ = createEffect(() => this.actions$.pipe(
    ofType(MonitoringDetailsActions.loadExecutionError),
    tap(() => this.router.navigate(['/', 'error']))
  ), { dispatch: false });
}
