// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { forkJoin, of } from 'rxjs';
import { catchError, first, map, switchMap, withLatestFrom } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { ROUTER_NAVIGATED } from '@ngrx/router-store';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { ExecutionApiService, ModuleApiService, WorkflowApiService } from '../../infrastructure/services';
import { MonitoringState, selectExecutionsPagination, selectExecutionsSearch, selectExecutionsSort } from '../reducers';
import { MonitoringDetailsActions, MonitoringListActions } from '../actions';
import { WorkflowPortalActions } from '../../../../../../workflow/domain/src/lib/application/actions';

@Injectable()
export class MonitoringListEffects {
  sort$ = this.store.select(selectExecutionsSort);
  pagination$ = this.store.select(selectExecutionsPagination);
  search$ = this.store.select(selectExecutionsSearch);

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store<MonitoringState>,
    private readonly router: Router,
    private readonly executionApi: ExecutionApiService,
    private readonly workflowApi: WorkflowApiService,
    private readonly moduleApi: ModuleApiService
  ) {}

  loadExecutions$ = createEffect(() => this.actions$.pipe(
    ofType(MonitoringListActions.init, MonitoringListActions.refresh, MonitoringListActions.updatePageIndex, MonitoringListActions.updateSort),
    withLatestFrom(this.search$, this.sort$, this.pagination$, (_, search, sort, pagination) => ({
      startIndex: (pagination.pageIndex - 1) * pagination.pageSize,
      endIndex: pagination.pageIndex * pagination.pageSize,
      sortKey: sort.key,
      sortValue: sort.value,
      workflow: search.workflow,
      module: search.module,
      state: search.state
    })),
    switchMap(({ startIndex, endIndex, sortKey, sortValue, workflow, module, state}) => this.executionApi.loadAll(startIndex, endIndex, sortKey, sortValue, workflow, module, state).pipe(
        map(({ items, total }) => MonitoringListActions.loadExecutionsSuccess(items, total)),
        catchError(() => of(MonitoringListActions.loadExecutionsError()))
    ))
  ));

  loadWorkflows$ = createEffect(() => this.actions$.pipe(
    ofType(MonitoringListActions.init, MonitoringListActions.refresh, MonitoringListActions.updatePageIndex, MonitoringListActions.updateSort, MonitoringListActions.updateFilter),
    switchMap(() => this.workflowApi.loadAll().pipe(
      map(workflows  => MonitoringListActions.loadWorkflowsSuccess(workflows)),
      catchError(() => of(MonitoringListActions.loadWorkflowsError()))
    ))
  ));

  loadModules$ = createEffect(() => this.actions$.pipe(
    ofType(MonitoringListActions.init,
      MonitoringListActions.refresh,
      MonitoringListActions.updatePageIndex,
      MonitoringListActions.updateSort,
      MonitoringListActions.updateFilter,
      //to be deleted when executions api is refactored
      WorkflowPortalActions.loadPortalSuccess
      ),
    switchMap(() => this.moduleApi.loadAll().pipe(
      map(modules => MonitoringListActions.loadModulesSuccess(modules)),
      catchError(() => of(MonitoringListActions.loadModulesError()))
    ))
  ));

  updateFilter$ = createEffect(() => this.actions$.pipe(
    ofType(MonitoringListActions.updateFilter),
    switchMap(({ payload }) => this.router.navigate(['/', 'workflows', 'monitoring'], {
      queryParams: {
        workflow: payload.search.workflow,
        state: payload.search.state,
        module: payload.search.module
      }
    })),
  ), { dispatch: false });

  reloadExecutionsAfterFilter$ = createEffect(() => this.actions$.pipe(
    ofType(MonitoringListActions.updateFilter),
    switchMap(() => this.actions$.pipe(
      ofType(ROUTER_NAVIGATED),
      first()
    )),
    map(() => MonitoringListActions.refresh())
  ));
}
