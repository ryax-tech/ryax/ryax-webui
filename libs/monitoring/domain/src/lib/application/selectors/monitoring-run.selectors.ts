// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
// import { createFeatureSelector, createSelector } from '@ngrx/store';
// import { MonitoringAdapters, MonitoringFeatureKey, MonitoringState } from '../reducers';
// import { Execution } from '../../entities';
// import { Dictionary } from '@ngrx/entity';
//
// const selectFeatureState = createFeatureSelector<MonitoringState>(MonitoringFeatureKey);
// const selectExecutionsState = createSelector(selectFeatureState, state => state.executions);
// const selectWorkflowsState = createSelector(selectFeatureState, state => state.workflows);
// const selectModulesState = createSelector(selectFeatureState, state => state.modules);
//
// const {
//   selectEntities: selectExecutionEntities
// } = MonitoringAdapters.execution.getSelectors(selectExecutionsState);
//
// const {
//   selectEntities: selectWorkflowEntities
// } = MonitoringAdapters.workflow.getSelectors(selectWorkflowsState);
//
// const {
//   selectEntities: selectModuleEntities
// } = MonitoringAdapters.module.getSelectors(selectModulesState);
//
// export const selectLoading = createSelector(selectExecutionsState, selectModulesState,
//   (executionState, moduleState) => !executionState.loaded && !moduleState.loaded);
//
// const selectExecutions = (executions: Dictionary<Execution>, executionId: string): Array<string> => {
//   const currentExecution = executions[executionId];
//   if(currentExecution) {
//     const nextExecutions = currentExecution.nextExecutions.map(nextExecutionId => executions[nextExecutionId]);
//     let executionSet = Array.from(new Set([currentExecution.id]));
//     nextExecutions.forEach(item => {
//       const nestedExecutionSet = Array.from(selectExecutions(executions, item.id));
//       executionSet = Array.from(new Set([...executionSet, ...nestedExecutionSet, item.id]))
//     });
//     return executionSet;
//   } else {
//     return [];
//   }
// };
//
// export const selectNextExecutions = createSelector(selectExecutionEntities, selectModuleEntities,
//   (executions, modules, executionId: string) => {
//     const nextExecutions = selectExecutions(executions, executionId);
//     return nextExecutions
//       .map((item) => executions[item])
//       .filter((item) => !!item)
//       .map((item: Execution) => ({ ...item, module: modules[item.moduleDeploymentId]}));
// });
