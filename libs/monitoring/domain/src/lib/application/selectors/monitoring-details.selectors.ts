// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
// import { createFeatureSelector, createSelector } from '@ngrx/store';
// import { MonitoringAdapters, MonitoringFeatureKey, MonitoringState } from '../reducers';
// import { NavigationQueries } from '@ryax/domain/navigation';
// import {
//   getMonitoringDetailsInformationsView, getMonitoringDetailsInputsView,
//   getMonitoringDetailsLogsView, getMonitoringDetailsOutputsView, getMonitoringDetailsTableView,
//   getMonitoringDetailsTimelineView,
// } from '../views';
// import { Execution } from '../../entities';
//
// const selectFeatureState = createFeatureSelector<MonitoringState>(MonitoringFeatureKey);
// export const selectExecutionsState = createSelector(selectFeatureState, state => state.executions);
// export const selectWorkflowsState = createSelector(selectFeatureState, state => state.workflows);
// export const selectModulesState = createSelector(selectFeatureState, state => state.modules);
//
// const {
//   selectEntities: selectExecutionEntities
// } = MonitoringAdapters.execution.getSelectors(selectExecutionsState);
//
// const {
//   selectEntities: selectWorkflowEntities
// } = MonitoringAdapters.workflow.getSelectors(selectWorkflowsState);
//
// const {
//   selectEntities: selectModuleEntities
// } = MonitoringAdapters.module.getSelectors(selectModulesState);
//
// const selectExecutionById = createSelector(selectExecutionEntities,
//   (executions) => (executionId: string) => executions[executionId]);
//
// const selectWorkflowById = createSelector(selectWorkflowEntities,
//   (workflows) => (workflowId: string) => workflows[workflowId]);
//
// const selectModuleById = createSelector(selectModuleEntities,
//   (modules) => (moduleId: string) => modules[moduleId]);
//
// export const selectExecutionId = createSelector(NavigationQueries.selectParams,
//   (routerParams) => routerParams && routerParams['executionId']);
//
// export const selectExecution = createSelector(selectExecutionById, selectExecutionId,
//   (getExecutionById, executionId) => getExecutionById(executionId));
//
// export const selectPreviousExecutions = createSelector(selectExecution, selectExecutionById,
//   (execution, getExecutionById) => execution ? execution.previousExecutions.map(item => getExecutionById(item)).filter(executionItem => !!executionItem): []);
//
// export const selectNextExecutions = createSelector(selectExecution, selectExecutionById,
//   (execution, getExecutionById) => execution ? execution.nextExecutions.map(item => getExecutionById(item)).filter(executionItem => !!executionItem): []);
//
// export const canAccess = createSelector(selectExecution, selectExecutionsState,
//   (execution, executionsState) => executionsState.loaded && !!execution);
//
// export const selectInformationsView = createSelector(selectExecution, selectWorkflowById, selectModuleById, selectExecutionsState, selectWorkflowsState, selectModulesState,
//   (execution, getWorkflowById, getModuleById, executionState, workflowState, moduleState) => getMonitoringDetailsInformationsView(execution, getWorkflowById, getModuleById, executionState.loaded, workflowState.loaded, moduleState.loaded));
//
// export const selectLogsView = createSelector(selectExecution, selectExecutionsState,
//   (execution, executionState) => getMonitoringDetailsLogsView(execution, executionState.loaded));
//
// export const selectTimelineView = createSelector(selectExecution, selectPreviousExecutions, selectNextExecutions, selectExecutionsState,
//   (execution, previousExecutions, nextExecutions, executionState) => getMonitoringDetailsTimelineView(execution, previousExecutions, nextExecutions, executionState.loaded));
//
// export const selectPreviousTableView = createSelector(selectPreviousExecutions, selectExecutionsState,
//   (previousExecutions, executionState) => getMonitoringDetailsTableView(previousExecutions, executionState.loaded));
//
// export const selectNextTableView = createSelector(selectNextExecutions, selectExecutionsState,
//   (nextExecutions, executionState) => getMonitoringDetailsTableView(nextExecutions, executionState.loaded));
//
// export const selectInputsView = createSelector(selectExecutionsState, selectExecution,
//   (executionState, execution: Execution) => getMonitoringDetailsInputsView(executionState.loaded, execution));
//
// export const selectOutputsView = createSelector(selectExecutionsState, selectExecution,
//   (executionState, execution: Execution) => getMonitoringDetailsOutputsView(executionState.loaded, execution));
