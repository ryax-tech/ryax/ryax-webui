// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { InjectionToken } from '@angular/core';
import { ActionReducerMap, createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromExecution from './execution.reducer';
import * as fromWorkflow from './workflow.reducer';
import * as fromModule from './module.reducer';
import { NavigationQueries } from '@ryax/domain/navigation';

export const MonitoringFeatureKey = 'monitoringDomain';

export interface MonitoringState {
  executions: fromExecution.State
  workflows: fromWorkflow.State
  modules: fromModule.State
}

export const MonitoringReducerToken = new InjectionToken<ActionReducerMap<MonitoringState>>(MonitoringFeatureKey);

export const MonitoringReducerProvider = {
  provide: MonitoringReducerToken,
  useValue: {
    executions: fromExecution.reducer,
    workflows: fromWorkflow.reducer,
    modules: fromModule.reducer,
  },
};


export const selectFeatureState = createFeatureSelector<MonitoringState>(MonitoringFeatureKey);
export const selectExecutionsState = createSelector(selectFeatureState, state => state.executions);
export const selectWorkflowsState = createSelector(selectFeatureState, state => state.workflows);
export const selectModulesState = createSelector(selectFeatureState, state => state.modules);

export const selectExecutionsLoading = createSelector(selectExecutionsState, fromExecution.selectLoading);
export const selectExecutionsSort = createSelector(selectExecutionsState, fromExecution.selectSort);
export const selectExecutionsPagination = createSelector(selectExecutionsState, fromExecution.selectPagination);
export const selectExecutionsSearch = createSelector(NavigationQueries.selectQuery, fromExecution.selectSearch);
export const selectExecutions = createSelector(selectExecutionsState, fromExecution.selectExecutions);
export const selectCurrentExecutionId = createSelector(NavigationQueries.selectParams, fromExecution.selectCurrentExecutionId);
export const selectCurrentExecution = createSelector(selectExecutionsState, selectCurrentExecutionId, fromExecution.selectExecutionById);
export const selectCurrentPreviousExecutions = createSelector(selectExecutionsState, selectCurrentExecution, fromExecution.selectPreviousExecutions);
export const selectCurrentNextExecutions = createSelector(selectExecutionsState, selectCurrentExecution, fromExecution.selectNextExecutions);
export const selectInputPasswordVisibility = createSelector(selectExecutionsState, fromExecution.selectInputPasswordVisibility);
export const selectOutputPasswordVisibility = createSelector(selectExecutionsState, fromExecution.selectOutputPasswordVisibility);
export const selectWorkflowExecutionStarted = createSelector(selectExecutionsState, fromExecution.selectWorkflowExecutionStarted);
export const selectWorkflowExecutionFinished = createSelector(selectExecutionsState, fromExecution.selectWorkflowExecutionFinished);
export const selectWorkflowExecutionFailed = createSelector(selectExecutionsState, fromExecution.selectWorkflowExecutionFailed);

export const selectWorkflowsLoading = createSelector(selectWorkflowsState, fromWorkflow.selectLoading);
export const selectWorkflows = createSelector(selectWorkflowsState, fromWorkflow.selectWorkflows);
export const selectWorkflowById = createSelector(selectWorkflowsState, fromWorkflow.selectWorkflowById);

export const selectModulesLoading = createSelector(selectModulesState, fromModule.selectLoading);
export const selectModules = createSelector(selectModulesState, fromModule.selectModules);
export const selectModuleById = createSelector(selectModulesState, fromModule.selectModuleById);


