// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import { Execution, ExecutionPagination, ExecutionSearch, ExecutionSortKey, ExecutionSortValue } from '../../entities';
import { MonitoringDetailsActions, MonitoringListActions, MonitoringPortalActions } from '../actions';
import { Params } from '@angular/router';

export interface State extends EntityState<Execution> {
  loading: boolean;
  inputPasswordVisibility: boolean;
  outputPasswordVisibility: boolean;
  sort: {
    key: ExecutionSortKey,
    value: ExecutionSortValue,
  }
  pagination: ExecutionPagination,
  resultVisibility: boolean,
  executionsVisibility: boolean,
  resultError: boolean,
}

export const adapter = createEntityAdapter<Execution>({
  selectId: execution => execution.id
});

export const initialState: State = adapter.getInitialState({
  loading: false,
  inputPasswordVisibility: false,
  outputPasswordVisibility: false,
  sort: {
    key: null,
    value: null
  },
  pagination: {
    pageIndex: 1,
    pageSize: 10,
    total: null
  },
  resultVisibility: false,
  executionsVisibility: false,
  resultError: false,
});

export const reducer = createReducer<State>(
  initialState,
  on(MonitoringListActions.init, (state) => ({
    ...state,
    loading: true
  })),
  on(MonitoringListActions.updatePageIndex, (state, { payload }) => ({
    ...state,
    loading: true,
    pagination: {
      ...state.pagination,
      pageIndex: payload.pageIndex
    }
  })),
  on(MonitoringListActions.updateFilter, (state, { payload }) => ({
    ...state,
    loading: true,
    pagination: {
      ...state.pagination,
      pageIndex: 1
    }
  })),
  on(MonitoringListActions.updateSort, (state, { payload }) => ({
    ...state,
    loading: true,
    sort: payload.sort
  })),
  on(MonitoringListActions.loadExecutionsSuccess, (state, { payload }) => adapter.setAll(payload.executions, {
    ...state,
    loading: false,
    pagination: {
      ...state.pagination,
      total: payload.total
    },
  })),
  on(MonitoringListActions.loadExecutionsError, (state) => adapter.removeAll({
    ...state,
    loading: false,
    pagination: {
      ...state.pagination,
      pageIndex: 1,
      total: null
    },
  })),
  on(MonitoringDetailsActions.init, (state) => ({
    ...state,
    loading: true
  })),
  on(MonitoringDetailsActions.loadExecutionSuccess, (state, { payload }) => adapter.upsertOne(payload.execution, {
    ...state,
    loading: false
  })),
  on(MonitoringDetailsActions.loadExecutionError, (state, { payload }) => adapter.removeOne(payload.executionId, {
    ...state,
    loading: false,
  })),
  on(MonitoringDetailsActions.loadPreviousExecutionsSuccess, (state, { payload }) => adapter.upsertMany(payload.executions, {
    ...state,
    loading: false
  })),
  on(MonitoringDetailsActions.loadPreviousExecutionsError, (state, { payload } ) => {
    const currentExecution = state.entities[payload.executionId];
    return adapter.removeMany(currentExecution.previousExecutions, {
      ...state,
      loading: false
    });
  }),
  on(MonitoringDetailsActions.loadNextExecutionsSuccess, (state, { payload }) => adapter.upsertMany(payload.executions, {
    ...state,
    loading: false
  })),
  on(MonitoringDetailsActions.loadNextExecutionsError, (state, { payload } ) => {
      const currentExecution = state.entities[payload.executionId];
      return adapter.removeMany(currentExecution.nextExecutions, {
        ...state,
        loading: false
      });
  }),
  on(MonitoringPortalActions.init, (state, { payload }) => adapter.removeAll({
    ...state,
    loading: !!payload.executionId
  })),
  on(MonitoringPortalActions.exit, (state) => adapter.removeAll({
    ...state,
    loading: false
  })),
  on(MonitoringPortalActions.loadExecutionSuccess, (state, { payload }) => adapter.setAll(payload.executions, {
    ...state,
  })),
  on(MonitoringPortalActions.loadExecutionError, (state) => adapter.removeAll({
    ...state,
    loading: false
  })),
  on(MonitoringDetailsActions.toggleInputVisibility, (state, { payload }) => ({
    ...state,
    inputPasswordVisibility: payload
  })),
  on(MonitoringDetailsActions.toggleOutputVisibility, (state, { payload }) => ({
    ...state,
    outputPasswordVisibility: payload
  })),
  on(MonitoringPortalActions.workflowExecutionFinishedSuccess, (state) => ({
    ...state,
    loading: false,
    resultVisibility: true,
    resultError: false,
  })),
  on(MonitoringPortalActions.workflowExecutionFinishedError, (state) => ({
    ...state,
    loading: false,
    resultVisibility: true,
    resultError: true,
  })),
  on(MonitoringPortalActions.showModuleExecutionsList, (state) => ({
    ...state,
    executionsVisibility: true
  })),
);

const {
  selectAll
} = adapter.getSelectors();

export const selectLoading = (state: State) => state.loading;
export const selectExecutions = (state: State) => selectAll(state);
export const selectExecutionById = (state: State, executionId: string) => state.entities[executionId];
export const selectPreviousExecutions = (state: State, execution?: Execution) => selectAll(state).filter(item => execution && execution.previousExecutions.includes(item.id));
export const selectNextExecutions = (state: State, execution?: Execution) => selectAll(state).filter(item => execution && execution.nextExecutions.includes(item.id));
export const selectPagination = (state: State) => state.pagination;
export const selectSort = (state: State) => state.sort;
export const selectSearch = (navParams: Params): ExecutionSearch => ({
  workflow: navParams && navParams["workflow"],
  state: navParams && navParams['state'],
  module: navParams && navParams['module']
});
export const selectCurrentExecutionId = (navParams: Params) => navParams && navParams["executionId"];
export const selectInputPasswordVisibility = (state: State) => state.inputPasswordVisibility;
export const selectOutputPasswordVisibility = (state: State) => state.outputPasswordVisibility;
export const selectWorkflowExecutionStarted = (state: State) => state.executionsVisibility;
export const selectWorkflowExecutionFinished = (state: State) => state.resultVisibility;
export const selectWorkflowExecutionFailed = (state: State) => state.resultError;
