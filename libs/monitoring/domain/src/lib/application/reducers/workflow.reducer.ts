// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { Workflow } from '../../entities';
import { createReducer, on } from '@ngrx/store';
import { MonitoringDetailsActions, MonitoringListActions } from '../actions';

export interface State extends EntityState<Workflow> {
  loading: boolean;
}

export const adapter = createEntityAdapter<Workflow>({
  selectId: workflow => workflow.id
});

export const initialState: State = adapter.getInitialState({
  loading: false,
});

export const reducer = createReducer<State>(
  initialState,
  on(MonitoringListActions.init, (state) => ({
    ...state,
    loading: true
  })),
  on(MonitoringListActions.loadWorkflowsSuccess, (state, { payload }) => adapter.setAll(payload.workflows, {
    ...state,
    loading: false
  })),
  on(MonitoringListActions.loadWorkflowsError, (state ) => adapter.removeAll({
    ...state,
    loading: false
  })),
  on(MonitoringDetailsActions.loadWorkflowSuccess, (state, { payload }) => adapter.upsertOne(payload.workflow, {
    ...state,
    loading: false
  })),
  on(MonitoringDetailsActions.loadWorkflowError, (state, { payload } ) => adapter.removeOne(payload.workflowId, {
    ...state,
    loading: false
  }))
);

const {
  selectAll,
} = adapter.getSelectors()

export const selectLoading = (state: State) => state.loading;
export const selectWorkflows = (state: State) => selectAll(state);
export const selectWorkflowById = (state: State, workflowId: string): Workflow => state.entities[workflowId];
