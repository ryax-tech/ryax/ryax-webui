// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import { Module } from '../../entities';
import { MonitoringDetailsActions, MonitoringListActions } from '../actions';

export interface State extends EntityState<Module> {
  loading: boolean;
}

export const adapter = createEntityAdapter<Module>({
  selectId: module => module.id
});

export const initialState: State = adapter.getInitialState({
  loading: false,
});

export const reducer = createReducer<State>(
  initialState,
  on(MonitoringListActions.loadModulesSuccess, (state, { payload }) => adapter.setAll(payload.modules, {
    ...state,
    loading: false
  })),
  on(MonitoringListActions.loadModulesError, (state) => adapter.removeAll({
    ...state,
    loading: false
  })),
  on(MonitoringDetailsActions.loadWorkflowSuccess, (state, { payload }) => adapter.upsertMany(payload.modules, {
    ...state,
    loading: false
  }))
);

const {
  selectAll,
} = adapter.getSelectors();

export const selectLoading = (state: State) => state.loading;
export const selectModules = (state: State) => selectAll(state);
export const selectModuleById = (state: State, moduleId: string) => state.entities[moduleId];
