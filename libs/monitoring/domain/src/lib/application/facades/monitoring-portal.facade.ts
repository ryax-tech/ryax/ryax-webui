// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { MonitoringState,
  selectExecutions,
  selectExecutionsLoading,
  selectWorkflowExecutionStarted,
  selectWorkflowExecutionFinished,
  selectWorkflowExecutionFailed } from '../reducers';
import { Store } from '@ngrx/store';
import { MonitoringPortalActions } from '../actions';

@Injectable()
export class MonitoringPortalFacade {
  loading$ = this.store.select(selectExecutionsLoading);
  executions$ = this.store.select(selectExecutions);
  workflowExecutionStarted$ = this.store.select(selectWorkflowExecutionStarted);
  workflowExecutionFinished$ = this.store.select(selectWorkflowExecutionFinished);
  workflowExecutionFailed$ = this.store.select(selectWorkflowExecutionFailed);

  constructor(
    private readonly store: Store<MonitoringState>
  ) {}

  init(executionId: string) {
    this.store.dispatch(MonitoringPortalActions.init(executionId));
  }

  exit(executionId: string) {
    this.store.dispatch(MonitoringPortalActions.exit(executionId));
  }
}
