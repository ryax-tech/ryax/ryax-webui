// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import {
  MonitoringState,
  selectExecutions,
  selectExecutionsLoading,
  selectExecutionsPagination, selectExecutionsSearch, selectModuleById, selectModules,
  selectWorkflowById,
  selectWorkflows,
} from '../reducers';
import { MonitoringListActions } from '../actions';
import { ExecutionSearch, ExecutionSort } from '../../entities';

@Injectable()
export class MonitoringListFacade {
  loading$ = this.store.select(selectExecutionsLoading);
  pagination$ = this.store.select(selectExecutionsPagination);
  search$ = this.store.select(selectExecutionsSearch);
  workflows$ = this.store.select(selectWorkflows);
  executions$ = this.store.select(selectExecutions);
  modules$ = this.store.select(selectModules);

  constructor(
    private readonly store: Store<MonitoringState>
  ) {}

  getWorkflowById$(workflowId: string) {
    return this.store.select(selectWorkflowById, workflowId);
  }

  getModuleById$(moduleById: string) {
    return this.store.select(selectModuleById, moduleById);
  }

  init() {
    this.store.dispatch(MonitoringListActions.init());
  }

  refresh() {
    this.store.dispatch(MonitoringListActions.refresh());
  }

  updatePageIndex(data: number) {
    this.store.dispatch(MonitoringListActions.updatePageIndex(data));
  }

  updateSort(data: ExecutionSort) {
    this.store.dispatch(MonitoringListActions.updateSort(data));
  }

  updateFilter(data: ExecutionSearch) {
    this.store.dispatch(MonitoringListActions.updateFilter(data));
  }
}
