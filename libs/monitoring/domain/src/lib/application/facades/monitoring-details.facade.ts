// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import {
  MonitoringState,
  selectCurrentExecution,
  selectCurrentNextExecutions,
  selectCurrentPreviousExecutions,
  selectExecutionsLoading,
  selectModuleById,
  selectWorkflowById,
  selectInputPasswordVisibility,
  selectOutputPasswordVisibility,
} from '../reducers';
import { MonitoringDetailsActions } from '../actions';

@Injectable()
export class MonitoringDetailsFacade {
  loading$ = this.store.select(selectExecutionsLoading);
  execution$ = this.store.select(selectCurrentExecution);
  previousExecutions$ = this.store.select(selectCurrentPreviousExecutions);
  nextExecutions$ = this.store.select(selectCurrentNextExecutions);
  inputPasswordVisibility$ = this.store.select(selectInputPasswordVisibility);
  outputPasswordVisibility$ = this.store.select(selectOutputPasswordVisibility);

  constructor(
    private readonly store: Store<MonitoringState>
  ) {}

  getWorkflowById$(workflowId: string) {
    return this.store.select(selectWorkflowById, workflowId);
  }

  getModuleById$(moduleId: string) {
    return this.store.select(selectModuleById, moduleId);
  }

  init() {
    this.store.dispatch(MonitoringDetailsActions.init())
  }

  toggleInputVisibility(visibility: boolean) {
    this.store.dispatch(MonitoringDetailsActions.toggleInputVisibility(visibility))
  }

  toggleOutputVisibility(visibility: boolean) {
    this.store.dispatch(MonitoringDetailsActions.toggleOutputVisibility(visibility))
  }
}
