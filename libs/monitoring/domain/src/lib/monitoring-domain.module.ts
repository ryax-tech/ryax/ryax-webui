// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { SharedUtilApiModule } from '@ryax/shared/util-api';
import { MonitoringFeatureKey, MonitoringReducerProvider, MonitoringReducerToken } from './application/reducers';
import { MonitoringDetailsEffects, MonitoringListEffects, MonitoringPortalEffects } from './application/effects';
import { MonitoringDetailsFacade, MonitoringListFacade, MonitoringPortalFacade } from './application/facades';
import { ExecutionApiService, ModuleApiService, WorkflowApiService } from './infrastructure/services';
import { ExecutionAdapter, ExecutionTreeAdapter, ModuleAdapter, WorkflowAdapter } from './infrastructure/adapters';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature(MonitoringFeatureKey, MonitoringReducerToken),
    EffectsModule.forFeature([MonitoringListEffects, MonitoringDetailsEffects, MonitoringPortalEffects]),
    SharedUtilApiModule
  ],
  providers: [
    MonitoringReducerProvider,
    ExecutionApiService,
    WorkflowApiService,
    ModuleApiService,
    MonitoringListFacade,
    MonitoringDetailsFacade,
    ExecutionAdapter,
    WorkflowAdapter,
    ModuleAdapter,
    MonitoringPortalFacade,
    ExecutionTreeAdapter
  ]
})
export class MonitoringDomainModule {}
