// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, OnInit } from '@angular/core';
import { ExecutionSearch, ExecutionSort, MonitoringListFacade } from '@ryax/monitoring/domain';

@Component({
  selector: 'ryax-monitoring-list',
  templateUrl: './monitoring-list.component.pug'
})
export class MonitoringListComponent implements OnInit {
  loading$ = this.facade.loading$;
  pagination$ = this.facade.pagination$;
  search$ = this.facade.search$;
  workflows$ = this.facade.workflows$;
  modules$ = this.facade.modules$;
  executions$ = this.facade.executions$;

  constructor(
    private readonly facade: MonitoringListFacade,
  ) {}

  getWorkflowById$(workflowId: string) {
    return this.facade.getWorkflowById$(workflowId);
  }

  getModuleById$(moduleId: string) {
    return this.facade.getModuleById$(moduleId);
  }

  ngOnInit(): void {
    this.facade.init();
  }

  onRefresh() {
    this.facade.refresh()
  }

  onUpdatePageIndex(data: number) {
    this.facade.updatePageIndex(data);
  }

  onUpdateSort(data: ExecutionSort) {
    this.facade.updateSort(data);
  }

  onSearchChange(data: ExecutionSearch) {
    this.facade.updateFilter(data);
  }
}
