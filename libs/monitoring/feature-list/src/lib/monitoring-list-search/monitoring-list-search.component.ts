// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Subject } from 'rxjs';
import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ExecutionSearch, ExecutionState, Module, Workflow } from '@ryax/monitoring/domain';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'ryax-monitoring-list-search',
  templateUrl: './monitoring-list-search.component.pug',
  styleUrls: ['./monitoring-list-search.component.scss']
})
export class MonitoringListSearchComponent implements OnInit, OnChanges, OnDestroy {
  executionStates = Object.values(ExecutionState);
  form = new FormGroup({
    workflow: new FormControl(null),
    module: new FormControl(null),
    state: new FormControl(null),
  });
  destroy$ = new Subject<void>();

  @Input() workflows: Workflow[];
  @Input() modules: Module[];
  @Input() search: ExecutionSearch;
  @Output() searchChange = new EventEmitter<ExecutionSearch>();

  ngOnInit(): void {
    this.form.valueChanges.pipe(
      takeUntil(this.destroy$)
    ).subscribe(value => this.searchChange.emit(value));
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes.search && changes.search.currentValue) {
      this.form.patchValue(changes.search.currentValue, { emitEvent: false });
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
