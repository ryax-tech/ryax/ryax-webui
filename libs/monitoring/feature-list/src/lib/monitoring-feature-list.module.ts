// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedUiCommonModule } from '@ryax/shared/ui-common';
import { MonitoringDomainModule } from '@ryax/monitoring/domain';
import { MonitoringListComponent } from './monitoring-list/monitoring-list.component';
import { MonitoringListHeaderComponent } from './monitoring-list-header/monitoring-list-header.component';
import { MonitoringListTableComponent } from './monitoring-list-table/monitoring-list-table.component';
import { MonitoringListSearchComponent } from './monitoring-list-search/monitoring-list-search.component';
import { MonitoringListTableRowComponent } from './monitoring-list-table-row/monitoring-list-table-row.component';
import { MonitoringUiModule } from '@ryax/monitoring/ui';

@NgModule({
  imports: [
    CommonModule,
    SharedUiCommonModule,
    MonitoringUiModule,
    MonitoringDomainModule,
  ],
  declarations: [
    MonitoringListComponent,
    MonitoringListHeaderComponent,
    MonitoringListTableComponent,
    MonitoringListSearchComponent,
    MonitoringListTableRowComponent
  ],
  exports: [
    MonitoringListComponent
  ]
})
export class MonitoringFeatureListModule {}
