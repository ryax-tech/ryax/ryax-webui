// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, Input } from '@angular/core';
import { Execution, Module, Workflow } from '@ryax/monitoring/domain';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'tr[ryax-monitoring-list-table-row]',
  templateUrl: './monitoring-list-table-row.component.pug',
  styleUrls: ['./monitoring-list-table-row.component.scss']
})
export class MonitoringListTableRowComponent {
  @Input() execution: Execution;
  @Input() workflow: Workflow;
  @Input() module: Module;

  get executionDuration() {
    return this.execution.endedAt - this.execution.startedAt;
  }
}
