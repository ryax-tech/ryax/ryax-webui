// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, EventEmitter, Input, Output, TemplateRef } from '@angular/core';
import { Execution, ExecutionPagination, ExecutionSort } from '@ryax/monitoring/domain';
import { Router } from '@angular/router';

@Component({
  selector: 'ryax-monitoring-list-table',
  templateUrl: './monitoring-list-table.component.pug',
})
export class MonitoringListTableComponent {
  @Input() loading: boolean;
  @Input() executions: Execution[];
  @Input() pagination: ExecutionPagination;
  @Input() tableItem: TemplateRef<any>;
  @Output() updatePageIndex = new EventEmitter<number>();
  @Output() updateSort = new EventEmitter<ExecutionSort>();

  constructor(
    private readonly router: Router
  ) {}

  trackItemById(index: number, item: Execution) {
    return item.id;
  }

  onPageIndexChange(index: number) {
    this.updatePageIndex.emit(index);
  }

  onSortChange(data: { key: string, value: string }) {
    this.updateSort.emit(data as ExecutionSort);
  }

  routeToExecution(executionId: string, newTab?: boolean){
    if(newTab) {
      window.open(window.location.href+`/${executionId}`, '_blank');
    } else {
      this.router.navigate(['/', 'workflows', 'monitoring', executionId]);
    }
  }
}
