// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, Input } from '@angular/core';
import { Execution, Module } from '@ryax/monitoring/domain';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'tr[ryax-monitoring-portal-table-row]',
  templateUrl: './monitoring-portal-table-row.component.pug'
})
export class MonitoringPortalTableRowComponent {
  @Input() execution: Execution;
  @Input() module: Module;
}
