// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, EventEmitter, Output } from '@angular/core';
import { MonitoringPortalFacade } from '@ryax/monitoring/domain';

@Component({
  selector: 'ryax-monitoring-processing',
  templateUrl: './monitoring-processing.component.pug'
})
export class MonitoringProcessingComponent {
  @Output() reload = new EventEmitter<void>();

  loading$ = this.facade.loading$;
  workflowExecutionFinished$ = this.facade.workflowExecutionFinished$;
  workflowExecutionFailed$ = this.facade.workflowExecutionFailed$;

  constructor(
    private readonly facade: MonitoringPortalFacade
  ) {}

  reloadPage() {
    this.reload.emit()
  }
}
