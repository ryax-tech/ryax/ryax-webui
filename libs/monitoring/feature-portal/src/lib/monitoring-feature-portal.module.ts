// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MonitoringPortalComponent } from './monitoring-portal/monitoring-portal.component';
import { MonitoringPortalTableComponent } from './monitoring-portal-table/monitoring-portal-table.component';
import { SharedUiCommonModule } from '@ryax/shared/ui-common';
import { MonitoringUiModule } from '@ryax/monitoring/ui';
import { MonitoringDomainModule } from '@ryax/monitoring/domain';
import { MonitoringPortalTableRowComponent } from './monitoring-portal-table-row/monitoring-portal-table-row.component';
import { MonitoringProcessingComponent } from './monitoring-processing/monitoring-processing.component';

@NgModule({
  imports: [
    CommonModule,
    SharedUiCommonModule,
    MonitoringUiModule,
    MonitoringDomainModule,
  ],
  declarations: [
    MonitoringPortalComponent,
    MonitoringPortalTableComponent,
    MonitoringPortalTableRowComponent,
    MonitoringProcessingComponent
  ],
  exports: [
    MonitoringPortalComponent,
    MonitoringProcessingComponent
  ]
})
export class MonitoringFeaturePortalModule {}
