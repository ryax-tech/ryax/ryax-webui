// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { MonitoringListFacade, MonitoringPortalFacade } from '@ryax/monitoring/domain';

@Component({
  selector: 'ryax-monitoring-portal',
  templateUrl: './monitoring-portal.component.pug'
})
export class MonitoringPortalComponent implements OnInit, OnDestroy, OnChanges {
  @Input() executionId: string;
  @Input() baseLink: string;

  loading$ = this.facade.loading$;
  executions$ = this.facade.executions$;
  workflowExecutionStarted$ = this.facade.workflowExecutionStarted$;

  constructor(
    private readonly facade: MonitoringPortalFacade,
    private readonly monitoringListFacade: MonitoringListFacade,
  ) {}

  getModuleById$(moduleId: string) {
    return this.monitoringListFacade.getModuleById$(moduleId);
  }

  ngOnInit(): void {
    this.facade.init(this.executionId);
  }

  ngOnDestroy(): void {
    this.facade.exit(this.executionId);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes.executionId && changes.executionId.currentValue) {
      this.facade.init(changes.executionId.currentValue);
    }
  }
}
