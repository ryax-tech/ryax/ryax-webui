// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, Input, TemplateRef } from '@angular/core';
import { Execution } from '@ryax/monitoring/domain';

@Component({
  selector: 'ryax-monitoring-portal-table',
  templateUrl: './monitoring-portal-table.component.pug'
})
export class MonitoringPortalTableComponent {
  @Input() baseLink: string;
  @Input() executions: Execution[];
  @Input() tableItem: TemplateRef<any>;

  trackItemById(index: number, item: Execution) {
    return item.id;
  }
}
