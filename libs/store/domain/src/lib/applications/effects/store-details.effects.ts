// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { of } from 'rxjs';
import { catchError, first, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { selectCurrentModuleId, StoreState } from '../reducers';
import { StoreDetailsActions } from '../actions';
import { ModuleApi } from '../../infrastructure/services';
import { MessageService } from '@ryax/shared/ui-common';

@Injectable()
export class StoreDetailsEffects {
  moduleId$ = this.store.select(selectCurrentModuleId);

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store<StoreState>,
    private readonly moduleApi: ModuleApi,
    private readonly router: Router,
    private readonly messageService: MessageService,
  ) {}

  loadModule$ = createEffect(() => this.actions$.pipe(
    ofType(StoreDetailsActions.init),
    withLatestFrom(this.moduleId$, (_, moduleId) => moduleId),
    switchMap(moduleId => this.moduleApi.loadOne(moduleId).pipe(
      map(({ module, inputs, outputs }) => StoreDetailsActions.loadOneSuccess(module, inputs, outputs)),
      catchError(() => of(StoreDetailsActions.loadOneError()))
    ))
  ));

  loadModuleVersions$ = createEffect(() => this.actions$.pipe(
    ofType(StoreDetailsActions.init),
    withLatestFrom(this.moduleId$, (_, moduleId) => moduleId),
    switchMap(moduleId => this.moduleApi.loadModuleVersion(moduleId).pipe(
      map((moduleVersions) => StoreDetailsActions.loadModuleVersionSuccess(moduleVersions)),
      catchError(() => of(StoreDetailsActions.loadModuleVersionError()))
    ))
  ));

  deleteModule$ = createEffect(() => this.actions$.pipe(
    ofType(StoreDetailsActions.validateModuleDelete),
    withLatestFrom(this.moduleId$, (_, moduleId) => moduleId),
    switchMap(moduleId => this.moduleApi.delete(moduleId).pipe(
      map(() => StoreDetailsActions.DeleteModuleSuccess(moduleId)),
      catchError((err) => {
        if(err.status === 400 && err.error.code === 1) {
          return of(StoreDetailsActions.DeleteModuleWarning(err.error.workflows))
        } else {
          return of(StoreDetailsActions.DeleteModuleError())
        }
      }),
    ))
  ));

  redirectToError$ = createEffect(() => this.actions$.pipe(
    ofType(StoreDetailsActions.init),
    switchMap(() => this.actions$.pipe(
      ofType(StoreDetailsActions.loadOneError),
      first()
    )),
    tap(() => this.router.navigate(['/', 'error']))
  ), { dispatch: false });

  redirectToStore$ = createEffect(() => this.actions$.pipe(
    ofType(StoreDetailsActions.DeleteModuleSuccess),
    tap(() => this.router.navigate(['/', 'modules', 'store']))
  ), { dispatch: false });

  routeToModule$ = createEffect(() => this.actions$.pipe(
    ofType(StoreDetailsActions.routeToModule),
    switchMap(({ payload }) => this.router.navigate(['/', 'store', payload])),
  ), { dispatch: false });

  notifySuccess$ = createEffect(() => this.actions$.pipe(
    ofType(
      StoreDetailsActions.DeleteModuleSuccess
    ),
    tap(({ notification }) => this.messageService.displaySuccessMessage(notification))
  ), { dispatch: false });

  notifyError$ = createEffect(() => this.actions$.pipe(
    ofType(
      StoreDetailsActions.DeleteModuleError,
    ),
    tap(({ notification }) => this.messageService.displayErrorMessage(notification))
  ), { dispatch: false });
}
