// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { ModuleApi } from '../../infrastructure/services';
import { StoreCatalogActions, StoreListActions } from '../actions';

@Injectable()
export class StoreCatalogEffects {
  constructor(
    private readonly actions$: Actions,
    private readonly moduleApi: ModuleApi
  ) {}

  loadModules$ = createEffect(() => this.actions$.pipe(
    ofType(StoreCatalogActions.init),
    switchMap(() => this.moduleApi.loadAll().pipe(
      map(modules => StoreCatalogActions.loadAllSuccess(modules)),
      catchError(() => of(StoreCatalogActions.loadAllError()))
    ))
  ));

  loadCategories = createEffect(() => this.actions$.pipe(
    ofType(StoreCatalogActions.init),
    switchMap(() => this.moduleApi.loadAllCategories().pipe(
      map(categories => StoreCatalogActions.loadAllCategoriesSuccess(categories)),
      catchError(() => of(StoreCatalogActions.loadAllCategoriesError()))
    ))
  ));

  filterModules$ = createEffect(() => this.actions$.pipe(
    ofType(StoreCatalogActions.filter),
    switchMap(({ payload }) => this.moduleApi.loadAll(payload).pipe(
      map(modules => StoreCatalogActions.loadAllSuccess(modules)),
      catchError(() => of(StoreCatalogActions.loadAllError()))
    ))
  ))
}
