// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { of } from 'rxjs';
import { catchError, map, switchMap, withLatestFrom } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { ModuleApi } from '../../infrastructure/services';
import { StoreListActions } from '../actions';
import { selectCurrentFilter, selectVisibility, StoreState } from '../reducers';
import { Store } from '@ngrx/store';

@Injectable()
export class StoreListEffects {
  constructor(
    private readonly actions$: Actions,
    private readonly moduleApi: ModuleApi,
    private readonly store: Store<StoreState>,
  ) {}

  currentFilter$ = this.store.select(selectCurrentFilter);

  loadModules$ = createEffect(() => this.actions$.pipe(
    ofType(
      StoreListActions.init,
      StoreListActions.refresh
    ),
    withLatestFrom(this.currentFilter$, (_, currentFilter) => ({ currentFilter })),
    switchMap(({ currentFilter }) => this.moduleApi.loadAll(currentFilter).pipe(
      map(modules => StoreListActions.loadAllSuccess(modules)),
      catchError(() => of(StoreListActions.loadAllError()))
    ))
  ));

  loadCategories = createEffect(() => this.actions$.pipe(
    ofType(
      StoreListActions.init,
      StoreListActions.refresh
    ),
    switchMap(() => this.moduleApi.loadAllCategories().pipe(
      map(categories => StoreListActions.loadAllCategoriesSuccess(categories)),
      catchError(() => of(StoreListActions.loadAllCategoriesError()))
    ))
  ));

  filterModules$ = createEffect(() => this.actions$.pipe(
    ofType(StoreListActions.filter),
    switchMap(({ payload }) => this.moduleApi.loadAll(payload).pipe(
      map(modules => StoreListActions.loadAllSuccess(modules)),
      catchError(() => of(StoreListActions.loadAllError()))
    ))
  ))
}
