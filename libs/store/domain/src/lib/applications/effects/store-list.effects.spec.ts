// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';
import { cold, hot } from 'jest-marbles';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { Store } from '@ngrx/store';
import { ModuleApi } from '../../infrastructure/services';
import { Module, ModuleFilter, ModuleKind } from '@ryax/store/domain';
import { StoreCatalogActions, StoreListActions } from '../actions';
import { StoreState } from '../reducers';
import { StoreListEffects } from './store-list.effects';

function provideMockModuleApi() {
  return {
    provide: ModuleApi,
    useValue: {
      loadAll: jest.fn(),
      loadOne: jest.fn(),
      delete: jest.fn(),
    }
  }
}

describe('StoreListEffects', () => {
  let actions$: Observable<any>;
  let effect: StoreListEffects;
  let moduleApi: ModuleApi;
  let store: MockStore<StoreState>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [],
      providers: [
        StoreListEffects,
        provideMockStore({}),
        provideMockActions(() => actions$),
        provideMockModuleApi(),
      ]
    });

    effect = TestBed.get(StoreListEffects);
    moduleApi = TestBed.get(ModuleApi);
    store = TestBed.get(Store);
  });

  describe('loadModules$', () => {
    let modules: Module[];

    beforeEach(() => {
      modules = [{
        id: "module_id_1",
        name: "module_name_1",
        description: "module_description_1",
        version: "module_version_1",
        kind: ModuleKind.PROCESSOR,
      },
        {
          id: "module_id_2",
          name: "module_name_2",
          description: "module_description_2",
          version: "module_version_2",
          kind: ModuleKind.PROCESSOR,
        },];
    });

    it.each([
      StoreListActions.init(),
      StoreListActions.refresh(),
    ])('when load success', (action) => {
      (moduleApi.loadAll as jest.Mock)
        .mockImplementation(() => cold('-a|', { a: modules }));

      actions$ = hot('-a', {
        a: action,
      });

      const expected$ = hot('--a', {
        a: StoreListActions.loadAllSuccess(modules)
      });

      expect(effect.loadModules$).toBeObservable(expected$)
    });

    it.each([
      StoreListActions.init(),
      StoreListActions.refresh(),
    ])('when load failed', (action) => {
      (moduleApi.loadAll as jest.Mock)
        .mockImplementation(() => cold('-#', { a: modules }));

      actions$ = hot('-a', {
        a: action,
      });

      const expected$ = hot('--a', {
        a: StoreListActions.loadAllError()
      });

      expect(effect.loadModules$).toBeObservable(expected$)
    });
  });

  describe('filterModules$', () => {
    let module1: Module;
    let module2: Module;
    let filter: ModuleFilter;

    beforeEach(() => {
      module1 = {
        id: "module_id_1",
        name: "module_name_1",
        description: "module_description_1",
        version: "module_version_1",
        kind: ModuleKind.PROCESSOR,
      };
      module2 = {
        id: "module_id_2",
        name: "module_name_2",
        description: "module_description_2",
        version: "module_version_2",
        kind: ModuleKind.PROCESSOR,
      };
      filter = {
        term:"name_1"
      }
    });

    it('when filter success', () => {
      (moduleApi.loadAll as jest.Mock)
        .mockImplementation(() => cold('-a|', { a: [module1] }));

      actions$ = hot('-a', {
        a: StoreListActions.filter(filter),
      });

      const expected$ = hot('--a', {
        a: StoreListActions.loadAllSuccess([module1])
      });

      expect(effect.filterModules$).toBeObservable(expected$)
    });

    it('when filter failed', () => {
      (moduleApi.loadAll as jest.Mock)
        .mockImplementation(() => cold('-#', { a: [module1] }));

      actions$ = hot('-a', {
        a: StoreListActions.filter(filter),
      });

      const expected$ = hot('--a', {
        a: StoreListActions.loadAllError()
      });

      expect(effect.filterModules$).toBeObservable(expected$)
    });
  });
});
