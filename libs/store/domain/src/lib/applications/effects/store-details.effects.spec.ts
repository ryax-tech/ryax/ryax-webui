// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';
import { cold, hot } from 'jest-marbles';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { Store } from '@ngrx/store';
import { ModuleApi } from '../../infrastructure/services';
import { Module, ModuleIO, ModuleKind } from '@ryax/store/domain';
import { StoreCatalogActions, StoreDetailsActions } from '../actions';
import { selectCurrentModuleId, StoreState } from '../reducers';
import { Router } from '@angular/router';
import { MessageService } from '@ryax/shared/ui-common';
import { StoreDetailsEffects } from './store-details.effects';

function provideMockModuleApi() {
  return {
    provide: ModuleApi,
    useValue: {
      loadAll: jest.fn(),
      loadOne: jest.fn(),
      delete: jest.fn(),
    }
  }
}

function provideMockMessageService() {
  return {
    provide: MessageService,
    useValue: {
      displaySuccessMessage: jest.fn(),
      displayErrorMessage: jest.fn()
    }
  }
}

function provideMockRouter() {
  return {
    provide: Router,
    useValue: {
      navigate: jest.fn(),
    }
  }
}


describe('StoreDetailsEffects', () => {
  let actions$: Observable<any>;
  let effect: StoreDetailsEffects;
  let moduleApi: ModuleApi;
  let store: MockStore<StoreState>;
  let router: Router;
  let messageService: MessageService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [],
      providers: [
        StoreDetailsEffects,
        provideMockStore({}),
        provideMockActions(() => actions$),
        provideMockModuleApi(),
        provideMockMessageService(),
        provideMockRouter(),
      ]
    });

    effect = TestBed.get(StoreDetailsEffects);
    moduleApi = TestBed.get(ModuleApi);
    store = TestBed.get(Store);
    router = TestBed.get(Router);
    messageService = TestBed.get(MessageService);
  });

  describe('loadModule$', () => {
    let module: Module;
    let inputs: ModuleIO[];
    let outputs: ModuleIO[];

    beforeEach(() => {
      module = {
        id: "module_id_1",
        name: "module_name_1",
        description: "module_description_1",
        version: "module_version_1",
        kind: ModuleKind.PROCESSOR,
      };
      inputs = [];
      outputs = [];
    });

    xit('when load success', () => {
      (moduleApi.loadOne as jest.Mock)
        .mockImplementation(() => cold('-a|', { a: module.id }));

      store.overrideSelector(selectCurrentModuleId, "module_id_1");

      actions$ = hot('-a', {
        a: StoreDetailsActions.init(),
      });

      const expected$ = hot('--a', {
        a: StoreDetailsActions.loadOneSuccess(module, inputs, outputs)
      });

      expect(effect.loadModule$).toBeObservable(expected$)
    });

    it('when load failed', () => {
      (moduleApi.loadOne as jest.Mock)
        .mockImplementation(() => cold('-#', { a: module.id }));

      store.overrideSelector(selectCurrentModuleId, "module_id_1");

      actions$ = hot('-a', {
        a: StoreDetailsActions.init(),
      });

      const expected$ = hot('--a', {
        a: StoreDetailsActions.loadOneError()
      });

      expect(effect.loadModule$).toBeObservable(expected$)
    });
  });

  describe('deleteModule$', () => {
    let module: Module;

    beforeEach(() => {
      module = {
        id: "module_id_1",
        name: "module_name_1",
        description: "module_description_1",
        version: "module_version_1",
        kind: ModuleKind.PROCESSOR,
      };
    });

    it('when delete success', () => {
      (moduleApi.delete as jest.Mock)
        .mockImplementation(() => cold('-a|', { a: module.id }));

      store.overrideSelector(selectCurrentModuleId, "module_id_1");

      actions$ = hot('-a', {
        a: StoreDetailsActions.validateModuleDelete(),
      });

      const expected$ = hot('--a', {
        a: StoreDetailsActions.DeleteModuleSuccess(module.id)
      });

      expect(effect.deleteModule$).toBeObservable(expected$)
    });

    it('when delete failed', () => {
      (moduleApi.delete as jest.Mock)
        .mockImplementation(() => cold('-#', { a: module.id }));

      store.overrideSelector(selectCurrentModuleId, "module_id_1");

      actions$ = hot('-a', {
        a: StoreDetailsActions.validateModuleDelete(),
      });

      const expected$ = hot('--a', {
        a: StoreDetailsActions.DeleteModuleError()
      });

      expect(effect.deleteModule$).toBeObservable(expected$)
    });
  });

  xit('redirectToError$', () => {
    actions$ = hot('-a', {
      a: StoreDetailsActions.loadOneError(),
    });
    expect(effect.redirectToError$).toSatisfyOnFlush(() => {
      expect(router.navigate).toHaveBeenCalledWith(['/', 'error'])
    });
  });

  it('redirectToStore$', () => {
    actions$ = hot('-a', {
      a: StoreDetailsActions.DeleteModuleSuccess("moduleId"),
    });
    expect(effect.redirectToStore$).toSatisfyOnFlush(() => {
      expect(router.navigate).toHaveBeenCalledWith(['/', 'modules', 'store'])
    });
  });

  describe('notifySuccess$', () => {
    let deleteSuccess: string;

    beforeEach(() => {
      deleteSuccess = "Module deleted successfully";
    });

    it('notification delete success', () => {
      actions$ = hot('-a', {
        a: StoreDetailsActions.DeleteModuleSuccess("moduleId"),
      });
      expect(effect.notifySuccess$).toSatisfyOnFlush(() => {
        expect(messageService.displaySuccessMessage).toHaveBeenCalledWith(deleteSuccess)
      });
    });
  });

  describe('notifyError$', () => {
    let deleteFailed: string;

    beforeEach(() => {
      deleteFailed = "Module delete failed";
    });

    it('notification delete failed', () => {
      actions$ = hot('-a', {
        a: StoreDetailsActions.DeleteModuleError(),
      });
      expect(effect.notifyError$).toSatisfyOnFlush(() => {
        expect(messageService.displayErrorMessage).toHaveBeenCalledWith(deleteFailed)
      });
    });
  });

});
