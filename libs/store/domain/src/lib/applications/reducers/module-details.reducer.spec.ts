// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import * as fromModule from './module-details.reducer';
import { StoreDetailsActions } from '../actions';

describe('ModuleDetailsReducer', () => {

  it('on store details open delete modal', () => {
    const state: fromModule.State = fromModule.initialState;
    const action = StoreDetailsActions.openDeleteModuleModal();
    expect(fromModule.reducer(state, action)).toEqual({
      ...state,
      deleteModalVisible: true,
    });
  });

  it('on store details validate delete', () => {
    const state: fromModule.State = fromModule.initialState;
    const action = StoreDetailsActions.validateModuleDelete();
    expect(fromModule.reducer(state, action)).toEqual({
      ...state,
      deleteModalVisible: false,
    });
  });

  it('on store details cancel delete', () => {
    const state: fromModule.State = fromModule.initialState;
    const action = StoreDetailsActions.cancelModuleDelete();
    expect(fromModule.reducer(state, action)).toEqual({
      ...state,
      deleteModalVisible: false,
    });
  });

});
