// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import * as fromModuleIO from './module-io.reducer';
import { StoreDetailsActions } from '../actions';
import { Module, ModuleIO, ModuleIOType, ModuleKind } from '@ryax/store/domain';

describe('ModuleIoReducer', () => {

  xit('on module api load one success', () => {
    const module: Module = {
      id: "module_id",
      name: "module_name",
      description: "module_description",
      version: "1.0",
      kind: ModuleKind.PROCESSOR
    };
    const inputs: ModuleIO[] = [{
      id: "inputs_id",
      technicalName: "inputs_technicalName",
      displayName: "inputs_displayName",
      help: "inputs_help",
      type: ModuleIOType.DIRECTORY,
      enumValues: [],
      inputForModuleId: "inputs_inputForModuleId",
      outputForModuleId: "inputs_outputForModuleId",
    }];
    const outputs: ModuleIO[] = [{
      id: "outputs_id",
      technicalName: "outputs_technicalName",
      displayName: "outputs_displayName",
      help: "outputs_help",
      type: ModuleIOType.DIRECTORY,
      enumValues: [],
      inputForModuleId: "outputs_inputForModuleId",
      outputForModuleId: "outputs_outputForModuleId",
    }];
    const state: fromModuleIO.State = fromModuleIO.initialState;
    const action = StoreDetailsActions.loadOneSuccess(module, inputs, outputs);
    expect(fromModuleIO.reducer(state, action)).toEqual({
      ...state,
      loading: false,
      ids: [module.id],
      entities: {
        [module.id]: module, inputs, outputs
      }
    });
  });
});
