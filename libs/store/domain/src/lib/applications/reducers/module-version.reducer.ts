// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { ModuleVersion } from '@ryax/store/domain';
import { createReducer, on } from '@ngrx/store';
import { StoreDetailsActions } from '../actions';


export interface State extends EntityState<ModuleVersion> {
  loading: boolean;
  moduleVersions: ModuleVersion[];
}

export const adapter = createEntityAdapter<ModuleVersion>({
  selectId: moduleVersion => moduleVersion.moduleId
});

export const initialState: State = adapter.getInitialState({
  loading: null,
  moduleVersions: null,
});

export const reducer = createReducer<State>(
  initialState,
  on(StoreDetailsActions.loadModuleVersionSuccess, (state, { payload }) => adapter.upsertMany(payload, {
    ...state,
    loading: false,
    moduleVersions: payload
  })),
  on(StoreDetailsActions.loadModuleVersionError, (state) => ({
    ...state,
    loading: false
  })),
);

export const selectLoading = (state: State) => state.loading;
export const selectAllModuleVersions = (state: State) => state.moduleVersions;

