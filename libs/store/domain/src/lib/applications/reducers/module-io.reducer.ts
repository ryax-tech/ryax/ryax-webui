// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { ModuleIO } from '@ryax/store/domain';
import { createReducer, on } from '@ngrx/store';
import { StoreDetailsActions } from '../actions';

export type State = EntityState<ModuleIO>

export const adapter = createEntityAdapter<ModuleIO>({
  selectId: moduleIO => moduleIO.id
});

export const initialState: State = adapter.getInitialState();

export const reducer = createReducer<State>(
  initialState,
  on(StoreDetailsActions.loadOneSuccess, (state, { payload }) => adapter.upsertMany([
    ...payload.inputs,
    ...payload.outputs
  ], state)),
);


const {
  selectAll,
} = adapter.getSelectors();


export const selectInputsByModuleId = (state: State, moduleId: string) => selectAll(state).filter(item => item.inputForModuleId === moduleId);
export const selectOutputsByModuleId = (state: State, moduleId: string) => selectAll(state).filter(item => item.outputForModuleId === moduleId);
