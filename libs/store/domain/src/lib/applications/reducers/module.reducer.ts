// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import { Module, ModuleCategory, ModuleFilter } from '../../entities';
import { StoreCatalogActions, StoreDetailsActions, StoreListActions } from '../actions';

export interface State extends EntityState<Module> {
  loading: boolean;
  visibility: boolean;
  categories: ModuleCategory[];
  currentFilter: ModuleFilter;
}

export const adapter = createEntityAdapter<Module>({
  selectId: module => module.id
});

export const initialState: State = adapter.getInitialState({
  loading: false,
  visibility: false,
  categories: null,
  currentFilter: null,
});

export const reducer = createReducer<State>(
  initialState,
  on(StoreListActions.init, StoreListActions.refresh, StoreListActions.filter, (state) => ({
    ...state,
    loading: true
  })),
  on(StoreListActions.loadAllSuccess, (state, { payload }) => adapter.setAll(payload, {
    ...state,
    loading: false
  })),
  on(StoreListActions.loadAllError, (state) => ({
    ...state,
    loading: false
  })),
  on(StoreCatalogActions.init, (state) => ({
    ...state,
    visibility: false
  })),
  on(StoreCatalogActions.init, StoreCatalogActions.filter, (state) => ({
    ...state,
    loading: true
  })),
  on(StoreCatalogActions.loadAllSuccess, (state, { payload }) => adapter.setAll(payload, {
    ...state,
    loading: false
  })),
  on(StoreCatalogActions.loadAllError, (state) => ({
    ...state,
    loading: false
  })),
  on(StoreDetailsActions.init, StoreDetailsActions.refresh, (state) => ({
    ...state,
    loading: true
  })),
  on(StoreDetailsActions.loadOneSuccess, (state, { payload }) => adapter.upsertOne(payload.module, {
    ...state,
    loading: false
  })),
  on(StoreDetailsActions.loadOneError, (state) => ({
    ...state,
    loading: false
  })),
  on(StoreDetailsActions.DeleteModuleSuccess, (state, { payload }) => adapter.removeOne(payload, {
    ...state,
    loading: false
  })),
  on(StoreListActions.loadAllCategoriesSuccess, StoreCatalogActions.loadAllCategoriesSuccess, (state, { payload }) => ({
    ...state,
    categories: payload
  })),
  on(StoreCatalogActions.toggleVisibility, (state, { payload }) => ({
    ...state,
    visibility: payload
  })),
  on(StoreListActions.filter, StoreCatalogActions.filter, (state, { payload }) => ({
    ...state,
    currentFilter: payload
  })),
);


const {
  selectAll,
  selectEntities
} = adapter.getSelectors();


export const selectLoading = (state: State) => state.loading;
export const selectModuleById = (state: State, moduleId: string) => selectEntities(state)[moduleId];
export const selectModules = selectAll;
export const selectCategories = (state: State) => state.categories;
export const selectVisibility = (state: State) => state.visibility;
export const selectCurrentFilter = (state: State) => state.currentFilter;
