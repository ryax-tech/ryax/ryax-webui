// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import { Module, Workflow } from '../../entities';
import { StoreDetailsActions } from '../actions';

export interface State extends EntityState<Module> {
  deleteModalVisible: boolean;
  warningModalVisible: boolean;
  workflowListWithUndeletableModule: Workflow[];
}

export const adapter = createEntityAdapter<Module>({
  selectId: module => module.id
});

export const initialState: State = adapter.getInitialState({
  deleteModalVisible: false,
  warningModalVisible: false,
  workflowListWithUndeletableModule: null,
});

export const reducer = createReducer<State>(
  initialState,
  on(StoreDetailsActions.openDeleteModuleModal, (state) => ({
    ...state,
    deleteModalVisible: true
  })),
  on(StoreDetailsActions.validateModuleDelete, (state) => ({
    ...state,
    deleteModalVisible: false
  })),
  on(StoreDetailsActions.cancelModuleDelete, (state) => ({
    ...state,
    deleteModalVisible: false
  })),
  on(StoreDetailsActions.DeleteModuleWarning, (state, {payload}) => ({
    ...state,
    warningModalVisible: true,
    workflowListWithUndeletableModule: payload.workflowListWithUndeletableModule,
  })),
  on(StoreDetailsActions.closeWarningModal, (state) => ({
    ...state,
    warningModalVisible: false
  })),
);

export const selectDeleteModalVisible = (state: State) => state.deleteModalVisible;
export const selectWarningModalVisible = (state: State) => state.warningModalVisible;
export const selectWorkflowListWithUndeletableModule = (state: State) => state.workflowListWithUndeletableModule;

