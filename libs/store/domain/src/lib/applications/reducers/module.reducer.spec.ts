// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import * as fromModule from './module.reducer';
import { StoreCatalogActions, StoreDetailsActions, StoreListActions } from '../actions';
import { Module, ModuleFilter, ModuleKind } from '@ryax/store/domain';

describe('ModuleReducer', () => {

  it('on StoreListActions module list init', () => {
    const state: fromModule.State = fromModule.initialState;
    const action = StoreListActions.init();
    expect(fromModule.reducer(state, action)).toEqual({
      ...state,
      loading: true,
    });
  });

  it('on StoreListActions module list refresh', () => {
    const state: fromModule.State = fromModule.initialState;
    const action = StoreListActions.refresh();
    expect(fromModule.reducer(state, action)).toEqual({
      ...state,
      loading: true,
    });
  });

  it('on StoreListActions module list filter', () => {
    const filterData: ModuleFilter = {
      term: "search term",
    };
    const state: fromModule.State = fromModule.initialState;
    const action = StoreListActions.filter(filterData);
    expect(fromModule.reducer(state, action)).toEqual({
      ...state,
      loading: true,
      //filter: filterData
    });
  });

  it('on StoreListActions load all success', () => {
    const module1: Module = {
      id: "module1_id",
      name: "module1_name",
      description: "module1_description",
      version: "1.0",
      kind: ModuleKind.PROCESSOR
    };
    const module2: Module = {
      id: "module2_id",
      name: "module2_name",
      description: "module2_description",
      version: "2.0",
      kind: ModuleKind.PROCESSOR
    };
    const state: fromModule.State = fromModule.initialState;
    const action = StoreListActions.loadAllSuccess([module1, module2]);
    expect(fromModule.reducer(state, action)).toEqual({
      ...state,
      ids: [module1.id, module2.id],
      entities: {
        [module1.id]: module1,
        [module2.id]: module2,
      },
      loading: false,
    });
  });

  it('on StoreListActions load all error', () => {
    const state: fromModule.State = fromModule.initialState;
    const action = StoreListActions.loadAllError();
    expect(fromModule.reducer(state, action)).toEqual({
      ...state,
      loading: false,
    });
  });

  it('on StoreCatalogActions init', () => {
    const state: fromModule.State = fromModule.initialState;
    const action = StoreCatalogActions.init();
    expect(fromModule.reducer(state, action)).toEqual({
      ...state,
      loading: true,
    });
  });

  it('on StoreCatalogActions filter', () => {
    const filterData: ModuleFilter = {
      term: "search term",
    };
    const state: fromModule.State = fromModule.initialState;
    const action = StoreCatalogActions.filter(filterData);
    expect(fromModule.reducer(state, action)).toEqual({
      ...state,
      loading: true,
      //filter: filterData
    });
  });

  it('on StoreCatalogActions load all success', () => {
    const module1: Module = {
      id: "module1_id",
      name: "module1_name",
      description: "module1_description",
      version: "1.0",
      kind: ModuleKind.PROCESSOR
    };
    const module2: Module = {
      id: "module2_id",
      name: "module2_name",
      description: "module2_description",
      version: "2.0",
      kind: ModuleKind.PROCESSOR
    };
    const state: fromModule.State = fromModule.initialState;
    const action = StoreCatalogActions.loadAllSuccess([module1, module2]);
    expect(fromModule.reducer(state, action)).toEqual({
      ...state,
      ids: [module1.id, module2.id],
      entities: {
        [module1.id]: module1,
        [module2.id]: module2,
      },
      loading: false,
    });
  });

  it('on StoreCatalogActions load all error', () => {
    const state: fromModule.State = fromModule.initialState;
    const action = StoreCatalogActions.loadAllError();
    expect(fromModule.reducer(state, action)).toEqual({
      ...state,
      loading: false,
    });
  });

  it('on modules details init', () => {
    const state: fromModule.State = fromModule.initialState;
    const action = StoreDetailsActions.init();
    expect(fromModule.reducer(state, action)).toEqual({
      ...state,
      loading: true,
    });
  });

  it('on StoreDetailsActions refresh', () => {
    const state: fromModule.State = fromModule.initialState;
    const action = StoreDetailsActions.refresh();
    expect(fromModule.reducer(state, action)).toEqual({
      ...state,
      loading: true,
    });
  });

  it('on StoreDetailsActions load one success', () => {
    const module1: Module = {
      id: "module1_id",
      name: "module1_name",
      description: "module1_description",
      version: "1.0",
      kind: ModuleKind.PROCESSOR
    };
    const state: fromModule.State = fromModule.initialState;
    const action = StoreDetailsActions.loadOneSuccess(module1, [], []);
    expect(fromModule.reducer(state, action)).toEqual({
      ...state,
      loading: false,
      ids: [module1.id],
      entities: {
        [module1.id]: module1
      }
    });
  });

  it('on StoreDetailsActions load one error', () => {
    const state: fromModule.State = fromModule.initialState;
    const action = StoreDetailsActions.loadOneError();
    expect(fromModule.reducer(state, action)).toEqual({
      ...state,
      loading: false,
    });
  });

  it('on StoreDetailsActions delete module success', () => {
    const moduleId = "moduleId";
    const state: fromModule.State = fromModule.initialState;
    const action = StoreDetailsActions.DeleteModuleSuccess(moduleId);
    expect(fromModule.reducer(state, action)).toEqual({
      ...state,
      loading: false,
    });
  });

});
