// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { InjectionToken } from '@angular/core';
import { ActionReducerMap, createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromModule from './module.reducer';
import * as fromModuleIO from './module-io.reducer';
import * as fromModuleDetails from './module-details.reducer';
import * as fromModuleVersions from './module-version.reducer';
import { NavigationQueries } from '@ryax/domain/navigation';
import { selectModuleById } from './module.reducer';

export const StoreFeatureKey = 'storeDomain';

export interface StoreState {
  modules: fromModule.State
  moduleDetails: fromModuleDetails.State
  moduleIOS: fromModuleIO.State
  moduleVersions: fromModuleVersions.State
}

export const StoreReducerToken = new InjectionToken<ActionReducerMap<StoreState>>(StoreFeatureKey);

export const StoreReducerProvider = {
  provide: StoreReducerToken,
  useValue: {
    modules: fromModule.reducer,
    moduleDetails: fromModuleDetails.reducer,
    moduleIOS: fromModuleIO.reducer,
    moduleVersions: fromModuleVersions.reducer
  },
};


export const selectFeatureState = createFeatureSelector<StoreState>(StoreFeatureKey);
export const selectModulesState = createSelector(selectFeatureState, state => state.modules);
export const selectModuleIOState = createSelector(selectFeatureState, state => state.moduleIOS);
export const selectModuleDetailsState = createSelector(selectFeatureState, state => state.moduleDetails);
export const selectModuleVersionsState = createSelector(selectFeatureState, state => state.moduleVersions);

export const selectCurrentModuleId = createSelector(NavigationQueries.selectParams, navParams => navParams && navParams['moduleId']);
/* ModulesState */
export const selectLoading = createSelector(selectModulesState, fromModule.selectLoading);
export const selectModules = createSelector(selectModulesState, fromModule.selectModules);
export const selectCurrentModule = createSelector(selectModulesState, selectCurrentModuleId, selectModuleById);
export const selectCurrentModuleVersions = createSelector(selectModuleVersionsState, fromModuleVersions.selectAllModuleVersions);
export const selectCategories = createSelector(selectModulesState, fromModule.selectCategories);
export const selectVisibility = createSelector(selectModulesState, fromModule.selectVisibility);
export const selectCurrentFilter = createSelector(selectModulesState, fromModule.selectCurrentFilter);

/* ModuleIOState */
export const selectCurrentModuleInputs = createSelector(selectModuleIOState, selectCurrentModuleId, fromModuleIO.selectInputsByModuleId);
export const selectCurrentModuleOutputs = createSelector(selectModuleIOState, selectCurrentModuleId, fromModuleIO.selectOutputsByModuleId);

/* ModuleDetailsState */
export const selectDetailsDeleteModalVisible = createSelector(selectModuleDetailsState, fromModuleDetails.selectDeleteModalVisible);
export const selectDetailsWarningModalVisible = createSelector(selectModuleDetailsState, fromModuleDetails.selectWarningModalVisible);

export const selectWorkflows = createSelector(selectModuleDetailsState, fromModuleDetails.selectWorkflowListWithUndeletableModule);
