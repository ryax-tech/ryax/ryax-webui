// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import {
  Module,
  ModuleFilter,
  ModuleKind,
  StoreListFacade,
} from '@ryax/store/domain';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import {
  selectModules,
  StoreState,
} from '../reducers';
import { TestBed } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { cold } from 'jest-marbles';
import { StoreListActions } from '../actions';

describe('StoreListFacade', () => {
  let facade: StoreListFacade;
  let store: MockStore<StoreState>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [],
      providers: [
        StoreListFacade,
        provideMockStore({})
      ]
    });

    facade = TestBed.get(StoreListFacade);
    store = TestBed.get(Store);
  });

  it('modules$', () => {
    const modules: Module[] = [{
      id: "id",
      name: "name",
      description: "description",
      version: "1.0",
      kind: ModuleKind.PROCESSOR,
    }];
    store.overrideSelector(selectModules, null);
    selectModules.setResult(modules);
    const expected = cold('a-', { a: modules });
    expect(facade.modules$).toBeObservable(expected);
  });

  it('init', () => {
    spyOn(store, 'dispatch');
    facade.init();
    expect(store.dispatch).toHaveBeenCalledWith(StoreListActions.init())
  });

  it('refresh', () => {
    spyOn(store, 'dispatch');
    facade.refresh();
    expect(store.dispatch).toHaveBeenCalledWith(StoreListActions.refresh())
  });

  it('filter', () => {
    const filter: ModuleFilter = {
      term: "filter"
    };
    spyOn(store, 'dispatch');
    facade.filter(filter);
    expect(store.dispatch).toHaveBeenCalledWith(StoreListActions.filter(filter))
  });
});
