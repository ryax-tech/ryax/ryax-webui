// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { selectLoading, StoreState, selectModules, selectCategories, selectVisibility } from '../reducers';
import { StoreCatalogActions } from '../actions';
import { ModuleFilter } from '../../entities';

@Injectable()
export class StoreCatalogFacade {
  loading$ = this.store.select(selectLoading);
  modules$ = this.store.select(selectModules);
  categories$ = this.store.select(selectCategories);
  visibility$ = this.store.select(selectVisibility);

  constructor(
    private readonly store: Store<StoreState>
  ) {}

  init() {
    this.store.dispatch(StoreCatalogActions.init())
  }

  filter(data: ModuleFilter) {
    this.store.dispatch(StoreCatalogActions.filter(data))
  }

  toggleVisibility(visibility: boolean): void {
    this.store.dispatch(StoreCatalogActions.toggleVisibility(visibility))
  }
}
