// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import {
  selectCurrentModule,
  selectCurrentModuleInputs,
  selectCurrentModuleOutputs,
  selectDetailsDeleteModalVisible,
  selectDetailsWarningModalVisible,
  selectLoading,
  selectWorkflows,
  selectCurrentModuleVersions,
  StoreState,
} from '../reducers';
import { StoreDetailsActions } from '../actions';

@Injectable()
export class StoreDetailsFacade {
  loading$ = this.store.select(selectLoading);
  module$ = this.store.select(selectCurrentModule);
  moduleVersions$ = this.store.select(selectCurrentModuleVersions);
  moduleInputs$ = this.store.select(selectCurrentModuleInputs);
  moduleOutputs$ = this.store.select(selectCurrentModuleOutputs);
  workflows$ = this.store.select(selectWorkflows);
  deleteModalVisible$ = this.store.select(selectDetailsDeleteModalVisible);
  warningModalVisible$ = this.store.select(selectDetailsWarningModalVisible);

  constructor(
    private readonly store: Store<StoreState>
  ) {}

  init() {
    this.store.dispatch(StoreDetailsActions.init());
  }

  openDeleteModuleModal () {
    this.store.dispatch(StoreDetailsActions.openDeleteModuleModal());
  }

  validateModuleDelete () {
    this.store.dispatch(StoreDetailsActions.validateModuleDelete());
  }

  cancelModuleDelete () {
    this.store.dispatch(StoreDetailsActions.cancelModuleDelete());
  }

  closeWarningModal () {
    this.store.dispatch(StoreDetailsActions.closeWarningModal());
  }

  routeToModule (moduleId: string) {
    this.store.dispatch(StoreDetailsActions.routeToModule(moduleId));
  }
}
