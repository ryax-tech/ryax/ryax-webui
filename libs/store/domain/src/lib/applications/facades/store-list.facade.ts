// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { ModuleFilter } from '../../entities';
import { selectLoading, selectModules, selectCategories, StoreState } from '../reducers';
import { StoreListActions } from '../actions';

@Injectable()
export class StoreListFacade {
  loading$ = this.store.select(selectLoading);
  modules$ = this.store.select(selectModules);
  categories$ = this.store.select(selectCategories);

  constructor(
    private readonly store: Store<StoreState>
  ) {}

  init() {
    this.store.dispatch(StoreListActions.init())
  }

  refresh() {
    this.store.dispatch(StoreListActions.refresh())
  }

  filter(data: ModuleFilter) {
    this.store.dispatch(StoreListActions.filter(data))
  }
}
