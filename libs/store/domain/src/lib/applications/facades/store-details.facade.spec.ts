// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Module, ModuleIO, ModuleIOType, ModuleKind, StoreDetailsFacade } from '@ryax/store/domain';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import {
  selectCurrentModule,
  selectCurrentModuleInputs,
  selectCurrentModuleOutputs, selectDetailsDeleteModalVisible,
  StoreState,
} from '../reducers';
import { TestBed } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { cold } from 'jest-marbles';
import { StoreDetailsActions } from '../actions';

describe('StoreDetailsFacade', () => {
  let facade: StoreDetailsFacade;
  let store: MockStore<StoreState>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [],
      providers: [
        StoreDetailsFacade,
        provideMockStore({})
      ]
    });

    facade = TestBed.get(StoreDetailsFacade);
    store = TestBed.get(Store);
  });

  it('modules$', () => {
    const module: Module = {
      id: "id",
      name: "name",
      description: "description",
      version: "1.0",
      kind: ModuleKind.PROCESSOR,
    };
    store.overrideSelector(selectCurrentModule, null);
    selectCurrentModule.setResult(module);
    const expected = cold('a-', { a: module });
    expect(facade.module$).toBeObservable(expected);
  });

  it('moduleInputs$', () => {
    const moduleInputs: ModuleIO[] = [{
      id: "id",
      technicalName: "technicalName",
      displayName: "displayName",
      help: "help",
      type: ModuleIOType.DIRECTORY,
      enumValues: [],
      inputForModuleId: "inputForModuleId",
      outputForModuleId: "outputForModuleId",
    }];
    store.overrideSelector(selectCurrentModuleInputs, null);
    selectCurrentModuleInputs.setResult(moduleInputs);
    const expected = cold('a-', { a: moduleInputs });
    expect(facade.moduleInputs$).toBeObservable(expected);
  });

  it('moduleOutputs$', () => {
    const moduleOutputs: ModuleIO[] = [{
      id: "id",
      technicalName: "technicalName",
      displayName: "displayName",
      help: "help",
      type: ModuleIOType.DIRECTORY,
      enumValues: [],
      inputForModuleId: "inputForModuleId",
      outputForModuleId: "outputForModuleId",
    }];
    store.overrideSelector(selectCurrentModuleOutputs, null);
    selectCurrentModuleOutputs.setResult(moduleOutputs);
    const expected = cold('a-', { a: moduleOutputs });
    expect(facade.moduleOutputs$).toBeObservable(expected);
  });

  it('deleteModalVisible$', () => {
    const modalIsVisible = false;
    store.overrideSelector(selectDetailsDeleteModalVisible, null);
    selectDetailsDeleteModalVisible.setResult(modalIsVisible);
    const expected = cold('a-', { a: modalIsVisible });
    expect(facade.deleteModalVisible$).toBeObservable(expected);
  });

  it('init', () => {
    spyOn(store, 'dispatch');
    facade.init();
    expect(store.dispatch).toHaveBeenCalledWith(StoreDetailsActions.init())
  });

  it('openDeleteModuleModal', () => {
    spyOn(store, 'dispatch');
    facade.openDeleteModuleModal();
    expect(store.dispatch).toHaveBeenCalledWith(StoreDetailsActions.openDeleteModuleModal())
  });

  it('validateModuleDelete', () => {
    spyOn(store, 'dispatch');
    facade.validateModuleDelete();
    expect(store.dispatch).toHaveBeenCalledWith(StoreDetailsActions.validateModuleDelete())
  });

  it('cancelModuleDelete', () => {
    spyOn(store, 'dispatch');
    facade.cancelModuleDelete();
    expect(store.dispatch).toHaveBeenCalledWith(StoreDetailsActions.cancelModuleDelete())
  });
});
