// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createAction } from '@ngrx/store';
import { Module, ModuleIO, ModuleVersion, Workflow } from '@ryax/store/domain';

export const init = createAction(
  '[Store Details] Init'
);

export const refresh = createAction(
  '[Store Details] Init'
);

export const loadOneSuccess = createAction(
  '[Store Details] Load one success',
  (module: Module, inputs: ModuleIO[], outputs: ModuleIO[]) => ({
    payload: { module, inputs, outputs }
  })
);

export const loadOneError = createAction(
  '[Store Details] Load one error'
);

export const DeleteModuleSuccess = createAction(
  '[Store Details] Delete module success',
  (moduleId: string) => ({
    payload: moduleId,
    notification: "Module deleted successfully"
  })
);

export const DeleteModuleError = createAction(
  '[Store Details] Delete module error',
  () => ({
    notification: "Module delete failed"
  })
);

export const DeleteModuleWarning = createAction(
  '[Store Details] Delete module warning',
  (workflowListWithUndeletableModule: Workflow[]) => ({
    payload: { workflowListWithUndeletableModule },
  })
);

export const closeWarningModal = createAction(
  '[Store Details] Close warning modal'
);

export const openDeleteModuleModal = createAction(
  '[Store Details] Open modal to delete module'
);

export const validateModuleDelete = createAction(
  '[Store Details] Validate module delete'
);

export const cancelModuleDelete = createAction(
  '[Store Details] Cancel module delete'
);

export const loadModuleVersionSuccess = createAction(
  '[Store Details] Load module versions success',
  (versions: ModuleVersion[])=> ({
    payload: versions
  })
);

export const loadModuleVersionError = createAction(
  '[Store Details] Load module versions error',
);

export const routeToModule = createAction(
  '[Store Details] Route to module version',
  (moduleId: string) => ({
    payload: moduleId
  })
);
