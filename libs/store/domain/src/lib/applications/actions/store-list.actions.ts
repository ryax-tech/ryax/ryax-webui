// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createAction } from '@ngrx/store';
import { Module, ModuleCategory, ModuleFilter } from '@ryax/store/domain';

export const init = createAction(
  '[Store List] Init'
);

export const refresh = createAction(
  '[Store List] Init'
);

export const filter = createAction(
  '[Store List] Filter',
  (data: ModuleFilter) => ({
    payload: data
  })
);

export const loadAllSuccess = createAction(
  '[Store List] Load all success',
  (modules: Module[]) => ({
    payload: modules
  })
);

export const loadAllError = createAction(
  '[Store List] Load all error'
);

export const loadAllCategoriesSuccess = createAction(
  '[Store List] Load all categories success',
  (categories: ModuleCategory[]) => ({
    payload: categories
  })
);

export const loadAllCategoriesError = createAction(
  '[Store List] Load all categories error'
);
