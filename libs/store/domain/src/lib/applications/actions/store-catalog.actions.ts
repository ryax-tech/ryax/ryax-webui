// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createAction } from '@ngrx/store';
import { Module, ModuleCategory, ModuleFilter } from '@ryax/store/domain';

export const init = createAction(
  '[Store Catalog] Init'
);

export const filter = createAction(
  '[Store Catalog] Filter',
  (data: ModuleFilter) => ({
    payload: data
  })
);

export const loadAllSuccess = createAction(
  '[Store Catalog] Load all success',
  (modules: Module[]) => ({
    payload: modules
  })
);

export const loadAllError = createAction(
  '[Store Catalog] Load all error'
);

export const loadAllCategoriesSuccess = createAction(
  '[Store Catalog] Load all categories success',
  (categories: ModuleCategory[]) => ({
    payload: categories
  })
);

export const loadAllCategoriesError = createAction(
  '[Store Catalog] Load all categories error'
);

export const toggleVisibility = createAction(
  '[Store Catalog] Toggle workflow module filter visibility',
  (visibility: boolean) => ({
    payload: visibility
  })
);
