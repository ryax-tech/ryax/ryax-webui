// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { ModuleVersion } from '@ryax/store/domain';
import { ModuleVersionDto } from '../dtos';

@Injectable()
export class ModuleVersionAdapter {
  adapt(dto: ModuleVersionDto): ModuleVersion {
    return {
      moduleId: dto.id,
      versionName: dto.version,
    };
  }
}
