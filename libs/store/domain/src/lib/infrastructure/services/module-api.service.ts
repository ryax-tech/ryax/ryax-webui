// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import {
  Module,
  ModuleCategory, ModuleDetails,
  ModuleFilter,
  ModuleIO,
  ModuleIOType,
  ModuleKind,
  Workflow,
} from '../../entities';
import { ModuleCategoryDto, ModuleDetailsDto, ModuleDto, ModuleIODto, ModuleVersionDto, WorkflowDto } from '../dtos';
import { ModuleVersionAdapter } from '../adapters';

@Injectable()
export class ModuleApi {
  private baseUrl = "/api/studio/modules";

  constructor(
    private readonly http: HttpClient,
    private readonly moduleVersionAdapter: ModuleVersionAdapter
  ) {}

  private toModule(dto: ModuleDto): Module {
    return {
      id: dto.id,
      name: dto.name,
      version: dto.version,
      description: dto.description,
      kind: dto.kind as ModuleKind,
      technicalName: dto.technical_name,
      technicalId: dto.technical_name+'-'+dto.version,
    }
  }

  private toModuleDetails(dto: ModuleDetailsDto, moduleId: string): ModuleDetails {
    return {
      id: dto.id,
      name: dto.name,
      version: dto.version,
      description: dto.description,
      kind: dto.kind as ModuleKind,
      technicalName: dto.technical_name,
      technicalId: dto.technical_name+'-'+dto.version,
      categories: dto.categories.map(item => this.toCategory(item)),
      inputs: dto.inputs.map(item => this.toModuleIO(item, moduleId, null)),
      outputs: dto.outputs.map(item => this.toModuleIO(item, null, moduleId)),
    }
  }

  private toModuleIO(dto: ModuleIODto, inputForModuleId: string = null, outputForModuleId: string = null): ModuleIO {
    return {
      id: dto.id,
      technicalName: dto.technical_name,
      displayName: dto.display_name,
      type: dto.type as ModuleIOType,
      help: dto.help,
      enumValues: dto.enum_values,
      inputForModuleId,
      outputForModuleId,
    }
  }

  private toCategory(dto: ModuleCategoryDto): ModuleCategory {
    return {
      categoryId: dto.id,
      categoryName: dto.name
    }
  }

  private toWorkflow(dto: WorkflowDto): Workflow {
    return {
      id: dto.id,
      name: dto.name
    }
  }

  loadAll(filter: ModuleFilter = null) {
    let params = new HttpParams();
    if(filter && filter.term) {
      params = params.set("search", filter.term);
    }
    if(filter && filter.category) {
      params = params.set("category", filter.category);
    }

    return this.http.get<ModuleDto[]>(this.baseUrl, { responseType: 'json', params }).pipe(
      map(dtos => dtos.map(item => this.toModule(item)))
    );
  }

  loadOne(moduleId: string) {
    const url = [this.baseUrl, moduleId].join('/');
    return this.http.get<ModuleDetailsDto>(url, { responseType: 'json' }).pipe(
      map(dto => ({
        module: this.toModuleDetails(dto, moduleId),
        inputs: dto.inputs.map(item => this.toModuleIO(item, moduleId, null)),
        outputs: dto.outputs.map(item => this.toModuleIO(item, null, moduleId)),
      }))
    );
  }

  loadModuleVersion(moduleId: string) {
    const url = [this.baseUrl, moduleId, "list_versions"].join('/');
    return this.http.get<ModuleVersionDto[]>(url, { responseType: 'json' }).pipe(
      map(dtos => dtos.map(item => this.moduleVersionAdapter.adapt(item)))
    );
  }

  delete(moduleId: string) {
    const url = [this.baseUrl, moduleId].join('/');
    return this.http.delete<void>(url, { responseType: 'json' });
  }

  loadAllCategories() {
    const url = [this.baseUrl, "list_categories"].join('/');
    return this.http.get<ModuleCategoryDto[]>(url, { responseType: 'json' }).pipe(
      map(dtos => dtos.map(item => this.toCategory(item)))
    );
  }
}
