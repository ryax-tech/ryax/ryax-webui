// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { ModuleApi } from './module-api.service';
import { ModuleDto } from '../models';
import { ModuleFilter } from '@ryax/store/domain';

describe("ModuleApi", () => {
  let moduleApi: ModuleApi;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        ModuleApi,
      ],
    });

    moduleApi = TestBed.get(ModuleApi);
    httpTestingController = TestBed.get(HttpTestingController);
  });

  describe('loadAll', () => {
    let module_dto_1: ModuleDto, module_dto_2: ModuleDto;

    beforeEach(() => {
      module_dto_1 = {
        id: "module_1_id",
        name: "Module 1 name",
        description: "Module 1 description",
        kind: "Source",
        version: "1.0"
      };
      module_dto_2 = {
        id: "module_2_id",
        name: "Module 1 name",
        description: "Module 1 description",
        kind: "Processor",
        version: "1.0"
      };
    });

    test('should work', () => {
      moduleApi.loadAll().subscribe(response => {
        expect(response).toEqual([module_dto_1, module_dto_2]);
      });
      const request = httpTestingController.expectOne(`/api/studio/modules`);
      expect(request.request.method).toEqual('GET')
      request.flush([module_dto_1, module_dto_2]);
    });

    test('when search should work', () => {
      const filter: ModuleFilter = { term: "search_term" };
      moduleApi.loadAll(filter).subscribe(response => {
        expect(response).toEqual([module_dto_1, module_dto_2]);
      });
      const request = httpTestingController.expectOne(`/api/studio/modules?search=${filter.term}`);
      expect(request.request.method).toEqual('GET')
      request.flush([module_dto_1, module_dto_2]);
    });
  })

  describe('delete', () => {
    test('delete', () => {
      const moduleId = "moduleId";
      moduleApi.delete(moduleId).subscribe(response => {
        expect(response).toEqual(null);
      });
      const request = httpTestingController.expectOne(`/api/studio/modules/${moduleId}`);
      expect(request.request.method).toEqual('DELETE');
      request.flush(null);
    });
  })
});
