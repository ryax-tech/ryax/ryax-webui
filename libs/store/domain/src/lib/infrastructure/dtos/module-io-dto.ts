// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
export type ModuleIOTypeDto = "string" | "integer" | "enum" | "longstring" | "password" | "file" | "float" | "directory"

export interface ModuleIODto {
  id: string;
  technical_name: string;
  display_name: string;
  help: string;
  type: ModuleIOTypeDto;
  enum_values: string[];
}
