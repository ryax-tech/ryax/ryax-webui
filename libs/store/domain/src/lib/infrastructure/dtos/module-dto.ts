// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { ModuleIODto } from './module-io-dto';

export type ModuleKindDto = "Source" | "Processor" | "StreamOperator" | "Publisher";

export interface ModuleCategoryDto {
  id: string;
  name: string;
}

export interface ModuleDto {
  id: string;
  name: string;
  version: string;
  description: string;
  kind: ModuleKindDto;
  technical_name: string;

}

export interface ModuleDetailsDto extends ModuleDto {
  inputs: ModuleIODto[];
  outputs: ModuleIODto[];
  categories: ModuleCategoryDto[];
}
