// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { ModuleIO } from './module-io';

export enum ModuleKind {
  SOURCE = "Source",
  PROCESSOR = "Processor",
  PUBLISHER = "Publisher",
  STREAM_OPERATOR = "StreamOperator",
}

export interface ModuleCategory {
  categoryId: string;
  categoryName: string;
}

export interface Module {
  id: string;
  name: string;
  description: string;
  version: string;
  kind: ModuleKind;
  technicalName: string;
  technicalId: string;
}

export interface ModuleDetails extends Module {
  inputs: ModuleIO[];
  outputs: ModuleIO[];
  categories: ModuleCategory[];
}
