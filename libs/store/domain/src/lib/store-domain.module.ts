// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreFeatureKey, StoreReducerProvider, StoreReducerToken } from './applications/reducers';
import { StoreListEffects, StoreDetailsEffects, StoreCatalogEffects } from './applications/effects';
import { StoreCatalogFacade, StoreDetailsFacade, StoreListFacade } from './applications/facades';
import { ModuleApi } from './infrastructure/services';
import { ModuleVersionAdapter } from './infrastructure/adapters';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature(StoreFeatureKey, StoreReducerToken),
    EffectsModule.forFeature([StoreListEffects, StoreDetailsEffects, StoreCatalogEffects])
  ],
  providers: [
    StoreReducerProvider,
    StoreCatalogFacade,
    StoreListFacade,
    StoreDetailsFacade,
    ModuleVersionAdapter,
    ModuleApi
  ]
})
export class StoreDomainModule {}
