// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, Input } from '@angular/core';
import { Module } from '@ryax/store/domain';

@Component({
  selector: 'ryax-module-card',
  templateUrl: './module-card.component.pug',
  styleUrls: ['./module-card.component.scss']
})
export class ModuleCardComponent {
  @Input() item: Module;

  get logoUrl() {
    return `/api/studio/modules/${this.item.id}/logo`;
  }
}
