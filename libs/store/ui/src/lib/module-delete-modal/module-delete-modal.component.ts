// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Module } from '@ryax/store/domain';

@Component({
  selector: 'ryax-module-delete-modal',
  templateUrl: './module-delete-modal.component.pug'
})
export class ModuleDeleteModalComponent {
  @Input() visible: boolean;
  @Input() module: Module;
  @Output() valid = new EventEmitter<string>();
  @Output() cancel = new EventEmitter<void>();


  handleCancel() {
    this.cancel.emit();
  }

  handleValid() {
    this.valid.emit(this.module.id);
  }
}
