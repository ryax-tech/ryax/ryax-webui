// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Module, Workflow } from '@ryax/store/domain';

@Component({
  selector: 'ryax-module-warning-modal',
  templateUrl: './module-warning-modal.component.pug',
})
export class ModuleWarningModalComponent {
  @Input() visible: boolean;
  @Input() module: Module;
  @Input() workflows: Workflow[];
  @Output() valid = new EventEmitter<string>();
  @Output() cancel = new EventEmitter<void>();

  get isPlural() {
    if(this.workflows) return this.workflows.length > 1 ;
  }

  handleCancel() {
    this.cancel.emit();
  }

  handleValid() {
    this.valid.emit();
  }
}
