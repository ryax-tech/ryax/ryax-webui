// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, Input, TemplateRef } from '@angular/core';
import { Module } from '@ryax/store/domain';

@Component({
  selector: 'ryax-module-list',
  templateUrl: './module-list.component.pug'
})
export class ModuleListComponent {
  span = 24;

  @Input() loading: boolean;
  @Input() items: Module[];
  @Input() itemRender: TemplateRef<Module>;
  @Input() set itemPerLine(value: number) {
    this.span = value > 0 ? 24 / value : 24;
  };

  trackItemById(index: number, item: Module) {
    return item.id;
  }
}
