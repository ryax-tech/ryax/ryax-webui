// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Pipe, PipeTransform } from '@angular/core';
import { ModuleKind } from '@ryax/store/domain';

@Pipe({
  name: 'ryaxModuleKindColor',
  pure: true
})
export class ModuleKindColorPipe implements PipeTransform {
  transform(value: ModuleKind): string {
    switch (value) {
      case ModuleKind.SOURCE:
        return 'green';
      case ModuleKind.PROCESSOR:
        return 'blue';
      case ModuleKind.PUBLISHER:
        return 'cyan';
      case ModuleKind.STREAM_OPERATOR:
        return 'purple';
      default:
        return null;
    }
  }
}
