// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, Input } from '@angular/core';
import { Module } from '@ryax/store/domain';

@Component({
  selector: 'ryax-module-inline',
  templateUrl: './module-inline.component.pug',
  styleUrls: ['./module-inline.component.scss']
})
export class ModuleInlineComponent {
  @Input() item: Module;
  @Input() baseLink: string;

  get logoUrl() {
    return `/api/studio/modules/${this.item.id}/logo`;
  }
}
