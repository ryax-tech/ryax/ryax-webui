// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Subject } from 'rxjs';
import { ModuleCategory, ModuleFilter } from '@ryax/store/domain';
import { FormBuilder } from '@angular/forms';
import { debounceTime, distinctUntilChanged, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'ryax-module-filter',
  templateUrl: './module-filter.component.pug'
})
export class ModuleFilterComponent implements OnInit, OnDestroy {
  destroy$ = new Subject<void>();
  form = this.formBuilder.group({
    term: this.formBuilder.control(""),
    category: this.formBuilder.control(""),
  });

  @Input() categories: ModuleCategory[];
  @Output() filterChange = new EventEmitter<ModuleFilter>();

  constructor(
    private readonly formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.form.valueChanges.pipe(
      distinctUntilChanged(),
      debounceTime(400),
      takeUntil(this.destroy$),
    ).subscribe(this.filterChange)
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
