// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedUiCommonModule } from '@ryax/shared/ui-common';
import { ModuleKindLabelPipe } from './pipes/module-kind-label.pipe';
import { ModuleKindColorPipe } from './pipes/module-kind-color.pipe';
import { ModuleVersionPipe } from './pipes/module-version.pipe';
import { ModuleListComponent } from './module-list/module-list.component';
import { ModuleCardComponent } from './module-card/module-card.component';
import { ModuleInlineComponent } from './module-inline/module-inline.component';
import { ModuleFilterComponent } from './module-filter/module-filter.component';
import { ModuleDeleteModalComponent } from './module-delete-modal/module-delete-modal.component';
import { ModuleWarningModalComponent } from './module-warning-modal/module-warning-modal.component';
import { WorkflowModuleFilterComponent } from './workflow-module-filter/workflow-module-filter.component';

@NgModule({
  imports: [
    CommonModule,
    SharedUiCommonModule
  ],
  declarations: [
    ModuleKindLabelPipe,
    ModuleKindColorPipe,
    ModuleVersionPipe,
    ModuleListComponent,
    ModuleCardComponent,
    ModuleInlineComponent,
    ModuleFilterComponent,
    ModuleDeleteModalComponent,
    ModuleWarningModalComponent,
    WorkflowModuleFilterComponent
  ],
  exports: [
    SharedUiCommonModule,
    ModuleKindLabelPipe,
    ModuleKindColorPipe,
    ModuleVersionPipe,
    ModuleListComponent,
    ModuleCardComponent,
    ModuleInlineComponent,
    ModuleFilterComponent,
    ModuleDeleteModalComponent,
    ModuleWarningModalComponent,
    WorkflowModuleFilterComponent
  ]
})
export class StoreUiModule {}
