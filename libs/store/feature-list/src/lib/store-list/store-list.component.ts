// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, OnInit } from '@angular/core';
import { ModuleFilter, StoreListFacade } from '@ryax/store/domain';

@Component({
  selector: 'ryax-store-list',
  templateUrl: './store-list.component.pug'
})
export class StoreListComponent implements OnInit {
  loading$ = this.facade.loading$;
  modules$ = this.facade.modules$;
  categories$ = this.facade.categories$;

  constructor(
    private readonly facade: StoreListFacade
  ) {}

  ngOnInit(): void {
    this.facade.init();
  }

  onRefresh() {
    this.facade.refresh();
  }

  onFilterChange(data: ModuleFilter) {
    this.facade.filter(data);
  }
}
