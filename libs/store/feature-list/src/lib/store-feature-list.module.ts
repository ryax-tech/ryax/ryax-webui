// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreUiModule } from '@ryax/store/ui';
import { StoreDomainModule } from '@ryax/store/domain';
import { StoreListComponent } from './store-list/store-list.component';
import { StoreListHeaderComponent } from './store-list-header/store-list-header.component';

@NgModule({
  imports: [
    CommonModule,
    StoreUiModule,
    StoreDomainModule
  ],
  declarations: [
    StoreListComponent,
    StoreListHeaderComponent
  ],
  exports: [StoreListComponent]
})
export class StoreFeatureListModule {}
