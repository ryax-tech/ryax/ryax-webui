// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, OnInit, Renderer2 } from '@angular/core';
import { Module, ModuleFilter, StoreCatalogFacade } from '@ryax/store/domain';

@Component({
  selector: 'ryax-store-catalog',
  templateUrl: './store-catalog.component.pug',
  styleUrls: ['./store-catalog.component.scss']
})
export class StoreCatalogComponent implements OnInit {
  loading$ = this.facade.loading$;
  modules$ = this.facade.modules$;
  categories$ = this.facade.categories$;
  visibility$ = this.facade.visibility$;

  constructor(
    private readonly facade: StoreCatalogFacade,
    private readonly renderer: Renderer2
  ) {}

  ngOnInit(): void {
    this.facade.init();
  }

  onFilterChange(data: ModuleFilter) {
    this.facade.filter(data);
  }

  onDragStart(event: DragEvent, item: Module) {
    const { target, offsetX, offsetY } = event;
    this.renderer.addClass(target, 'dragging');
    event.dataTransfer.setDragImage(target as any, offsetX, offsetY);
    event.dataTransfer.effectAllowed = 'move';
    event.dataTransfer.setData('text/plain', item.id);
  }

  onDragEnd(event: DragEvent) {
    this.renderer.removeClass(event.target as any, 'dragging');
  }

  toggleVisibility(visibility: boolean): void {
    this.facade.toggleVisibility(visibility);
  }
}
