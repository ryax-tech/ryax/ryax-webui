// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreUiModule } from '@ryax/store/ui';
import { StoreDomainModule } from '@ryax/store/domain';
import { StoreCatalogComponent } from './store-catalog/store-catalog.component';

@NgModule({
  imports: [
    CommonModule,
    StoreUiModule,
    StoreDomainModule,
  ],
  declarations: [
    StoreCatalogComponent,
  ],
  exports: [
    StoreCatalogComponent
  ]
})
export class StoreFeatureCatalogModule {}
