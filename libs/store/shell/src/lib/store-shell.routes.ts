// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Routes } from '@angular/router';
import { StoreListComponent } from '@ryax/store/feature-list';
import { StoreDetailsAccessGuard, StoreDetailsComponent } from '@ryax/store/feature-details';

export const routes: Routes = [
  {
    path: '',
    pathMatch: "full",
    component: StoreListComponent
  },
  {
    path: ':moduleId',
    component: StoreDetailsComponent,
    canActivate: [StoreDetailsAccessGuard],
    data: {
      breadcrumb: null
    }
  },
];
