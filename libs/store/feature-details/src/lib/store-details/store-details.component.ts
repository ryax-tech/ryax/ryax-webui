// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component } from '@angular/core';
import { StoreDetailsFacade } from '@ryax/store/domain';

@Component({
  selector: 'ryax-store-details',
  templateUrl: './store-details.component.pug'
})
export class StoreDetailsComponent {
  loading$ = this.facade.loading$;
  module$ = this.facade.module$;
  moduleVersions$ = this.facade.moduleVersions$;
  moduleInputs$ = this.facade.moduleInputs$;
  moduleOutputs$ = this.facade.moduleOutputs$;
  workflows$ = this.facade.workflows$;
  deleteModalVisible$ = this.facade.deleteModalVisible$;
  warningModalVisible$ = this.facade.warningModalVisible$;

  constructor(
    private readonly facade: StoreDetailsFacade
  ) {}

  showModal() {
    this.facade.openDeleteModuleModal();
  }

  validateModuleDelete () {
    this.facade.validateModuleDelete();
  }

  cancelModuleDelete () {
    this.facade.cancelModuleDelete();
  }

  validateModuleWarning () {
    this.facade.closeWarningModal();
  }

  cancelModuleWarning () {
    this.facade.closeWarningModal();
  }

  routeToModule (moduleId: string) {
    this.facade.routeToModule(moduleId)
  }

}
