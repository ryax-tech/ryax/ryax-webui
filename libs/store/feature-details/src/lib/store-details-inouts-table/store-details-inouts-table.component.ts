// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, Input } from '@angular/core';
import { ModuleIO } from '@ryax/store/domain';

@Component({
  selector: 'ryax-store-details-inouts-table',
  templateUrl: './store-details-inouts-table.component.pug'
})
export class StoreDetailsInoutsTableComponent {
  cardBodyStyle = { padding: 0 };

  @Input() title: string;
  @Input() loading: boolean;
  @Input() items: ModuleIO[];

  trackItemById(index: number, item: ModuleIO) {
    return item.id;
  }
}
