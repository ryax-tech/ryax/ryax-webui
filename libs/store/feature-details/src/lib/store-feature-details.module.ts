// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreUiModule } from '@ryax/store/ui';
import { StoreDomainModule } from '@ryax/store/domain';
import { StoreDetailsAccessGuard } from './guards/store-details-access.guard';
import { StoreDetailsComponent } from './store-details/store-details.component';
import { StoreDetailsHeaderComponent } from './store-details-header/store-details-header.component';
import { StoreDetailsInoutsTableComponent } from './store-details-inouts-table/store-details-inouts-table.component';

@NgModule({
  imports: [
    CommonModule,
    StoreUiModule,
    StoreDomainModule
  ],
  providers: [
    StoreDetailsAccessGuard
  ],
  declarations: [
    StoreDetailsComponent,
    StoreDetailsHeaderComponent,
    StoreDetailsInoutsTableComponent
  ],
  exports: [
    StoreDetailsComponent
  ]
})
export class StoreFeatureDetailsModule {}
