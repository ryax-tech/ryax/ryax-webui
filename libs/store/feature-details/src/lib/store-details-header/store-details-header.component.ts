// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Module, ModuleDetails, ModuleVersion } from '@ryax/store/domain';
import { FormControl, FormGroup } from '@angular/forms';
import { Subject } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'ryax-store-details-header',
  templateUrl: './store-details-header.component.pug'
})
export class StoreDetailsHeaderComponent implements OnInit, OnDestroy {
  form = new FormGroup({
    newVersionModuleId: new FormControl(null),
  }, { updateOn: 'change' });
  destroy$ = new Subject<void>();

  @Input() module: ModuleDetails;
  @Input() versions: ModuleVersion[];
  @Output() showModal = new EventEmitter<void>();
  @Output() save = new EventEmitter<string>();

  ngOnInit(): void {
    this.form.valueChanges.pipe(
      filter(() => this.form.valid),
      takeUntil(this.destroy$),
    ).subscribe((formData) => {
      const { newVersionModuleId } = formData;
      this.save.emit(newVersionModuleId);
    });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  onShowModal() {
    this.showModal.emit();
  }

  get hasCategories() {
    return this.module.categories.length
  }

  get logoUrl() {
    if(this.module)
      return `/api/studio/modules/${this.module.id}/logo`;
  }
}
