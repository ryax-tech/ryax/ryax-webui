// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { filter, map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { StoreDetailsFacade } from '@ryax/store/domain';

@Injectable()
export class StoreDetailsAccessGuard implements CanActivate {
  private module$ = this.facade.module$;

  constructor(
    private readonly facade: StoreDetailsFacade
  ) {}

  canActivate() {
    this.facade.init();
    return this.module$.pipe(
      map(module => !!module),
      filter(moduleExists => moduleExists)
    );
  }
}
