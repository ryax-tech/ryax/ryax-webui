// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ErrorNotFoundComponent } from './error-not-found/error-not-found.component';
import { ErrorForbiddenComponent } from './error-forbidden/error-forbidden.component';
import { ErrorUnauthorizedComponent } from './error-unauthorized/error-unauthorized.component';
import { ErrorUnassignedComponent } from './error-unassigned/error-unassigned.component';

const routes: Routes = [
  {
    path: '',
    component: ErrorNotFoundComponent
  },
  {
    path: 'forbidden',
    component: ErrorForbiddenComponent
  },
  {
    path: 'unauthorized',
    component: ErrorUnauthorizedComponent
  },
  {
    path: 'unassigned',
    component: ErrorUnassignedComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ErrorUiRoutingModule {}
