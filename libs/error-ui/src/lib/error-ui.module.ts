// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ErrorUiRoutingModule } from './error-ui-routing.module';
import { ErrorNotFoundComponent } from './error-not-found/error-not-found.component';
import { ErrorForbiddenComponent } from './error-forbidden/error-forbidden.component';
import { ErrorUnauthorizedComponent } from './error-unauthorized/error-unauthorized.component';
import { ErrorUnassignedComponent } from './error-unassigned/error-unassigned.component';

@NgModule({
  imports: [
    CommonModule,
    ErrorUiRoutingModule
  ],
  declarations: [
    ErrorNotFoundComponent,
    ErrorForbiddenComponent,
    ErrorUnauthorizedComponent,
    ErrorUnassignedComponent,
  ],
})
export class ErrorUiModule {}
