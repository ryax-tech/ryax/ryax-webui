// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component } from '@angular/core';
import {Location} from '@angular/common';

@Component({
  selector: 'ryax-error-not-found',
  templateUrl: './error-not-found.component.pug'
})
export class ErrorNotFoundComponent {
  constructor(private _location: Location)
  {}

  goBack() {
    this._location.back();
  }
}
