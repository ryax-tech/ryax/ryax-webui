// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Pipe, PipeTransform } from '@angular/core';
import { UserEditMode } from '@ryax/user/domain';

@Pipe({
  name: "ryaxUserEditTitle",
  pure: true
})
export class UserEditTitlePipe implements PipeTransform {
  transform(value: UserEditMode): string {
    switch (value) {
      case UserEditMode.EditUserInfos:
        return "Edit user informations";
      case UserEditMode.EditUserPassword:
        return "Edit user password";
      case UserEditMode.NewUser:
        return "New user";
    }
  }
}
