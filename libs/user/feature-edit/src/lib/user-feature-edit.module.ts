// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedUiCommonModule } from '@ryax/shared/ui-common';
import { UserEditComponent } from './user-edit/user-edit.component';
import { UserEditFormComponent } from './user-edit-form/user-edit-form.component';
import { UserEditTitlePipe } from './pipes/user-edit-title.pipe';
import { UserSettingsEditFormComponent } from './user-settings-edit-form/user-settings-edit-form.component';
import { UserSettingsEditComponent } from './user-settings-edit/user-settings-edit.component';

@NgModule({
  imports: [
    CommonModule,
    SharedUiCommonModule
  ],
  declarations: [
    UserEditComponent,
    UserEditFormComponent,
    UserSettingsEditComponent,
    UserSettingsEditFormComponent,
    UserEditTitlePipe
  ],
  exports: [
    UserEditComponent,
    UserSettingsEditComponent
  ]
})
export class UserFeatureEditModule {}
