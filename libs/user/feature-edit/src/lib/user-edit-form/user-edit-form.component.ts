// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { User, UserEditData, UserEditMode, UserRole } from '@ryax/user/domain';
import { passwordConfirmMatch } from './user-edit-form.validators';

@Component({
  selector: 'ryax-user-edit-form',
  templateUrl: './user-edit-form.component.pug'
})
export class UserEditFormComponent implements OnChanges {
  roles = Object.values(UserRole);
  form = this.formBuilder.group({});

  get isInfosPartDisplayed() {
    return this.form.contains("username");
  }

  get isPasswordPartDisplayed() {
    return this.form.contains("password");
  }

  @Input() mode: UserEditMode;
  @Input() user: User;
  @Output() save = new EventEmitter<UserEditData>();

  constructor(
    private readonly formBuilder: FormBuilder
  ) {}

  private buildNewUserForm() {
    const passwordControl = this.formBuilder.control(null, Validators.required);
    this.form = this.formBuilder.group({
      "username": this.formBuilder.control(null, Validators.required),
      "email": this.formBuilder.control(null, Validators.required),
      "role": this.formBuilder.control(null, Validators.required),
      "description": this.formBuilder.control(null),
      "password": passwordControl,
      "passwordConfirm": this.formBuilder.control(null, [
        Validators.required,
        passwordConfirmMatch(passwordControl)
      ]),
    });
  }

  private buildEditUserInfosForm() {
    this.form = this.formBuilder.group({
      "username": this.formBuilder.control(null, Validators.required),
      "email": this.formBuilder.control(null, Validators.required),
      "role": this.formBuilder.control(null, Validators.required),
      "description": this.formBuilder.control(null),
    });
  }

  private buildEditUserPasswordForm() {
    const passwordControl = this.formBuilder.control(null, Validators.required);
    this.form = this.formBuilder.group({
      "password": passwordControl,
      "passwordConfirm": this.formBuilder.control(null, [
        Validators.required,
        passwordConfirmMatch(passwordControl)
      ]),
    });
  }

  private initForm(user: User) {
    switch(this.mode) {
      case UserEditMode.EditUserInfos:
        const { id, ...formValue } = user;
        this.form.setValue(formValue);
        break;
      default:
        this.form.reset();
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(!!changes.mode) {
      switch(changes.mode.currentValue) {
        case UserEditMode.NewUser:
          this.buildNewUserForm();
          break;
        case UserEditMode.EditUserInfos:
          this.buildEditUserInfosForm();
          break;
        case UserEditMode.EditUserPassword:
          this.buildEditUserPasswordForm();
          break;
      }
    }

    if(!!changes.user) {
      this.initForm(changes.user.currentValue);
    }
  }

  onSubmit() {
    const formValue = this.form.value;
    this.save.emit(formValue);
  }
}

