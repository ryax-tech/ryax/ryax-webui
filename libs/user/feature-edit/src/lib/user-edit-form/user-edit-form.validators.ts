// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { AbstractControl, FormControl, ValidatorFn } from '@angular/forms';

export function passwordConfirmMatch(passwordControl: FormControl): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} | null => {
    const passwordValue = passwordControl ? passwordControl.value : null;
    const passwordConfirmValue = control.value;
    if(!!passwordValue && !!passwordConfirmValue) {
      return passwordValue !== passwordConfirmValue ? { 'mismatch': true } : null;
    } else {
      return null;
    }
  };
}
