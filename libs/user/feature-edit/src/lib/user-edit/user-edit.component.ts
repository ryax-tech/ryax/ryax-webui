// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, OnInit } from '@angular/core';
import { UserEditData, UserEditFacade } from '@ryax/user/domain';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'ryax-user-edit',
  templateUrl: './user-edit.component.pug'
})
export class UserEditComponent {
  visible$ = this.facade.visible$;
  mode$ = this.facade.mode$;
  user$ = this.facade.user$;

  constructor(
    private readonly facade: UserEditFacade,
  ) {}

  onSave(value: UserEditData) {
    this.facade.save(value);
  }

  onClose() {
    this.facade.close();
  }
}
