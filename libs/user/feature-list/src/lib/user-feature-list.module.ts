// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedUiCommonModule } from '@ryax/shared/ui-common';
import { UserDomainModule } from '@ryax/user/domain';
import { UserFeatureEditModule } from '@ryax/user/feature-edit';
import { UserListComponent } from './user-list/user-list.component';
import { UserListHeaderComponent } from './user-list-header/user-list-header.component';
import { UserListTableComponent } from './user-list-table/user-list-table.component';
import { UserUiModule } from '@ryax/user/ui';

@NgModule({
  imports: [
    CommonModule,
    SharedUiCommonModule,
    UserDomainModule,
    UserFeatureEditModule,
    UserUiModule
  ],
  declarations: [
    UserListComponent,
    UserListHeaderComponent,
    UserListTableComponent,
  ],
  exports: [
    UserListComponent
  ]
})
export class UserFeatureListModule {}
