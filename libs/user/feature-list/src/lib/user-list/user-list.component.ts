// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, OnInit } from '@angular/core';
import { User, UserListFacade } from '@ryax/user/domain';

@Component({
  selector: 'ryax-user-list',
  templateUrl: './user-list.component.pug'
})
export class UserListComponent implements OnInit {
  loading$ = this.facade.loading$;
  users$ = this.facade.users$;

  constructor(
    private readonly facade: UserListFacade
  ) {}

  ngOnInit(): void {
    this.facade.init();
  }

  onRefresh() {
    this.facade.refresh();
  }

  onAddUser() {
    this.facade.addUser();
  }

  onEditUserInfos(user: User) {
    this.facade.editUserInfos(user);
  }

  onEditUserPassword(user: User) {
    this.facade.editUserPassword(user);
  }

  onDeleteUser(user: User) {
    this.facade.deleteUser(user);
  }
}
