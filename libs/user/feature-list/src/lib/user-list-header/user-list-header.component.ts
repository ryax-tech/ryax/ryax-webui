// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'ryax-user-list-header',
  templateUrl: './user-list-header.component.pug'
})
export class UserListHeaderComponent {
  @Output() refresh = new EventEmitter<void>();
  @Output() addUser = new EventEmitter<void>();

  onAddUser() {
    this.addUser.emit();
  }

  onRefresh() {
    this.refresh.emit();
  }
}
