// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { User } from '@ryax/user/domain';

@Component({
  selector: 'ryax-user-list-table',
  templateUrl: './user-list-table.component.pug'
})
export class UserListTableComponent {
  @Input() loading: boolean;
  @Input() items: User[];
  @Output() editInfos = new EventEmitter<User>();
  @Output() editPassword = new EventEmitter<User>();
  @Output() delete = new EventEmitter<User>();

  trackIByItemId(index: number, item: User) {
    return item.id;
  }

  onEditInfos(item: User) {
    this.editInfos.emit(item);
  }

  onEditPassword(item: User) {
    this.editPassword.emit(item);
  }

  onDelete(item: User) {
    this.delete.emit(item);
  }
}
