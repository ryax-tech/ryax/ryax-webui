// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { UserRole } from './user-role';

export interface UserEditData {
  username: string,
  email: string,
  role: UserRole,
  description: string,
  password: string;
}

export enum UserEditMode {
  NewUser ,
  EditUserInfos ,
  EditUserPassword
}
