// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User, UserEditData, UserRole } from '@ryax/user/domain';
import { UpdateUserDto, UserDto } from '../models';

export interface UserInfosDto {
  id: string;
  username: string,
  role: "Admin" | "Developer" | "User" | "Anonymous",
  email: string,
  comment: string,
}

@Injectable()
export class UserSettingsApi {
  private readonly authUrl = "/api/authorization";

  adaptUserInfos(dto: UserInfosDto): User {
    return {
      id: dto.id,
      username: dto.username,
      role: dto.role as UserRole,
      email: dto.email,
      description: dto.comment
    }
  }

  constructor(
    private readonly http: HttpClient
  ) {}

  private makeUrl(...urlParts: string[]) {
    return [this.authUrl, ...urlParts].join('/');
  }

  loadUser() {
    const url = this.makeUrl('me');
    return this.http.get<UserInfosDto>(url).pipe(
      map(dto => this.adaptUserInfos(dto))
    );
  }

  updateSelf(data: UserEditData) {
    const url = [this.authUrl, 'me'].join('/');
    const updateDto: UpdateUserDto = {
      username: data.username,
      email: data.email,
      password: data.password,
      role: data.role,
      comment: data.description,
    };
    return this.http.put<UserInfosDto>(url, updateDto, { responseType: 'json'}).pipe(
      map(dto => this.adaptUserInfos(dto))
    );
  }
}
