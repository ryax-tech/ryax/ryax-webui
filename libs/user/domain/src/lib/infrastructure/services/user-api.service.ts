// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User, UserEditData, UserRole } from '../../entities';
import { UserDto, CreateUserDto, UpdateUserDto } from '../models';

@Injectable()
export class UserApi {
  private baseUrl = "/api/authorization/users";

  constructor(
    private readonly http: HttpClient
  ) {}

  private toUser(userDto: UserDto): User {
    return {
      id: userDto.id,
      username: userDto.username,
      email: userDto.email,
      role: UserRole[userDto.role],
      description: userDto.comment
    }
  }

  loadAll() {
    return this.http.get<UserDto[]>(this.baseUrl, { responseType: 'json' }).pipe(
      map(dtos => dtos.map(item => this.toUser(item)))
    )
  }

  create(data: UserEditData) {
    const createDto: CreateUserDto = {
      username: data.username,
      email: data.email,
      password: data.password,
      role: data.role,
      comment: data.description ? data.description : "",
    };
    return this.http.post<UserDto>(this.baseUrl, createDto, { responseType: 'json'}).pipe(
      map(dto => this.toUser(dto))
    );
  }

  update(userId: string, data: UserEditData) {
    const url = [this.baseUrl, userId].join('/');
    const updateDto: UpdateUserDto = {
      username: data.username,
      email: data.email,
      password: data.password,
      role: data.role,
      comment: data.description,
    };
    return this.http.put<UserDto>(url, updateDto, { responseType: 'json'}).pipe(
      map(dto => this.toUser(dto))
    );
  }

  delete(userId: string) {
    const url = [this.baseUrl, userId].join('/');
    return this.http.delete<void>(url, { responseType: 'json' });
  }
}
