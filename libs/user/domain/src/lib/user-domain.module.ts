// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { UserFeatureKey, UserReducerProvider, UserReducerToken } from './application/reducers';
import { UserDetailsEffects, UserEditEffects, UserListEffects, UserSelectEffects } from './application/effects';
import { UserApi, UserSettingsApi } from './infrastructure/services';
import { UserDetailsFacade, UserEditFacade, UserListFacade, UserSelectFacade } from './application/facade';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature(UserFeatureKey, UserReducerToken),
    EffectsModule.forFeature([UserListEffects, UserEditEffects, UserSelectEffects, UserDetailsEffects])
  ],
  providers: [
    UserReducerProvider,
    UserListFacade,
    UserEditFacade,
    UserSelectFacade,
    UserDetailsFacade,
    UserApi,
    UserSettingsApi
  ]
})
export class UserDomainModule {}
