// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createAction } from '@ngrx/store';
import { User } from '@ryax/user/domain';

export const init = createAction(
  '[User Details] Init'
);

export const refresh = createAction(
  '[User Details] Refresh'
);

export const loadSuccess = createAction(
  '[User Details] Load success',
  (user: User) => ({
    payload: user
  })
);

export const loadError = createAction(
  '[User Details] Load error',
);

export const editUserInfos = createAction(
  '[User Details] Edit user infos',
  (userId: string) => ({
    payload: userId
  })
);

export const editUserPassword = createAction(
  '[User Details] Edit user password',
  (userId: string) => ({
    payload: userId
  })
);

export const notifySuccess = createAction(
  '[User Details] Notify success',
  (message: string) => ({
    payload: message
  })
);

export const notifyError = createAction(
  '[User Details] Notify error',
  (message: string) => ({
    payload: message
  })
);
