// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createAction } from '@ngrx/store';
import { User, UserEditData } from '@ryax/user/domain';

export const save = createAction(
  '[User Edit] Save',
  (value: UserEditData) => ({
    payload: value
  })
);
export const saveSelf = createAction(
  '[User Edit] Save self',
  (value: UserEditData) => ({
    payload: value
  })
);

export const close = createAction(
  '[User Edit] Close'
);

export const createUserSuccess = createAction(
  '[User Edit] Create user success',
  (user: User) => ({
    payload: user,
    notification: "User created successfully"
  })
);

export const createUserError = createAction(
  '[User Edit] Create user error',
  () => ({
    notification: "User creation failed"
  })
);

export const updateUserSuccess = createAction(
  '[User Edit] Update user success',
  (user: User) => ({
    payload: user,
    notification: "User updated successfully"
  })
);

export const updateUserError = createAction(
  '[User Edit] Update user error',
  () => ({
    notification: "User update failed"
  })
);
