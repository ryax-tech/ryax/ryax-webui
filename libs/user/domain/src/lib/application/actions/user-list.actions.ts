// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createAction } from '@ngrx/store';
import { User } from '@ryax/user/domain';
import { HttpErrorResponse } from '@angular/common/http';

export const init = createAction(
  '[User List] Init'
);

export const refresh = createAction(
  '[User List] Refresh'
);

export const loadAllSuccess = createAction(
  '[User List] Load all success',
  (users: User[]) => ({
    payload: users
  })
);

export const loadAllError = createAction(
  '[User List] Load all error',
  (error?: HttpErrorResponse) => ({
    payload: error
  })
);

export const deleteSuccess = createAction(
  '[User List] Delete success',
  (userId: string) => ({
    payload: userId
  })
);

export const deleteError = createAction(
  '[User List] Delete error',
);

// export const createSuccess = createAction(
//   '[User List] Create success',
//   (user: User) => ({
//     payload: user
//   })
// );
//
// export const createError = createAction(
//   '[User List] Create error'
// );

// export const updateSuccess = createAction(
//   '[User List] Update success',
//   (user: User) => ({
//     payload: user
//   })
// );
//
// export const updateError = createAction(
//   '[User List] Update error'
// );

export const addUser = createAction(
  '[User List] Add user'
);

export const editUserInfos = createAction(
  '[User List] Edit user infos',
  (userId: string) => ({
    payload: userId
  })
);

export const editUserPassword = createAction(
  '[User List] Edit user password',
  (userId: string) => ({
    payload: userId
  })
);

export const deleteUser = createAction(
  '[User List] Delete user',
  (userId: string) => ({
    payload: userId
  })
);

export const notifySuccess = createAction(
  '[User List] Notify success',
  (message: string) => ({
    payload: message
  })
);

export const notifyError = createAction(
  '[User List] Notify error',
  (message: string) => ({
    payload: message
  })
);
