// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
// import { createFeatureSelector, createSelector } from '@ngrx/store';
// import { UserAdapters, UserFeatureKey, UserState } from '../reducers';
//
//
// const selectFeatureState = createFeatureSelector<UserState>(UserFeatureKey);
// const selectUserDataState = createSelector(selectFeatureState, state => state.userData);
// const selectUserEditState = createSelector(selectFeatureState, state => state.userEdit);
//
// const {
//   selectEntities: selectUsers,
// } = UserAdapters.userData.getSelectors(selectUserDataState);
//
// export const selectVisible = createSelector(selectUserEditState,
//   state => state.mode !== null);
//
// export const selectMode = createSelector(selectUserEditState,
//   state => state.mode);
//
// export const selectUserId = createSelector(selectUserEditState,
//   (state) => state.user);
//
// export const selectUser = createSelector(selectUsers, selectUserId,
//   (users, userId) => users[userId]);
//
