// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { selectUsers, UserState } from '../reducers';
import { UserSelectActions } from '../actions';

@Injectable()
export class UserSelectFacade {
  users$ = this.store.select(selectUsers);

  constructor(
    private readonly store: Store<UserState>
  ) {}

  init() {
    this.store.dispatch(UserSelectActions.init())
  }
}
