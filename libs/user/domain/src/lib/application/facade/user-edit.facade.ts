// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { UserEditData } from '@ryax/user/domain';
import { UserEditActions } from '../actions';
import { selectEditMode, selectEditUser, selectEditVisible, UserState } from '../reducers';
import { Router } from '@angular/router';

@Injectable()
export class UserEditFacade {
  visible$ = this.store.select(selectEditVisible);
  mode$ = this.store.select(selectEditMode);
  user$ = this.store.select(selectEditUser);

  constructor(
    private readonly store: Store<UserState>,
    private readonly router: Router
  ) {}

  save(value: UserEditData) {
    //TODO Ugly code, Bad dirty hack, must be redone when /api/users rework begin
    const url = this.router.url;
    switch (url) {
      case "/users/list":
        return this.store.dispatch(UserEditActions.save(value));
      case "/users/settings":
        return this.store.dispatch(UserEditActions.saveSelf(value));
    }
  }

  close() {
    this.store.dispatch(UserEditActions.close());
  }
}
