// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { UserState } from '../reducers';
import { UserEditData, UserEditFacade, UserEditMode } from '@ryax/user/domain';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { TestBed } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { selectMode, selectUser } from '../selectors/user-edit.selectors';
import { cold } from 'jest-marbles';
import { User, UserRole } from '@ryax/domain/user';
import { UserEditActions } from '../actions';

describe('UserEditFacade', () => {
  let facade: UserEditFacade;
  let store: MockStore<UserState>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [],
      providers: [
        UserEditFacade,
        provideMockStore({})
      ]
    });

    facade = TestBed.get(UserEditFacade);
    store = TestBed.get(Store);
  });

  test('mode$', () => {
    const mode: UserEditMode = UserEditMode.NewUser;
    store.overrideSelector(selectMode, UserEditMode.NewUser);
    selectMode.setResult(mode);
    const expected = cold('a-', { a: mode });
    expect(facade.mode$).toBeObservable(expected);
  });

  test('user$', () => {
    const user: User = {
      id: "user_id",
      user: "user_user",
      role: UserRole.Admin,
      email: "user_email",
      comment: "user_comment",
    };
    store.overrideSelector(selectUser, null);
    selectUser.setResult(user);
    const expected = cold('a-', { a: user });
    expect(facade.user$).toBeObservable(expected);
  });

  test('save user', () => {
    const value: UserEditData = {
      username: "user_username",
      email: "user_email",
      role: UserRole.Admin,
      description: "user_description",
      password: "user_password",
    };
    spyOn(store, 'dispatch');
    facade.save(value);
    expect(store.dispatch).toHaveBeenCalledWith(UserEditActions.save(value))
  });

  test('close panel', () => {
    spyOn(store, 'dispatch');
    facade.close();
    expect(store.dispatch).toHaveBeenCalledWith(UserEditActions.close())
  });
});
