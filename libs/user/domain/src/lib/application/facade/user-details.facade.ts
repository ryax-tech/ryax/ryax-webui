// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { User } from '@ryax/user/domain';
import { selectInfosLoading, selectUser, UserState } from '../reducers';
import { UserDetailsActions } from '../actions';

@Injectable()
export class UserDetailsFacade {
  loading$ = this.store.select(selectInfosLoading);
  user$ = this.store.select(selectUser);

  constructor(
    private readonly store: Store<UserState>
  ) {}

  init() {
    this.store.dispatch(UserDetailsActions.init());
  }

  refresh() {
    this.store.dispatch(UserDetailsActions.refresh());
  }

  editUserInfos(user: User) {
    const { id } = user;
    this.store.dispatch(UserDetailsActions.editUserInfos(id));
  }

  editUserPassword(user: User) {
    const { id } = user;
    this.store.dispatch(UserDetailsActions.editUserPassword(id));
  }
}
