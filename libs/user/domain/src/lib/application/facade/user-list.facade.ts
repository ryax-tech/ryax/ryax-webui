// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { User } from '@ryax/user/domain';
import { UserListActions } from '../actions';
import { selectLoading, selectUsers, UserState } from '../reducers';

@Injectable()
export class UserListFacade {
  loading$ = this.store.select(selectLoading);
  users$ = this.store.select(selectUsers);

  constructor(
    private readonly store: Store<UserState>
  ) {}

  init() {
    this.store.dispatch(UserListActions.init());
  }

  refresh() {
    this.store.dispatch(UserListActions.refresh());
  }

  addUser() {
    this.store.dispatch(UserListActions.addUser());
  }

  editUserInfos(user: User) {
    const { id } = user;
    this.store.dispatch(UserListActions.editUserInfos(id));
  }

  editUserPassword(user: User) {
    const { id } = user;
    this.store.dispatch(UserListActions.editUserPassword(id));
  }

  deleteUser(user: User) {
    const { id } = user;
    this.store.dispatch(UserListActions.deleteUser(id));
  }
}
