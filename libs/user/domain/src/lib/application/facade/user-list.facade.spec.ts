// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { UserState } from '../reducers';
import { UserListFacade } from '@ryax/user/domain';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { TestBed } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { selectAllUsers } from '../selectors/user-list.selectors';
import { cold } from 'jest-marbles';
import { User, UserRole } from '@ryax/domain/user';
import { UserListActions } from '../actions';

describe('UserListFacade', () => {
  let facade: UserListFacade;
  let store: MockStore<UserState>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [],
      providers: [
        UserListFacade,
        provideMockStore({})
      ]
    });

    facade = TestBed.get(UserListFacade);
    store = TestBed.get(Store);
  });

  test('users$', () => {
    const users: User[] = [{
      id: "user_id",
      user: "user_user",
      role: UserRole.Admin,
      email: "user_email",
      comment: "user_comment",
    }];
    store.overrideSelector(selectAllUsers, null);
    selectAllUsers.setResult(users);
    const expected = cold('a-', { a: users });
    expect(facade.users$).toBeObservable(expected);
  });

  test('init', () => {
    spyOn(store, 'dispatch');
    facade.init();
    expect(store.dispatch).toHaveBeenCalledWith(UserListActions.init())
  });

  test('refresh', () => {
    spyOn(store, 'dispatch');
    facade.refresh();
    expect(store.dispatch).toHaveBeenCalledWith(UserListActions.refresh())
  });

  test('addUser', () => {
    spyOn(store, 'dispatch');
    facade.addUser();
    expect(store.dispatch).toHaveBeenCalledWith(UserListActions.addUser())
  });

  test('editUserInfos', () => {
    const user: User = {
      id: "user_id",
      user: "user_user",
      role: UserRole.Admin,
      email: "user_email",
      comment: "user_comment",
    };
    const { id } = user;
    spyOn(store, 'dispatch');
    facade.editUserInfos(user);
    expect(store.dispatch).toHaveBeenCalledWith(UserListActions.editUserInfos(id))
  });

  test('editUserPassword', () => {
    const user: User = {
      id: "user_id",
      user: "user_user",
      role: UserRole.Admin,
      email: "user_email",
      comment: "user_comment",
    };
    const { id } = user;
    spyOn(store, 'dispatch');
    facade.editUserPassword(user);
    expect(store.dispatch).toHaveBeenCalledWith(UserListActions.editUserPassword(id))
  });

  test('deleteUser', () => {
    const user: User = {
      id: "user_id",
      user: "user_user",
      role: UserRole.Admin,
      email: "user_email",
      comment: "user_comment",
    };
    const { id } = user;
    spyOn(store, 'dispatch');
    facade.deleteUser(user);
    expect(store.dispatch).toHaveBeenCalledWith(UserListActions.deleteUser(id))
  });
});
