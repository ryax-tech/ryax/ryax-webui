// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createReducer, on } from '@ngrx/store';
import { UserEditMode } from '../../entities';
import { UserDetailsActions, UserEditActions, UserListActions } from '../actions';

export interface State {
  userId: string;
  mode: UserEditMode;
}

export const initialState: State = {
  userId: null,
  mode: null
};

export const reducer = createReducer<State>(
  initialState,
  on(UserListActions.addUser, (state) => ({
    ...state,
    userId: null,
    mode: UserEditMode.NewUser
  })),
  on(UserListActions.editUserInfos, UserDetailsActions.editUserInfos, (state, { payload }) => ({
    ...state,
    userId: payload,
    mode: UserEditMode.EditUserInfos
  })),
  on(UserListActions.editUserPassword, UserDetailsActions.editUserPassword, (state, { payload }) => ({
    ...state,
    userId: payload,
    mode: UserEditMode.EditUserPassword
  })),
  on(UserEditActions.createUserSuccess, (state) => ({
    ...state,
    userId: null,
    mode: null,
  })),
  on(UserEditActions.updateUserSuccess, (state, { payload }) => ({
    ...state,
    userId: payload.id,
    mode: null,
  })),
  on(UserEditActions.close, (state) => ({
    ...state,
    userId: null,
    mode: null
  })),
);

export const selectUserId = (state: State) => state.userId;
export const selectVisible = (state: State) => state.mode !== null;
export const selectMode = (state: State) => state.mode;
