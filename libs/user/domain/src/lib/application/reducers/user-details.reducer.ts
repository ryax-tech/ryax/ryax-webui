// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createReducer, on } from '@ngrx/store';
import { UserDetailsActions } from '../actions';

export interface State {
  loading: boolean;
  userId: string;
}

export const initialState: State = {
  loading: false,
  userId: null,
};

export const reducer = createReducer<State>(
  initialState,
  on(UserDetailsActions.init, UserDetailsActions.refresh, (state) => ({
    ...state,
    loading: true,
  })),
  on(UserDetailsActions.loadSuccess, (state, { payload }) => ({
    ...state,
    loading: false,
    userId: payload.id
  })),
  on(UserDetailsActions.loadError, (state) => ({
    ...state,
    loading: false
  })),
);

export const selectLoading = (state: State) => state.loading;
export const selectUserId = (state: State) => state.userId;
