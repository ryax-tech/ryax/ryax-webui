// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { InjectionToken } from '@angular/core';
import { ActionReducerMap, createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromUserData from './user-data.reducer';
import * as fromUserList from './user-list.reducer';
import * as fromUserEdit from './user-edit.reducer';
import * as fromUserDetails from './user-details.reducer';

export const UserFeatureKey = 'userDomain';

export interface UserState {
  userData: fromUserData.State;
  userList: fromUserList.State;
  userEdit: fromUserEdit.State;
  userDetails: fromUserDetails.State;
}

export const UserReducerToken = new InjectionToken<ActionReducerMap<UserState>>(UserFeatureKey);

export const UserReducerProvider = {
  provide: UserReducerToken,
  useValue: {
    userData: fromUserData.reducer,
    userList: fromUserList.reducer,
    userEdit: fromUserEdit.reducer,
    userDetails: fromUserDetails.reducer
  },
};

export const selectFeatureState = createFeatureSelector<UserState>(UserFeatureKey);
export const selectUserDataState = createSelector(selectFeatureState, state => state.userData);
export const selectUserListState = createSelector(selectFeatureState, state => state.userList);
export const selectUserEditState = createSelector(selectFeatureState, state => state.userEdit);
export const selectUserDetailsState = createSelector(selectFeatureState, state => state.userDetails);


export const selectUsers = createSelector(selectUserDataState, fromUserData.selectAllUsers);

export const selectUserId = createSelector(selectUserDetailsState, fromUserDetails.selectUserId);
export const selectUser = createSelector(selectUserDataState, selectUserId, fromUserData.selectUserById);
export const selectInfosLoading = createSelector(selectUserDetailsState, fromUserDetails.selectLoading);

//user list
export const selectLoading = createSelector(selectUserListState, fromUserList.selectLoading);

export const selectEditUserId = createSelector(selectUserEditState, fromUserEdit.selectUserId);
export const selectEditUser = createSelector(selectUserDataState, selectEditUserId, fromUserData.selectUserById);
export const selectEditVisible = createSelector(selectUserEditState, fromUserEdit.selectVisible);
export const selectEditMode = createSelector(selectUserEditState, fromUserEdit.selectMode);
