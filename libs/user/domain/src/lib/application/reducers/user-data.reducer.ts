// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import { User } from '../../entities';
import { UserDetailsActions, UserEditActions, UserListActions } from '../actions';

export type State = EntityState<User>;

export const adapter = createEntityAdapter<User>({
  selectId: user => user.id
});

export const initialState: State = adapter.getInitialState();

export const reducer = createReducer<State>(
  initialState,
  on(UserListActions.loadAllSuccess, (state, { payload }) => adapter.setAll(payload, state)),
  on(UserDetailsActions.loadSuccess, (state, { payload }) => adapter.addOne(payload, state)),
  on(UserEditActions.createUserSuccess, (state, { payload }) => adapter.addOne(payload, state)),
  on(UserEditActions.updateUserSuccess, (state, { payload }) => adapter.upsertOne(payload, state)),
  on(UserListActions.deleteSuccess, (state, { payload }) => adapter.removeOne(payload, state)),
);

const {
  selectAll,
} = adapter.getSelectors();

export const selectAllUsers = selectAll;
export const selectUserById = (state: State, userId: string) => state.entities[userId];
