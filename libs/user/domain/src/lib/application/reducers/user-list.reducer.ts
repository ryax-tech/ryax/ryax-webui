// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createReducer, on } from '@ngrx/store';
import { UserListActions } from '../actions';

export interface State {
  loading: boolean;
}

export const initialState: State = {
  loading: false,
};

export const reducer = createReducer<State>(
  initialState,
  on(UserListActions.init, UserListActions.refresh, (state) => ({
    ...state,
    loading: true,
  })),
  on(UserListActions.loadAllSuccess, UserListActions.loadAllError, (state) => ({
    ...state,
    loading: false
  })),
);

export const selectLoading = (state: State) => state.loading;
