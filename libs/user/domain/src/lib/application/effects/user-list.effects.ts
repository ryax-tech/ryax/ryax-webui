// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { from, of } from 'rxjs';
import { catchError, flatMap, map, switchMap, tap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { UserEditActions, UserListActions } from '../actions';
import { UserApi } from '../../infrastructure/services';
import { MessageService } from '@ryax/shared/ui-common';
import { Router } from '@angular/router';

@Injectable()
export class UserListEffects {
  constructor(
    private readonly actions$: Actions,
    private readonly userApi: UserApi,
    private readonly messageService: MessageService,
    private readonly router: Router
  ) {}

  loadUsers$ = createEffect(() => this.actions$.pipe(
    ofType(
      UserListActions.init,
      UserListActions.refresh,
      UserEditActions.createUserSuccess
      ),
    switchMap(() => this.userApi.loadAll().pipe(
      map(users => UserListActions.loadAllSuccess(users)),
      catchError((err) => {
        if(err.status === 401) {
          this.router.navigate(['/', 'error', 'unauthorized']);
          return of(UserListActions.loadAllError(err))
        } else {
          return of(UserListActions.loadAllError())
        }
      }),
    ))
  ));

  deleteUser$ = createEffect(() => this.actions$.pipe(
    ofType(UserListActions.deleteUser),
    switchMap(({ payload }) => this.userApi.delete(payload).pipe(
      flatMap(() => [
        UserListActions.deleteSuccess(payload),
        UserListActions.notifySuccess('User deleted successfully')
      ]),
      catchError(() => from([
        UserListActions.deleteError(),
        UserListActions.notifyError('User deletion failed')
      ]))
    ))
  ));

  notifySuccess$ = createEffect(() => this.actions$.pipe(
    ofType(UserListActions.notifySuccess),
    tap(({ payload }) => this.messageService.displaySuccessMessage(payload))
  ), { dispatch: false });

  notifyError$ = createEffect(() => this.actions$.pipe(
    ofType(UserListActions.notifyError),
    tap(({ payload }) => this.messageService.displayErrorMessage(payload))
  ), { dispatch: false });
}
