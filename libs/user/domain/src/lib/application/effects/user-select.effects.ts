// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { UserListActions, UserSelectActions } from '../actions';
import { UserApi } from '../../infrastructure/services';

@Injectable()
export class UserSelectEffects {
  constructor(
    private readonly actions$: Actions,
    private readonly userApi: UserApi
  ) {}

  loadUsers$ = createEffect(() => this.actions$.pipe(
    ofType(UserSelectActions.init),
    switchMap(() => this.userApi.loadAll().pipe(
      map(users => UserListActions.loadAllSuccess(users)),
      catchError(() => of(UserListActions.loadAllError()))
    ))
  ))
}
