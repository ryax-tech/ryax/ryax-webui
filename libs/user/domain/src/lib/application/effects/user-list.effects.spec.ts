// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { UserApi } from '../../infrastructure/services';
import { MessageService } from '@ryax/shared/ui-common';
import { Observable } from 'rxjs';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Store } from '@ngrx/store';
import { User, UserRole } from '@ryax/user/domain';
import { UserState } from '../reducers';
import { cold, hot } from 'jest-marbles';
import { UserListActions } from '../actions';
import { UserListEffects } from './user-list.effects';
import { Router } from '@angular/router';

function provideMockUserApi() {
  return {
    provide: UserApi,
    useValue: {
      loadAll: jest.fn(),
      create: jest.fn(),
      update: jest.fn(),
      delete: jest.fn(),
    }
  }
}

function provideMockMessageService() {
  return {
    provide: MessageService,
    useValue: {
      displaySuccessMessage: jest.fn(),
      displayErrorMessage: jest.fn()
    }
  }
}

function provideMockRouter() {
  return {
    provide: Router,
    useValue: {
      navigate: jest.fn(),
    }
  }
}

describe('UserListEffects', () => {
  let actions$: Observable<any>;
  let effect: UserListEffects;
  let userApi: UserApi;
  let messageService: MessageService;
  let store: MockStore<UserState>;
  let router: Router;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [],
      providers: [
        UserListEffects,
        provideMockStore({}),
        provideMockActions(() => actions$),
        provideMockUserApi(),
        provideMockMessageService(),
        provideMockRouter(),
      ]
    });

    effect = TestBed.get(UserListEffects);
    userApi = TestBed.get(UserApi);
    messageService = TestBed.get(MessageService);
    store = TestBed.get(Store);
    router = TestBed.get(Router);
  });

  describe('loadUsers$', () => {
    let users: User[];

    beforeEach(() => {
      users = [{
        id: "user_id",
        username: "user_username",
        email: "user_email",
        role: UserRole.Admin,
        description: "user_description",
      }];
    });

    xit.each([
      UserListActions.init(),
      UserListActions.refresh(),
    ])('when load success', (action) => {
      (userApi.loadAll as jest.Mock)
        .mockImplementation(() => cold('-a|', { a : users }));

      actions$ = hot('-a', {
        a: action
      });

      const expected$ = hot('--ab', {
        a: UserListActions.loadAllSuccess(users)
      });

      expect(effect.loadUsers$).toBeObservable(expected$)
    });

    it.each([
      UserListActions.init(),
      UserListActions.refresh(),
    ])('when load fails', (action) => {
      (userApi.loadAll as jest.Mock)
        .mockImplementation(() => cold('-#', { a : users }));

      actions$ = hot('-a', {
        a: action
      });

      const expected$ = hot('--a', {
        a: UserListActions.loadAllError(),
      });
      expect(effect.loadUsers$).toBeObservable(expected$)
    });
  });

  describe('deleteUser$', () => {
    const userId = "userId";

    it('when delete success', () => {
      (userApi.delete as jest.Mock)
        .mockImplementation(() => cold('-a|', { a : userId }));

      actions$ = hot('-a', {
        a: UserListActions.deleteUser(userId)
      });

      const expected$ = hot('--(ab)', {
        a: UserListActions.deleteSuccess(userId),
        b: UserListActions.notifySuccess('User deleted successfully')
      });

      expect(effect.deleteUser$).toBeObservable(expected$)
    });

    it('when delete fails', () => {
      (userApi.delete as jest.Mock)
        .mockImplementation(() => cold('-#', { a : userId }));

      actions$ = hot('-a', {
        a: UserListActions.deleteUser(userId)
      });

      const expected$ = hot('--(ab)', {
        a: UserListActions.deleteError(),
        b: UserListActions.notifyError('User deletion failed')
      });
      expect(effect.deleteUser$).toBeObservable(expected$)
    });
  });

  test('notifySuccess$', () => {
    const message = "message";
    actions$ = hot('-a', {
      a: UserListActions.notifySuccess(message),
    });
    expect(effect.notifySuccess$).toSatisfyOnFlush(() => {
      expect(messageService.displaySuccessMessage).toHaveBeenCalledWith(message)
    });
  });

  test('notifyError$', () => {
    const message = "message";
    actions$ = hot('-a', {
      a: UserListActions.notifyError(message),
    });
    expect(effect.notifyError$).toSatisfyOnFlush(() => {
      expect(messageService.displayErrorMessage).toHaveBeenCalledWith(message)
    });
  });

});
