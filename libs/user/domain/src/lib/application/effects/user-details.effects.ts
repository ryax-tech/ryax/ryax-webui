// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { of } from 'rxjs';
import { catchError, map, switchMap, tap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { UserDetailsActions, UserEditActions } from '../actions';
import { UserSettingsApi } from '../../infrastructure/services';
import { MessageService } from '@ryax/shared/ui-common';

@Injectable()
export class UserDetailsEffects {
  constructor(
    private readonly actions$: Actions,
    private readonly settingsApi: UserSettingsApi,
    private readonly messageService: MessageService,
  ) {}

  loadUser$ = createEffect(() => this.actions$.pipe(
    ofType(UserDetailsActions.init),
    switchMap(() => this.settingsApi.loadUser().pipe(
      map(user => UserDetailsActions.loadSuccess(user)),
      catchError(() => of(UserDetailsActions.loadError())),
    ))
  ));

  refreshUserDetails$ = createEffect(() => this.actions$.pipe(
    ofType(
      UserDetailsActions.refresh,
      UserEditActions.updateUserSuccess
    ),
    switchMap(() => this.settingsApi.loadUser().pipe(
      map(user => UserDetailsActions.loadSuccess(user)),
      catchError(() => of(UserDetailsActions.loadError())),
    ))
  ));

  notifySuccess$ = createEffect(() => this.actions$.pipe(
    ofType(UserDetailsActions.notifySuccess),
    tap(({ payload }) => this.messageService.displaySuccessMessage(payload))
  ), { dispatch: false });

  notifyError$ = createEffect(() => this.actions$.pipe(
    ofType(UserDetailsActions.notifyError),
    tap(({ payload }) => this.messageService.displayErrorMessage(payload))
  ), { dispatch: false });
}
