// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { of } from 'rxjs';
import { catchError, filter, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { UserApi, UserSettingsApi } from '../../infrastructure/services';
import { UserEditMode } from '../../entities';
import { selectEditMode, selectEditUserId, UserState } from '../reducers';
import { UserEditActions } from '../actions';
import { MessageService } from '@ryax/shared/ui-common';

@Injectable()
export class UserEditEffects {
  mode$ = this.store.select(selectEditMode);
  userId$ = this.store.select(selectEditUserId);

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store<UserState>,
    private readonly userApi: UserApi,
    private readonly userSettingsApi: UserSettingsApi,
    private readonly messageService: MessageService,
  ) {}

  createUser$ = createEffect(() => this.actions$.pipe(
    ofType(UserEditActions.save),
    withLatestFrom(this.mode$, ({ payload }, mode) => ({ payload, mode })),
    filter(({ mode }) => mode === UserEditMode.NewUser),
    switchMap(({ payload }) => this.userApi.create(payload).pipe(
      map((user) => UserEditActions.createUserSuccess(user)),
      catchError(() => of(UserEditActions.createUserError()))
    ))
  ));

  updateUser$ = createEffect(() => this.actions$.pipe(
    ofType(UserEditActions.save),
    withLatestFrom(this.mode$, this.userId$, ({ payload }, mode, userId) => ({ payload, mode, userId })),
    filter(({ mode, userId }) => userId && [UserEditMode.EditUserInfos, UserEditMode.EditUserPassword].includes(mode)),
    switchMap(({ userId, payload }) => this.userApi.update(userId, payload).pipe(
      map((user) => UserEditActions.updateUserSuccess(user)),
      catchError(() => of(UserEditActions.updateUserError()))
    ))
  ));

  updateSelf$ = createEffect(() => this.actions$.pipe(
    ofType(UserEditActions.saveSelf),
    withLatestFrom(this.mode$, ({ payload }, mode) => ({ payload, mode })),
    filter(({ mode }) => [UserEditMode.EditUserInfos, UserEditMode.EditUserPassword].includes(mode)),
    switchMap(({ payload }) => this.userSettingsApi.updateSelf(payload).pipe(
      map((user) => UserEditActions.updateUserSuccess(user)),
      catchError(() => of(UserEditActions.updateUserError()))
    ))
  ));

  notifySuccess$ = createEffect(() => this.actions$.pipe(
    ofType(UserEditActions.createUserSuccess, UserEditActions.updateUserSuccess),
    tap(({ notification }) => this.messageService.displaySuccessMessage(notification))
  ), { dispatch: false });

  notifyError$ = createEffect(() => this.actions$.pipe(
    ofType(UserEditActions.createUserError, UserEditActions.updateUserError),
    tap(({ notification }) => this.messageService.displayErrorMessage(notification))
  ), { dispatch: false });
}
