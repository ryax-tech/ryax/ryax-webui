// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { UserApi } from '../../infrastructure/services';
import { Observable } from 'rxjs';
import { UserSelectEffects } from './user-select.effects';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { UserState } from '../reducers';
import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Store } from '@ngrx/store';
import { User, UserRole } from '@ryax/user/domain';
import { cold, hot } from 'jest-marbles';
import { UserListActions, UserSelectActions } from '../actions';

function provideMockUserApi() {
  return {
    provide: UserApi,
    useValue: {
      loadAll: jest.fn(),
      create: jest.fn(),
      update: jest.fn(),
      delete: jest.fn(),
    }
  }
}

describe('UserSelectEffects', () => {
  let actions$: Observable<any>;
  let effect: UserSelectEffects;
  let userApi: UserApi;
  let store: MockStore<UserState>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [],
      providers: [
        UserSelectEffects,
        provideMockStore({}),
        provideMockActions(() => actions$),
        provideMockUserApi(),
      ]
    });

    effect = TestBed.get(UserSelectEffects);
    userApi = TestBed.get(UserApi);
    store = TestBed.get(Store);
  });

  describe('loadUsers$', () => {
    let users: User[];

    beforeEach(() => {
      users = [{
        id: "user_id",
        username: "user_username",
        email: "user_email",
        role: UserRole.Admin,
        description: "user_description",
      }];
    });

    it('when load success', () => {
      (userApi.loadAll as jest.Mock)
        .mockImplementation(() => cold('-a|', { a : users }));

      actions$ = hot('-a', {
        a: UserSelectActions.init()
      });

      const expected$ = hot('--a', {
        a: UserListActions.loadAllSuccess(users)
      });

      expect(effect.loadUsers$).toBeObservable(expected$)
    });

    it('when load fails', () => {
      (userApi.loadAll as jest.Mock)
        .mockImplementation(() => cold('-#', { a : users }));

      actions$ = hot('-a', {
        a: UserSelectActions.init()
      });

      const expected$ = hot('--a', {
        a: UserListActions.loadAllError(),
      });
      expect(effect.loadUsers$).toBeObservable(expected$)
    });
  });

});
