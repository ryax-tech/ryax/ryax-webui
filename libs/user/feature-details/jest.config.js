module.exports = {
  name: 'user-feature-details',
  preset: '../../../jest.config.js',
  coverageDirectory: '../../../coverage/libs/user/feature-details',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js',
  ],
};
