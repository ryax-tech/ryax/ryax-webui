// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'ryax-user-details-header',
  templateUrl: './user-details-header.component.pug'
})
export class UserDetailsHeaderComponent {
  @Output() refresh = new EventEmitter<void>();

  onRefresh() {
    this.refresh.emit();
  }
}
