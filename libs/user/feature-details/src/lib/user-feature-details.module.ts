// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserDetailsComponent } from './user-details/user-details.component';
import { UserDetailsHeaderComponent } from './user-details-header/user-details-header.component';
import { UserDetailsBodyComponent } from './user-details-body/user-details-body.component';
import { SharedUiCommonModule } from '@ryax/shared/ui-common';
import { UserDomainModule } from '@ryax/user/domain';
import { UserFeatureEditModule } from '@ryax/user/feature-edit';
import { UserUiModule } from '@ryax/user/ui';

@NgModule({
  imports: [
    CommonModule,
    SharedUiCommonModule,
    UserDomainModule,
    UserFeatureEditModule,
    UserUiModule
  ],
  declarations: [
    UserDetailsComponent,
    UserDetailsHeaderComponent,
    UserDetailsBodyComponent,
  ],
  exports: [
    UserDetailsComponent
  ],
})
export class UserFeatureDetailsModule {}
