// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { User } from '@ryax/user/domain';

@Component({
  selector: 'ryax-user-details-body',
  templateUrl: './user-details-body.component.pug',
  styleUrls: ['./user-details-body.component.scss'],
})
export class UserDetailsBodyComponent {
  @Input() loading: boolean;
  @Input() user: User;
  @Output() editInfos = new EventEmitter<User>();
  @Output() editPassword = new EventEmitter<User>();

  get hasDescription() {
    return this.user.description;
  }

  onEditInfos(user: User) {
    this.editInfos.emit(user);
  }

  onEditPassword(user: User) {
    this.editPassword.emit(user);
  }
}
