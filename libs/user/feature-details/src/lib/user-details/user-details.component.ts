// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, OnInit } from '@angular/core';
import { User, UserDetailsFacade } from '@ryax/user/domain';

@Component({
  selector: 'ryax-user-details',
  templateUrl: './user-details.component.pug'
})
export class UserDetailsComponent implements OnInit {
  loading$ = this.facade.loading$;
  user$ = this.facade.user$;

  constructor(
    private readonly facade: UserDetailsFacade
  ) {}

  ngOnInit(): void {
    this.facade.init();
  }

  onRefresh() {
    this.facade.refresh();
  }

  onEditUserInfos(user: User) {
    this.facade.editUserInfos(user);
  }

  onEditUserPassword(user: User) {
    this.facade.editUserPassword(user);
  }
}
