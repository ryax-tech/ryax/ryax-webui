// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedUiCommonModule } from '@ryax/shared/ui-common';
import { UserRoleColorPipe } from './pipes/user-role-color.pipe';
import { UserRoleLabelPipe } from './pipes/user-role-label.pipe';

@NgModule({
  imports: [
    CommonModule,
    SharedUiCommonModule
  ],
  declarations: [
    UserRoleColorPipe,
    UserRoleLabelPipe
  ],
  exports: [
    UserRoleColorPipe,
    UserRoleLabelPipe
  ],
})
export class UserUiModule {}
