// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Pipe, PipeTransform } from '@angular/core';
import { UserRole } from '@ryax/user/domain';

@Pipe({
  name: "ryaxUserRoleColor",
  pure: true
})
export class UserRoleColorPipe implements PipeTransform {
  transform(value: UserRole): string {
    switch (value) {
      case UserRole.Anonymous:
        return "green";
      case UserRole.User:
        return "cyan";
      case UserRole.Developer:
        return "blue";
      case UserRole.Admin:
        return "geekblue";
    }
  }
}
