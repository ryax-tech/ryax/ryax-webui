module.exports = {
  name: 'user-ui',
  preset: '../../../jest.config.js',
  coverageDirectory: '../../../coverage/libs/user/ui',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js',
  ],
};
