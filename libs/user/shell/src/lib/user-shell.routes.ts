// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { UserListComponent } from '@ryax/user/feature-list';
import { UserDetailsComponent } from '@ryax/user/feature-details';

export const routes = [
  {
    path: '',
    patchMatch: "full",
    redirectTo: "list"
  },
  {
    path: 'list',
    component: UserListComponent,
    data: {
      breadcrumb: 'List'
    }
  },
  {
    path: 'settings',
    component: UserDetailsComponent,
    data: {
      breadcrumb: 'Settings'
    }
  }
];
