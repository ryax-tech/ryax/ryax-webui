// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { UserFeatureListModule } from '@ryax/user/feature-list';
import { routes } from './user-shell.routes';
import { UserFeatureDetailsModule } from '@ryax/user/feature-details';

@NgModule({
  imports: [
    CommonModule,
    UserFeatureListModule,
    UserFeatureDetailsModule,
    RouterModule.forChild(routes)
  ],
})
export class UserShellModule {}
