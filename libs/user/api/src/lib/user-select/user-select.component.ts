// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, forwardRef, Input, OnDestroy, OnInit } from '@angular/core';
import { ControlValueAccessor, FormBuilder, NG_VALUE_ACCESSOR } from '@angular/forms';
import { UserSelectFacade } from '@ryax/user/domain';
import { Subject } from 'rxjs';
import { takeUntil, withLatestFrom } from 'rxjs/operators';

@Component({
  selector: 'ryax-user-select',
  templateUrl: './user-select.component.pug',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => UserSelectComponent),
      multi: true
    }
  ]
})
export class UserSelectComponent implements OnInit, OnDestroy, ControlValueAccessor {
  users$ = this.facade.users$;
  destroy$ = new Subject();
  control = this.formBuilder.control(null);

  @Input() placeholder = "Select user";

  constructor(
    private readonly facade: UserSelectFacade,
    private readonly formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.facade.init();
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  registerOnChange(fn: any): void {
    this.control.valueChanges
      .pipe(takeUntil(this.destroy$))
      .subscribe(userId => fn(userId));
  }

  registerOnTouched(fn: any): void {
    this.control.valueChanges
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => fn());
  }

  writeValue(userId: string): void {
    this.control.setValue(userId, { emitEvent: false });
  }

  setDisabledState(isDisabled: boolean): void {
    if(isDisabled) {
      this.control.disable({ emitEvent: false });
    } else {
      this.control.enable({ emitEvent: false });
    }
  }
}
