// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedUiCommonModule } from '@ryax/shared/ui-common';
import { UserDomainModule } from '@ryax/user/domain';
import { UserSelectComponent } from './user-select/user-select.component';

@NgModule({
  imports: [
    CommonModule,
    UserDomainModule,
    SharedUiCommonModule,
  ],
  declarations: [
    UserSelectComponent
  ],
  exports: [
    UserSelectComponent
  ]
})
export class UserApiModule {}
