// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, EventEmitter, forwardRef, Input, Output } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { NzUploadFile, NzUploadListType } from 'ng-zorro-antd/upload';

@Component({
  selector: "ryax-file-input",
  templateUrl: "./file-input.component.pug",
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => FileInputComponent),
      multi: true
    }
  ]
})
export class FileInputComponent implements ControlValueAccessor {
  private onChange: Function;
  private onTouched: Function;
  public fileName: string;
  private disabled: boolean;

  @Input() listType: NzUploadListType = "text";
  @Output() delete = new EventEmitter<any>();

  get isDisabled() {
    return this.disabled;
  }

  get hasFile() {
    return !!this.fileName;
  }

  get isUploadZone() {
    return this.listType === 'picture-card'
  }

  beforeUpload = (file: NzUploadFile): boolean => {
    this.fileName = file.name;
    this.onChange(file);
    this.onTouched(file);
    return false;
  };

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  writeValue(value: string | undefined): void {
   this.fileName = value;
  }

  clearFile() {
    this.fileName = null;
    this.onChange(null);
    this.onTouched(null);
    this.delete.emit();
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }
}
