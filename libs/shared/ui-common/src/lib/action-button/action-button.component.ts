// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, Input } from '@angular/core';
import { NzButtonShape, NzButtonType } from 'ng-zorro-antd/button';
import { NzSizeLDSType } from 'ng-zorro-antd/core/types';

@Component({
  selector: 'ryax-action-button',
  templateUrl: './action-button.component.pug'
})
export class ActionButtonComponent {
  iconTheme = "outline";
  iconThemeColor = null;

  @Input() label: string;
  @Input() icon: string = null;
  @Input() set iconColor(value: string) {
    this.iconTheme = value ? "twotone" : "outline";
    this.iconThemeColor = value ? value : null;
  }
  @Input() type: NzButtonType = null;
  @Input() size: NzSizeLDSType = "default";
  @Input() shape: NzButtonShape = null;
  @Input() tooltipText: string;
  @Input() tooltipPlacement = "top";
  @Input() disabled = false;
  @Input() danger = false;
}
