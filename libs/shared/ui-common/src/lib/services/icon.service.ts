// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { IconDefinition } from '@ant-design/icons-angular';
import {
  BarsOutline,
  CaretDownOutline,
  CaretUpOutline, CheckCircleTwoTone, CloseCircleOutline,
  DashboardOutline,
  DeleteTwoTone,
  EditOutline,
  EllipsisOutline, ExclamationCircleTwoTone,
  EyeOutline,
  FileOutline,
  FilterOutline,
  LockOutline,
  LoginOutline, LogoutOutline,
  MinusSquareOutline,
  PlayCircleOutline,
  PlayCircleTwoTone,
  PlusOutline,
  PlusSquareOutline,
  PoweroffOutline,
  QuestionCircleOutline,
  SearchOutline, StepForwardOutline,
  StopOutline,
  SyncOutline,
  ToolOutline, UploadOutline,
  UserOutline,
} from '@ant-design/icons-angular/icons';
import { NZ_ICONS } from 'ng-zorro-antd/icon';

const icons: IconDefinition[] = [
  QuestionCircleOutline,
  ToolOutline,
  EyeOutline,
  SyncOutline,
  PlusOutline,
  EditOutline,
  SearchOutline,
  CaretUpOutline,
  CaretDownOutline,
  FilterOutline,
  PlusSquareOutline,
  MinusSquareOutline,
  FileOutline,
  BarsOutline,
  EllipsisOutline,
  DashboardOutline,
  PlayCircleOutline,
  PlayCircleTwoTone,
  StopOutline,
  DeleteTwoTone,
  PoweroffOutline,
  UserOutline,
  LockOutline,
  LoginOutline,
  LogoutOutline,
  StepForwardOutline,
  UploadOutline,
  CloseCircleOutline,
  ExclamationCircleTwoTone,
  CheckCircleTwoTone,
];

export const IconService = {
  provide: NZ_ICONS, useValue: icons
};
