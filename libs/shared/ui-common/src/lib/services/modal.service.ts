// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';

@Injectable()
export class ModalService {

  constructor(
    private readonly modal: NzModalService
  ) {}

  confirm(title: string, description: string, okText: string = "Ok", cancelText: string = "Cancel") {
    const modalRef: NzModalRef<void, boolean> = this.modal.confirm({
      nzTitle: title,
      nzContent: description,
      nzOkText: okText,
      nzCancelText: cancelText,
      nzOnOk: () => true,
    });
    return modalRef.afterClose;
  }
}
