// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';

@Injectable()
export class MessageService {

  constructor(
    private readonly message: NzMessageService
  ) {}

  displaySuccessMessage(message: string) {
    this.message.success(message);
  }

  displayErrorMessage(message: string) {
    this.message.error(message);
  }
}
