// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { NzUploadFile } from 'ng-zorro-antd/upload';

@Component({
  selector: "ryax-directory-input",
  templateUrl: "./directory-input.component.pug",
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DirectoryInputComponent),
      multi: true
    }
  ]
})
export class DirectoryInputComponent implements ControlValueAccessor {
  private onChange: Function;
  private onTouched: Function;
  private disabled: boolean;

  fileList: NzUploadFile[] = [];

  beforeUpload = (file: NzUploadFile, uploadedFiles: NzUploadFile[]): boolean => {
  // beforeUpload = (file: NzUploadFile): boolean => {
    if(this.fileList != null) {
      // uploadedFiles = uploadedFiles.concat(file);
      // uploadedFiles.pop();
      this.fileList = this.fileList.concat(file);
    }

    this.onChange(file);
    this.onTouched(file);

    this.onChange(this.fileList);
    this.onTouched(this.fileList);

    // this.onChange(uploadedFiles);
    // this.onTouched(uploadedFiles);
    return false;
  };

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  writeValue(value: NzUploadFile[]): void {
    if(value != null && this.fileList != null) {
      this.fileList = value;
    }
  }
}
