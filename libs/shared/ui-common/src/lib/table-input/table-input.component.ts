// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import {
  Component, ElementRef, EventEmitter,
  forwardRef, Output,
} from '@angular/core';
import * as XLSX from 'xlsx';
import * as canvasDatagrid from 'canvas-datagrid';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { NzUploadFile } from 'ng-zorro-antd/upload';
import { ViewChild } from '@angular/core';

@Component({
  selector: 'ryax-table-input',
  templateUrl: './table-input.component.pug',
  styleUrls: ['./table-input.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => TableInputComponent),
      multi: true
    }
  ]
})
export class TableInputComponent implements ControlValueAccessor {
  @Output() unsupportedExtension = new EventEmitter<void>();
  @ViewChild('fileInput') fileInput: ElementRef;

  private onChange: Function;
  private onTouched: Function;

  public fileName: string;
  public workbook = null;
  public sheets = null;
  public grid = null;
  public data = [];

  beforeUpload = (file: NzUploadFile): boolean => {
    this.fileName = file.name;
    this.onChange(file);
    this.onTouched(file);
    return false;
  };

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  writeValue(value: string | undefined): void {
    this.fileName = value;
  }

  onFileChange(evt: any) {
    /* wire up file reader */
    const target: DataTransfer = <DataTransfer>(evt.target);

    if (target.files.length !== 1) throw new Error('Cannot use multiple files');

    const str = target.files[0].name;
    const slug = str.split('.').pop();
    switch (slug) {
      case 'xlsx':
      case 'xlsm':
      case 'xlsb':
      case 'biff8':
      case 'biff5':
      case 'biff2':
      case 'xlml':
      case 'ods':
      case 'fods':
      case 'csv':
      case 'txt':
      case 'sylk':
      case 'html':
      case 'dif':
      case 'dbf':
      case 'rtf':
      case 'prn':
      case 'eth':
        console.log('Supported file extension');
        break;
      default:
        this.unsupportedExtension.emit();
        this.reset();
    }

    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      /* read workbook */
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary', sheetStubs: true });

      this.workbook = wb;
      this.sheets = wb.Sheets;

      /* grab first sheet */
      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];

      /* save data */
      this.data = <any>(XLSX.utils.sheet_to_json(ws, { header: 1, defval: '', blankrows: true })); // <AOA>
      this.beforeUpload(target.files[0] as any);

      /* canvas grid thing */
      this.grid = canvasDatagrid({
        parentNode: document.getElementById('gridctr'),
        editable: false,
      });
      this.grid.data = this.data;
    };
    reader.readAsBinaryString(target.files[0]);
  }

  changeSheet(sheet: string) {
    const ws: XLSX.WorkSheet = this.workbook.Sheets[sheet];
    this.data = <any>(XLSX.utils.sheet_to_json(ws, { header: 1, defval: '', blankrows: true }));
    this.grid.data = this.data;
  }

  reset() {
    this.fileInput.nativeElement.value = "";
  }

}
