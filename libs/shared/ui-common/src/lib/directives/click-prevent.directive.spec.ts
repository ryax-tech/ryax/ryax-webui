// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { ClickPreventDirective } from './click-prevent.directive';

describe('ClickPreventDirective', () => {
  it('should create an instance', () => {
    const directive = new ClickPreventDirective();
    expect(directive).toBeTruthy();
  });
});
