// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { AfterViewInit, Directive, ElementRef } from '@angular/core';
import * as $ from 'jquery';

@Directive({
  selector: 'input[ryaxAutofocus]'
})
export class AutofocusDirective implements AfterViewInit {

  constructor(
    private readonly elementRef: ElementRef
  ) {}

  ngAfterViewInit(): void {
    setTimeout(() => $(this.elementRef.nativeElement).focus(), 100);
  }
}
