// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, Input } from '@angular/core';
import { ModuleKind } from './module-kind';

@Component({
  selector: 'ryax-module-identifier',
  templateUrl: './module-identifier.component.pug',
  styleUrls: ['./module-identifier.component.scss'],
})

export class ModuleIdentifierComponent {
  @Input() kind: ModuleKind;

  getModuleKindColor(kind: ModuleKind){
    switch (kind) {
      case ModuleKind.SOURCE:
        return 'source';
      case ModuleKind.PROCESSOR:
        return 'processor';
      case ModuleKind.PUBLISHER:
        return 'publisher';
      case ModuleKind.STREAM_OPERATOR:
        return 'operator';
      default:
        return null;
    }
  }
}
