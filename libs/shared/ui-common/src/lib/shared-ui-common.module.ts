// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzTypographyModule } from 'ng-zorro-antd/typography';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzBreadCrumbModule } from 'ng-zorro-antd/breadcrumb';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzPageHeaderModule } from 'ng-zorro-antd/page-header';
import { NzPaginationModule } from 'ng-zorro-antd/pagination';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzRadioModule } from 'ng-zorro-antd/radio';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzUploadModule } from 'ng-zorro-antd/upload';
import { NzAvatarModule } from 'ng-zorro-antd/avatar';
import { NzBadgeModule } from 'ng-zorro-antd/badge';
import { NzDescriptionsModule } from 'ng-zorro-antd/descriptions';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzEmptyModule } from 'ng-zorro-antd/empty';
import { NzListModule } from 'ng-zorro-antd/list';
import { NzPopconfirmModule } from 'ng-zorro-antd/popconfirm';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzTabsModule } from 'ng-zorro-antd/tabs';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzAlertModule } from 'ng-zorro-antd/alert';
import { NzDrawerModule } from 'ng-zorro-antd/drawer';
import { NzMessageModule } from 'ng-zorro-antd/message';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzSpinModule } from 'ng-zorro-antd/spin';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { NzCollapseModule } from 'ng-zorro-antd/collapse';
import { ClickPreventDirective } from './directives/click-prevent.directive';
import { AutofocusDirective } from './directives/autofocus.directive';
import { DefaultPipe } from './pipes/default.pipe';
import { DurationPipe } from './pipes/duration.pipe';
import { CreateButtonComponent } from './create-button/create-button.component';
import { UploadButtonComponent } from './upload-button/upload-button.component';
import { ActionButtonComponent } from './action-button/action-button.component';
import { ConfirmButtonComponent } from './confirm-button/confirm-button.component';
import { LogoAvatarComponent } from './logo-avatar/logo-avatar.component';
import { StatusTagComponent } from './status-tag/status-tag.component';
import { LinkButtonComponent } from './link-button/link-button.component';
import { DeleteButtonComponent } from './delete-button/delete-button.component';
import { EditButtonComponent } from './edit-button/edit-button.component';
import { ModalService } from './services/modal.service';
import { MessageService } from './services/message.service';
import { IconService } from './services/icon.service';
import { FileInputComponent } from './file-input/file-input.component';
import { ActionDropdownComponent } from './action-dropdown/action-dropdown.component';
import { en_US, NZ_I18N } from 'ng-zorro-antd/i18n';
import { ModuleIdentifierComponent } from './module-identifier/module-identifier.component';
import { DirectoryInputComponent } from './directory-input/directory-input.component';
import { NzTransferModule } from 'ng-zorro-antd/transfer';
import { TableInputComponent } from './table-input/table-input.component';
import { NzProgressModule } from 'ng-zorro-antd/progress';
import { NzResultModule } from 'ng-zorro-antd/result';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    ...SharedUiCommonModule.NgZorroModules,
  ],
  providers: SharedUiCommonModule.Providers,
  declarations: [
    ...SharedUiCommonModule.Components,
    ...SharedUiCommonModule.Directives,
    ...SharedUiCommonModule.Pipes,
  ],
  exports: [
    RouterModule,
    ReactiveFormsModule,
    ...SharedUiCommonModule.NgZorroModules,
    ...SharedUiCommonModule.Components,
    ...SharedUiCommonModule.Directives,
    ...SharedUiCommonModule.Pipes,
  ]
})
export class SharedUiCommonModule {

  static NgZorroModules: any[] = [
    NzButtonModule,
    NzIconModule,
    NzTypographyModule,
    NzGridModule,
    NzBreadCrumbModule,
    NzDropDownModule,
    NzMenuModule,
    NzPageHeaderModule,
    NzPaginationModule,
    NzFormModule,
    NzInputModule,
    NzRadioModule,
    NzSelectModule,
    NzUploadModule,
    NzAvatarModule,
    NzBadgeModule,
    NzCardModule,
    NzDescriptionsModule,
    NzEmptyModule,
    NzListModule,
    NzPopconfirmModule,
    NzTableModule,
    NzTabsModule,
    NzTagModule,
    NzToolTipModule,
    NzAlertModule,
    NzDrawerModule,
    NzMessageModule,
    NzModalModule,
    NzSpinModule,
    NzDividerModule,
    NzCollapseModule,
    NzTransferModule,
    NzProgressModule,
    NzResultModule
  ];

  static Providers = [
    { provide: NZ_I18N, useValue: en_US },
    IconService,
    ModalService,
    MessageService,
  ];

  static Components = [
    CreateButtonComponent,
    EditButtonComponent,
    DeleteButtonComponent,
    LinkButtonComponent,
    UploadButtonComponent,
    StatusTagComponent,
    LogoAvatarComponent,
    ActionButtonComponent,
    ConfirmButtonComponent,
    FileInputComponent,
    ActionDropdownComponent,
    ModuleIdentifierComponent,
    DirectoryInputComponent,
    TableInputComponent
  ];

  static Directives = [
    ClickPreventDirective,
    AutofocusDirective,
  ];

  static Pipes = [
    DurationPipe,
    DefaultPipe,
  ];
}
