// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { NzButtonType } from 'ng-zorro-antd/button';
import { NzSizeLDSType } from 'ng-zorro-antd/core/types';
import { NzUploadListType } from 'ng-zorro-antd/upload';

@Component({
  selector: 'ryax-upload-button',
  templateUrl: './upload-button.component.pug',
  styleUrls: ['./upload-button.component.scss'],
})
export class UploadButtonComponent {
  @Input() label: string;
  @Input() type: NzButtonType = null;
  @Input() icon: string = null;
  @Input() size: NzSizeLDSType = "default";
  @Input() listType: NzUploadListType = "text";
  @Output() upload = new EventEmitter<File>();

  get isCard() {
    return this.listType === 'picture-card'
  }

  beforeUpload = (file: File) => {
    this.upload.emit(file);
    return false;
  };
}
