// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, Input } from '@angular/core';
import { NzSizeLDSType } from 'ng-zorro-antd/core/types';

@Component({
  selector: 'ryax-create-button',
  templateUrl: './create-button.component.pug'
})
export class CreateButtonComponent {
  @Input() size: NzSizeLDSType;
  @Input() tooltipText: string;
}
