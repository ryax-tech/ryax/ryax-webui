// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { NzButtonShape } from 'ng-zorro-antd/button';
import { NzSizeLDSType } from 'ng-zorro-antd/core/types';
import { NzDropdownMenuComponent, NzPlacementType } from 'ng-zorro-antd/dropdown';

@Component({
  selector: 'ryax-action-dropdown',
  templateUrl: './action-dropdown.component.pug',
  styleUrls: ['./action-dropdown.component.scss'],
})
export class ActionDropdownComponent {
  @Input() size: NzSizeLDSType = "default";
  @Input() shape: NzButtonShape = null;
  @Input() icon: string = null;
  @Input() menu: NzDropdownMenuComponent;
  @Input() menuPlacement: NzPlacementType = "bottomCenter";
  @Input() disabled = false;
  @Output() visibleChange = new EventEmitter<boolean>();


  onVisibleChange(visible: boolean) {
    this.visibleChange.emit(visible);
  }
}
