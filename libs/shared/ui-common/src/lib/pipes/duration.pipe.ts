// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'duration'
})
export class DurationPipe implements PipeTransform {
  transform(value: number, args?: any): any {
    if(value) {
      const duration = moment.duration(value);
      return moment.utc(duration.asMilliseconds()).format('HH:mm:ss:SSS');
    } else {
      return null;
    }
  }
}
