// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, Input } from '@angular/core';
import { NzSizeLDSType } from 'ng-zorro-antd/core/types';

@Component({
  selector: 'ryax-edit-button',
  templateUrl: './edit-button.component.pug'
})
export class EditButtonComponent {
  @Input() size: NzSizeLDSType;
  @Input() tooltipText: string;
}
