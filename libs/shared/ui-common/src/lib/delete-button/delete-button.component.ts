// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { NzSizeLDSType } from 'ng-zorro-antd/core/types';

@Component({
  selector: 'ryax-delete-button',
  templateUrl: './delete-button.component.pug'
})
export class DeleteButtonComponent {
  @Input() size: NzSizeLDSType;
  @Input() tooltipText: string;
  @Input() confirmText = "Are you sure ?";
  @Output() confirmOk = new EventEmitter<void>();
  @Output() confirmCancel = new EventEmitter<void>();
}
