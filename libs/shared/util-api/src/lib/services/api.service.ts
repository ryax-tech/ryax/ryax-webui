// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

@Injectable()
export class Api {
  private contentRangeHeaderKey = "Content-Range";

  public readonly baseUrl: string = "api";

  constructor(
    private readonly _http: HttpClient
  ) {}

  private buildRequestUrl(name: string) {
    return this.baseUrl + name;
  }

  private buildRequestParams(params: Object) {
    return Object.keys(params)
      .filter(key => !!params[key])
      .reduce((prev, key) => prev.set(key, params[key]), new HttpParams());
  }

  private buildRequestFormData(fieldName: string, file: File) {
    const formData = new FormData();
    formData.append(fieldName, file);
    return formData;
  }

  getUrl(name: string) {
    return this.baseUrl + name;
  }

  parseContentRangeHeader(headers: HttpHeaders) {
    if(!headers.has(this.contentRangeHeaderKey)) return null;
    const headerData = headers.get(this.contentRangeHeaderKey);
    const matches = headerData.match(/(\d+)-(\d+)\/(\d+|\*)/);
    return {
      startIndex: matches ? parseInt(matches[1], 0) : null,
      endIndex: matches ? parseInt(matches[2], 0) : null,
      total: matches && matches[3] !== "*" ? parseInt( matches[3], 0) : null,
    }
  }

  get<T = any>(name: string, params?: Object, responseType: any = 'json') {
    const requestUrl = this.buildRequestUrl(name);
    const requestParams = params? this.buildRequestParams(params) : undefined;
    return this._http.get<T>(requestUrl, { params: requestParams, observe: "body", responseType });
  }

  getWithResponse<T = any>(name: string, params?: Object, responseType: any = 'json') {
    const requestUrl = this.buildRequestUrl(name);
    const requestParams = params? this.buildRequestParams(params) : undefined;
    return this._http.get<T>(requestUrl, { params: requestParams, observe: "response", responseType });
  }

  post<T = any>(name: string, data?: Object) {
    const requestUrl = this.buildRequestUrl(name);
    return this._http.post<T>(requestUrl, data);
  }

  upload<T = any>(name: string, fieldName: string, file: File) {
    const requestUrl = this.buildRequestUrl(name);
    const requestFormData = this.buildRequestFormData(fieldName, file);
    return this._http.post<T>(requestUrl, requestFormData);
  }

  put<T = any>(name: string, data?: Object) {
    const requestUrl = this.buildRequestUrl(name);
    return this._http.put<T>(requestUrl, data);
  }

  patch<T = any>(name: string, data?: Object) {
    const requestUrl = this.buildRequestUrl(name);
    return this._http.patch<T>(requestUrl, data);
  }

  delete<T = any>(name: string) {
    const requestUrl = this.buildRequestUrl(name);
    return this._http.delete<T>(requestUrl);
  }
}
