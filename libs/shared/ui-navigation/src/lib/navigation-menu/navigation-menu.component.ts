// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'ryax-navigation-menu',
  templateUrl: './navigation-menu.component.pug',
  styleUrls: ['./navigation-menu.component.scss']
})
export class NavigationMenuComponent {
  @Input() isCollapsed: boolean;
  @Output() logout = new EventEmitter<void>();

  get logoSrc(): string {
    return this.isCollapsed ?
      "assets/logo-white-small.svg":
      "assets/logo-white.svg";
  }

  onLogout() {
    this.logout.emit();
  }
}
