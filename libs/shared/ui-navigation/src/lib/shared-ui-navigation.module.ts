// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzPopoverModule } from 'ng-zorro-antd/popover';
import { NavigationMenuComponent } from './navigation-menu/navigation-menu.component';
import { AuthorizationApiModule } from '@ryax/authorization/api';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    NzMenuModule,
    NzPopoverModule,
    NzIconModule,
    AuthorizationApiModule,
  ],
  declarations: [
    NavigationMenuComponent
  ],
  exports: [
    NavigationMenuComponent
  ]
})
export class SharedUiNavigationModule {}
