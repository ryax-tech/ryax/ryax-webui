// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, Input, TemplateRef } from '@angular/core';

@Component({
  selector: 'ryax-sider-layout',
  templateUrl: './sider-layout.component.pug',
  styleUrls: ['./sider-layout.component.scss']
})
export class SiderLayoutComponent {
  isSiderCollapsed = false;

  @Input() sider: TemplateRef<void>;
  @Input() content: TemplateRef<void>;


  onCollapsedChange() {
    this.isSiderCollapsed = !this.isSiderCollapsed;
  }
}
