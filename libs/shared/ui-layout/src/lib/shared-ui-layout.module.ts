// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { BasicLayoutComponent } from './basic-layout/basic-layout.component';
import { SiderLayoutComponent } from './sider-layout/sider-layout.component';

@NgModule({
  imports: [
    CommonModule,
    NzLayoutModule
  ],
  declarations: [
    BasicLayoutComponent,
    SiderLayoutComponent,
  ],
  exports: [
    BasicLayoutComponent,
    SiderLayoutComponent,
  ]
})
export class SharedUiLayoutModule {}
