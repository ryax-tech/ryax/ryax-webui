// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, Input } from '@angular/core';
import { WorkflowPortal } from '@ryax/workflow/domain';

@Component({
  selector: 'ryax-workflow-portal-header',
  templateUrl: './workflow-portal-header.component.pug',
  styleUrls: ['./workflow-portal-header.component.scss']
})
export class WorkflowPortalHeaderComponent {
  @Input() portal: WorkflowPortal;

  get portalName() {
    return this.portal.customName ? this.portal.customName : this.portal.name;
  }
}
