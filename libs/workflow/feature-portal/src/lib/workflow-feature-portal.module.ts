// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WorkflowUiModule } from '@ryax/workflow/ui';
import { WorkflowDomainModule } from '@ryax/workflow/domain';
import { MonitoringApiModule } from '@ryax/monitoring/api';
import { WorkflowPortalAccessGuard } from './guards/workflow-portal-access.guard';
import { WorkflowPortalComponent } from './workflow-portal/workflow-portal.component';
import { WorkflowPortalHeaderComponent } from './workflow-portal-header/workflow-portal-header.component';
import { WorkflowPortalFormComponent } from './workflow-portal-form/workflow-portal-form.component';
import { AuthApiModule } from '@ryax/auth/api';

@NgModule({
  imports: [
    CommonModule,
    WorkflowUiModule,
    WorkflowDomainModule,
    MonitoringApiModule,
    AuthApiModule
  ],
  providers: [
    WorkflowPortalAccessGuard
  ],
  declarations: [
    WorkflowPortalComponent,
    WorkflowPortalHeaderComponent,
    WorkflowPortalFormComponent,
  ],
  exports: [
    WorkflowPortalComponent
  ]
})
export class WorkflowFeaturePortalModule {}
