// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { first, map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { WorkflowPortalFacade } from '@ryax/workflow/domain';

@Injectable()
export class WorkflowPortalAccessGuard implements CanActivate {
  private workflowPortal$ = this.facade.workflowPortal$;

  constructor(
    private readonly facade: WorkflowPortalFacade
  ) {}

  canActivate() {
    this.facade.init();
    return this.workflowPortal$.pipe(
      first(workflowPortal => !!workflowPortal),
      map(workflowPortal => !!workflowPortal)
    );
  }
}
