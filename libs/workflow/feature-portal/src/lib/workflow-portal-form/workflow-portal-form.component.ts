// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { WorkflowModuleIOType, WorkflowModuleOutput } from '@ryax/workflow/domain';

@Component({
  selector: 'ryax-workflow-portal-form',
  templateUrl: './workflow-portal-form.component.pug'
})
export class WorkflowPortalFormComponent implements OnChanges {
  @Input() loading: boolean;
  @Input() workflowPortalOutputs: WorkflowModuleOutput[];
  @Output() send = new EventEmitter<object>();
  @Output() unsupportedExtension = new EventEmitter<void>();

  form: FormGroup = this.formBuilder.group({});

  constructor(
    private readonly formBuilder: FormBuilder
  ) {}

  private initForm() {
    const newForm = this.formBuilder.group({});
    this.workflowPortalOutputs.forEach(item => {
      const outputControl = this.formBuilder.control(null, [Validators.required]);
      newForm.addControl(item.technicalName, outputControl);
    });
    this.form = newForm;
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes.workflowPortalOutputs) this.initForm();
  }

  hasTextInput(outputType: any) {
    return [WorkflowModuleIOType.STRING].includes(outputType);
  }

  hasTextArea(outputType: any) {
    return [WorkflowModuleIOType.LONGSTRING].includes(outputType);
  }

  hasPasswordInput(outputType: any) {
    return [WorkflowModuleIOType.PASSWORD].includes(outputType);
  }

  hasNumberInput(outputType: any) {
    return [WorkflowModuleIOType.INTEGER, WorkflowModuleIOType.FLOAT].includes(outputType);
  }

  hasSelectInput(outputType: any) {
    return [WorkflowModuleIOType.ENUM].includes(outputType);
  }

  hasFileInput(outputType: any) {
    return [WorkflowModuleIOType.FILE].includes(outputType);
  }

  hasDirectoryInput(outputType: any) {
    return [WorkflowModuleIOType.DIRECTORY].includes(outputType);
  }

  hasTableInput(outputType: any) {
    return [WorkflowModuleIOType.TABLE].includes(outputType);
  }

  // hasNoInput(outputType: any) {
  //   return [WorkflowModuleIOType.DIRECTORY].includes(outputType);
  // }

  onSubmit() {
    const value = this.form.value;
    this.send.emit(value);
  }

  onUnsupportedExtension() {
    this.unsupportedExtension.emit();
  }
}
