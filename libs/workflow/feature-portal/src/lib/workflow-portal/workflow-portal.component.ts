// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, OnInit } from '@angular/core';
import { WorkflowPortalFacade } from '@ryax/workflow/domain';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'ryax-workflow-portal',
  templateUrl: './workflow-portal.component.pug',
  styleUrls: ['./workflow-portal.component.scss'],
})
export class WorkflowPortalComponent {
  loading$ = this.facade.loading$;
  workflowPortal$ = this.facade.workflowPortal$;
  workflowPortalOutputs$ = this.facade.workflowPortalOutputs$;
  workflowPortalResult$ = this.facade.workflowPortalResult$;
  unsupportedExtensionModalVisibility$ = this.facade.unsupportedExtensionModalVisibility$;

  constructor(
    private readonly facade: WorkflowPortalFacade,
    private _location: Location,
  ) {}

  onSend(data: object) {
    this.facade.sendData(data);
  }

  onUnsupportedExtension() {
    this.facade.openUnsupportedExtensionModal()
  }

  closeUnsupportedExtensionModal() {
    this.facade.closeUnsupportedExtensionModal()
  }

  reloadPage() {
    document.location.reload()
  }
}
