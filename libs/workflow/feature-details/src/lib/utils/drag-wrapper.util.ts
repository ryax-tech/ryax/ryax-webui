// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { GridsterDraggable } from 'angular-gridster2/lib/gridsterDraggable.service';
import { WorkflowDetailsGraphConnectivity } from '../workflow-details-graph/workflow-details-graph-connectivity.service';
import { WorkflowDetailsGraphPan } from '../workflow-details-graph/workflow-details-graph-pan.service';

export const DraggableWrapper = (
  gridDraggable: GridsterDraggable,
  connectivity: WorkflowDetailsGraphConnectivity,
  view: WorkflowDetailsGraphPan
) => {

  // Wrap calculateItemPosition
  const oldCalculateItemPositionFromMousePosition = gridDraggable
    .calculateItemPositionFromMousePosition.bind(gridDraggable,);
  gridDraggable.calculateItemPositionFromMousePosition = (e: MouseEvent) => {
    const zoomScale = view.getZoomScale();
    const overrideEvent = new MouseEvent('mousemove', {
      ...e,
      clientX: e.clientX / zoomScale,
      clientY: e.clientY / zoomScale,
    });
    oldCalculateItemPositionFromMousePosition(overrideEvent);
  };

  // Wrap dragStart method
  const oldDragStart = gridDraggable.dragStart.bind(gridDraggable);
  gridDraggable.dragStart = (e: MouseEvent) => {
    const zommScale = view.getZoomScale();
    const overrideEvent = new MouseEvent('mousemove', {
      ...e,
      clientX: e.clientX / zommScale,
      clientY: e.clientY / zommScale,
    });
    oldDragStart(overrideEvent);
    connectivity.refreshNode(gridDraggable.gridsterItem.el);
  };

  // Wrap dragMove method
  const oldDragMove = gridDraggable.dragMove.bind(gridDraggable);
  gridDraggable.dragMove = (event: any) => {
    oldDragMove(event);
    connectivity.refreshNode(gridDraggable.gridsterItem.el);
  };

  return gridDraggable;
};
