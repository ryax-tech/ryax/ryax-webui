// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { WorkflowDetailsGraphPan } from '../workflow-details-graph/workflow-details-graph-pan.service';
import { GridsterEmptyCell } from 'angular-gridster2/lib/gridsterEmptyCell.service';
import { GridsterItem } from 'angular-gridster2';

export const EmptyCellWrapper = (
  gridsterEmptyCell: GridsterEmptyCell,
  view: WorkflowDetailsGraphPan
) => {

  // Wrap getValidateItem
  const oldGetValidItemFromEvent = gridsterEmptyCell.getValidItemFromEvent.bind(gridsterEmptyCell);
  gridsterEmptyCell.getValidItemFromEvent = (e: MouseEvent, oldItem?: GridsterItem | null) => {
    const zoomScale = view.getZoomScale();
    const rect = view.getGridContainerBoundingRect();
    const overrideEvent = new MouseEvent('dragover', {
      ...e,
      clientX: (e.clientX - rect.left) / zoomScale + rect.left,
      clientY: (e.clientY - rect.top) / zoomScale + rect.top,
    });
    return oldGetValidItemFromEvent(overrideEvent, oldItem);
  };

  return gridsterEmptyCell;
};
