// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Workflow, WorkflowDeploymentStatus, WorkflowPortal, WorkflowStatus } from '@ryax/workflow/domain';

@Component({
  selector: 'ryax-workflow-details-header',
  templateUrl: './workflow-details-header.component.pug'
})
export class WorkflowDetailsHeaderComponent {
  @Input() workflow: Workflow;
  @Input() workflowPortals: WorkflowPortal[] = [];
  @Output() refresh = new EventEmitter<void>();
  @Output() deploy = new EventEmitter<void>();
  @Output() stop = new EventEmitter<void>();
  @Output() edit = new EventEmitter<void>();
  @Output() copy = new EventEmitter<void>();
  @Output() delete = new EventEmitter<void>();
  @Output() showModal = new EventEmitter<Workflow>();
  @Output() export = new EventEmitter<void>();

  get isDeployVisible() {
    return this.workflow.status === WorkflowStatus.INVALID ||
      this.workflow.deploymentStatus !== WorkflowDeploymentStatus.NONE;
  }

  get isStopVisible() {
    return this.workflow.deploymentStatus === WorkflowDeploymentStatus.NONE ||
      this.workflow.deploymentStatus === WorkflowDeploymentStatus.UNDEPLOYING;
  }

  get isDeleteDisabled() {
    return this.workflow.deploymentStatus === WorkflowDeploymentStatus.DEPLOYING ||
      this.workflow.deploymentStatus === WorkflowDeploymentStatus.DEPLOYED;
  }

  get isEditDisabled() {
    return this.workflow.deploymentStatus === WorkflowDeploymentStatus.DEPLOYING ||
      this.workflow.deploymentStatus === WorkflowDeploymentStatus.DEPLOYED;
  }

  get isWorkflowStatusVisible() {
    return this.workflow.deploymentStatus === WorkflowDeploymentStatus.NONE
  }

  get isWorkflowDeployStatusVisible() {
    return this.workflow.deploymentStatus !== WorkflowDeploymentStatus.NONE
  }

  onRefresh() {
    this.refresh.emit();
  }

  onDeploy() {
    this.deploy.emit();
  }

  onStop() {
    this.stop.emit();
  }

  onCopy() {
    this.copy.emit();
  }

  onEdit() {
    this.edit.emit();
  }

  onDelete() {
    this.delete.emit();
  }

  onShowModal(workflow: Workflow) {
    this.showModal.emit(workflow);
  }

  onExport() {
    this.export.emit();
  }
}
