// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { first, map, tap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';
import { WorkflowDetailsFacade } from '@ryax/workflow/domain';

@Injectable()
export class CanAccessGuard implements CanActivate {

  constructor(
    private readonly facade: WorkflowDetailsFacade
  ) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    this.facade.init();
    return this.facade.workflow$.pipe(
      map(workflow => !!workflow),
      first(isPresent => isPresent),
    );
  }
}
