// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  Output,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import { WorkflowDetailsGraphConnectivity } from '../workflow-details-graph/workflow-details-graph-connectivity.service';
import { Workflow, WorkflowDeploymentStatus, WorkflowModuleLink, WorkflowStatus } from '@ryax/workflow/domain';

@Component({
  selector: 'ryax-workflow-details-graph-edge',
  templateUrl: './workflow-details-graph-edge.component.pug'
})
export class WorkflowDetailsGraphEdgeComponent implements OnChanges, AfterViewInit, OnDestroy {
  @Input() workflow: Workflow;
  @Input() moduleLink: WorkflowModuleLink;
  @Input() activeModuleId: string;
  @Output() delete = new EventEmitter<string>();
  @ViewChild("deleteOverlay", { static: true, read: ElementRef }) deleteOverlay: ElementRef<any>;

  constructor(
    private readonly graphConnectivity: WorkflowDetailsGraphConnectivity
  ) {}

  get selected() {
    return this.activeModuleId === this.moduleLink.inputModuleId ||
      this.activeModuleId === this.moduleLink.outputModuleId;
  }

  get animated() {
    return this.workflow && this.workflow.deploymentStatus === WorkflowDeploymentStatus.DEPLOYED
  }

  get disableDelete() {
    return this.workflow.deploymentStatus === WorkflowDeploymentStatus.DEPLOYING ||
      this.workflow.deploymentStatus === WorkflowDeploymentStatus.DEPLOYED;
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes.moduleLink && changes.moduleLink.currentValue) {
      const { inputModuleId, outputModuleId } = changes.moduleLink.currentValue;
      this.graphConnectivity.setHighlightedEdge(inputModuleId, outputModuleId, this.selected);
    }

    if(changes.activeModuleId) {
      this.graphConnectivity.setHighlightedEdge(this.moduleLink.inputModuleId, this.moduleLink.outputModuleId, this.selected);
    }

    if(changes.workflow) {
      this.graphConnectivity.setAnimatedEdge(this.moduleLink.inputModuleId, this.moduleLink.outputModuleId, this.animated);
      this.refreshEdge()
    }
  }

  refreshEdge() {
    this.graphConnectivity.removeEdge(this.moduleLink.inputModuleId, this.moduleLink.outputModuleId);
    this.graphConnectivity.addEdge(this.moduleLink.inputModuleId, this.moduleLink.outputModuleId, this.deleteOverlay.nativeElement);
  }

  ngAfterViewInit(): void {
    // TODO why do I need to have refreshEdge() to paint the link after the US 583 "update module version in a workflow"
    // this.graphConnectivity.addEdge(this.moduleLink.inputModuleId, this.moduleLink.outputModuleId, this.deleteOverlay.nativeElement);
    this.graphConnectivity.setHighlightedEdge(this.moduleLink.inputModuleId, this.moduleLink.outputModuleId, this.selected);
    this.graphConnectivity.setAnimatedEdge(this.moduleLink.inputModuleId, this.moduleLink.outputModuleId, this.animated);
  }

  ngOnDestroy(): void {
    this.graphConnectivity.removeEdge(this.moduleLink.inputModuleId, this.moduleLink.outputModuleId);
  }

  onDelete() {
    this.delete.emit(this.moduleLink.id);
  }
}
