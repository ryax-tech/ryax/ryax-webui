// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, OnDestroy } from '@angular/core';
import {
  AddWorkflowModuleData,
  AddWorkflowModuleLinkData,
  MoveWorkflowModuleData,
  UpdateWorkflowModuleData,
  UpdateWorkflowModuleInputData,
  UpdateWorkflowModuleOutputData,
  WorkflowDetailsFacade,
  WorkflowDetailsTool,
  WorkflowModuleIOType,
} from '@ryax/workflow/domain';

@Component({
  selector: 'ryax-workflow-details',
  templateUrl: './workflow-details.component.pug'
})
export class WorkflowDetailsComponent implements OnDestroy {
  loading$ = this.facade.loading$;
  exportLoading$ = this.facade.exportLoading$;
  loadingInputs$ = this.facade.loadingInputs$;
  loadingOutputs$ = this.facade.loadingOutputs$;
  loadingWorkflowErrors$ = this.facade.loadingWorkflowErrors$;
  workflow$ = this.facade.workflow$;
  workflowModules$ = this.facade.workflowModules$;
  workflowModuleLinks$ = this.facade.workflowModuleLinks$;
  workflowModulesOutputs$ = this.facade.workflowModulesOutputs$;
  workflowErrors$ = this.facade.workflowErrors$;
  activeTool$ = this.facade.activeTool$;
  activeWorkflowModule$ = this.facade.activeWorkflowModule$;
  activeWorkflowModuleId$ = this.facade.activeWorkflowModuleId$;
  activeWorkflowModuleInputs$ = this.facade.activeWorkflowModuleInputs$;
  activeWorkflowModuleOutputs$ = this.facade.activeWorkflowModuleOutputs$;
  activeWorkflowModuleVersions$ = this.facade.activeWorkflowModuleVersions$;
  deleteModalVisible$ = this.facade.deleteModalVisible$;
  searchWorkflowModuleOutputsIds$ = this.facade.searchWorkflowModuleOutputsIds$;
  exportModalVisible$ = this.facade.exportModalVisible$;

  constructor(
    private readonly facade: WorkflowDetailsFacade
  ) {}

  getWorkflowPortals$(workflowId: string) {
    return this.facade.getWorkflowPortals$(workflowId);
  }

  ngOnDestroy(): void {
    this.facade.exit();
  }

  onRefresh() {
    this.facade.refresh();
  }

  onDeploy() {
    this.facade.deployWorkflow();
  }

  onStop() {
    this.facade.stopWorkflow();
  }

  onCopy() {
    this.facade.copyWorkflow();
  }

  onEdit() {
    this.facade.editWorkflow();
  }

  onDelete() {
    this.facade.openDeleteWorkflowModal();
  }

  onActiveTool(tool: WorkflowDetailsTool) {
    this.facade.activeTool(tool);
  }

  onOpenedToolItem(data: any) {
    this.facade.openedTool(data);
  }

  onSelectNode(moduleId: string) {
    this.facade.selectModule(moduleId);
  }

  onSelectPane() {
    this.facade.unselectModule();
  }

  onAddNode(data: AddWorkflowModuleData) {
    this.facade.addModule(data);
  }

  onMoveNode(data: MoveWorkflowModuleData) {
    this.facade.moveModule(data);
  }

  onDeleteNode(moduleId: string) {
    this.facade.deleteModule(moduleId);
  }

  onOpenWarningsTool() {
    this.facade.openWarningsTool();
  }

  onAddEdge(data: AddWorkflowModuleLinkData) {
    this.facade.addLink(data.inputModuleId, data.outputModuleId);
  }

  onDeleteEdge(moduleLinkId: string) {
    this.facade.deleteLink(moduleLinkId);
  }

  onSaveWorkflowModulePropertiesSetup(data: UpdateWorkflowModuleData) {
    this.facade.updateModulePropertiesSetup(data);
  }

  onSaveWorkflowModuleInputSetup(data: UpdateWorkflowModuleInputData) {
    this.facade.updateModuleInputSetup(data);
  }

  onSearchWorkflowModulesOutputs(withType: WorkflowModuleIOType) {
    this.facade.searchModulesOutputs(withType);
  }

  onAddWorkflowModuleOutput() {
    this.facade.addWorkflowModuleOutput();
  }

  onUpdateWorkflowModuleOutput(data: UpdateWorkflowModuleOutputData) {
    this.facade.updateWorkflowModuleOutput(data);
  }

  onDeleteWorkflowModuleOutput(workflowModuleOutputId: string) {
    this.facade.deleteWorkflowModuleOutput(workflowModuleOutputId);
  }

  showModal() {
    this.facade.openDeleteWorkflowModal();
  }

  validateDeleteWorkflow() {
    this.facade.validateDeleteWorkflow();
  }

  cancelDeleteWorkflow() {
    this.facade.cancelDeleteWorkflow();
  }

  onExport() {
    this.facade.exportWorkflow();
  }

  deleteFile(moduleInputId: string) {
    this.facade.deleteFile(moduleInputId);
  }

  onUpdateModuleVersion(moduleId: string) {
    this.facade.updateModuleVersion(moduleId)
  }
}
