// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Subject } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';
import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { UpdateWorkflowModuleData, WorkflowModule, WorkflowModuleDeploymentStatus } from '@ryax/workflow/domain';

@Component({
  selector: 'ryax-module-properties-setup',
  templateUrl: './module-properties-setup.component.pug'
})
export class ModulePropertiesSetupComponent implements OnInit, OnChanges, OnDestroy {
  form = new FormGroup({
    customName: new FormControl(null),
    //scheduledOn: new FormControl(null),
    //scheduledForEach: new FormControl(null)
  }, { updateOn: 'blur' });
  destroy$ = new Subject<void>();

  @Input() workflowModule: WorkflowModule;
  @Output() save = new EventEmitter<UpdateWorkflowModuleData>();

  get disableForm() {
    return this.workflowModule.deploymentStatus === WorkflowModuleDeploymentStatus.NONE
  }

  private canDisableForm() {
    if(this.disableForm){
      this.form.enable({ emitEvent: false });
    } else {
      this.form.disable({ emitEvent: false });
    }
  }

  ngOnInit(): void {
    this.form.valueChanges.pipe(
      filter(() => this.form.valid),
      takeUntil(this.destroy$)
    ).subscribe(value => {
      const { customName } = value;
      this.save.emit({ customName });
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes.workflowModule && changes.workflowModule.currentValue) {
      this.canDisableForm();
      const { customName } = changes.workflowModule.currentValue;
      this.form.setValue({ customName }, { emitEvent: false });
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
