// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges } from '@angular/core';
import {
  WorkflowModule,
  WorkflowModuleDeploymentStatus,
  WorkflowModuleStatus, WorkflowModuleVersion,
} from '@ryax/workflow/domain';
import { FormControl, FormGroup } from '@angular/forms';
import { filter, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'ryax-module-informations-setup',
  templateUrl: './module-informations-setup.component.pug',
  styleUrls: ['./module-informations-setup.component.scss']
})
export class ModuleInformationsSetupComponent  implements OnInit, OnChanges, OnDestroy {
  form = new FormGroup({
    newVersionModuleId: new FormControl(null),
  }, { updateOn: 'change' });
  destroy$ = new Subject<void>();

  @Input() workflowModule: WorkflowModule;
  @Input() versions: WorkflowModuleVersion[];
  @Output() save = new EventEmitter<string>();

  get disableForm() {
    return this.workflowModule.deploymentStatus === WorkflowModuleDeploymentStatus.DEPLOYING ||
      this.workflowModule.deploymentStatus === WorkflowModuleDeploymentStatus.DEPLOYED
  }

  private canDisableForm() {
    if(this.disableForm){
      this.form.disable({ emitEvent: false });
    } else {
      this.form.enable({ emitEvent: false });
    }
  }

  ngOnInit(): void {
    this.form.valueChanges.pipe(
      filter(() => this.form.valid),
      takeUntil(this.destroy$),
    ).subscribe((formData) => {
      const { newVersionModuleId } = formData;
      this.save.emit(newVersionModuleId);
    });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes.workflowModule && changes.workflowModule.currentValue) {
      this.canDisableForm();
    }
  }

  get isModule() {
    return this.workflowModule
  }

  get hasCategories() {
    return this.workflowModule.categories.length
  }

  get isValid() {
    if(this.isModule)
      return this.workflowModule.status === WorkflowModuleStatus.VALID &&
        this.workflowModule.deploymentStatus === WorkflowModuleDeploymentStatus.NONE;
  }

  get isInvalid() {
    if(this.isModule)
      return this.workflowModule.status === WorkflowModuleStatus.INVALID &&
      this.workflowModule.deploymentStatus === WorkflowModuleDeploymentStatus.NONE;
  }

  get isDeploying() {
    if(this.isModule)
      return this.workflowModule.deploymentStatus === WorkflowModuleDeploymentStatus.DEPLOYING;
  }

  get isDeployed() {
    if(this.isModule)
      return this.workflowModule.deploymentStatus === WorkflowModuleDeploymentStatus.DEPLOYED;
  }
}
