// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { WorkflowError, WorkflowModule } from '@ryax/workflow/domain';

export interface WorkflowErrorGroup {
  title: string;
  items: WorkflowError[]
}

@Component({
  selector: 'ryax-workflow-warnings',
  templateUrl: './workflow-warnings.component.pug',
  styleUrls: ['./workflow-warnings.component.scss']
})
export class WorkflowWarningsComponent implements OnChanges {
  errorGroups: WorkflowErrorGroup[];

  @Input() loading: boolean;
  @Input() workflowModules: WorkflowModule[];
  @Input() workflowErrors: WorkflowError[];

  private setErrorGroups() {
    const workflowGroup: WorkflowErrorGroup = {
      title: "Workflow",
      items: this.workflowErrors.filter(item => !item.workflowModuleId)
    };
    const moduleGroups: WorkflowErrorGroup[] = this.workflowModules
      .map(workflowModuleItem => ({
        title: workflowModuleItem.customName ? workflowModuleItem.customName : workflowModuleItem.name,
        items: this.workflowErrors.filter(workflowErrorItem => workflowErrorItem.workflowModuleId === workflowModuleItem.id)
      }));
    this.errorGroups = [workflowGroup, ...moduleGroups]
      .filter(moduleGroupItem => moduleGroupItem.items.length > 0);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes.workflowModules && changes.workflowModules.currentValue) {
      this.setErrorGroups();
    }
    if(changes.workflowErrors && changes.workflowErrors.currentValue) {
      this.setErrorGroups();
    }
  }
}
