// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, Input, TemplateRef } from '@angular/core';
import { WorkflowModuleInput } from '@ryax/workflow/domain';

@Component({
  selector: 'ryax-module-input-setup-list',
  templateUrl: './module-input-setup-list.component.pug',
})
export class ModuleInputSetupListComponent {
  @Input() loading: boolean;
  @Input() workflowModuleInputs: WorkflowModuleInput[];
  @Input() itemTemplate: TemplateRef<void>;

  trackItemById(index: number, item: WorkflowModuleInput) {
    return item.id;
  }
}
