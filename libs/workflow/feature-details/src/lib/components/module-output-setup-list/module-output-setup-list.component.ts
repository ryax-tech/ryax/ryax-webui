// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, EventEmitter, Input, Output, TemplateRef } from '@angular/core';
import { WorkflowModule, WorkflowModuleDeploymentStatus, WorkflowModuleOutput } from '@ryax/workflow/domain';

@Component({
  selector: 'ryax-module-output-setup-list',
  templateUrl: './module-output-setup-list.component.pug'
})
export class ModuleOutputSetupListComponent {
  @Input() loading: boolean;
  @Input() workflowModule: WorkflowModule;
  @Input() workflowModuleOutputs: WorkflowModuleOutput[];
  @Input() itemTemplate: TemplateRef<void>;
  @Output() addOutput = new EventEmitter<void>();

  trackItemById(index: number, item: WorkflowModuleOutput) {
    return item.id;
  }

  onAddOutput() {
    this.addOutput.emit();
  }

  get isButtonDisabled() {
    return this.workflowModule.deploymentStatus !== WorkflowModuleDeploymentStatus.NONE;
  }
}
