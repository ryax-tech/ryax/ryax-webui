// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, EventEmitter, Input, Output, TemplateRef } from '@angular/core';
import { animate, style, transition, trigger } from '@angular/animations';
import {
  Workflow,
  WorkflowDeploymentStatus,
  WorkflowDetailsTool,
  WorkflowError,
} from '@ryax/workflow/domain';

@Component({
  selector: 'ryax-editor-tools',
  templateUrl: './editor-tools.component.pug',
  styleUrls: ['./editor-tools.component.scss'],
  animations: [
    trigger('open', [
      transition(':enter', [
        style({ 'z-index': 1, width: 0, opacity: 0 }),
        animate(300, style ({ width: '*', opacity: 1 }))
      ]),
    ]),
    trigger('close', [
      transition(':leave', [
        style({ 'z-index': 0, width: '*', opacity: 0.5 }),
        animate(150, style ({ width: 0, opacity: 0 }))
      ]),
    ]),
  ],
})
export class EditorToolsComponent {
  @Input() workflow: Workflow;
  @Input() activeTool: WorkflowDetailsTool;
  @Input() activeWorkflowModuleId: string;
  @Input() moduleStoreToolContent: TemplateRef<void>;
  @Input() moduleSetupToolContent: TemplateRef<void>;
  @Input() warningsToolContent: TemplateRef<void>;
  @Input() workflowErrors: WorkflowError[];
  @Output() selectTool = new EventEmitter<WorkflowDetailsTool>();
  @Output() contentOpened = new EventEmitter<any>();

  get offset() {
    return [-30, 30];
  }

  trackToolById(index: number, item: any) {
    return item.value;
  }

  onClickToolItem(tool: WorkflowDetailsTool) {
    this.selectTool.emit(tool);
  }

  isModuleStoreToolActive() {
    return this.activeTool === WorkflowDetailsTool.Store;
  }

  isModuleStoreToolDisabled() {
    return this.workflow.deploymentStatus !== WorkflowDeploymentStatus.NONE;
  }

  onClickModuleStoreTool() {
    this.selectTool.emit(WorkflowDetailsTool.Store);
  }

  isModuleSetupToolActive() {
    return this.activeTool === WorkflowDetailsTool.Setup;
  }

  isModuleSetupToolDisabled() {
    return !this.activeWorkflowModuleId;
  }

  onClickModuleSetupTool() {
    this.selectTool.emit(WorkflowDetailsTool.Setup);
  }

  isWarningsToolActive() {
    return this.activeTool === WorkflowDetailsTool.Warnings;
  }

  isWarningsToolDisabled() {
    return false;
    //return this.workflow.status === WorkflowStatus.DEPLOYED;
  }

  onClickWarningsTool() {
    this.selectTool.emit(WorkflowDetailsTool.Warnings);
  }

  onToolsContentOpened() {
    this.contentOpened.emit(this.activeTool);
  }
}
