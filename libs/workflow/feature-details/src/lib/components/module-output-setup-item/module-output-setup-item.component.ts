// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges } from '@angular/core';
import {
  UpdateWorkflowModuleOutputData,
  WorkflowModule,
  WorkflowModuleDeploymentStatus,
  WorkflowModuleIOType,
  WorkflowModuleOutput,
} from '@ryax/workflow/domain';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { debounceTime, filter, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'ryax-module-output-setup-item',
  templateUrl: './module-output-setup-item.component.pug'
})
export class ModuleOutputSetupItemComponent implements OnInit, OnChanges, OnDestroy {
  form = new FormGroup({
    displayName: new FormControl(null, Validators.required),
    type: new FormControl(null, Validators.required),
    help: new FormControl(null),
  }, { updateOn: 'change' });
  types = [
    WorkflowModuleIOType.STRING,
    WorkflowModuleIOType.PASSWORD,
    WorkflowModuleIOType.LONGSTRING,
    WorkflowModuleIOType.INTEGER,
    WorkflowModuleIOType.FLOAT,
    WorkflowModuleIOType.ENUM,
    WorkflowModuleIOType.FILE,
    WorkflowModuleIOType.DIRECTORY,
    WorkflowModuleIOType.TABLE,
  ];
  destroy$ = new Subject<void>();

  @Input() workflowModule: WorkflowModule;
  @Input() workflowModuleOutput: WorkflowModuleOutput;
  @Output() save = new EventEmitter<UpdateWorkflowModuleOutputData>();
  @Output() delete = new EventEmitter<string>();

  private updateFormState() {
    if(this.workflowModule.hasDynamicOutputs) {
      if(this.workflowModule.deploymentStatus === WorkflowModuleDeploymentStatus.NONE) {
        this.form.enable({ emitEvent: false });
      } else {
        this.form.disable({ emitEvent: false });
      }
    } else {
      this.form.disable({ emitEvent: false });
    }
  }

  ngOnInit(): void {
    this.form.valueChanges.pipe(
      filter(() => this.form.valid),
      debounceTime(350),
      takeUntil(this.destroy$),
    ).subscribe((formData) => {
      const { displayName, type, help } = formData;
      this.save.emit({
        workflowModuleOutputId: this.workflowModuleOutput.id,
        displayName, type, help
      });
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes.workflowModule && changes.workflowModule.currentValue) {
      this.updateFormState()
    }

    if(changes.workflowModuleOutput && changes.workflowModuleOutput.currentValue) {
      const { displayName, type, help } = changes.workflowModuleOutput.currentValue;
      this.form.setValue({ displayName, type, help }, { emitEvent: false });
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  onDeleteItem() {
    this.delete.emit(this.workflowModuleOutput.id);
  }
}
