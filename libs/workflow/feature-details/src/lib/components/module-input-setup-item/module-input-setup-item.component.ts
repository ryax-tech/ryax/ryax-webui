// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges } from '@angular/core';
import { Subject } from 'rxjs';
import { FormControl, FormGroup } from '@angular/forms';
import { debounceTime, filter, takeUntil, tap } from 'rxjs/operators';
import {
  UpdateWorkflowModuleInputData,
  WorkflowModule,
  WorkflowModuleDeploymentStatus,
  WorkflowModuleInput,
  WorkflowModuleIOType,
  WorkflowModuleOutput,
} from '@ryax/workflow/domain';

export interface ReferenceGroupOption {
  label: string;
  items: ReferenceItemOption[];
}

export interface ReferenceItemOption {
  label: string;
  value: string;
}

@Component({
  selector: 'ryax-module-input-setup-item',
  templateUrl: './module-input-setup-item.component.pug'
})
export class ModuleInputSetupItemComponent implements OnInit, OnChanges, OnDestroy {
  referenceOptions: ReferenceGroupOption[];

  form = new FormGroup({
    setupMode: new FormControl(null),
    setupValue: new FormControl(null, { updateOn: 'change' })
  });
  destroy$ = new Subject<void>();

  @Input() workflowModules: WorkflowModule[];
  @Input() workflowModule: WorkflowModule;
  @Input() workflowModuleInput: WorkflowModuleInput;
  @Input() workflowModuleOutputs: WorkflowModuleOutput[];
  @Input() searchWorkflowModuleOutputsIds: string[];
  @Output() save = new EventEmitter<UpdateWorkflowModuleInputData>();
  @Output() search = new EventEmitter<WorkflowModuleIOType>();
  @Output() delete = new EventEmitter<string>();

  private setReferenceOptions() {
    this.referenceOptions = this.workflowModules
      .map(workflowModuleItem => ({
        label: workflowModuleItem.customName ? workflowModuleItem.customName : workflowModuleItem.name,
        items: this.workflowModuleOutputs
          .filter(workflowModuleOutputItem => workflowModuleOutputItem.workflowModuleId === workflowModuleItem.id)
          .filter(workflowModuleOutputItem => {
            return workflowModuleOutputItem.id === (this.workflowModuleInput && this.workflowModuleInput.referenceValue) ||
              this.searchWorkflowModuleOutputsIds.includes(workflowModuleOutputItem.id)
          })
          .map(workflowModuleOutputItem => ({
            label: workflowModuleOutputItem.displayName,
            value: workflowModuleOutputItem.id
          }))
      }))
      .filter(referenceGroupOptionItem => referenceGroupOptionItem.items.length > 0)
  }

  get disableForm() {
    return this.workflowModule.deploymentStatus === WorkflowModuleDeploymentStatus.DEPLOYING ||
      this.workflowModule.deploymentStatus === WorkflowModuleDeploymentStatus.DEPLOYED
  }

  private canDisableForm() {
    if(this.disableForm){
      this.form.disable({ emitEvent: false });
    } else {
      this.form.enable({ emitEvent: false });
    }
  }

  get hasTextFieldType() {
    return this.workflowModuleInput &&
      [WorkflowModuleIOType.STRING].includes(this.workflowModuleInput.type)
  }

  get hasPasswordFieldType() {
    return this.workflowModuleInput &&
      [WorkflowModuleIOType.PASSWORD].includes(this.workflowModuleInput.type)
  }

  get hasNumberFieldType() {
    return this.workflowModuleInput &&
      [WorkflowModuleIOType.FLOAT, WorkflowModuleIOType.INTEGER].includes(this.workflowModuleInput.type)
  }

  get hasSelectFieldType() {
    return this.workflowModuleInput &&
      [WorkflowModuleIOType.ENUM].includes(this.workflowModuleInput.type)
  }

  get hasTextAreaFieldType() {
    return this.workflowModuleInput &&
      [WorkflowModuleIOType.LONGSTRING].includes(this.workflowModuleInput.type)
  }

  get hasFileFieldType() {
    return this.workflowModuleInput &&
      [WorkflowModuleIOType.FILE].includes(this.workflowModuleInput.type)
  }

  get hasNoFieldType() {
    return this.workflowModuleInput &&
      [WorkflowModuleIOType.DIRECTORY].includes(this.workflowModuleInput.type)
  }

  ngOnInit(): void {
    // When user go on the other mode: Set value to null
    this.form.controls.setupMode.valueChanges.pipe(
      debounceTime(350),
      takeUntil(this.destroy$)
    ).subscribe(() => {
      this.form.controls.setupValue.setValue(null, { emitEvent: false });
    });

    this.form.controls.setupMode.valueChanges.pipe(
      debounceTime(350),
      filter((value) => value === "reference"),
      takeUntil(this.destroy$)
    ).subscribe(() => this.search.emit(this.workflowModuleInput.type));

    this.form.controls.setupValue.valueChanges.pipe(
      debounceTime(350),
      filter(() => this.form.value.setupMode === "static"),
      takeUntil(this.destroy$),
      tap(staticValue => this.save.emit({ workflowModuleInputId: this.workflowModuleInput.id, staticValue}))
    ).subscribe();

    this.form.controls.setupValue.valueChanges.pipe(
      debounceTime(350),
      filter(() => this.form.value.setupMode === "reference"),
      takeUntil(this.destroy$),
      tap(referenceValue => this.save.emit({ workflowModuleInputId: this.workflowModuleInput.id, referenceValue}))
    ).subscribe();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes.workflowModuleInput && changes.workflowModuleInput.currentValue) {
      const newWorkflowModuleInput: WorkflowModuleInput = changes.workflowModuleInput.currentValue;
      let setupValue = null;
      const setupMode = !!newWorkflowModuleInput.referenceValue ? "reference" : "static";
      if(setupMode === "static") {
        if(newWorkflowModuleInput.type === WorkflowModuleIOType.FILE) {
          if(!!newWorkflowModuleInput.workflowFile) {
            setupValue = `${newWorkflowModuleInput.workflowFile.name}.${newWorkflowModuleInput.workflowFile.extension}`;
          }
        } else {
          setupValue = newWorkflowModuleInput.staticValue;
        }
        this.form.setValue({ setupValue, setupMode }, { emitEvent: false });
      } else {
        setupValue = newWorkflowModuleInput.referenceValue;
        this.form.setValue({ setupValue, setupMode }, { emitEvent: false });
        this.setReferenceOptions();
      }
    }

    if(changes.workflowModules && changes.workflowModules.currentValue) {
      this.setReferenceOptions();
    }

    if(changes.workflowModuleOutputs && changes.workflowModuleOutputs.currentValue) {
      this.setReferenceOptions();
    }

    if(changes.searchWorkflowModuleOutputsIds && changes.searchWorkflowModuleOutputsIds.currentValue) {
      this.setReferenceOptions();
    }

    if(changes.workflowModule && changes.workflowModule.currentValue) {
      this.canDisableForm();
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  onOpenDropdown(isOpen: boolean) {
    if(isOpen) this.search.emit(this.workflowModuleInput.type)
  }

  deleteFile() {
    this.delete.emit(this.workflowModuleInput.id)
  }
}
