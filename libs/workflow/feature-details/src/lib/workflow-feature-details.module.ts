// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GridsterModule } from 'angular-gridster2';
import { StoreApiModule } from '@ryax/store/api';
import { WorkflowUiModule } from '@ryax/workflow/ui';
import { WorkflowDomainModule } from '@ryax/workflow/domain';
import { CanAccessGuard } from './guards/can-access.guard';
import { WorkflowDetailsComponent } from './workflow-details/workflow-details.component';
import { WorkflowDetailsHeaderComponent } from './workflow-details-header/workflow-details-header.component';
import { WorkflowDetailsLayoutComponent } from './workflow-details-layout/workflow-details-layout.component';
import { WorkflowDetailsGraphComponent } from './workflow-details-graph/workflow-details-graph.component';
import { WorkflowDetailsGraphNodeComponent } from './workflow-details-graph-node/workflow-details-graph-node.component';
import { WorkflowDetailsGraphEdgeComponent } from './workflow-details-graph-edge/workflow-details-graph-edge.component';
import { WorkflowDetailsSetupComponent } from './workflow-details-setup/workflow-details-setup.component';
import { ModuleInformationsSetupComponent } from './components/module-informations-setup/module-informations-setup.component';
import { ModuleInputSetupListComponent } from './components/module-input-setup-list/module-input-setup-list.component';
import { ModuleInputSetupItemComponent } from './components/module-input-setup-item/module-input-setup-item.component';
import { ModuleOutputSetupListComponent } from './components/module-output-setup-list/module-output-setup-list.component';
import { ModuleOutputSetupItemComponent } from './components/module-output-setup-item/module-output-setup-item.component';
import { ModulePropertiesSetupComponent } from './components/module-properties-setup/module-properties-setup.component';
import { WorkflowWarningsComponent } from './components/workflow-warnings/workflow-warnings.component';
import { EditorToolsComponent } from './components/editor-tools/editor-tools.component';


@NgModule({
  imports: [
    CommonModule,
    GridsterModule,
    WorkflowUiModule,
    WorkflowDomainModule,
    StoreApiModule
  ],
  providers: [
    CanAccessGuard
  ],
  declarations: [
    WorkflowDetailsComponent,
    WorkflowDetailsHeaderComponent,
    WorkflowDetailsLayoutComponent,
    WorkflowDetailsGraphComponent,
    WorkflowDetailsGraphNodeComponent,
    WorkflowDetailsGraphEdgeComponent,
    WorkflowDetailsSetupComponent,
    ModuleInformationsSetupComponent,
    ModuleInputSetupListComponent,
    ModuleInputSetupItemComponent,
    ModuleInputSetupItemComponent,
    ModuleOutputSetupListComponent,
    ModuleOutputSetupItemComponent,
    ModulePropertiesSetupComponent,
    WorkflowWarningsComponent,
    EditorToolsComponent
  ],
  exports: [
    WorkflowDetailsComponent
  ],
})
export class WorkflowFeatureDetailsModule {}
