// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'ryax-workflow-details-warnings',
  templateUrl: './workflow-details-warnings.component.pug',
  styleUrls: ['./workflow-details-warnings.component.scss']
})
export class WorkflowDetailsWarningsComponent {
  @Input() data: any;
  @Output() deploy = new EventEmitter<void>();

  onClickDeploy() {
    this.deploy.emit();
  }
}
