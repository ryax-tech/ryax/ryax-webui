// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, Input, TemplateRef } from '@angular/core';
import { animate, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'ryax-workflow-details-layout',
  templateUrl: './workflow-details-layout.component.pug',
  styleUrls: ['./workflow-details-layout.component.scss'],
  animations: [
    trigger('detailsContainer', [
      transition(':enter', [
        style({ height: 0 }),
        animate(250, style ({ height: '*' }))
      ]),
      transition(':leave', [
        animate(250, style ({ height: 0 }))
      ]),
    ]),
    trigger('toolsContentContainer', [
      transition(':enter', [
        style({ width: 0 }),
        animate(300, style ({ width: '*' }))
      ]),
      transition(':leave', [
        animate(300, style ({ width: 0 }))
      ]),
    ])
  ]
})
export class WorkflowDetailsLayoutComponent {
  @Input() header: TemplateRef<void>;
  @Input() graph: TemplateRef<void>;
  @Input() tools: TemplateRef<void>;
}
