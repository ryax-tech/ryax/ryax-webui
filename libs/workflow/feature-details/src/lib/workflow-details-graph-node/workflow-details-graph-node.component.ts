// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import { GridsterItem, GridsterItemComponent } from 'angular-gridster2';
import { WorkflowDetailsGraphConnectivity } from '../workflow-details-graph/workflow-details-graph-connectivity.service';
import {
  WorkflowModule,
  WorkflowModuleDeploymentStatus,
  WorkflowModuleKind,
  WorkflowModuleStatus,
} from '@ryax/workflow/domain';

@Component({
  selector: 'ryax-workflow-details-graph-node',
  templateUrl: './workflow-details-graph-node.component.pug',
  styleUrls: ['./workflow-details-graph-node.component.scss']
})
export class WorkflowDetailsGraphNodeComponent implements OnInit, OnChanges, AfterViewInit, OnDestroy {
  gridItem: GridsterItem = null;

  @Input() workflowModule: WorkflowModule;
  @Input() activeModuleId: string;
  @Output() select = new EventEmitter<string>();
  @Output() delete = new EventEmitter<string>();
  @Output() openWarnings = new EventEmitter<void>();
  @ViewChild(GridsterItemComponent, { static: true }) gridItemComponent: GridsterItemComponent;

  constructor(
    private readonly graphConnectivity: WorkflowDetailsGraphConnectivity
  ) {}

  get isSelected() {
    return this.workflowModule && this.workflowModule.id === this.activeModuleId
  }

  get isValid() {
    return this.workflowModule.status === WorkflowModuleStatus.VALID
      && this.workflowModule.deploymentStatus === WorkflowModuleDeploymentStatus.NONE;
  }

  get isInvalid() {
    return this.workflowModule.status === WorkflowModuleStatus.INVALID
      && this.workflowModule.deploymentStatus === WorkflowModuleDeploymentStatus.NONE;
  }

  get isDeploying() {
    return this.workflowModule.deploymentStatus === WorkflowModuleDeploymentStatus.DEPLOYING;
  }

  get isDeployed() {
    return this.workflowModule.deploymentStatus === WorkflowModuleDeploymentStatus.DEPLOYED;
  }

  get isUndeployed() {
    return this.workflowModule.deploymentStatus === WorkflowModuleDeploymentStatus.NONE;
  }

  get isUndeploying() {
    return this.workflowModule.deploymentStatus === WorkflowModuleDeploymentStatus.UNDEPLOYING;
  }

  get displayName() {
    return this.workflowModule.customName ? this.workflowModule.customName : this.workflowModule.name;
  }

  get logoUrl() {
    return `/api/studio/modules/${this.workflowModule.moduleId}/logo`;
  }

  get disableDelete() {
    return this.workflowModule.deploymentStatus === WorkflowModuleDeploymentStatus.DEPLOYING ||
      this.workflowModule.deploymentStatus === WorkflowModuleDeploymentStatus.DEPLOYED;
  }

  // Need this to make link update working while dragging node
  private overrideDragStart() {
    const oldDragStart = this.gridItemComponent.drag.dragStart.bind(this.gridItemComponent.drag);
    this.gridItemComponent.drag.dragStart = (e: MouseEvent) => {
      oldDragStart(e);
      this.graphConnectivity.refreshNode(this.gridItemComponent.el);
    };
  }

  // Need this to make link update working while dragging node
  private overrideDrag() {
    const oldDrag = this.gridItemComponent.drag.dragMove.bind(this.gridItemComponent.drag);
    this.gridItemComponent.drag.dragMove = (e: MouseEvent) => {
      oldDrag(e);
      this.graphConnectivity.refreshNode(this.gridItemComponent.el);
    };
  }

  // Need this to make link update working while dragging node
  private overrideDragStop() {
    const oldDragStop = this.gridItemComponent.drag.dragStop.bind(this.gridItemComponent.drag);
    this.gridItemComponent.drag.dragStop = (e: MouseEvent) => {
      oldDragStop(e);
      this.graphConnectivity.refreshNode(this.gridItemComponent.el);
    };
  }


  ngOnInit(): void {
    this.overrideDragStart();
    this.overrideDrag();
    this.overrideDragStop();
  }

  ngAfterViewInit(): void {
    this.graphConnectivity.addNode(this.workflowModule.id, this.gridItemComponent.el);
    this.graphConnectivity.setHighlightedNode(this.gridItemComponent.el, this.isSelected);
    this.graphConnectivity.setSourceNode(this.gridItemComponent.el, this.isUndeployed);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.workflowModule) {
      this.graphConnectivity.setHighlightedNode(this.gridItemComponent.el, this.isSelected);
      this.graphConnectivity.setSourceNode(this.gridItemComponent.el, this.isUndeployed);
      this.gridItem = {
        id: changes.workflowModule.currentValue.id,
        x: changes.workflowModule.currentValue.positionX,
        y: changes.workflowModule.currentValue.positionY,
        rows: 1,
        cols: 1,
      };
    }

    if (changes.activeModuleId) {
      this.graphConnectivity.setHighlightedNode(this.gridItemComponent.el, this.isSelected);
    }
  }

  ngOnDestroy(): void {
    this.graphConnectivity.removeNode(this.workflowModule.id, this.gridItemComponent.el);
  }

  onSelectNode() {
    if(this.gridItemComponent.gridster.dragInProgress) return;
    if(this.graphConnectivity.dragInProgress) return;
    this.select.emit(this.workflowModule.id);
  }

  onDelete() {
    this.delete.emit(this.workflowModule.id);
  }

  onClickOpenWarnings() {
    this.openWarnings.emit();
  }
}
