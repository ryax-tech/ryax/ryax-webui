// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, Input, TemplateRef } from '@angular/core';

@Component({
  selector: 'ryax-workflow-details-setup',
  templateUrl: './workflow-details-setup.component.pug',
  styleUrls: ['./workflow-details-setup.component.scss']
})
export class WorkflowDetailsSetupComponent {
  @Input() informations: TemplateRef<void>;
  @Input() inputs: TemplateRef<void>;
  @Input() outputs: TemplateRef<void>;
  @Input() properties: TemplateRef<void>;
}
