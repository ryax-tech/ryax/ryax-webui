// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable, Renderer2 } from '@angular/core';
import { jsPlumb, jsPlumbInstance, OnConnectionBindInfo } from 'jsplumb';
import { Subject } from 'rxjs';

@Injectable()
export class WorkflowDetailsGraphConnectivity {
  private instance: jsPlumbInstance;
  private nodes = new Map<string, Element>();
  private draggingEdge = false;
  private dragEdgeStart = new Subject<OnConnectionBindInfo>();
  private dragEdgeEnd = new Subject<OnConnectionBindInfo>();
  private dragEdgeAbort = new Subject<OnConnectionBindInfo>();

  constructor(
    private readonly renderer: Renderer2
  ) {}

  get dragInProgress() {
    return this.draggingEdge;
  }

  get dragEdgeStart$() {
    return this.dragEdgeStart.asObservable();
  }

  get dragEdgeEnd$() {
    return this.dragEdgeEnd.asObservable();
  }

  get dragEdgeAbort$() {
    return this.dragEdgeAbort.asObservable();
  }

  init(element: Element) {
    this.instance = jsPlumb.getInstance({
      Container: element
    });
    this.nodes.clear();

    /** Source endpoint types **/
    this.instance.registerEndpointType("basic", {
      connectionType: "basic"
    });
    this.instance.registerEndpointType("highlighted", {
      cssClass: "highlighted",
    });

    /** Connection types **/
    this.instance.registerConnectionType('basic', {
      anchors: ["Right", [ "Continuous", { faces:[ "left" ] } ]],
      endpoint: "Blank",
      overlays: [
        ['Arrow', { width: 10, length: 10, location: 1 }],
      ],
      connector: ['Flowchart', { stub: 15, cornerRadius: 5, alwaysRespectStubs: true }],
    });
    this.instance.registerConnectionType('highlighted', {
      cssClass: "highlighted"
    });
    this.instance.registerConnectionType('animated', {
      cssClass: "animated"
    });

    /** Instance event bindings **/
    this.instance.bind('beforeDrag', (event) => {
      this.draggingEdge = true;
      this.dragEdgeStart.next(event);
    });
    this.instance.bind('beforeDrop', (event) => {
      this.draggingEdge = false;
      this.dragEdgeEnd.next(event);
    });
    this.instance.bind('connectionAborted', (event) => {
      this.draggingEdge = false;
      this.dragEdgeAbort.next(event);
    });
  };

  addNode(nodeId: string, element: Element) {
    this.instance.addEndpoint(element, {
      endpoint: ["Dot", { radius: 8 }],
      anchor: "Right",
      isSource: true,
      isTarget: false,
      maxConnections: -1,
      connectionsDetachable: false,
      type: "basic"
    });
    this.instance.makeTarget(element, {
      allowLoopback:false,
      uniqueEndpoint: false,
      // Need to customize
      maxConnections: -1,
      connectionsDetachable:false
    });
    this.nodes.set(nodeId, element);
  }

  refreshNode(element: Element) {
    this.instance.revalidate(element);
  }

  removeNode(nodeId: string, element: Element) {
    this.instance.remove(element);
    this.nodes.delete(nodeId);
  }

  setHighlightedNode(element: Element, highlighted: boolean) {
    if(highlighted) {
      this.renderer.addClass(element, 'highlighted');
    } else {
      this.renderer.removeClass(element, 'highlighted');
    }
    this.instance.getEndpoints(element)
      .forEach((endpoint: any) => {
        if(highlighted) {
          endpoint.addType("highlighted")
        } else {
          endpoint.removeType("highlighted");
        }
    });
  }

  setSourceNode(element: Element, isSource: boolean) {
    this.instance.getEndpoints(element)
      .forEach(item => item.setEnabled(isSource))
  }

  addEdge(sourceNodeId: string, targetNodeId: string, deleteOverlay: Element) {
    const sourceElement = this.nodes.get(sourceNodeId);
    const targetElement = this.nodes.get(targetNodeId);
    this.instance.connect({
      source: sourceElement, // it is the id of source div
      target: targetElement, // it is the id of target div
      type: "basic",
      overlays: [
        ["Custom", { id: "deleteOverlay", location: 0.5, create: () => deleteOverlay }]
      ]
    } as any);
  }

  removeEdge(sourceNodeId: string, targetNodeId: string) {
    this.instance.getAllConnections()
      .filter(connection => connection.sourceId === sourceNodeId && connection.targetId === targetNodeId && !!connection.source && !!connection.target)
      .forEach((connection) => this.instance.deleteConnection(connection));
  }

  setHighlightedEdge(sourceId: string, targetId: string, highlighted: boolean) {
    this.instance.getAllConnections()
      .filter(connection => connection.sourceId === sourceId && connection.targetId === targetId)
      .forEach((connection: any) => {
        if(highlighted) {
          connection.addType("highlighted")
        } else {
          connection.removeType("highlighted");
        }
      });
  }

  setAnimatedEdge(sourceId: string, targetId: string, animated: boolean) {
    this.instance.getAllConnections()
      .filter(connection => connection.sourceId === sourceId && connection.targetId === targetId)
      .forEach((connection: any) => {
        if(animated) {
          connection.addType("animated")
        } else {
          connection.removeType("animated");
        }
      });
  }

  /** Handle connectivity zoom **/
  setScale(scale: number) {
    if(this.instance) {
      (this.instance as any).setZoom(scale);
    }
  }

}
