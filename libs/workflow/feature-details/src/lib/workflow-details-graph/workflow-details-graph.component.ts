// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, Renderer2, ViewChild } from '@angular/core';
import {
  CompactType,
  DisplayGrid,
  GridsterComponent,
  GridsterComponentInterface,
  GridsterConfig,
  GridsterItem, GridsterItemComponentInterface,
  GridType,
} from 'angular-gridster2';
import { WorkflowDetailsGraphConnectivity } from './workflow-details-graph-connectivity.service';
import {
  AddWorkflowModuleData,
  AddWorkflowModuleLinkData,
  MoveWorkflowModuleData,
  Workflow,
  WorkflowModule,
  WorkflowModuleLink,
} from '@ryax/workflow/domain';
import { WorkflowDetailsGraphPan } from './workflow-details-graph-pan.service';
import { EmptyCellWrapper } from '../utils/empty-cell-wrapper.util';
import { DraggableWrapper } from '../utils/drag-wrapper.util';

@Component({
  selector: 'ryax-workflow-details-graph',
  templateUrl: './workflow-details-graph.component.pug',
  styleUrls: ['./workflow-details-graph.component.scss'],
  providers: [
    WorkflowDetailsGraphConnectivity,
    WorkflowDetailsGraphPan,
  ]
})
export class WorkflowDetailsGraphComponent implements OnInit, OnDestroy {
  destroySubject = new Subject<void>();
  paneOptions: GridsterConfig = {
    gridType: GridType.Fixed,
    compactType: CompactType.None,
    margin: 50,
    outerMargin: true, // true
    outerMarginTop: 30,
    outerMarginBottom: 30,
    outerMarginLeft: 30,
    outerMarginRight: 30,
    useTransformPositioning: false,
    mobileBreakpoint: 640,
    minCols: 32,
    minRows: 32,
    // see workflows business rules "the field has a size of 32x32"
    maxCols: 32,
    maxRows: 32,
    maxItemCols: 100,
    minItemCols: 1,
    maxItemRows: 100,
    minItemRows: 1,
    maxItemArea: 2500,
    minItemArea: 1,
    defaultItemCols: 1,
    defaultItemRows: 1,
    fixedColWidth: 200, // 105
    fixedRowHeight: 85, // 105
    keepFixedHeightInMobile: true,
    keepFixedWidthInMobile: true,
    scrollSensitivity: 10,
    scrollSpeed: 20,
    emptyCellDragMaxCols: 50,
    emptyCellDragMaxRows: 50,
    ignoreMarginInRow: false,
    scale: 1,
    draggable: {
      enabled: true,
      delayStart: 350,
      ignoreContent: false,
    },
    resizable: {
      enabled: false,
    },
    swap: false,
    pushItems: true,
    disablePushOnDrag: false,
    pushDirections: {
      north: true,
      east: true,
      south: true,
      west: true
    },
    pushResizeItems: false,
    setGridSize: true,
    displayGrid: DisplayGrid.None,
    disableWindowResize: false,
    disableWarnings: false,
    disableScrollHorizontal: true,
    disableScrollVertical: true,
    scrollToNewItems: false,
    enableEmptyCellDrop: true,
    // callback to call after grid has initialized. Arguments: gridsterComponent
    initCallback: (gridster: GridsterComponentInterface) => {
      gridster.emptyCell = EmptyCellWrapper(gridster.emptyCell, this.graphPan);
    },
    // callback to call for each item when is initialized. Arguments: gridsterItem, gridsterItemComponent
    itemInitCallback: (item: GridsterItem, itemComponent: GridsterItemComponentInterface) => {
      itemComponent.drag = DraggableWrapper(itemComponent.drag, this.graphConnectivity, this.graphPan);
    },
    emptyCellDropCallback: (event: DragEvent, item: GridsterItem) => {
      const { x, y } = item;
      const moduleId: string = event.dataTransfer.getData('text'); // work with chrome, doesn't with firefox
      this.addNode.emit({ moduleId, positionX: x, positionY: y });

      // https://stackoverflow.com/questions/19055264/why-doesnt-html5-drag-and-drop-work-in-firefox
      // https://stackoverflow.com/questions/11656061/why-is-event-clientx-incorrectly-showing-as-0-in-firefox-for-dragend-event
      // https://bugzilla.mozilla.org/show_bug.cgi?id=339293
      event.preventDefault(); // WHY TF DO I NEED THIS MOZILLA PLS FIX
    },
    itemChangeCallback: (item: GridsterItem) => {
      const { id, x, y } = item;
      this.moveNode.emit({ workflowModuleId: id, positionX: x, positionY: y });
      return false;
    },
  };

  @Input() loading: boolean;
  @Input() workflow: Workflow;
  @Input() workflowModules: WorkflowModule[];
  @Input() workflowModuleLinks: WorkflowModuleLink[];
  @Input() activeWorkflowModuleId: string;
  @Output() selectPane = new EventEmitter<void>();
  @Output() selectNode = new EventEmitter<string>();
  @Output() addNode = new EventEmitter<AddWorkflowModuleData>();
  @Output() moveNode = new EventEmitter<MoveWorkflowModuleData>();
  @Output() deleteNode = new EventEmitter<string>();
  @Output() openWarnings = new EventEmitter<void>();
  @Output() addEdge = new EventEmitter<AddWorkflowModuleLinkData>();
  @Output() deleteEdge = new EventEmitter<string>();
  @ViewChild(GridsterComponent, { static: true }) gridComponent: GridsterComponentInterface;

  constructor(
    private readonly graphConnectivity: WorkflowDetailsGraphConnectivity,
    private readonly graphPan: WorkflowDetailsGraphPan,
  ) {}

  // Need this to make link update working while dragging node
  private overrideRendererUpdate() {
    const oldUpdateItem = this.gridComponent.gridRenderer.updateItem.bind(this.gridComponent.gridRenderer);
    this.gridComponent.gridRenderer.updateItem = (el: any, item: GridsterItem, renderer: Renderer2) => {
      oldUpdateItem(el, item, renderer);
      this.graphConnectivity.refreshNode(el);
    };
  }

  ngOnInit(): void {
    this.graphPan.setGridContainer(this.gridComponent.el);
    this.graphConnectivity.init(this.gridComponent.el);
    this.overrideRendererUpdate();

    // Subscribe to instance events
    this.graphConnectivity.dragEdgeEnd$
      .pipe(takeUntil(this.destroySubject))
      .subscribe((data) => {
        const { sourceId, targetId } = data;
        this.addEdge.emit({ inputModuleId: sourceId, outputModuleId: targetId})
      });
  }

  ngOnDestroy(): void {
    this.destroySubject.next();
    this.destroySubject.complete();
  }

  trackNodeById(index: number, item: WorkflowModule) {
    return item.id;
  }

  trackEdgeById(index: number, item: WorkflowModuleLink) {
    return item.id;
  }

  onPaneMouseDown(event: MouseEvent) {
    this.graphPan.startPanning(event)
  }

  onPaneMouseUp(event: MouseEvent) {
    if(this.graphConnectivity.dragInProgress) return;
    if(event.target !== this.gridComponent.el) return;
    this.selectPane.emit();
    this.graphPan.stopPanning()
  }

  onPaneMouseMove(event: MouseEvent) {
    this.graphPan.panning(event)
  }

  onPaneMouseLeave() {
    this.graphPan.stopPanning();
  }

  onPaneMouseWheel(event: WheelEvent) {
    this.graphPan.zooming(event)
  }

  onPaneDblClick() {
    this.graphPan.reset();
  }

  onSelectNode(workflowModuleId: string) {
    this.selectNode.emit(workflowModuleId);
  }

  onOpenWarnings() {
    this.openWarnings.emit();
  }

  onDeleteNode(workflowModuleId: string) {
    this.deleteNode.emit(workflowModuleId);
  }

  onDeleteEdge(workflowModuleLinkId: string) {
    this.deleteEdge.emit(workflowModuleLinkId);
  }
}
