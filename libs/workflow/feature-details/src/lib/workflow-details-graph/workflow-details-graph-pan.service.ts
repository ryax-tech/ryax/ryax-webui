// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable, Renderer2 } from '@angular/core';
import * as $ from 'jquery';
import { WorkflowDetailsGraphConnectivity } from './workflow-details-graph-connectivity.service';

@Injectable()
export class WorkflowDetailsGraphPan {
  private parentContainerElement: Element;
  private gridContainerElement: Element;
  private isPanning = false;
  private scale = 1;
  private translateX = 0;
  private translateY = 0;

  constructor(
    private readonly renderer: Renderer2,
    private readonly graphicalConnectivity: WorkflowDetailsGraphConnectivity
  ) {}

  setGridContainer(gridContainerElement: Element) {
    this.parentContainerElement = gridContainerElement.parentElement;
    this.gridContainerElement = gridContainerElement;
  }

  reset() {
    this.translateX = 0;
    this.translateY = 0;
    this.scale = 1;
    this.updateView();
  }

  startPanning(event: MouseEvent) {
    if(event.button !== 0) return;
    if(event.target !== this.gridContainerElement) return;
    this.isPanning = true;
    this.renderer.addClass(this.gridContainerElement, 'panning');
  }

  stopPanning() {
    this.isPanning = false;
    this.renderer.removeClass(this.gridContainerElement, 'panning');
  }

  panning(event: MouseEvent) {
    if(!this.isPanning) return;
    this.updateTranslate(event.movementX, event.movementY);
    this.updateView();
  }

  getZoomScale() {
    return this.scale;
  }

  getGridContainerBoundingRect() {
    return this.gridContainerElement.getBoundingClientRect();
  }

  zooming(event: WheelEvent) {
    if(event.preventDefault) event.preventDefault();

    const absDeltaY = Math.abs(event.deltaY);
    if(absDeltaY === 0) return;
    this.updateScale(-1 * event.deltaY / absDeltaY * 0.05);
    this.updateView();
  }

  private getElementDimensions(element: Element) {
    const jqueryElement = $(element);
    return {
      width: jqueryElement.width(),
      height: jqueryElement.height()
    };
  }

  private updateScale(ratio: number) {
    const gridDimensions = this.getElementDimensions(this.gridContainerElement);
    const parentDimensions = this.getElementDimensions(this.parentContainerElement);

    // Compute minimun value for scale
    const scaleMin = Math.max(
      parentDimensions.width / (gridDimensions.width - this.translateX),
      parentDimensions.height / (gridDimensions.height - this.translateY),
    );
    this.scale = Math.max(Math.min(this.scale + ratio, 1), scaleMin);
    this.graphicalConnectivity.setScale(this.scale);
  }

  private updateTranslate(mouseMoveX: number, mouseMoveY: number) {
    const gridDimensions = this.getElementDimensions(this.gridContainerElement);
    const parentDimensions = this.getElementDimensions(this.parentContainerElement);

    // Compute max translate values
    const maxTranslateX = this.scale * gridDimensions.width - parentDimensions.width;
    const maxTranslateY = this.scale * gridDimensions.height - parentDimensions.height;


    // Compute translate values due to mouse move
    const newTranslateX = this.translateX - mouseMoveX;
    const newTranslateY = this.translateY - mouseMoveY;

    // Update translate values
    this.translateX = (newTranslateX >= 0) ? ((newTranslateX < maxTranslateX) ? newTranslateX : maxTranslateX) : 0;
    this.translateY = (newTranslateY >= 0) ? ((newTranslateY < maxTranslateY) ? newTranslateY : maxTranslateY) : 0;
  }

  private updateView() {
    const translateStyle = `translate(${-1 * this.translateX}px ,${-1 * this.translateY}px)`;
    const scaleStyle = `scale(${this.scale})`;
    this.renderer.setStyle(this.gridContainerElement, 'transform-origin', `0px 0px`);
    this.renderer.setStyle(this.gridContainerElement, 'transform', `${translateStyle} ${scaleStyle}`);
  }
}
