// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WorkflowDomainModule } from '@ryax/workflow/domain';
import { UserApiModule } from '@ryax/user/api';
import { SharedUiCommonModule } from '@ryax/shared/ui-common';
import { WorkflowEditComponent } from './workflow-edit/workflow-edit.component';
import { WorkflowEditFormComponent } from './workflow-edit-form/workflow-edit-form.component';
import { WorkflowEditTitlePipe } from './pipes/workflow-edit-title.pipe';

@NgModule({
  imports: [
    CommonModule,
    WorkflowDomainModule,
    UserApiModule,
    SharedUiCommonModule
  ],
  declarations: [
    WorkflowEditComponent,
    WorkflowEditFormComponent,
    WorkflowEditTitlePipe
  ],
  exports: [
    WorkflowEditComponent
  ]
})
export class WorkflowFeatureEditModule {}
