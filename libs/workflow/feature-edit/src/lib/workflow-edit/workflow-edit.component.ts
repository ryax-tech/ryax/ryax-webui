// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component } from '@angular/core';
import { WorkflowEditData, WorkflowEditFacade } from '@ryax/workflow/domain';

@Component({
  selector: 'ryax-workflow-edit',
  templateUrl: './workflow-edit.component.pug'
})
export class WorkflowEditComponent {
  mode$ = this.facade.mode$;
  workflow$ = this.facade.workflow$;

  constructor(
    private readonly facade: WorkflowEditFacade
  ) {}

  onSave(data: WorkflowEditData) {
    this.facade.save(data);
  }

  onClose() {
    this.facade.close();
  }
}
