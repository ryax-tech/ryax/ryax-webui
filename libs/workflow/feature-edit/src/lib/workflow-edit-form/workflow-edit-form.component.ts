// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Workflow, WorkflowEditData, WorkflowEditMode } from '@ryax/workflow/domain';

@Component({
  selector: 'ryax-workflow-edit-form',
  templateUrl: './workflow-edit-form.component.pug'
})
export class WorkflowEditFormComponent {
  private _mode: WorkflowEditMode;
  private _workflow: Workflow;

  form = new FormGroup({
    name: new FormControl(null, Validators.required),
    description: new FormControl(null),
  });

  @Input() set mode(value: WorkflowEditMode) {
    this._mode = value;
    this.initForm();
  }
  @Input() set workflow(value: Workflow) {
    this._workflow = value;
    this.initForm();
  }
  @Output() save = new EventEmitter<WorkflowEditData>();
  @Output() cancel = new EventEmitter<void>();

  constructor(
    private readonly formBuilder: FormBuilder
  ) {}

  private initForm() {
    const formValue = {
      name: this._workflow ? this._workflow.name : null,
      description: this._workflow ? this._workflow.description : null,
    };
    this.form.reset(formValue);
  }

  onSave() {
    const formView = this.form.value;
    this.save.emit(formView);
  }

  onCancel() {
    this.cancel.emit();
  }
}
