// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Pipe, PipeTransform } from '@angular/core';
import { WorkflowEditMode } from '@ryax/workflow/domain';

@Pipe({
  name: 'ryaxWorkflowEditTitle',
  pure: true
})
export class WorkflowEditTitlePipe implements PipeTransform {
  transform(value: WorkflowEditMode): string {
    switch (value) {
      case WorkflowEditMode.Edit:
        return "Edit workflow";
      case WorkflowEditMode.Create:
        return "New workflow";
      case WorkflowEditMode.Copy:
        return "Copy workflow";
      default:
        return undefined;
    }
  }
}
