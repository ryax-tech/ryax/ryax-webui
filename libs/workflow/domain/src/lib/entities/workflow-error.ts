// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
export enum WorkflowErrorCode {
  WORKFLOW_NOT_CONNECTED = 1,
  WORKFLOW_HAS_CYCLES = 2,
  WORKFLOW_MODULES_UPPER_LIMIT_REACHED = 3,
  WORKFLOW_SOURCES_LOWER_LIMIT_REACHED = 4,

  WORKFLOW_MODULE_LINK_INPUT_UPPER_LIMIT_REACHED = 101,
  WORKFLOW_MODULE_LINK_INPUT_LOWER_LIMIT_REACHED = 102,
  WORKFLOW_MODULE_LINK_OUTPUT_UPPER_LIMIT_REACHED = 103,
  WORKFLOW_MODULE_LINK_OUTPUT_LOWER_LIMIT_REACHED = 104,
  WORKFLOW_MODULE_INPUTS_NOT_DEFINED = 105,
}

export interface WorkflowError {
  id: string;
  code: WorkflowErrorCode;
  workflowId: string;
  workflowModuleId: string;
}
