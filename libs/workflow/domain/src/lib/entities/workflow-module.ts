// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
export enum WorkflowModuleKind {
  SOURCE = "Source",
  PROCESSOR = "Processor",
  PUBLISHER = "Publisher",
  STREAM_OPERATOR = "StreamOperator"
}

export enum WorkflowModuleStatus {
  VALID = "valid",
  INVALID = "invalid",
}

export enum WorkflowModuleDeploymentStatus {
  NONE = "None",
  DEPLOYING = "Deploying",
  UNDEPLOYING = "Undeploying",
  DEPLOYED = "Deployed",
  ERROR = "Error"
}

export interface WorkflowModuleCategory {
  categoryId: string;
  categoryName: string;
}

export interface WorkflowModule {
  id: string;
  name: string;
  description: string;
  version: string;
  kind: WorkflowModuleKind;
  technicalName: string;
  technicalId: string;
  hasDynamicOutputs: boolean;
  moduleId: string;
  customName: string;
  positionX:  number;
  positionY: number;
  workflowId: string;
  status: WorkflowModuleStatus;
  deploymentStatus: WorkflowModuleDeploymentStatus;
  categories: WorkflowModuleCategory[];
}

export interface AddWorkflowModuleData {
  moduleId: string;
  positionX: number;
  positionY: number;
}

export interface MoveWorkflowModuleData {
  workflowModuleId: string;
  positionX: number;
  positionY: number;
}

export interface UpdateWorkflowModuleData {
  customName?: string;
  positionX?: number;
  positionY?: number;
}
