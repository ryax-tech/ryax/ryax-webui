// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
export enum WorkflowStatus {
  VALID = "valid",
  INVALID = "invalid",
}

export enum WorkflowDeploymentStatus {
  NONE = "None",
  DEPLOYING = "Deploying",
  UNDEPLOYING = "Undeploying",
  DEPLOYED = "Deployed",
  ERROR = "Error",
}

export interface Workflow {
  id: string;
  name: string;
  description: string;
  status: WorkflowStatus;
  deploymentStatus: WorkflowDeploymentStatus;
}

export interface AddWorkflowData {
  name: string;
  description?: string;
  fromWorkflow?: string;
}

export interface UpdateWorkflowData {
  name?: string;
  description?: string;
}
