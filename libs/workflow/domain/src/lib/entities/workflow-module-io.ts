// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { WorkflowFile } from './workflow-file';

export enum WorkflowModuleIOType {
  STRING = "string",
  LONGSTRING = "longstring",
  INTEGER = "integer",
  FLOAT = "float",
  PASSWORD = "password",
  ENUM = "enum",
  FILE = "file",
  DIRECTORY = "directory",
  TABLE = "table"
}

export interface WorkflowModuleIO {
  id: string;
  technicalName: string;
  displayName: string;
  type: WorkflowModuleIOType;
  help: string;
  enumValues: string[];
  workflowModuleId: string;
  workflowId: string;
}

export interface WorkflowModuleInput extends WorkflowModuleIO {
  staticValue: string;
  referenceValue: string;
  workflowFile: WorkflowFile;
}

export type WorkflowModuleOutput = WorkflowModuleIO

export interface UpdateWorkflowModuleInputData {
  workflowModuleInputId: string;
  staticValue?: string | File;
  referenceValue?: string;
}

export interface UpdateWorkflowModuleOutputData {
  workflowModuleOutputId: string;
  displayName?: string;
  help?: string;
  type?: WorkflowModuleIOType;
  enumValues?: string[];
}
