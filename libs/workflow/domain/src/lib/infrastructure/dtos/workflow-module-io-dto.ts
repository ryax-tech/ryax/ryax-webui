// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { WorkflowFileDto } from './workflow-file-dto';

export type WorkflowModuleIOTypeDto = "string" | "longstring" | "integer" | "float" | "password" | "enum" | "file" | "directory" | "table"

export interface WorkflowModuleIODto {
  id: string;
  technical_name: string;
  display_name: string;
  type: WorkflowModuleIOTypeDto;
  help: string;
  enum_values: string[];
}

export interface WorkflowModuleInputDto extends WorkflowModuleIODto {
  static_value: string
  reference_value: string
  reference_output: WorkflowModuleOutputDto;
  workflow_file: WorkflowFileDto;
}

export interface UpdateWorkflowModuleInputValueDto {
  reference_value: string;
  static_value: string;
}

export interface WorkflowModuleOutputDto extends WorkflowModuleIODto {
  workflow_module_id: string;
}

export interface AddWorkflowModuleOutputDto {
  technical_name: string;
  display_name: string;
  help: string;
  type: WorkflowModuleIOTypeDto;
  enum_values: string[]
}

export interface UpdateWorkflowModuleOutputDto {
  technical_name: string;
  display_name: string;
  help: string;
  type: WorkflowModuleIOTypeDto;
  enum_values: string[]
}


