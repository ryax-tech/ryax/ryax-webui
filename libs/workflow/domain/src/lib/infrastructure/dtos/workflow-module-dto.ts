// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
export type WorkflowModuleKindDto = "Source" | "Processor" | "Publisher" | "StreamOperator"

export type WorkflowModuleStatusDto = "valid" | "invalid";

export type WorkflowModuleDeploymentStatusDto = "none" | "deploying" | "deployed" | 'undeploying' | "error";

export interface WorkflowModuleCategoryDto {
  id: string;
  name: string;
}

export interface WorkflowModuleDto {
  id: string;
  module_id: string;
  name: string;
  custom_name: string;
  description: string;
  kind: WorkflowModuleKindDto;
  technical_name: string;
  has_dynamic_outputs: boolean;
  version: string;
  position_x: number;
  position_y: number;
  status: WorkflowModuleStatusDto;
  deployment_status: WorkflowModuleDeploymentStatusDto;
  categories: WorkflowModuleCategoryDto[]
}

export interface AddWorkflowModuleDto {
  module_id: string;
  custom_name?: string;
  position_y?: number;
  position_x?: number;
}

export interface UpdateWorkflowModuleDto {
  custom_name?: string;
  position_x?: number;
  position_y?: number;
}
