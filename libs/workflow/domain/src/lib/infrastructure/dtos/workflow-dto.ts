// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { WorkflowModuleDto } from './workflow-module-dto';
import { WorkflowModuleLinkDto } from './workflow-module-link-dto';

export type WorkflowStatusDto = 'valid' | 'invalid';

export type WorkflowDeployedStatusDto = 'None' | 'Deploying' | 'Deployed' | 'Undeploying' | 'Error';

export interface WorkflowDto {
  id: string;
  name: string;
  description: string;
  status: WorkflowStatusDto;
  deployment_status: WorkflowDeployedStatusDto;
}

export interface WorkflowDetailsDto extends WorkflowDto {
  modules: WorkflowModuleDto[];
  modules_links: WorkflowModuleLinkDto[];
}

export interface AddWorkflowDto {
  from_workflow?: string;
  name?: string;
  description?: string;
}

export interface AddWorkflowResponseDto {
  workflow_id: string;
}

export interface UpdateWorkflowDto {
  name?: string;
  description?: string;
}
