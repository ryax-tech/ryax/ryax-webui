// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { WorkflowError } from '@ryax/workflow/domain';
import { WorkflowErrorDto } from '../dtos';

@Injectable()
export class WorkflowErrorAdapter {
  adapt(dto: WorkflowErrorDto, workflowId: string): WorkflowError {
    return {
      id: dto.id,
      code: dto.code,
      workflowId,
      workflowModuleId: dto.workflow_module_id
    };
  }
}
