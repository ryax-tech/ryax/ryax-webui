// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { TestBed } from '@angular/core/testing';
import { WorkflowAdapter } from './workflow.adapter';
import { WorkflowDto } from '../dtos';
import { WorkflowStatus } from '@ryax/workflow/domain';

describe("WorkflowAdapter", () => {
  let workflowAdapter: WorkflowAdapter;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        WorkflowAdapter,
      ],
    });

    workflowAdapter = TestBed.get(WorkflowAdapter);
  });

  // TODO : add tests for each status
  test('adapt', () => {
    const workflowDto: any = {
      id: "workflow-id",
      name: "workflow-name",
      description: "workflow-description",
      status: 'valid'
    };
    const result = workflowAdapter.adapt(workflowDto);
    expect(result).toEqual({
      id: "workflow-id",
      name: "workflow-name",
      description: "workflow-description",
      status: WorkflowStatus.VALID
    })
  });
});
