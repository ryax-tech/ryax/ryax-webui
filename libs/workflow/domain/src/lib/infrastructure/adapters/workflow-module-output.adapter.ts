// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { WorkflowModuleIOType, WorkflowModuleOutput } from '@ryax/workflow/domain';
import { WorkflowModuleOutputDto } from '../dtos';

@Injectable()
export class WorkflowModuleOutputAdapter {

  adapt(dto: WorkflowModuleOutputDto, workflowId: string): WorkflowModuleOutput {
    return {
      id: dto.id,
      technicalName: dto.technical_name,
      displayName: dto.display_name,
      help: dto.help,
      type: dto.type as WorkflowModuleIOType,
      enumValues: dto.enum_values,
      workflowModuleId: dto.workflow_module_id,
      workflowId,
    };
  }
}
