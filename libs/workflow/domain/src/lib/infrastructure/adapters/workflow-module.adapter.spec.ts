// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { TestBed } from '@angular/core/testing';
import { WorkflowModuleDto } from '../dtos';
import { WorkflowModuleAdapter } from './workflow-module.adapter';
import { WorkflowModuleKind } from '@ryax/workflow/domain';

describe("WorkflowModuleAdapter", () => {
  let workflowModuleAdapter: WorkflowModuleAdapter;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        WorkflowModuleAdapter,
      ],
    });

    workflowModuleAdapter = TestBed.get(WorkflowModuleAdapter);
  });

  test('adapt', () => {
    const workflowId = "worklfowId";
    const workflowModuleDto: WorkflowModuleDto = {
      id: "id",
      module_id: "module_id",
      name: "name",
      custom_name: "custom_name",
      description: "description",
      kind: "Source",
      has_dynamic_outputs: true,
      version: "version",
      position_x: 1,
      position_y: 1,
      status: "valid",
    };
    const result = workflowModuleAdapter.adapt(workflowModuleDto, workflowId);
    expect(result).toEqual({
      id: "id",
      name: "name",
      version: "version",
      kind: WorkflowModuleKind.SOURCE,
      description: "description",
      moduleId: "module_id",
      customName: "custom_name",
      positionX:  1,
      positionY: 1,
      workflowId: workflowId,
    })
  });
});
