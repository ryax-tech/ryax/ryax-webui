// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { WorkflowFileDto } from '../dtos';
import { WorkflowFile } from '@ryax/workflow/domain';

@Injectable()
export class WorkflowFileAdapter {

  adapt(dto: WorkflowFileDto): WorkflowFile {
    return {
      id : dto.id,
      name : dto.name,
      extension : dto.extension,
      size : dto.size,
    };
  }
}
