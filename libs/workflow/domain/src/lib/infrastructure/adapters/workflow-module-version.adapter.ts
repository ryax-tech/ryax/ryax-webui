// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { UpdateModuleVersionResponse, WorkflowModuleVersion } from '@ryax/workflow/domain';
import { UpdateModuleVersionResponseDto, WorkflowModuleVersionDto } from '../dtos';

@Injectable()
export class WorkflowModuleVersionAdapter {

  adapt(dto: WorkflowModuleVersionDto): WorkflowModuleVersion {
    return {
      moduleId: dto.id,
      versionName: dto.version
    };
  }

  adaptResponse(dto: UpdateModuleVersionResponseDto): UpdateModuleVersionResponse {
    return {
      workflowModuleId: dto.workflow_module_id,
    };
  }
}
