// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { WorkflowDto} from '../dtos';
import { Workflow, WorkflowDeploymentStatus, WorkflowStatus } from '@ryax/workflow/domain';

@Injectable()
export class WorkflowAdapter {

  adapt(dto: WorkflowDto): Workflow {
    return {
      id: dto.id,
      name: dto.name,
      description: dto.description,
      status: dto.status as WorkflowStatus,
      deploymentStatus: dto.deployment_status as WorkflowDeploymentStatus,
    };
  }
}
