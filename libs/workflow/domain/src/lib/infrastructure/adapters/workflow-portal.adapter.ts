// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { WorkflowPortal } from '../../entities';
import { WorkflowPortalDto } from '../dtos';

@Injectable()
export class WorkflowPortalAdapter {

  constructor() {}

  adapt(dto: WorkflowPortalDto, workflowId: string): WorkflowPortal {
    return {
      id: dto.id,
      name: dto.name,
      customName: dto.custom_name,
      description: dto.description,
      accessPath: dto.access_path,
      workflowId
    };
  }
}
