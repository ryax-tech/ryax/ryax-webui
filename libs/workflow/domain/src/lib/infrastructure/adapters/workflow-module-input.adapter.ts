// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { WorkflowModuleInputDto } from '../dtos';
import { WorkflowModuleInput, WorkflowModuleIOType } from '@ryax/workflow/domain';

@Injectable()
export class WorkflowModuleInputAdapter {

  adapt(dto: WorkflowModuleInputDto, workflowId: string, workflowModuleId: string): WorkflowModuleInput {
    return {
      id: dto.id,
      technicalName: dto.technical_name,
      displayName: dto.display_name,
      help: dto.help,
      type: dto.type as WorkflowModuleIOType,
      enumValues: dto.enum_values,
      staticValue: dto.static_value,
      referenceValue: dto.reference_value,
      workflowFile: dto.workflow_file,
      workflowId,
      workflowModuleId
    };
  }
}
