// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { ModuleRunDto } from '../dtos';
import { ModuleRun } from '@ryax/workflow/domain';

@Injectable()
export class ModuleRunAdapter {
  adapt(dto: ModuleRunDto): ModuleRun {
    return {
      executionId: dto.execution_id,
    };
  }
}
