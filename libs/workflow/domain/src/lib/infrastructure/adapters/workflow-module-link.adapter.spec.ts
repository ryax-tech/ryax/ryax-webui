// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { TestBed } from '@angular/core/testing';
import { WorkflowModuleLinkDto } from '../dtos';
import { WorkflowModuleLinkAdapter } from './workflow-module-link.adapter';

describe("WorkflowModuleLinkAdapter", () => {
  let workflowModuleLinkAdapter: WorkflowModuleLinkAdapter;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        WorkflowModuleLinkAdapter,
      ],
    });

    workflowModuleLinkAdapter = TestBed.get(WorkflowModuleLinkAdapter);
  });

  test('adapt', () => {
    const workflowId = "workflowId";
    const workflowModuleLinkDto: WorkflowModuleLinkDto = {
      id: "id",
      input_module_id: "input_module_id",
      output_module_id: "output_module_id"
    };

    const result = workflowModuleLinkAdapter.adapt(workflowModuleLinkDto, workflowId);
    expect(result).toEqual({
      id: "id",
      inputModuleId: "input_module_id",
      outputModuleId: "output_module_id",
      workflowId,
    })
  });
});
