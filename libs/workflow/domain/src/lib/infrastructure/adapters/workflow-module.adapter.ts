// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { WorkflowModuleCategoryDto, WorkflowModuleDto } from '../dtos';
import {
  WorkflowModule, WorkflowModuleCategory,
  WorkflowModuleDeploymentStatus,
  WorkflowModuleKind,
  WorkflowModuleStatus,
} from '@ryax/workflow/domain';

@Injectable()
export class WorkflowModuleAdapter {

  adapt(dto: WorkflowModuleDto, workflowId: string): WorkflowModule {
    return {
      id: dto.id,
      name: dto.name,
      version: dto.version,
      kind: dto.kind as WorkflowModuleKind,
      technicalName: dto.technical_name,
      technicalId: dto.technical_name+'-'+dto.version,
      hasDynamicOutputs: dto.has_dynamic_outputs,
      description: dto.description,
      moduleId: dto.module_id,
      customName: dto.custom_name,
      positionX:  dto.position_x,
      positionY: dto.position_y,
      status: dto.status as WorkflowModuleStatus,
      deploymentStatus: dto.deployment_status as WorkflowModuleDeploymentStatus,
      categories: dto.categories.map(item => this.adaptCategory(item)),
      workflowId
    };
  }

  adaptCategory(dto: WorkflowModuleCategoryDto): WorkflowModuleCategory {
    return {
      categoryId: dto.id,
      categoryName: dto.name
    }
  }
}
