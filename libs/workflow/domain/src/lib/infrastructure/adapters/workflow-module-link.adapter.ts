// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { WorkflowModuleLink } from '@ryax/workflow/domain';
import { WorkflowModuleLinkDto } from '../dtos';

@Injectable()
export class WorkflowModuleLinkAdapter {

  adapt(dto: WorkflowModuleLinkDto, workflowId: string): WorkflowModuleLink {
    return {
      id: dto.id,
      inputModuleId: dto.input_module_id,
      outputModuleId: dto.output_module_id,
      workflowId
    };
  }
}
