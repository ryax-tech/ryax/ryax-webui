// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { map, tap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import {
  AddWorkflowDto,
  AddWorkflowModuleDto,
  AddWorkflowModuleLinkDto, AddWorkflowModuleOutputDto,
  AddWorkflowResponseDto, UpdateModuleVersionResponseDto,
  UpdateWorkflowDto,
  UpdateWorkflowModuleDto,
  UpdateWorkflowModuleInputValueDto, UpdateWorkflowModuleOutputDto,
  WorkflowDetailsDto,
  WorkflowDto, WorkflowErrorDto,
  WorkflowModuleInputDto, WorkflowModuleOutputDto, WorkflowModuleVersionDto,
} from '../dtos';
import {
  AddWorkflowData,
  AddWorkflowModuleData,
  AddWorkflowModuleLinkData,
  UpdateWorkflowData,
  UpdateWorkflowModuleData,
  UpdateWorkflowModuleInputData, UpdateWorkflowModuleOutputData, WorkflowDeploymentStatus,
  WorkflowFilter, WorkflowModuleIOType, WorkflowModuleVersion,
  WorkflowStatusFilter,
} from '../../entities';
import {
  WorkflowAdapter,
  WorkflowModuleAdapter,
  WorkflowModuleInputAdapter,
  WorkflowModuleLinkAdapter,
  WorkflowModuleOutputAdapter,
  WorkflowErrorAdapter, WorkflowModuleVersionAdapter,
} from '../adapters';
import { saveAs } from 'file-saver';

@Injectable()
export class WorkflowApi {
  private baseUrl = "/api/studio/workflows";

  constructor(
    private readonly http: HttpClient,
    private readonly workflowAdapter: WorkflowAdapter,
    private readonly workflowModuleAdapter: WorkflowModuleAdapter,
    private readonly workflowModuleLinkAdapter: WorkflowModuleLinkAdapter,
    private readonly workflowModuleInputAdapter: WorkflowModuleInputAdapter,
    private readonly workflowModuleOutputAdapter: WorkflowModuleOutputAdapter,
    private readonly workflowErrorAdapter: WorkflowErrorAdapter,
    private readonly workflowModuleVersionAdapter: WorkflowModuleVersionAdapter,
  ) {}

  loadAll(filter: WorkflowFilter = null) {
    let params = new HttpParams();
    if(filter && filter.term) {
      params = params.set("search", filter.term.toLowerCase());
    }
    if(filter && !!filter.status) {
      const value = filter.status === WorkflowStatusFilter.Deployed ? WorkflowDeploymentStatus.DEPLOYED : WorkflowDeploymentStatus.NONE;
      params = params.set("deployment_status", value);
    }
    return this.http.get<WorkflowDto[]>(this.baseUrl, { responseType: "json", params }).pipe(
      map(dtos => dtos.map(item => this.workflowAdapter.adapt(item)))
    );
  }

  create(data: AddWorkflowData) {
    const createDto: AddWorkflowDto = {
      name: data.name ? data.name : undefined,
      description: data.description ? data.description : undefined,
      from_workflow: data.fromWorkflow ? data.fromWorkflow : undefined
    };
    return this.http.post<AddWorkflowResponseDto>(this.baseUrl, createDto, { responseType: 'json'}).pipe(
      map(dto => dto.workflow_id)
    );
  }

  loadOne(workflowId: string) {
    const url = [this.baseUrl, workflowId].join('/');
    return this.http.get<WorkflowDetailsDto>(url, { responseType: 'json' }).pipe(
      map(dto => ({
        workflow: this.workflowAdapter.adapt(dto),
        modules: dto.modules.map(item => this.workflowModuleAdapter.adapt(item, workflowId)),
        moduleLinks: dto.modules_links.map(item => this.workflowModuleLinkAdapter.adapt(item, workflowId))
      }))
    );
  }

  update(workflowId: string, data: UpdateWorkflowData) {
    const url = [this.baseUrl, workflowId].join('/');
    const updateDto: UpdateWorkflowDto = {
      name: data.name ? data.name : undefined,
      description: data.description ? data.description : undefined,
    };
    return this.http.put<void>(url, updateDto, { responseType: 'json'}).pipe(
      map(dto => dto)
    );
  }

  delete(workflowId: string) {
    const url = [this.baseUrl, workflowId].join('/');
    return this.http.delete<void>(url, { responseType: 'json' });
  }

  deploy(workflowId: string) {
    const url = [this.baseUrl, workflowId, 'deploy'].join('/');
    return this.http.post<void>(url, null, { responseType: 'json' });
  }

  stop(workflowId: string) {
    const url = [this.baseUrl, workflowId, "stop"].join('/');
    return this.http.post<WorkflowDto>(url, null, { responseType: 'json'});
  }

  addModule(workflowId: string, data: AddWorkflowModuleData) {
    const url = [this.baseUrl, workflowId, "modules"].join('/');
    const createDto: AddWorkflowModuleDto = {
      module_id: data.moduleId,
      position_x: !isNaN(data.positionX) ? data.positionX : undefined,
      position_y: !isNaN(data.positionY) ? data.positionY : undefined
    };
    return this.http.post<void>(url, createDto, { responseType: 'json'});
  }

  deleteModule(workflowId: string, moduleId: string) {
    const url = [this.baseUrl, workflowId, "modules", moduleId].join('/');
    return this.http.delete<void>(url, { responseType: 'json' });
  }

  updateModule(workflowId: string, moduleId: string, data: UpdateWorkflowModuleData) {
    const url = [this.baseUrl, workflowId, "modules", moduleId].join('/');
    const updateDto: UpdateWorkflowModuleDto = {
      custom_name: data.customName ? data.customName : undefined,
      position_x: !isNaN(data.positionX) ? data.positionX : undefined,
      position_y: !isNaN(data.positionY) ? data.positionY : undefined,
    };
    return this.http.put<void>(url, updateDto, { responseType: 'json'});
  }

  addModuleLink(workflowId: string, data: AddWorkflowModuleLinkData) {
    const url = [this.baseUrl, workflowId, "modules-links"].join('/');
    const createDto: AddWorkflowModuleLinkDto = {
      input_module_id: data.inputModuleId,
      output_module_id: data.outputModuleId
    };
    return this.http.post<void>(url, createDto, { responseType: 'json'});
  }

  deleteModuleLink(workflowId: string, moduleLinkId: string) {
    const url = [this.baseUrl, workflowId, "modules-links", moduleLinkId].join('/');
    return this.http.delete<void>(url, { responseType: 'json' });
  }

  loadModuleInputs(workflowId: string, workflowModuleId: string) {
    const url = [this.baseUrl, workflowId, "modules", workflowModuleId, "inputs"].join('/');
    return this.http.get<WorkflowModuleInputDto[]>(url, { responseType: 'json' }).pipe(
      map(dtos => ({
        workflowModuleInputs: dtos.map(item => this.workflowModuleInputAdapter.adapt(item, workflowId, workflowModuleId)),
        workflowModuleOutputs: dtos.filter(item => item.reference_output).map(item => this.workflowModuleOutputAdapter.adapt(item.reference_output, workflowId))
      }))
    );
  }

  loadModuleOutputs(workflowId: string, workflowModuleId: string) {
    // const url = [this.baseUrl, workflowId, "modules", workflowModuleId, "outputs"].join('/');
    const url = ["studio", "portals", workflowId, "modules", workflowModuleId, "outputs"].join('/');
    return this.http.get<WorkflowModuleOutputDto[]>(url, { responseType: 'json' }).pipe(
      map(dtos => dtos.map(item => this.workflowModuleOutputAdapter.adapt(item, workflowId)))
    );
  }

  searchModuleOutputs(workflowId: string, workflowModuleOutputType: WorkflowModuleIOType, rootWorkflowModuleId: string) {
    const url = [this.baseUrl, workflowId, "modules-outputs"].join('/');
    let params = new HttpParams();
    if(workflowModuleOutputType) params = params.set("with_type", workflowModuleOutputType);
    if(rootWorkflowModuleId) params = params.set("accessible_from", rootWorkflowModuleId);
    return this.http.get<WorkflowModuleOutputDto[]>(url, { responseType: 'json', params }).pipe(
      map(dtos => dtos.map(item => this.workflowModuleOutputAdapter.adapt(item, workflowId)))
    );
  }

  updateModuleInput(workflowId: string, moduleId: string, inputId: string, data: UpdateWorkflowModuleInputData) {
    const url = [this.baseUrl, workflowId, "modules", moduleId, "inputs", inputId, "value"].join('/');
    const updateDto: UpdateWorkflowModuleInputValueDto = {
      static_value: data.staticValue as string,
      reference_value: data.referenceValue ? data.referenceValue : undefined,
    };
    return this.http.put<void>(url, updateDto, { responseType: 'json'});
  }

  updateModuleInputFile(workflowId: string, moduleId: string, inputId: string, data: UpdateWorkflowModuleInputData) {
    const url = [this.baseUrl, workflowId, "modules", moduleId, "inputs", inputId, "file"].join('/');
    const fileData = new FormData();
    fileData.append("input_file", data.staticValue);
    return this.http.post<void>(url, fileData, { responseType: 'json'});
  }

  deleteInputFile(workflowId: string, moduleId: string, inputId: string) {
    const url = [this.baseUrl, workflowId, "modules", moduleId, "inputs", inputId, "file"].join('/');
    return this.http.delete<void>(url, { responseType: 'json'});
  }

  addWorkflowModuleOutput(workflowId: string, workflowModuleId: string) {
    const url = [this.baseUrl, workflowId, "modules", workflowModuleId, "outputs"].join('/');
    const dataDto: AddWorkflowModuleOutputDto = {
      technical_name: "new-output",
      display_name: "New output",
      help: "New output help",
      type: "string",
      enum_values: []
    };
    return this.http.post<void>(url, dataDto, { responseType: 'json'});
  }

  updateWorkflowModuleOutput(workflowId: string, workflowModuleId: string, data: UpdateWorkflowModuleOutputData) {
    const url = [this.baseUrl, workflowId, "modules", workflowModuleId, "outputs", data.workflowModuleOutputId].join('/');
    const dataDto: Partial<UpdateWorkflowModuleOutputDto> = {
      display_name: data.displayName,
      help: data.help,
      type: data.type,
      enum_values: data.enumValues
    };
    return this.http.put<void>(url, dataDto, { responseType: 'json'});
  }

  loadModuleVersion(moduleId: string) {
    const url = ["studio", "modules", moduleId, "list_versions"].join('/');
    return this.http.get<WorkflowModuleVersionDto[]>(url, { responseType: 'json' }).pipe(
      map(dtos => dtos.map(item => this.workflowModuleVersionAdapter.adapt(item)))
    );
  }

  updateModuleVersion(workflowId: string, workflowModuleId: string, newVersionModuleId: string) {
    const url = [this.baseUrl, workflowId, "modules", workflowModuleId, "change_version"].join('/');
    const data = {
      module_id: newVersionModuleId
    };
    return this.http.post<UpdateModuleVersionResponseDto>(url, data, { responseType: 'json'}).pipe(
      map(dto => this.workflowModuleVersionAdapter.adaptResponse(dto))
    );
  }

  deleteWorkflowModuleOutput(workflowId: string, workflowModuleId: string, workflowModuleOutputId: string) {
    const url = [this.baseUrl, workflowId, "modules", workflowModuleId, "outputs", workflowModuleOutputId].join('/');
    return this.http.delete<void>(url, { responseType: 'json'});
  }

  loadWorkflowErrors(workflowId: string) {
    const url = [this.baseUrl, workflowId, "errors"].join('/');
    return this.http.get<WorkflowErrorDto[]>(url, { responseType: 'json' }).pipe(
      map(dtos => dtos.map(item => this.workflowErrorAdapter.adapt(item, workflowId)))
    );
  }

  import(file: File) {
    const url = [this.baseUrl, "import"].join('/');
    const fileData = new FormData();
    fileData.append("import_file", file);
    return this.http.post<AddWorkflowResponseDto>(url, fileData).pipe(
      map(dto => dto)
    );
  }

  export(workflowId: string) {
    const url = [this.baseUrl, workflowId, "export"].join('/');
    return this.http.get(url, { responseType: "blob" }).pipe(
      tap(fileContent => saveAs(fileContent, `workflow-${workflowId}.zip`, ))
    );
  }
}
