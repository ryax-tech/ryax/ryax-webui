// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { ModuleRunAdapter } from '../adapters';
import { ModuleRunDto, WorkflowPortalDto } from '../dtos';
import { WorkflowPortalAdapter } from '../adapters';

@Injectable()
export class WorkflowPortalApi {
  constructor(
    private readonly http: HttpClient,
    private readonly portalAdapter: WorkflowPortalAdapter,
    private readonly moduleRunAdapter: ModuleRunAdapter,
  ) {}

  loadPortals(workflowId: string) {
    const url = `/api/studio/workflows/${workflowId}/portals`;
    return this.http.get<WorkflowPortalDto[]>(url, { responseType: "json" }).pipe(
      map(dtos => dtos.map(item => this.portalAdapter.adapt(item, workflowId)))
    );
  }

  loadPortal(workflowId: string, workflowPortalId: string) {
    // const url = `/studio/workflows/${workflowId}/portals/${workflowPortalId}`;
    const url = `/api/studio/portals/${workflowId}/${workflowPortalId}`;
    return this.http.get<WorkflowPortalDto>(url, { responseType: "json" }).pipe(
      map(dto => this.portalAdapter.adapt(dto, workflowId))
    );
  }

  sendData(externalUrl: string, data: any) {
    const url = `${externalUrl}/send`, formData = new FormData();

    Object.entries(data).forEach(
      (
        [key, value]
      ) => {
        const arr = value as any;
        if(arr.constructor === Array && arr.length > 0) {
          for (let i = 0; i < arr.length; i++) {
            formData.append(key, arr[i]);
          }
        } else {
          formData.append(key, value as any)
        }
      });
    return this.http.post<ModuleRunDto>(url, formData, { responseType: "json" }).pipe(
      map(dto => this.moduleRunAdapter.adapt(dto))
    );
  }
}
