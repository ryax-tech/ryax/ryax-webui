// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { WorkflowApi } from './workflow-api.service';
import { WorkflowDetailsDto, WorkflowDto } from '../dtos';
import {
  AddWorkflowModuleData,
  AddWorkflowModuleLinkData,
  UpdateWorkflowData,
  UpdateWorkflowModuleData, UpdateWorkflowModuleInputData, WorkflowFilter, WorkflowStatus, WorkflowStatusFilter,
} from '@ryax/workflow/domain';
import { WorkflowAdapter, WorkflowModuleAdapter, WorkflowModuleLinkAdapter } from '../adapters';

describe("WorkflowApi", () => {
  let workflowApi: WorkflowApi;
  let workflowAdapter: WorkflowAdapter;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        WorkflowApi,
        WorkflowAdapter,
        WorkflowModuleAdapter,
        WorkflowModuleLinkAdapter
      ],
    });

    workflowApi = TestBed.get(WorkflowApi);
    workflowAdapter = TestBed.get(WorkflowAdapter);
    httpTestingController = TestBed.get(HttpTestingController);
  });

  test('should return workflows', () => {
    const workflow1Dto: WorkflowDto = {
      id: "workflow1Dto",
      name:"workflow1Dto-name",
      description:"workflow1Dto-description",
      status: WorkflowStatus.VALID
    };
    const workflow2Dto: WorkflowDto = {
      id: "workflow2Dto",
      name:"workflow2Dto-name",
      description:"workflow2Dto-description",
      status: WorkflowStatus.VALID
    };
    const filter: WorkflowFilter = {
      term: "",
      status: WorkflowStatusFilter.All,
    };
    workflowApi.loadAll(filter).subscribe(response => {
      expect(response).toEqual([workflow1Dto, workflow2Dto]);
    });
    const request = httpTestingController.expectOne('/studio/workflows');
    expect(request.request.method).toEqual('GET');
    request.flush([workflow1Dto, workflow2Dto]);
  });

  test('should return filtered workflows', () => {
    const workflow1Dto: WorkflowDto = {
      id: "workflow1Dto",
      name:"workflow1Dto-name",
      description:"workflow1Dto-description",
      status: WorkflowStatus.VALID
    };
    const workflow2Dto: WorkflowDto = {
      id: "workflow2Dto",
      name:"workflow2Dto-name",
      description:"workflow2Dto-description",
      status: WorkflowStatus.VALID
    };
    const filter: WorkflowFilter = {
      term: "workflow1Dto",
      status: WorkflowStatusFilter.All,
    };
    workflowApi.loadAll(filter).subscribe(response => {
      expect(response).toEqual([workflow1Dto]);
  });
    const request = httpTestingController.expectOne('/studio/workflows?search=workflow1Dto');
    expect(request.request.method).toEqual('GET');
    request.flush([workflow1Dto]);
  });

  test.each([
    [{ name: "Workflow name" }, {name: "Workflow name" }],
    [{ name: "Workflow name", description: null }, { name: "Workflow name" }],
    [{ name: "Workflow name", description: "" }, { name: "Workflow name" }],
    [{ name: "Workflow name", fromWorkflow: null }, { name: "Workflow name" }],
    [{ name: "Workflow name", fromWorkflow: "" }, { name: "Workflow name" }],
    [{ name: "Workflow name", description: "Description" }, { name: "Workflow name", description: "Description" }],
    [{ name: "Workflow name", description: "Description", fromWorkflow: "workflowID" }, { name: "Workflow name", description: "Description", from_workflow: "workflowID" }],
  ])('create', (data, body) => {
    const newWorkflowId = "newWorkflowId";
    workflowApi.create(data).subscribe(response => {
      expect(response).toEqual(newWorkflowId);
    });
    const request = httpTestingController.expectOne('/studio/workflows');
    expect(request.request.method).toEqual('POST');
    expect(request.request.body).toEqual(body);
    request.flush({ workflowId: newWorkflowId });
  });

  test('load one', () => {
    const workflowId = "workflowID";
    const workflowDetailsDto: WorkflowDetailsDto = {
      id: "workflow2Dto",
      name:"workflow2Dto-name",
      description:"workflow2Dto-description",
      status: "valid",
      modules: [],
      modules_links: []
    };

    workflowApi.loadOne(workflowId).subscribe(response => {
      expect(response).toEqual(workflowDetailsDto);
    });
    const request = httpTestingController.expectOne(`/studio/workflows/${workflowId}`);
    expect(request.request.method).toEqual('GET')
    request.flush(workflowDetailsDto);
    httpTestingController.verify();
  });

  test.each([
    [{ name: "Workflow name" }, {name: "Workflow name" }],
    [{ name: "Workflow name", description: null }, { name: "Workflow name" }],
    [{ name: "Workflow name", description: "" }, { name: "Workflow name" }],
    [{ description: "Workflow description" }, {description: "Workflow description" }],
    [{ description: "Workflow description", name: null }, { description: "Workflow description" }],
    [{ description: "Workflow description", name: "" }, { description: "Workflow description" }],
  ])('update', (data: UpdateWorkflowData, body: any) => {
    const workflowId = "workflowID";
    workflowApi.update(workflowId, data).subscribe(response => {
      expect(response).toEqual(null);
    });
    const request = httpTestingController.expectOne(`/studio/workflows/${workflowId}`);
    expect(request.request.method).toEqual('PUT');
    expect(request.request.body).toEqual(body);
    request.flush(null);
  });

  test('delete', () => {
    const workflowId = "workflowID";
    workflowApi.delete(workflowId).subscribe(response => {
      expect(response).toEqual(null);
    });
    const request = httpTestingController.expectOne(`/studio/workflows/${workflowId}`);
    expect(request.request.method).toEqual('DELETE');
    request.flush(null);
  });


  test.each([
    [{ moduleId: "moduleID" }, { module_id: "moduleID" }],
    [{ moduleId: "moduleID", positionX: 0 }, { module_id: "moduleID", position_x: 0 }],
    [{ moduleId: "moduleID", positionX: 1 }, { module_id: "moduleID", position_x: 1 }],
    [{ moduleId: "moduleID", positionY: 0 }, { module_id: "moduleID", position_y: 0 }],
    [{ moduleId: "moduleID", positionY: 1 }, { module_id: "moduleID", position_y: 1 }],
  ])('add module', (data: AddWorkflowModuleData, body: any) => {
    const workflowId = "workflowID";
    workflowApi.addModule(workflowId, data).subscribe(response => {
      expect(response).toEqual(null);
    });
    const request = httpTestingController.expectOne(`/studio/workflows/${workflowId}/modules`);
    expect(request.request.method).toEqual('POST');
    expect(request.request.body).toEqual(body);
    request.flush(null);
  });

  test('deleteModule', () => {
    const workflowId = "workflowId", moduleId = "moduleId";
    workflowApi.deleteModule(workflowId, moduleId).subscribe(response => {
      expect(response).toEqual(null);
    });
    const request = httpTestingController.expectOne(`/studio/workflows/${workflowId}/modules/${moduleId}`);
    expect(request.request.method).toEqual('DELETE');
    request.flush(null);
  });

  test.each([
    [{ customName: "moduleID" }, { custom_name: "moduleID" }],
    [{ positionX: 0 }, { position_x: 0 }],
    [{ positionX: 1 }, { position_x: 1 }],
    [{ positionY: 0 }, { position_y: 0 }],
    [{ positionY: 1 }, { position_y: 1 }],
  ])('update module', (data: UpdateWorkflowModuleData, body: any) => {
    const workflowId = "workflowID", moduleId = "moduleId";
    workflowApi.updateModule(workflowId, moduleId, data).subscribe(response => {
      expect(response).toEqual(null);
    });
    const request = httpTestingController.expectOne(`/studio/workflows/${workflowId}/modules/${moduleId}`);
    expect(request.request.method).toEqual('PUT');
    expect(request.request.body).toEqual(body);
    request.flush(null);
  });

  test('add module link', () => {
    const workflowId = "workflowId", data: AddWorkflowModuleLinkData = { inputModuleId: "inputModuleID", outputModuleId: "outputModuleId" };
    workflowApi.addModuleLink(workflowId, data).subscribe(response => {
      expect(response).toEqual(null);
    });
    const request = httpTestingController.expectOne(`/studio/workflows/${workflowId}/modules-links`);
    expect(request.request.method).toEqual('POST');
    expect(request.request.body).toEqual({
      input_module_id: data.inputModuleId,
      output_module_id: data.outputModuleId
    });
    request.flush(null);
  });

  test('delete module link', () => {
    const workflowId = "workflowId";
    const moduleLinkId = "moduleLinkId";
    workflowApi.deleteModuleLink(workflowId, moduleLinkId).subscribe(response => {
      expect(response).toEqual(null);
    });
    const request = httpTestingController.expectOne(`/studio/workflows/${workflowId}/modules-links/${moduleLinkId}`);
    expect(request.request.method).toEqual('DELETE');
    request.flush(null);
  });

  test.each([
    [{ referenceValue: "refValue" }, { reference_value: "refValue" }],
    [{ staticValue: "staticValue" }, { static_value: "staticValue" }],
  ])('update module input', (data: UpdateWorkflowModuleInputData, body: any) => {
    const workflowId = "workflowID", moduleId = "moduleId", inputId = "inputId";
    workflowApi.updateModuleInput(workflowId, moduleId, inputId, data).subscribe(response => {
      expect(response).toEqual(null);
    });
    const request = httpTestingController.expectOne(`/studio/workflows/${workflowId}/modules/${moduleId}/inputs/${inputId}/value`);
    expect(request.request.method).toEqual('PUT');
    expect(request.request.body).toEqual(body);
    request.flush(null);
  });
});
