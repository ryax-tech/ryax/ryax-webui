// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  WorkflowDetailsFacade,
  WorkflowEditFacade,
  WorkflowListFacade,
  WorkflowPortalFacade,
} from './application/facades';
import { StoreModule } from '@ngrx/store';
import { WorkflowFeatureKey, WorkflowReducerProvider, WorkflowReducerToken } from './application/reducers';
import { EffectsModule } from '@ngrx/effects';
import { WorkflowApi, WorkflowPortalApi } from './infrastructure/services';
import {
  WorkflowAdapter,
  WorkflowModuleAdapter,
  WorkflowModuleLinkAdapter,
  WorkflowModuleInputAdapter,
  WorkflowModuleOutputAdapter,
  WorkflowErrorAdapter,
  WorkflowPortalAdapter,
  ModuleRunAdapter, WorkflowModuleVersionAdapter,
} from './infrastructure/adapters';
import {
  WorkflowListEffects,
  WorkflowDetailsEffects,
  WorkflowEditEffects,
  WorkflowPortalEffects,
} from './application/effects';


@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature(WorkflowFeatureKey, WorkflowReducerToken),
    EffectsModule.forFeature([
      WorkflowListEffects,
      WorkflowDetailsEffects,
      WorkflowEditEffects,
      WorkflowPortalEffects
    ]),
  ],
  providers: [
    WorkflowReducerProvider,
    WorkflowListFacade,
    WorkflowDetailsFacade,
    WorkflowEditFacade,
    WorkflowPortalFacade,
    WorkflowAdapter,
    WorkflowModuleAdapter,
    WorkflowModuleLinkAdapter,
    WorkflowModuleInputAdapter,
    WorkflowModuleOutputAdapter,
    WorkflowErrorAdapter,
    WorkflowPortalAdapter,
    ModuleRunAdapter,
    WorkflowModuleVersionAdapter,
    WorkflowApi,
    WorkflowPortalApi
  ],
})
export class WorkflowDomainModule {
}
