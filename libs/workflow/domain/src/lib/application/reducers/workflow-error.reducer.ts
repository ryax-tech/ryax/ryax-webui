// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { WorkflowDetailsTool, WorkflowError } from '../../entities';
import { createReducer, on } from '@ngrx/store';
import { WorkflowDetailsActions } from '../actions';

export interface State extends EntityState<WorkflowError> {
  loading: boolean;
}

export const adapter = createEntityAdapter<WorkflowError>({
  selectId: workflowError => workflowError.id
});

export const initialState: State = adapter.getInitialState({
  loading: false
});

export const reducer = createReducer<State>(
  initialState,
  on(WorkflowDetailsActions.activeTool, (state, { payload }) => ({
    ...state,
    loading: payload === WorkflowDetailsTool.Warnings
  })),
  on(WorkflowDetailsActions.loadWorkflowErrorsSuccess, (state, { payload }) => adapter.setAll(payload, {
    ...state,
    loading: false
  })),
  on(WorkflowDetailsActions.loadWorkflowErrorsError, (state) => adapter.removeAll({
    ...state,
    loading: false
  })),
);

const {
  selectAll,
} = adapter.getSelectors();

export const selectLoading = (state: State) => state.loading;
export const selectWorkflowErrorsByWorkflowId = (state: State, workflowId: string) => selectAll(state).filter(item => item.workflowId === workflowId)
