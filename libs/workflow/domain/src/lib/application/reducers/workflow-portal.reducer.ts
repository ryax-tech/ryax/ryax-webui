// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import { WorkflowPortal } from '../../entities';
import { Params } from '@angular/router';
import { WorkflowDetailsActions, WorkflowListActions, WorkflowPortalActions } from '../actions';

export interface State extends EntityState<WorkflowPortal>{
  loading: boolean;
  unsupportedExtensionModalVisibility: boolean;
  sendResult: string;
}

export const adapter = createEntityAdapter<WorkflowPortal>({
  selectId: workflowPortal => workflowPortal.id
});

export const initialState: State = adapter.getInitialState({
  loading: false,
  unsupportedExtensionModalVisibility: false,
  sendResult: null
});

export const reducer = createReducer<State>(
  initialState,
  on(WorkflowListActions.loadPortals, WorkflowDetailsActions.loadPortals, (state) => ({
    ...state,
    loading: true
  })),
  on(WorkflowListActions.loadPortalsSuccess, WorkflowDetailsActions.loadPortalsSuccess, (state, { payload }) => adapter.addMany(payload.workflowPortals, {
    ...state,
    loading: false,
  })),
  on(WorkflowListActions.loadPortalsError, WorkflowDetailsActions.loadPortalsError, (state, { payload }) => adapter.removeMany(item => item.workflowId === payload.workflowId, {
    ...state,
    loading: false,
  })),
  on(WorkflowPortalActions.init, (state) => ({
    ...state,
    loading: true,
    sendResult: null
  })),
  on(WorkflowPortalActions.loadPortalSuccess, (state, { payload }) => adapter.upsertOne(payload, {
    ...state,
    loading: false
  })),
  on(WorkflowPortalActions.loadPortalError, (state, { payload }) => adapter.removeOne(payload, {
    ...state,
    loading: false
  })),
  on(WorkflowPortalActions.sendSuccess, (state, { payload }) => ({
    ...state,
    sendResult: payload.executionId
  })),
  on(WorkflowPortalActions.sendError, (state) => ({
    ...state,
    sendResult: null
  })),
  on(WorkflowPortalActions.openUnsupportedExtensionModal, (state) => ({
    ...state,
    unsupportedExtensionModalVisibility: true
  })),
  on(WorkflowPortalActions.closeUnsupportedExtensionModal, (state) => ({
    ...state,
    unsupportedExtensionModalVisibility: false
  })),
);

export const {
  selectAll
} = adapter.getSelectors();

// Define state selectors
export const selectLoading = (state: State) => state.loading;
export const selectSendResult = (state: State) => state.sendResult;
export const selectUnsupportedExtensionModalVisibility = (state: State) => state.unsupportedExtensionModalVisibility;
export const selectWorkflowId = (navParams: Params) => navParams && navParams['workflowId'];
export const selectWorkflowPortalId = (navParams: Params) => navParams && navParams['workflowPortalId'];
export const selectWorkflowPortalById = (state: State, workflowPortalId: string) => state.entities[workflowPortalId];
export const selectWorkflowPortalsByWorkflowId = (state: State, workflowId: string) => selectAll(state).filter(item => item.workflowId === workflowId)
