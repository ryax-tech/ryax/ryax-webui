// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import { Workflow } from '../../entities';
import { WorkflowListActions } from '../actions';

export interface State extends EntityState<Workflow>{
  selectedWorkflowId: string,
  importedWorkflowId: string;
  deleteModalVisible: boolean;
  importModalVisible: boolean;
  importSuccessModalVisible: boolean;
  importErrorModalVisible: boolean;
  importErrorMessage: string;
}

export const adapter = createEntityAdapter<Workflow>({
  selectId: workflow => workflow.id
});

export const initialState: State = adapter.getInitialState({
  selectedWorkflowId: null,
  importedWorkflowId: null,
  deleteModalVisible: false,
  importModalVisible: false,
  importSuccessModalVisible: false,
  importErrorModalVisible: false,
  importErrorMessage: null,
});

export const reducer = createReducer<State>(
  initialState,
  on(WorkflowListActions.openDeleteWorkflowModal, (state, { payload }) => ({
    ...state,
    selectedWorkflowId: payload,
    deleteModalVisible: true
  })),
  on(WorkflowListActions.validateDeleteWorkflow, (state) => ({
    ...state,
    deleteModalVisible: false
  })),
  on(WorkflowListActions.cancelDeleteWorkflow, (state) => ({
    ...state,
    deleteModalVisible: false
  })),
  on(WorkflowListActions.openImportWorkflowModal, (state) => ({
    ...state,
    importModalVisible: true,
    importErrorModalVisible: false,
  })),
  on(WorkflowListActions.closeImportWorkflowModal, (state) => ({
    ...state,
    importModalVisible: false
  })),
  on(WorkflowListActions.importSuccess, (state, {payload}) => ({
    ...state,
    importModalVisible: false,
    importSuccessModalVisible: true,
    importErrorModalVisible: false,
    importedWorkflowId: payload,
  })),
  on(WorkflowListActions.closeImportSuccessWorkflowModal, (state) => ({
    ...state,
    importSuccessModalVisible: false,
  })),
  on(WorkflowListActions.importError, (state, { payload }) => ({
    ...state,
    importModalVisible: false,
    importErrorModalVisible: true,
    importErrorMessage: payload.error.name
  })),
  on(WorkflowListActions.closeImportErrorWorkflowModal, (state) => ({
    ...state,
    importErrorModalVisible: false,
  })),
);

// Define state selectors
export const selectSelectedWorkflowId = (state: State) => state.selectedWorkflowId;
export const selectDeleteModalVisible = (state: State) => state.deleteModalVisible;
export const selectImportModalVisible = (state: State) => state.importModalVisible;
export const selectImportSuccessModalVisible = (state: State) => state.importSuccessModalVisible;
export const selectImportErrorModalVisible = (state: State) => state.importErrorModalVisible;
export const selectImportedWorkflowId = (state: State) => state.importedWorkflowId;
export const selectImportErrorMessage = (state: State) => state.importErrorMessage;
