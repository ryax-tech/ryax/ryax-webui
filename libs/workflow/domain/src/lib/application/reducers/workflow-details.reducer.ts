// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createReducer, on } from '@ngrx/store';
import { WorkflowDetailsTool } from '../../entities';
import { Params } from '@angular/router';
import { WorkflowDetailsActions } from '../actions';

export interface State {
  activeTool: WorkflowDetailsTool;
  activeModuleId: string;
  deleteModalVisible: boolean;
  exportModalVisible: boolean;
}

export const initialState: State = {
  activeTool: null,
  activeModuleId: null,
  deleteModalVisible: false,
  exportModalVisible: false,
};

export const reducer = createReducer<State>(
  initialState,
  on(WorkflowDetailsActions.init, () => initialState),
  on(WorkflowDetailsActions.initSuccess, (state, { payload }) => ({
    ...state,
    activeTool: payload.workflowModules.length === 0 ? WorkflowDetailsTool.Store : null
  })),
  on(WorkflowDetailsActions.activeTool, (state, { payload }) => ({
    ...state,
    activeTool: state.activeTool === payload ? null : payload,
  })),
  on(WorkflowDetailsActions.selectModule, (state, { payload }) => ({
    ...state,
    activeTool: WorkflowDetailsTool.Setup,
    activeModuleId: payload,
  })),
  on(WorkflowDetailsActions.unselectModule, (state ) => ({
    ...state,
    activeTool: state.activeTool === WorkflowDetailsTool.Setup ? null : state.activeTool,
    activeModuleId: null,
  })),
  on(WorkflowDetailsActions.deleteModuleSuccess, (state, { payload }) => ({
    ...state,
    activeTool: payload === state.activeModuleId  && state.activeTool === WorkflowDetailsTool.Setup ? null : state.activeTool,
    activeModuleId: payload === state.activeModuleId ? null : state.activeModuleId,
  })),
  on(WorkflowDetailsActions.openDeleteWorkflowModal, (state) => ({
    ...state,
    deleteModalVisible: true
  })),
  on(WorkflowDetailsActions.validateDeleteWorkflow, (state) => ({
    ...state,
    deleteModalVisible: false
  })),
  on(WorkflowDetailsActions.cancelDeleteWorkflow, (state) => ({
    ...state,
    deleteModalVisible: false
  })),
  on(WorkflowDetailsActions.exportWorkflow, (state) => ({
    ...state,
    exportModalVisible: true
  })),
  on(WorkflowDetailsActions.exportSuccess, WorkflowDetailsActions.exportError,(state) => ({
    ...state,
    exportModalVisible: false
  })),
  on(WorkflowDetailsActions.updateModuleVersion, (state) => ({
    ...state,
    activeTool: null,
  })),
  on(WorkflowDetailsActions.updateModuleVersionSuccess, (state, { payload }) => ({
    ...state,
    activeTool: WorkflowDetailsTool.Setup,
    activeModuleId: payload.workflowModuleId
  })),
  on(WorkflowDetailsActions.deployWorkflow, (state) => ({
    ...state,
    activeTool: null,
  })),


  /*
  on(WorkflowDetailsActions.deleteModule, (state, { payload }) => ({
    ...state,
    tool: payload === state.selectedModule && state.tool === WorkflowDetailsTool.Setup ? null : state.tool,
    selectedModule: payload === state.selectedModule ? null : state.selectedModule
  })),
  on(WorkflowDetailsActions.deployWorkflow, (state) => ({
    ...state,
    tool: [WorkflowDetailsTool.Store, WorkflowDetailsTool.Warnings].includes(state.tool) ? null : state.tool,
  })),
   */
);


export const selectWorkflowId = (navParams: Params) => navParams && navParams['workflowId'];
export const selectActiveTool = (state: State) => state.activeTool;
export const selectActiveModuleId = (state: State) => state.activeModuleId;
export const selectDeleteModalVisible = (state: State) => state.deleteModalVisible;
export const selectExportModalVisible = (state: State) => state.exportModalVisible;
