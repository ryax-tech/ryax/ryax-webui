// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { InjectionToken } from '@angular/core';
import { ActionReducerMap, createFeatureSelector, createSelector } from '@ngrx/store';
import { NavigationQueries } from '@ryax/domain/navigation';
import * as fromWorkflow from './workflow.reducer';
import * as fromWorkflowModule from './workflow-module.reducer';
import * as fromWorkflowModuleLink from './workflow-module-link.reducer';
import * as fromWorkflowModuleInput from './workflow-module-input.reducer';
import * as fromWorkflowModuleOutput from './workflow-module-output.reducer';
import * as fromWorkflowError from './workflow-error.reducer';
import * as fromWorkflowEdit from './workflow-edit.reducer';
import * as fromWorkflowDetails from './workflow-details.reducer';
import * as fromWorkflowList from './workflow-list.reducer';
import * as fromWorkflowPortal from './workflow-portal.reducer';
import * as fromWorkflowModuleVersion from './workflow-module-version.reducer';

export const WorkflowFeatureKey = 'workflowDomain';

export interface WorkflowState {
  workflows: fromWorkflow.State
  workflowModules: fromWorkflowModule.State
  workflowModuleLinks: fromWorkflowModuleLink.State
  workflowModuleInputs: fromWorkflowModuleInput.State
  workflowModuleOutputs: fromWorkflowModuleOutput.State
  workflowErrors: fromWorkflowError.State
  workflowEdit: fromWorkflowEdit.State
  workflowDetails: fromWorkflowDetails.State
  workflowList: fromWorkflowList.State
  workflowPortal: fromWorkflowPortal.State
  workflowModuleVersion: fromWorkflowModuleVersion.State
}

export const WorkflowReducerToken = new InjectionToken<ActionReducerMap<WorkflowState>>(WorkflowFeatureKey);

export const WorkflowReducerProvider = {
  provide: WorkflowReducerToken,
  useValue: {
    workflows: fromWorkflow.reducer,
    workflowModules: fromWorkflowModule.reducer,
    workflowModuleLinks: fromWorkflowModuleLink.reducer,
    workflowModuleInputs: fromWorkflowModuleInput.reducer,
    workflowModuleOutputs: fromWorkflowModuleOutput.reducer,
    workflowErrors: fromWorkflowError.reducer,
    workflowEdit: fromWorkflowEdit.reducer,
    workflowDetails: fromWorkflowDetails.reducer,
    workflowList: fromWorkflowList.reducer,
    workflowPortal: fromWorkflowPortal.reducer,
    workflowModuleVersion: fromWorkflowModuleVersion.reducer,
  },
};

export const selectFeatureState = createFeatureSelector<WorkflowState>(WorkflowFeatureKey);
export const selectWorkflowsState = createSelector(selectFeatureState, state => state.workflows);
export const selectWorkflowModulesState = createSelector(selectFeatureState, state => state.workflowModules);
export const selectWorkflowModuleLinksState = createSelector(selectFeatureState, state => state.workflowModuleLinks);
export const selectWorkflowModuleInputsState = createSelector(selectFeatureState, state => state.workflowModuleInputs);
export const selectWorkflowModuleOutputsState = createSelector(selectFeatureState, state => state.workflowModuleOutputs);
export const selectWorkflowErrorsState = createSelector(selectFeatureState, state => state.workflowErrors);
export const selectWorkflowEditState = createSelector(selectFeatureState, state => state.workflowEdit);
export const selectWorkflowDetailsState = createSelector(selectFeatureState, state => state.workflowDetails);
export const selectWorkflowListState = createSelector(selectFeatureState, state => state.workflowList);
export const selectWorkflowPortalState = createSelector(selectFeatureState, state => state.workflowPortal);
export const selectWorkflowModuleVersionsState = createSelector(selectFeatureState, state => state.workflowModuleVersion);

export const selectLoading = createSelector(selectWorkflowsState, fromWorkflow.selectLoading);
export const selectExportLoading = createSelector(selectWorkflowsState, fromWorkflow.selectExportLoading);
export const selectFilter = createSelector(selectWorkflowsState, fromWorkflow.selectFilter);
export const selectWorkflows = createSelector(selectWorkflowsState, fromWorkflow.selectWorkflows);
export const selectLoadingInputs = createSelector(selectWorkflowModuleInputsState, fromWorkflowModuleInput.selectLoading);
export const selectLoadingOutputs = createSelector(selectWorkflowModuleOutputsState, fromWorkflowModuleOutput.selectLoading);
export const selectLoadingWorkflowErrors = createSelector(selectWorkflowErrorsState, fromWorkflowError.selectLoading);


// Feature list selectors
export const selectListLoading = createSelector(selectWorkflowsState, fromWorkflow.selectLoading);
export const selectImportLoading = createSelector(selectWorkflowsState, fromWorkflow.selectImportLoading);
export const selectRetryImportLoading = createSelector(selectWorkflowsState, fromWorkflow.selectRetryImportLoading);

export const selectListWorkflows = createSelector(selectWorkflowsState, fromWorkflow.selectWorkflows);
export const selectListFilter = createSelector(selectWorkflowsState, fromWorkflow.selectFilter);
export const selectListWorkflowPortalsLoading = createSelector(selectWorkflowPortalState, fromWorkflowPortal.selectLoading);
export const selectListWorkflowPortals = createSelector(selectWorkflowPortalState, fromWorkflowPortal.selectWorkflowPortalsByWorkflowId);


// Feature list modal
export const selectSelectedWorkflowId = createSelector(selectWorkflowListState, fromWorkflowList.selectSelectedWorkflowId);
export const selectSelectedWorkflow = createSelector(selectWorkflowsState, selectSelectedWorkflowId, fromWorkflow.selectWorkflowById);

export const selectImportedWorkflowId = createSelector(selectWorkflowListState, fromWorkflowList.selectImportedWorkflowId);

export const selectDeleteModalVisible = createSelector(selectWorkflowListState, fromWorkflowList.selectDeleteModalVisible);
export const selectImportModalVisible = createSelector(selectWorkflowListState, fromWorkflowList.selectImportModalVisible);
export const selectImportSuccessModalVisible = createSelector(selectWorkflowListState, fromWorkflowList.selectImportSuccessModalVisible);
export const selectImportErrorModalVisible = createSelector(selectWorkflowListState, fromWorkflowList.selectImportErrorModalVisible);

export const selectImportErrorMessage = createSelector(selectWorkflowListState, fromWorkflowList.selectImportErrorMessage);

// Feature edit related selectors
export const selectEditMode = createSelector(selectWorkflowEditState, fromWorkflowEdit.selectMode);
export const selectEditWorkflowId = createSelector(selectWorkflowEditState, NavigationQueries.selectParams, fromWorkflowEdit.selectWorkflowId);
export const selectEditWorkflow = createSelector(selectWorkflowsState, selectEditWorkflowId, fromWorkflow.selectWorkflowById);

// Feature portal
export const selectPortalLoading = createSelector(selectWorkflowPortalState, fromWorkflowPortal.selectLoading);
export const selectPortalWorkflowId = createSelector(NavigationQueries.selectParams, fromWorkflowPortal.selectWorkflowId);
export const selectPortalWorkflowPortalId = createSelector(NavigationQueries.selectParams, fromWorkflowPortal.selectWorkflowPortalId);
export const selectPortalWorkflowPortal = createSelector(selectWorkflowPortalState, selectPortalWorkflowPortalId, fromWorkflowPortal.selectWorkflowPortalById);
export const selectPortalWorkflowPortalOutputs = createSelector(selectWorkflowModuleOutputsState, selectPortalWorkflowId, selectPortalWorkflowPortalId, fromWorkflowModuleOutput.selectAllWithWorkflowIdAndWorkflowModuleId);
export const selectPortalWorkflowPortalResult = createSelector(selectWorkflowPortalState, fromWorkflowPortal.selectSendResult);
export const selectUnsupportedExtensionModalVisibility = createSelector(selectWorkflowPortalState, fromWorkflowPortal.selectUnsupportedExtensionModalVisibility);

// Feature details related selectors
export const selectDetailsWorkflowId = createSelector(NavigationQueries.selectParams, fromWorkflowDetails.selectWorkflowId);
export const selectDetailsWorkflow = createSelector(selectWorkflowsState, selectDetailsWorkflowId, fromWorkflow.selectWorkflowById);
export const selectDetailsWorkflowModules = createSelector(selectWorkflowModulesState, selectDetailsWorkflowId, fromWorkflowModule.selectWorkflowModulesByWorkflowId);
export const selectDetailsWorkflowModuleLinks = createSelector(selectWorkflowModuleLinksState, selectDetailsWorkflowId, fromWorkflowModuleLink.selectWorkflowModuleLinksByWorkflowId);
export const selectDetailsWorkflowModulesOutputs = createSelector(selectWorkflowModuleOutputsState, selectDetailsWorkflowId, fromWorkflowModuleOutput.selectAllWithWorkflowId);
export const selectDetailsWorkflowErrors = createSelector(selectWorkflowErrorsState, selectDetailsWorkflowId, fromWorkflowError.selectWorkflowErrorsByWorkflowId);
export const selectDetailsActiveTool = createSelector(selectWorkflowDetailsState, fromWorkflowDetails.selectActiveTool);

export const selectDetailsActiveWorkflowModuleId = createSelector(selectWorkflowDetailsState, fromWorkflowDetails.selectActiveModuleId);
export const selectDetailsActiveWorkflowModule = createSelector(selectWorkflowModulesState, selectDetailsActiveWorkflowModuleId, fromWorkflowModule.selectWorkflowModuleById);

export const selectDetailsActiveWorkflowModuleInputs = createSelector(selectWorkflowModuleInputsState, selectDetailsWorkflowId, selectDetailsActiveWorkflowModuleId, fromWorkflowModuleInput.selectAllByWorkflowAndWorkflowModuleId);
export const selectDetailsActiveWorkflowModuleOutputs = createSelector(selectWorkflowModuleOutputsState, selectDetailsWorkflowId, selectDetailsActiveWorkflowModuleId, fromWorkflowModuleOutput.selectAllWithWorkflowIdAndWorkflowModuleId);
export const selectDetailsActiveWorkflowModuleVersions = createSelector(selectWorkflowModuleVersionsState, fromWorkflowModuleVersion.selectAllWorkflowModuleVersions);

export const selectDetailsDeleteModalVisible = createSelector(selectWorkflowDetailsState, fromWorkflowDetails.selectDeleteModalVisible);
export const selectDetailsSearchWorkflowModuleOutputsIds = createSelector(selectWorkflowModuleOutputsState, fromWorkflowModuleOutput.selectSearchIds);
export const selectExportModalVisible = createSelector(selectWorkflowDetailsState, fromWorkflowDetails.selectExportModalVisible);

// Workflow module input selectors
export const selectWorkflowModuleInputById = createSelector(selectWorkflowModuleInputsState, fromWorkflowModuleInput.selectById);
