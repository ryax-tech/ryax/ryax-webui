// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Params } from '@angular/router';
import { WorkflowEditMode } from '../../entities';
import { createReducer, on } from '@ngrx/store';
import { WorkflowDetailsActions, WorkflowEditActions, WorkflowListActions } from '../actions';

export interface State {
  workflowId: string;
  mode: WorkflowEditMode;
}

export const initialState: State = {
  workflowId: null,
  mode: null
};

export const reducer = createReducer<State>(
  initialState,
  on(WorkflowListActions.addWorkflow, (state) => ({
    ...state,
    workflowId: null,
    mode: WorkflowEditMode.Create
  })),
  on(WorkflowListActions.copyWorkflow, (state, { payload }) => ({
    ...state,
    workflowId: payload,
    mode: WorkflowEditMode.Copy
  })),
  on(WorkflowListActions.editWorkflow, (state, { payload }) => ({
    ...state,
    workflowId: payload,
    mode: WorkflowEditMode.Edit
  })),
  on(WorkflowDetailsActions.copyWorkflow, (state ) => ({
    ...state,
    mode: WorkflowEditMode.Copy
  })),
  on(WorkflowDetailsActions.editWorkflow, (state ) => ({
    ...state,
    mode: WorkflowEditMode.Edit
  })),
  on(WorkflowEditActions.createSuccess, (state) => ({
    ...state,
    workflowId: null,
    mode: null
  })),
  on(WorkflowEditActions.copySuccess, (state) => ({
    ...state,
    workflowId: null,
    mode: null
  })),
  on(WorkflowEditActions.updateSuccess, (state) => ({
    ...state,
    workflowId: null,
    mode: null
  })),
  on(WorkflowEditActions.close, (state) => ({
    ...state,
    workflowId: null,
    mode: null
  })),
);


export const selectMode = (state: State) => state.mode;
export const selectWorkflowId = (state: State, navParams: Params): string =>
  navParams && navParams['workflowId'] ? navParams && navParams['workflowId'] : state.workflowId;
