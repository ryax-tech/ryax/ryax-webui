// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { WorkflowModuleOutput } from '@ryax/workflow/domain';
import { createReducer, on } from '@ngrx/store';
import { WorkflowDetailsActions, WorkflowPortalActions } from '../actions';

export interface State extends EntityState<WorkflowModuleOutput> {
  loading: boolean;
  searchIds: string[];
}

export const adapter = createEntityAdapter<WorkflowModuleOutput>({
  selectId: workflowModuleOutput => workflowModuleOutput.id
});

export const initialState: State = adapter.getInitialState({
  loading: null,
  searchIds: []
});

export const reducer = createReducer<State>(
  initialState,
  on(WorkflowDetailsActions.selectModule, (state) => ({
    ...state,
    loading: true
  })),
  on(WorkflowDetailsActions.loadModuleOutputsSuccess, (state, { payload }) => adapter.upsertMany(payload, {
    ...state,
    loading: false
  })),
  on(WorkflowDetailsActions.loadModuleOutputsError, (state, { payload }) => adapter.removeMany(item => item.workflowModuleId === payload, {
    ...state,
    loading: false
  })),
  on(WorkflowDetailsActions.loadModuleInputsSuccess, (state, { payload }) => adapter.upsertMany(payload.workflowModuleOutputs, state)),
  on(WorkflowDetailsActions.loadModuleInputsError, (state, { payload }) => adapter.removeMany(item => item.workflowModuleId === payload, {
    ...state,
    loading: false
  })),
  on(WorkflowDetailsActions.searchModulesOutputsSuccess, (state, { payload }) => adapter.upsertMany(payload, {
    ...state,
    loading: false,
    searchIds: payload.map(item => item.id)
  })),
  on(WorkflowDetailsActions.searchModulesOutputsError, (state ) => ({
    ...state,
    loading: false,
    searchIds: []
  })),
  on(WorkflowDetailsActions.deleteWorkflowModuleOutputSuccess, (state, { payload }) => adapter.removeOne(payload, state)),
  on(WorkflowPortalActions.loadPortalOutputsSuccess, (state, { payload }) => adapter.upsertMany(payload.workflowPortalOutputs, {
    ...state,
    loading: false
  })),
  on(WorkflowPortalActions.loadPortalOutputsError, (state, { payload }) => adapter.removeMany(item => item.workflowModuleId === payload.workflowPortalId, {
    ...state,
    loading: false
  })),
);

const {
  selectAll
} = adapter.getSelectors();

export const selectLoading = (state: State) => state.loading;

export const selectSearchIds = (state: State) => state.searchIds;

export const selectAllWithWorkflowId = (state: State, workflowId: string) =>
  selectAll(state).filter(item => item.workflowId === workflowId);

export const selectAllWithWorkflowIdAndWorkflowModuleId = (state: State, workflowId: string, workflowModuleId: string) =>
  selectAll(state).filter(item => item.workflowId === workflowId && item.workflowModuleId === workflowModuleId);
