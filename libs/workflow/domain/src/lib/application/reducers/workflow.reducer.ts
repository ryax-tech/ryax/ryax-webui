// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import { Workflow, WorkflowFilter } from '../../entities';
import { WorkflowDetailsActions, WorkflowListActions } from '../actions';

export interface State extends EntityState<Workflow>{
  loading: boolean;
  importLoading: boolean;
  retryImportLoading: boolean;
  exportLoading: boolean;
  filter: WorkflowFilter;
}

export const adapter = createEntityAdapter<Workflow>({
  selectId: workflow => workflow.id
});

export const initialState: State = adapter.getInitialState({
  loading: null,
  importLoading: null,
  retryImportLoading: null,
  exportLoading: null,
  filter: {
    term: null,
    status: null
  },
});

export const reducer = createReducer<State>(
  initialState,
  on(WorkflowListActions.init, (state) => ({
    ...state,
    loading: true
  })),
  on(WorkflowListActions.initSuccess, (state, { payload }) => adapter.setAll(payload, {
    ...state,
    loading: false
  })),
  on(WorkflowListActions.initError, (state) => adapter.removeAll(state)),
  on(WorkflowListActions.refresh, (state) => ({
    ...state,
    loading: true
  })),
  on(WorkflowListActions.refreshSuccess, (state, { payload }) => adapter.setAll(payload, {
    ...state,
    loading: false
  })),
  on(WorkflowListActions.refreshError, (state) => adapter.removeAll(state)),
  on(WorkflowListActions.filter, (state, { payload }) => ({
    ...state,
    loading: true,
    filter: payload
  })),
  on(WorkflowListActions.deleteWorkflowSuccess, (state, { payload}) => adapter.removeOne(payload, state)),
  on(WorkflowListActions.deployWorkflow, (state) => ({
    ...state,
    loading: true
  })),
  on(WorkflowDetailsActions.init, (state) => ({
    ...state,
    loading: true
  })),
  on(WorkflowDetailsActions.initSuccess, (state, { payload }) => adapter.upsertOne(payload.workflow, {
    ...state,
    loading: false
  })),
  on(WorkflowDetailsActions.initError, (state, { payload }) => adapter.removeOne(payload, {
    ...state,
    loading: false
  })),
  on(WorkflowDetailsActions.refresh, (state) => ({
    ...state,
    loading: true
  })),
  on(WorkflowDetailsActions.refreshSuccess, (state, { payload }) => adapter.upsertOne(payload.workflow, {
    ...state,
    loading: false
  })),
  on(WorkflowDetailsActions.refreshError, (state,) => ({
    ...state,
    loading: false
  })),
  on(WorkflowDetailsActions.deleteModuleSuccess, (state, { payload }) => adapter.removeOne(payload, state)),
  // on(WorkflowListActions.importSuccess, (state, { payload }) => adapter.addOne(payload, state)),
  on(WorkflowListActions.importWorkflow, (state) => ({
    ...state,
    importLoading: true,
    retryImportLoading: true
  })),
  on(WorkflowListActions.importSuccess, WorkflowListActions.importError, (state) => ({
    ...state,
    importLoading: false,
    retryImportLoading: false
  })),
  on(WorkflowDetailsActions.exportWorkflow, (state) => ({
    ...state,
    exportLoading: true,
  })),
  on(WorkflowDetailsActions.exportSuccess, WorkflowDetailsActions.exportError, (state) => ({
    ...state,
    exportLoading: false,
  })),
);

const {
  selectAll
} = adapter.getSelectors();

// Define state selectors
export const selectLoading = (state: State) => state.loading;
export const selectImportLoading = (state: State) => state.importLoading;
export const selectRetryImportLoading = (state: State) => state.retryImportLoading;
export const selectExportLoading = (state: State) => state.exportLoading;
export const selectFilter = (state: State) => state.filter;
export const selectWorkflowById = (state: State, workflowId: string) => state.entities[workflowId];
export const selectWorkflows = selectAll;
