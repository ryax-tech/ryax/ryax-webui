// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createReducer, on } from '@ngrx/store';
import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { WorkflowModule } from '../../entities';
import { WorkflowDetailsActions } from '../actions';

export type State = EntityState<WorkflowModule>

export const adapter = createEntityAdapter<WorkflowModule>({
  selectId: workflowModule => workflowModule.id
});

export const initialState: State = adapter.getInitialState();

export const reducer = createReducer<State>(
  initialState,
  on(WorkflowDetailsActions.initSuccess, (state, { payload }) => adapter.setAll(payload.workflowModules, state)),
  on(WorkflowDetailsActions.initError, (state, { payload }) => adapter.removeAll(state)),
  on(WorkflowDetailsActions.refreshSuccess, (state, { payload }) => adapter.setAll(payload.workflowModules, state)),
  on(WorkflowDetailsActions.refreshError, (state, { payload }) => adapter.removeAll(state)),
  on(WorkflowDetailsActions.deleteWorkflowSuccess, (state, { payload }) => adapter.removeAll(state))
);

const {
  selectAll,
  selectEntities
} = adapter.getSelectors();


export const selectWorkflowModuleById = (state: State, workflowModuleId: string) =>
  selectEntities(state)[workflowModuleId];

export const selectWorkflowModulesByWorkflowId = (state: State, workflowId: string) =>
  selectAll(state).filter(item => item.workflowId === workflowId);
