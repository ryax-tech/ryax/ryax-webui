// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { WorkflowModuleVersion } from '@ryax/workflow/domain';
import { createReducer, on } from '@ngrx/store';
import { WorkflowDetailsActions } from '../actions';

export interface State extends EntityState<WorkflowModuleVersion> {
  loading: boolean;
  workflowModuleVersions: WorkflowModuleVersion[];
}

export const adapter = createEntityAdapter<WorkflowModuleVersion>({
  selectId: workflowModuleVersion => workflowModuleVersion.moduleId
});

export const initialState: State = adapter.getInitialState({
  loading: null,
  workflowModuleVersions: null,
});

export const reducer = createReducer<State>(
  initialState,
  on(WorkflowDetailsActions.selectModule, (state) => ({
    ...state,
    loading: true
  })),
  on(WorkflowDetailsActions.loadModuleVersionsSuccess, (state, { payload }) => adapter.upsertMany(payload, {
    ...state,
    loading: false,
    workflowModuleVersions: payload
  })),
  on(WorkflowDetailsActions.loadModuleVersionsError, (state) => ({
    ...state,
    loading: false
  })),
);

export const selectLoading = (state: State) => state.loading;
export const selectAllWorkflowModuleVersions = (state: State) => state.workflowModuleVersions;
