// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { WorkflowModuleInput } from '@ryax/workflow/domain';
import { createReducer, on } from '@ngrx/store';
import { WorkflowDetailsActions } from '../actions';

export interface State extends EntityState<WorkflowModuleInput> {
  loading: boolean;
}

export const adapter = createEntityAdapter<WorkflowModuleInput>({
  selectId: workflowModuleInput => workflowModuleInput.id
});

export const initialState: State = adapter.getInitialState({
  loading: null
});

export const reducer = createReducer<State>(
  initialState,
  on(WorkflowDetailsActions.selectModule, (state) => ({
    ...state,
    loading: true
  })),
  on(WorkflowDetailsActions.loadModuleInputsSuccess, (state, { payload }) => adapter.upsertMany(payload.workflowModuleInputs, {
    ...state,
    loading: false
  })),
  on(WorkflowDetailsActions.loadModuleInputsError, (state, { payload }) => adapter.removeMany(item => item.workflowModuleId === payload, {
    ...state,
    loading: false
  })),
  on(WorkflowDetailsActions.updateModuleInput, (state) => ({
    ...state,
    loading: true
  })),
  on(WorkflowDetailsActions.updateModuleInputSuccess, (state) => ({
    ...state,
    loading: false
  })),
  on(WorkflowDetailsActions.updateModuleInputError, (state) => ({
    ...state,
    loading: false
  })),
  on(WorkflowDetailsActions.deleteFileSuccess, (state) => ({
    ...state,
    loading: false
  })),
  on(WorkflowDetailsActions.deleteFileError, (state) => ({
    ...state,
    loading: false
  })),
);

const {
  selectAll
} = adapter.getSelectors();

export const selectLoading = (state: State) => state.loading;
export const selectAllByWorkflowAndWorkflowModuleId = (state: State, workflowId: string, workflowModuleId: string) =>
  selectAll(state).filter(item => item.workflowId === workflowId && item.workflowModuleId === workflowModuleId);
export const selectById = (state: State, workflowModuleInputId: string) => state.entities[workflowModuleInputId];
