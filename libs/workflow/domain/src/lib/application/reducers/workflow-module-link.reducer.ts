// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { WorkflowModuleLink } from '@ryax/workflow/domain';
import { createReducer, on } from '@ngrx/store';
import { WorkflowDetailsActions } from '../actions';

export type State = EntityState<WorkflowModuleLink>

export const adapter = createEntityAdapter<WorkflowModuleLink>({
  selectId: workflowModuleLink => workflowModuleLink.id
});

export const initialState: State = adapter.getInitialState();

export const reducer = createReducer<State>(
  initialState,
  on(WorkflowDetailsActions.initSuccess, (state, { payload }) => adapter.setAll(payload.workflowModuleLinks, state)),
  on(WorkflowDetailsActions.initError, (state, { payload }) => adapter.removeAll(state)),
  on(WorkflowDetailsActions.refreshSuccess, (state, { payload }) => adapter.setAll(payload.workflowModuleLinks, state)),
  on(WorkflowDetailsActions.refreshError, (state, { payload }) => adapter.removeAll(state)),
  on(WorkflowDetailsActions.deleteWorkflowSuccess, (state, { payload }) => adapter.removeAll(state)),
);

const {
  selectAll
} = adapter.getSelectors();

export const selectWorkflowModuleLinksByWorkflowId = (state: State, workflowId: string) =>
  selectAll(state).filter(item => item.workflowId === workflowId);
