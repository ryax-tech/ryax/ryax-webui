// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { selectEditMode, selectEditWorkflowId, WorkflowState } from '../reducers';
import { WorkflowApi } from '../../infrastructure/services';
import { MessageService } from '@ryax/shared/ui-common';
import { WorkflowEditActions } from '../actions';
import { catchError, filter, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';
import { WorkflowEditMode } from '../../entities';
import { of } from 'rxjs';
import { Router } from '@angular/router';

@Injectable()
export class WorkflowEditEffects {
  private mode$ = this.store.select(selectEditMode);
  private workflowId$ = this.store.select(selectEditWorkflowId);

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store<WorkflowState>,
    private readonly workflowApi: WorkflowApi,
    private readonly messageService: MessageService,
    private readonly router: Router,
  ) {}

  createWorkflow$ = createEffect(() => this.actions$.pipe(
    ofType(WorkflowEditActions.save),
    withLatestFrom(this.mode$, ({ payload }, mode) => ({ payload, mode })),
    filter(({ mode }) => mode === WorkflowEditMode.Create),
    switchMap(({ payload }) => this.workflowApi.create(payload).pipe(
      map(newWorkflowId => WorkflowEditActions.createSuccess(newWorkflowId)),
      catchError(() => of(WorkflowEditActions.createError()))
    ))
  ));

  copyWorkflow$ = createEffect(() => this.actions$.pipe(
    ofType(WorkflowEditActions.save),
    withLatestFrom(this.mode$, this.workflowId$, ({ payload }, mode, workflowId) => ({ payload, mode, workflowId })),
    filter(({ mode, workflowId }) => !!workflowId && mode === WorkflowEditMode.Copy),
    switchMap(({ payload, workflowId }) => this.workflowApi.create({ ...payload, fromWorkflow: workflowId }).pipe(
      map(newWorkflowId => WorkflowEditActions.copySuccess(newWorkflowId)),
      catchError(() => of(WorkflowEditActions.copyError()))
    ))
  ));

  updateWorkflow$ = createEffect(() => this.actions$.pipe(
    ofType(WorkflowEditActions.save),
    withLatestFrom(this.mode$, this.workflowId$, ({ payload }, mode, workflowId) => ({ payload, mode, workflowId })),
    filter(({ mode, workflowId }) => !!workflowId && mode === WorkflowEditMode.Edit),
    switchMap(({ workflowId, payload }) => this.workflowApi.update(workflowId, payload).pipe(
      map(() => WorkflowEditActions.updateSuccess(workflowId)),
      catchError(() => of(WorkflowEditActions.updateError()))
    ))
  ));

  notifySuccess$ = createEffect(() => this.actions$.pipe(
    ofType(WorkflowEditActions.createSuccess, WorkflowEditActions.copySuccess, WorkflowEditActions.updateSuccess),
    tap(({ notification }) => this.messageService.displaySuccessMessage(notification))
  ), { dispatch: false });

  notifyError$ = createEffect(() => this.actions$.pipe(
    ofType(WorkflowEditActions.createError, WorkflowEditActions.copyError, WorkflowEditActions.updateError),
    tap(({ notification }) => this.messageService.displayErrorMessage(notification))
  ), { dispatch: false });

  goToWorkflowDetails$ = createEffect(() => this.actions$.pipe(
    ofType(WorkflowEditActions.createSuccess, WorkflowEditActions.copySuccess),
    tap(({ payload }) => this.router.navigate(['/', 'studio', 'workflows', payload]))
  ), { dispatch: false });
}
