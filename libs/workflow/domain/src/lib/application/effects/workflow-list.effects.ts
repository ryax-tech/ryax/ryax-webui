// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { of } from 'rxjs';
import { catchError, concatMap, flatMap, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { MessageService } from '@ryax/shared/ui-common';
import { selectFilter, WorkflowState } from '../reducers';
import { WorkflowApi } from '../../infrastructure/services/workflow-api.service';
import { WorkflowEditActions, WorkflowListActions } from '../actions';
import { WorkflowPortalApi } from '../../infrastructure/services';
import { HttpErrorResponse } from '@angular/common/http';


@Injectable()
export class WorkflowListEffects {
  private filter$ = this.store.select(selectFilter);

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store<WorkflowState>,
    private readonly workflowApi: WorkflowApi,
    private readonly workflowPortalApi: WorkflowPortalApi,
    private readonly messageService: MessageService,
  ) {}

  init$ = createEffect(() => this.actions$.pipe(
    ofType(WorkflowListActions.init),
    withLatestFrom(this.filter$, (_, filter) => filter),
    switchMap((filter) => this.workflowApi.loadAll(filter).pipe(
      map(workflows => WorkflowListActions.initSuccess(workflows)),
      catchError(() => of(WorkflowListActions.initError()))
    ))
  ));

  refresh$ = createEffect(() => this.actions$.pipe(
    ofType(
      WorkflowListActions.refresh,
      WorkflowListActions.filter,
      WorkflowEditActions.updateSuccess,
      WorkflowListActions.stopWorkflowSuccess,
      WorkflowListActions.deployWorkflowSuccess,
      WorkflowListActions.importSuccess
    ),
    withLatestFrom(this.filter$, (_, filter) => filter),
    switchMap((filter) => this.workflowApi.loadAll(filter).pipe(
      map(workflows => WorkflowListActions.refreshSuccess(workflows)),
      catchError(() => of(WorkflowListActions.refreshError()))
    ))
  ));

  stopWorkflow$ = createEffect(() => this.actions$.pipe(
    ofType(WorkflowListActions.stopWorkflow),
    concatMap(({ payload }) => this.workflowApi.stop(payload).pipe(
      map(() => WorkflowListActions.stopWorkflowSuccess()),
      catchError(() => of(WorkflowListActions.stopWorkflowError()))
    ))
  ));

  deployWorkflow$ = createEffect(() => this.actions$.pipe(
    ofType(WorkflowListActions.deployWorkflow),
    concatMap(({ payload }) => this.workflowApi.deploy(payload).pipe(
      map(() => WorkflowListActions.deployWorkflowSuccess()),
      catchError(() => of(WorkflowListActions.deployWorkflowError()))
    ))
  ));

  deleteWorkflow$ = createEffect(() => this.actions$.pipe(
    ofType(WorkflowListActions.validateDeleteWorkflow),
    concatMap(({ payload }) => this.workflowApi.delete(payload).pipe(
      map(() => WorkflowListActions.deleteWorkflowSuccess(payload)),
      catchError(() => of(WorkflowListActions.deleteWorkflowError()))
    ))
  ));

  loadPortals$ = createEffect(() => this.actions$.pipe(
    ofType(WorkflowListActions.loadPortals),
    switchMap(({ payload }) => this.workflowPortalApi.loadPortals(payload.workflowId).pipe(
      map(portals => WorkflowListActions.loadPortalsSuccess(payload.workflowId, portals)),
      catchError(() => of(WorkflowListActions.loadPortalsError(payload.workflowId)))
    ))
  ));

  importWorkflow$ = createEffect(() => this.actions$.pipe(
    ofType(WorkflowListActions.importWorkflow),
    switchMap(({ payload }) => this.workflowApi.import(payload).pipe(
      map(workflow => WorkflowListActions.importSuccess(workflow.workflow_id)),
      catchError((err: HttpErrorResponse) => of(WorkflowListActions.importError(err)))
    ))
  ));

  notifySuccess$ = createEffect(() => this.actions$.pipe(
    ofType(
      WorkflowListActions.deleteWorkflowSuccess,
      WorkflowListActions.stopWorkflowSuccess,
      WorkflowListActions.deployWorkflowSuccess,
    ),
    tap(({ notification }) => this.messageService.displaySuccessMessage(notification))
  ), { dispatch: false });

  notifyError$ = createEffect(() => this.actions$.pipe(
    ofType(
      WorkflowListActions.deleteWorkflowError,
      WorkflowListActions.stopWorkflowError,
      WorkflowListActions.deployWorkflowError,
    ),
    tap(({ notification }) => this.messageService.displayErrorMessage(notification))
  ), { dispatch: false })
}
