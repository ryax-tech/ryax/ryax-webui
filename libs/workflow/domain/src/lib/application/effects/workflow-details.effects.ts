// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import {
  selectDetailsActiveTool, selectDetailsActiveWorkflowModule,
  selectDetailsActiveWorkflowModuleId, selectDetailsActiveWorkflowModuleInputs,
  selectDetailsWorkflowId,
  selectWorkflowModuleInputById,
  WorkflowState,
} from '../reducers';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Action, Store } from '@ngrx/store';
import { WorkflowApi, WorkflowPortalApi } from '../../infrastructure/services';
import { MessageService } from '@ryax/shared/ui-common';
import { WorkflowDetailsActions } from '../actions';
import { catchError, concatMap, filter, flatMap, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';
import { of } from 'rxjs';
import { Router } from '@angular/router';
import { WorkflowDetailsTool, WorkflowModuleIOType } from '../../entities';

@Injectable()
export class WorkflowDetailsEffects {
  private workflowId$ = this.store.select(selectDetailsWorkflowId);
  private activeWorkflowModuleId$ = this.store.select(selectDetailsActiveWorkflowModuleId);
  private activeWorkflowModule$ = this.store.select(selectDetailsActiveWorkflowModule);
  private activeTool$ = this.store.select(selectDetailsActiveTool);

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store<WorkflowState>,
    private readonly workflowApi: WorkflowApi,
    private readonly workflowPortalApi: WorkflowPortalApi,
    private readonly messageService: MessageService,
    private readonly router: Router,
  ) {}

  init$ = createEffect(() => this.actions$.pipe(
    ofType(WorkflowDetailsActions.init),
    withLatestFrom(this.workflowId$, (_, workflowId) => workflowId),
    switchMap(workflowId => this.workflowApi.loadOne(workflowId).pipe(
      flatMap(({ workflow, modules, moduleLinks}) => [
        WorkflowDetailsActions.initSuccess(workflow, modules, moduleLinks),
        WorkflowDetailsActions.loadPortals(workflow.id)
      ]),
      catchError(() => of(WorkflowDetailsActions.initError(workflowId)))
    ))
  ));

  refresh$ = createEffect(() => this.actions$.pipe(
    ofType(
      WorkflowDetailsActions.refresh,
      WorkflowDetailsActions.addModuleSuccess,
      WorkflowDetailsActions.deleteModuleSuccess,
      WorkflowDetailsActions.addLinkSuccess,
      WorkflowDetailsActions.deleteLinkSuccess,
      WorkflowDetailsActions.updateModuleSuccess,
      WorkflowDetailsActions.updateModuleInputSuccess,
      WorkflowDetailsActions.updateModuleVersionSuccess,
      WorkflowDetailsActions.stopWorkflowSuccess,
      WorkflowDetailsActions.deployWorkflowSuccess,
      WorkflowDetailsActions.deleteFileSuccess,
    ),
    withLatestFrom(this.workflowId$, (_, workflowId) => workflowId),
    switchMap(workflowId => this.workflowApi.loadOne(workflowId).pipe(
      map(({ workflow, modules, moduleLinks}) => WorkflowDetailsActions.refreshSuccess(workflow, modules, moduleLinks)),
      catchError(() => of(WorkflowDetailsActions.refreshError(workflowId)))
    ))
  ));

  loadWorkflowErrors$ = createEffect(() => this.actions$.pipe(
    ofType(
      WorkflowDetailsActions.initSuccess,
      WorkflowDetailsActions.activeTool,
      WorkflowDetailsActions.addModuleSuccess,
      WorkflowDetailsActions.deleteModuleSuccess,
      WorkflowDetailsActions.addLinkSuccess,
      WorkflowDetailsActions.deleteLinkSuccess,
      WorkflowDetailsActions.updateModuleSuccess,
      WorkflowDetailsActions.updateModuleInputSuccess,
    ),
    withLatestFrom(this.workflowId$, (_  , workflowId) => ({ workflowId })),
    switchMap(({ workflowId }) => this.workflowApi.loadWorkflowErrors(workflowId).pipe(
      map((workflowErrors) => WorkflowDetailsActions.loadWorkflowErrorsSuccess(workflowErrors)),
      catchError(() => of(WorkflowDetailsActions.loadWorkflowErrorsError()))
    ))
  ));

  deployWorkflow$ = createEffect(() => this.actions$.pipe(
    ofType(WorkflowDetailsActions.deployWorkflow),
    withLatestFrom(this.workflowId$, (_, workflowId) => workflowId),
    switchMap(workflowId => this.workflowApi.deploy(workflowId).pipe(
      map(() => WorkflowDetailsActions.deployWorkflowSuccess()),
      catchError(() => of(WorkflowDetailsActions.deployWorkflowError()))
    ))
  ));

  stopWorkflow$ = createEffect(() => this.actions$.pipe(
    ofType(WorkflowDetailsActions.stopWorkflow),
    withLatestFrom(this.workflowId$, (_, workflowId) => workflowId),
    switchMap(workflowId => this.workflowApi.stop(workflowId).pipe(
      map(() => WorkflowDetailsActions.stopWorkflowSuccess()),
      catchError(() => of(WorkflowDetailsActions.stopWorkflowError()))
    ))
  ));

  deleteWorkflow$ = createEffect(() => this.actions$.pipe(
    ofType(WorkflowDetailsActions.validateDeleteWorkflow),
    withLatestFrom(this.workflowId$, (_, workflowId) => workflowId),
    switchMap(workflowId => this.workflowApi.delete(workflowId).pipe(
      map(() => WorkflowDetailsActions.deleteWorkflowSuccess(workflowId)),
      catchError(() => of(WorkflowDetailsActions.deleteWorkflowError()))
    ))
  ));

  addModule$ = createEffect(() => this.actions$.pipe(
    ofType(WorkflowDetailsActions.addModule),
    withLatestFrom(this.workflowId$, ({ payload }, workflowId) => ({ payload, workflowId })),
    switchMap(({ payload, workflowId }) => this.workflowApi.addModule(workflowId, payload).pipe(
      map(() => WorkflowDetailsActions.addModuleSuccess()),
      catchError(() => of(WorkflowDetailsActions.addModuleError()))
    ))
  ));

  moveModule$ = createEffect(() => this.actions$.pipe(
    ofType(WorkflowDetailsActions.moveModule),
    withLatestFrom(this.workflowId$, ({ payload }, workflowId) => ({ payload, workflowId })),
    switchMap(({ payload, workflowId }) => this.workflowApi.updateModule(workflowId, payload.workflowModuleId, payload).pipe(
      map(() => WorkflowDetailsActions.moveModuleSuccess()),
      catchError(() => of(WorkflowDetailsActions.moveModuleError()))
    ))
  ));

  deleteModule$ = createEffect(() => this.actions$.pipe(
    ofType(WorkflowDetailsActions.deleteModule),
    withLatestFrom(this.workflowId$, ({ payload }, workflowId) => ({ payload, workflowId })),
    switchMap(({ payload, workflowId }) => this.workflowApi.deleteModule(workflowId, payload).pipe(
      map(() => WorkflowDetailsActions.deleteModuleSuccess(payload)),
      catchError(() => of(WorkflowDetailsActions.deleteModuleError()))
    ))
  ));

  addLink$ = createEffect(() => this.actions$.pipe(
    ofType(WorkflowDetailsActions.addLink),
    withLatestFrom(this.workflowId$, ({ payload }, workflowId) => ({ payload, workflowId })),
    switchMap(({ payload, workflowId }) => this.workflowApi.addModuleLink(workflowId, payload).pipe(
      map(() => WorkflowDetailsActions.addLinkSuccess()),
      catchError(() => of(WorkflowDetailsActions.addLinkError()))
    ))
  ));

  deleteLink$ = createEffect(() => this.actions$.pipe(
    ofType(WorkflowDetailsActions.deleteLink),
    withLatestFrom(this.workflowId$, ({ payload }, workflowId) => ({ payload, workflowId })),
    switchMap(({ payload, workflowId }) => this.workflowApi.deleteModuleLink(workflowId, payload).pipe(
      map(() => WorkflowDetailsActions.deleteLinkSuccess()),
      catchError(() => of(WorkflowDetailsActions.deleteLinkError()))
    ))
  ));

  updateWorkflowModule$ = createEffect(() => this.actions$.pipe(
    ofType(WorkflowDetailsActions.updateModule),
    withLatestFrom(this.workflowId$, this.activeWorkflowModuleId$,({ payload } , workflowId, workflowModuleId) => ({ workflowId, workflowModuleId, payload })),
    switchMap(({ workflowId, workflowModuleId, payload }) => this.workflowApi.updateModule(workflowId, workflowModuleId, payload).pipe(
      map(() => WorkflowDetailsActions.updateModuleSuccess()),
      catchError(() => of(WorkflowDetailsActions.updateModuleError()))
    ))
  ));


  loadWorkflowModuleInputs$ = createEffect(() => this.actions$.pipe(
    ofType(
      WorkflowDetailsActions.selectModule,
      WorkflowDetailsActions.updateModuleVersionSuccess,
      WorkflowDetailsActions.activeTool,
    ),
    switchMap(({ payload }) => this.store.select(selectDetailsActiveTool, payload).pipe(
      filter(activeTool => activeTool === WorkflowDetailsTool.Setup,),
      map(() => ({ payload }))
    )),
    withLatestFrom(this.workflowId$, this.activeWorkflowModuleId$, (_, workflowId, workflowModuleId) => ({ workflowId, workflowModuleId })),
    switchMap(({ workflowId, workflowModuleId }) => this.workflowApi.loadModuleInputs(workflowId, workflowModuleId).pipe(
      map(({ workflowModuleInputs, workflowModuleOutputs }) => WorkflowDetailsActions.loadModuleInputsSuccess(workflowModuleInputs, workflowModuleOutputs)),
      catchError(() => of(WorkflowDetailsActions.loadModuleInputsError(workflowModuleId)))
    ))
  ));

  updateWorkflowModuleInput$ = createEffect(() => this.actions$.pipe(
    ofType(WorkflowDetailsActions.updateModuleInput),
    switchMap(({ payload }) => this.store.select(selectWorkflowModuleInputById, payload.workflowModuleInputId).pipe(
      filter(workflowModuleInput => !!payload.referenceValue || workflowModuleInput.type !== WorkflowModuleIOType.FILE),
      map(() => ({ payload }))
    )),
    withLatestFrom(this.workflowId$, this.activeWorkflowModuleId$, ({ payload } , workflowId, workflowModuleId) => ({ workflowId, workflowModuleId, payload })),
    switchMap(({ workflowId, workflowModuleId, payload }) => this.workflowApi.updateModuleInput(workflowId, workflowModuleId, payload.workflowModuleInputId, payload).pipe(
      map(() => WorkflowDetailsActions.updateModuleInputSuccess()),
      catchError(() => of(WorkflowDetailsActions.updateModuleInputError()))
    ))
  ));

  updateWorkflowModuleInputFile$ = createEffect(() => this.actions$.pipe(
    ofType(WorkflowDetailsActions.updateModuleInput),
    switchMap(({ payload }) => this.store.select(selectWorkflowModuleInputById, payload.workflowModuleInputId).pipe(
      filter(workflowModuleInput => !!payload.staticValue && workflowModuleInput.type === WorkflowModuleIOType.FILE),
      map(() => ({ payload }))
    )),
    withLatestFrom(this.workflowId$, this.activeWorkflowModuleId$,({ payload } , workflowId, workflowModuleId) => ({ workflowId, workflowModuleId, payload })),
    switchMap(({ workflowId, workflowModuleId, payload }) => this.workflowApi.updateModuleInputFile(workflowId, workflowModuleId, payload.workflowModuleInputId, payload).pipe(
      map(() => WorkflowDetailsActions.updateModuleInputSuccess()),
      catchError(() => of(WorkflowDetailsActions.updateModuleInputError()))
    ))
  ));

  deleteModuleInputSetupFile$ = createEffect(() => this.actions$.pipe(
    ofType(WorkflowDetailsActions.deleteFile),
    withLatestFrom(this.workflowId$, this.activeWorkflowModuleId$,({ payload }, workflowId, workflowModuleId) => ({ workflowId, workflowModuleId, payload })),
    switchMap(({ workflowId, workflowModuleId, payload }) => this.workflowApi.deleteInputFile(workflowId, workflowModuleId, payload).pipe(
      map(() => WorkflowDetailsActions.deleteFileSuccess()),
      catchError(() => of(WorkflowDetailsActions.deleteFileError()))
    ))
  ));


  loadWorkflowModuleOutputs$ = createEffect(() => this.actions$.pipe(
    ofType(
      WorkflowDetailsActions.selectModule,
      WorkflowDetailsActions.addWorkflowModuleOutputSuccess,
      WorkflowDetailsActions.updateModuleVersionSuccess,
      ),
    switchMap(() => this.store.select(selectDetailsActiveTool).pipe(
      filter(activeTool => activeTool === WorkflowDetailsTool.Setup,),
    )),
    withLatestFrom(this.workflowId$, this.activeWorkflowModuleId$, (_, workflowId, workflowModuleId) => ({ workflowId, workflowModuleId })),
    switchMap(({ workflowId, workflowModuleId }) => this.workflowApi.loadModuleOutputs(workflowId, workflowModuleId).pipe(
      map((workflowModuleOutputs) => WorkflowDetailsActions.loadModuleOutputsSuccess(workflowModuleOutputs)),
      catchError(() => of(WorkflowDetailsActions.loadModuleOutputsError(workflowModuleId)))
    ))
  ));

  updateWorkflowModuleOutput$ = createEffect(() => this.actions$.pipe(
    ofType(WorkflowDetailsActions.updateWorkflowModuleOutput),
    withLatestFrom(this.workflowId$, this.activeWorkflowModuleId$,({ payload } , workflowId, workflowModuleId) => ({ payload, workflowId, workflowModuleId })),
    concatMap(({ payload, workflowId, workflowModuleId }) => this.workflowApi.updateWorkflowModuleOutput(workflowId, workflowModuleId, payload).pipe(
      map(() => WorkflowDetailsActions.updateWorkflowModuleOutputSuccess()),
      catchError(() => of(WorkflowDetailsActions.updateWorkflowModuleOutputError()))
    ))
  ));

  addWorkflowModuleOutput$ = createEffect(() => this.actions$.pipe(
    ofType(WorkflowDetailsActions.addWorkflowModuleOutput),
    withLatestFrom(this.workflowId$, this.activeWorkflowModuleId$,(_ , workflowId, workflowModuleId) => ({ workflowId, workflowModuleId })),
    concatMap(({ workflowId, workflowModuleId }) => this.workflowApi.addWorkflowModuleOutput(workflowId, workflowModuleId).pipe(
      map(() => WorkflowDetailsActions.addWorkflowModuleOutputSuccess()),
      catchError(() => of(WorkflowDetailsActions.addWorkflowModuleOutputError()))
    ))
  ));

  searchWorkflowModulesOutputs$ = createEffect(() => this.actions$.pipe(
    ofType(WorkflowDetailsActions.searchModulesOutputs),
    withLatestFrom(this.workflowId$, this.activeWorkflowModuleId$, ({ payload } , workflowId, workflowModuleId) => ({ workflowId, workflowModuleId, payload })),
    switchMap(({ workflowId, workflowModuleId, payload }) => this.workflowApi.searchModuleOutputs(workflowId, payload, workflowModuleId).pipe(
      map(workflowModuleOutputs => WorkflowDetailsActions.searchModulesOutputsSuccess(workflowModuleOutputs)),
      catchError(() => of(WorkflowDetailsActions.searchModulesOutputsError()))
    ))
  ));

  deleteWorkflowModuleOutput$ = createEffect(() => this.actions$.pipe(
    ofType(WorkflowDetailsActions.deleteWorkflowModuleOutput),
    withLatestFrom(this.workflowId$, this.activeWorkflowModuleId$,({ payload } , workflowId, workflowModuleId) => ({ payload, workflowId, workflowModuleId })),
    concatMap(({ payload, workflowId, workflowModuleId }) => this.workflowApi.deleteWorkflowModuleOutput(workflowId, workflowModuleId, payload).pipe(
      map(() => WorkflowDetailsActions.deleteWorkflowModuleOutputSuccess(payload)),
      catchError(() => of(WorkflowDetailsActions.deleteWorkflowModuleOutputError()))
    ))
  ));


  loadWorkflowModuleVersion$ = createEffect(() => this.actions$.pipe(
    ofType(WorkflowDetailsActions.selectModule),
    withLatestFrom(this.workflowId$, this.activeWorkflowModule$, (_, workflowId, workflowModule) => ({ workflowId, workflowModule })),
    switchMap(({ workflowModule }) => this.workflowApi.loadModuleVersion(workflowModule.moduleId).pipe(
      map(( workflowModuleVersions ) => WorkflowDetailsActions.loadModuleVersionsSuccess(workflowModuleVersions)),
      catchError(() => of(WorkflowDetailsActions.loadModuleVersionsError()))
    ))
  ));

  updateWorkflowModuleVersion$ = createEffect(() => this.actions$.pipe(
    ofType(WorkflowDetailsActions.updateModuleVersion),
    withLatestFrom(this.workflowId$, this.activeWorkflowModuleId$, ({ payload } , workflowId, workflowModuleId) => ({ workflowId, workflowModuleId, payload })),
    switchMap(({ workflowId, workflowModuleId, payload }) => this.workflowApi.updateModuleVersion(workflowId, workflowModuleId, payload).pipe(
      map((newModuleId) => WorkflowDetailsActions.updateModuleVersionSuccess(newModuleId),),
      catchError(() => of(WorkflowDetailsActions.updateModuleVersionError()))
    ))
  ));


  exportWorkflowDraft$ = createEffect(() => this.actions$.pipe(
    ofType(WorkflowDetailsActions.exportWorkflow),
    withLatestFrom(this.workflowId$, (_, workflowId) => workflowId),
    switchMap(workflowId => this.workflowApi.export(workflowId).pipe(
      map(() => WorkflowDetailsActions.exportSuccess()),
      catchError(() => of(WorkflowDetailsActions.exportError()))
    )),
  ));

  loadPortals$ = createEffect(() => this.actions$.pipe(
    ofType(WorkflowDetailsActions.loadPortals),
    switchMap(({ payload }) => this.workflowPortalApi.loadPortals(payload.workflowId).pipe(
      map(portals => WorkflowDetailsActions.loadPortalsSuccess(payload.workflowId, portals)),
      catchError(() => of(WorkflowDetailsActions.loadPortalsError(payload.workflowId)))
    ))
  ));


  notifySuccess$ = createEffect(() => this.actions$.pipe(
    ofType<Action & { notification: string }>(
      WorkflowDetailsActions.addModuleSuccess,
      WorkflowDetailsActions.deleteModuleSuccess,
      WorkflowDetailsActions.addLinkSuccess,
      WorkflowDetailsActions.deleteLinkSuccess,
      WorkflowDetailsActions.updateModuleSuccess,
      WorkflowDetailsActions.updateModuleInputSuccess,
      WorkflowDetailsActions.addWorkflowModuleOutputSuccess,
      WorkflowDetailsActions.updateWorkflowModuleOutputSuccess,
      WorkflowDetailsActions.deleteWorkflowModuleOutputSuccess,
      WorkflowDetailsActions.stopWorkflowSuccess,
      WorkflowDetailsActions.deployWorkflowSuccess,
      WorkflowDetailsActions.exportSuccess,
      WorkflowDetailsActions.deleteFileSuccess,
      WorkflowDetailsActions.updateModuleVersionSuccess,
    ),
    tap(({ notification }) => this.messageService.displaySuccessMessage(notification))
  ), { dispatch: false });

  notifyError$ = createEffect(() => this.actions$.pipe(
    ofType<Action & { notification: string }>(
      WorkflowDetailsActions.addModuleError,
      WorkflowDetailsActions.deleteModuleError,
      WorkflowDetailsActions.addLinkError,
      WorkflowDetailsActions.deleteModuleError,
      WorkflowDetailsActions.updateModuleError,
      WorkflowDetailsActions.updateModuleInputError,
      WorkflowDetailsActions.addWorkflowModuleOutputError,
      WorkflowDetailsActions.updateWorkflowModuleOutputError,
      WorkflowDetailsActions.deleteWorkflowModuleOutputError,
      WorkflowDetailsActions.stopWorkflowError,
      WorkflowDetailsActions.deployWorkflowError,
      WorkflowDetailsActions.exportError,
      WorkflowDetailsActions.deleteFileError,
      WorkflowDetailsActions.updateModuleVersionError,
    ),
    tap(({ notification }) => this.messageService.displayErrorMessage(notification))
  ), { dispatch: false });

  goToWorklflowList$ = createEffect(() => this.actions$.pipe(
    ofType(WorkflowDetailsActions.deleteWorkflowSuccess),
    tap(() => this.router.navigate(['/', 'studio', 'workflows']))
  ), { dispatch: false });

  goToNotFoundError$ = createEffect(() => this.actions$.pipe(
    ofType(WorkflowDetailsActions.initError),
    tap(() => this.router.navigate(['/', 'error']))
  ), { dispatch: false });
}
