// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { of } from 'rxjs';
import { catchError, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Action, Store } from '@ngrx/store';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { MessageService } from '@ryax/shared/ui-common';
import { WorkflowPortalActions } from '../actions';
import {
  selectPortalWorkflowId,
  selectPortalWorkflowPortal,
  selectPortalWorkflowPortalId,
  WorkflowState,
} from '../reducers';
import { WorkflowApi, WorkflowPortalApi } from '../../infrastructure/services';

@Injectable()
export class WorkflowPortalEffects {
  private workflowId$ = this.store.select(selectPortalWorkflowId);
  private workflowPortalId$ = this.store.select(selectPortalWorkflowPortalId);
  private workflowPortal$ = this.store.select(selectPortalWorkflowPortal);

  constructor(
    private readonly store: Store<WorkflowState>,
    private readonly actions$: Actions,
    private readonly workflowApi: WorkflowApi,
    private readonly workflowPortalApi: WorkflowPortalApi,
    private readonly messageService: MessageService
  ) {}

  loadPortal$ = createEffect(() => this.actions$.pipe(
    ofType(WorkflowPortalActions.init),
    withLatestFrom(this.workflowId$, this.workflowPortalId$, (_, workflowId, workflowPortalId) => ({
      workflowId, workflowPortalId
    })),
    switchMap(({ workflowId, workflowPortalId }) => this.workflowPortalApi.loadPortal(workflowId, workflowPortalId).pipe(
      map(portal => WorkflowPortalActions.loadPortalSuccess(portal)),
      catchError(() => of(WorkflowPortalActions.loadPortalError(workflowId)))
    ))
  ));

  loadPortalOutputs$ = createEffect(() => this.actions$.pipe(
    ofType(WorkflowPortalActions.init),
    withLatestFrom(this.workflowId$, this.workflowPortalId$, (_, workflowId, workflowPortalId) => ({
      workflowId, workflowPortalId
    })),
    switchMap(({ workflowId, workflowPortalId }) => this.workflowApi.loadModuleOutputs(workflowId, workflowPortalId).pipe(
      map(portalOutputs => WorkflowPortalActions.loadPortalOutputsSuccess(portalOutputs)),
      catchError(() => of(WorkflowPortalActions.loadPortalOutputsError(workflowPortalId)))
    ))
  ));

  sendPortalData$ = createEffect(() => this.actions$.pipe(
    ofType(WorkflowPortalActions.send),
    withLatestFrom(this.workflowPortal$, ({ payload }, portal) => ({ payload, portal })),
    switchMap(({ payload, portal }) => this.workflowPortalApi.sendData(portal.accessPath, payload).pipe(
      map(moduleRun => WorkflowPortalActions.sendSuccess(moduleRun)),
      catchError(() => of(WorkflowPortalActions.sendError()))
    ))
  ));

  notifyError$ = createEffect(() => this.actions$.pipe(
    ofType<Action & { notification: string }>(
      WorkflowPortalActions.sendError
    ),
    tap(({ notification }) => this.messageService.displayErrorMessage(notification))
  ), { dispatch: false });
}
