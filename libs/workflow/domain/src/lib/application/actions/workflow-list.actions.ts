// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createAction } from '@ngrx/store';
import { Workflow, WorkflowFilter, WorkflowPortal } from '@ryax/workflow/domain';
import { HttpErrorResponse } from '@angular/common/http';

export const init = createAction(
  '[Workflow List] Init'
);

export const initSuccess = createAction(
  '[Workflow List] Init success',
  (workflows: Workflow[]) => ({
    payload: workflows
  })
);

export const initError = createAction(
  '[Workflow List] Init error'
);

export const exit = createAction(
  '[Workflow List] Exit'
);

export const refresh = createAction(
  '[Workflow List] Refresh'
);

export const refreshSuccess = createAction(
  '[Workflow List] Refresh success',
  (workflows: Workflow[]) => ({
    payload: workflows
  })
);

export const refreshError = createAction(
  '[Workflow List] Refresh error'
);

export const addWorkflow = createAction(
  '[Workflow List] Add workflow'
);

export const editWorkflow = createAction(
  '[Workflow List] Edit workflow',
  (workflowId: string) => ({
    payload: workflowId
  })
);

export const copyWorkflow = createAction(
  '[Workflow List] Copy workflow',
  (workflowId: string) => ({
    payload: workflowId
  })
);

export const deleteWorkflow = createAction(
  '[Workflow List] Delete workflow',
  (workflowId: string) => ({
    payload: workflowId
  })
);

export const deleteWorkflowSuccess = createAction(
  '[Workflow List] Delete workflow success',
  (workflowId: string) => ({
    payload: workflowId,
    notification: "Workflow deleted successfully"
  })
);

export const deleteWorkflowError = createAction(
  '[Workflow List] Delete workflow error',
  () => ({
    notification: "Workflow delete failed"
  })
);

export const deployWorkflow = createAction(
  '[Workflow List] Deploy workflow',
  (workflowId: string) => ({
    payload: workflowId
  })
);

export const deployWorkflowSuccess = createAction(
  '[Workflow List] Deploy workflow success',
  () => ({
    notification: "Workflow deployment started"
  })
);

export const deployWorkflowError = createAction(
  '[Workflow List] Deploy workflow error',
  () => ({
    notification: "Workflow deployment start fail"
  })
);

export const stopWorkflow = createAction(
  '[Workflow List] Stop workflow',
  (workflowId: string) => ({
    payload: workflowId
  })
);

export const stopWorkflowSuccess = createAction(
  '[Workflow List] Stop workflow success',
  () => ({
    notification: "Workflow stopped successfully"
  })
);

export const stopWorkflowError = createAction(
  '[Workflow List] Stop workflow error',
  () => ({
    notification: "Workflow stop failed"
  })
);

export const filter = createAction(
  '[Workflow List] Filter',
  (data: WorkflowFilter) => ({
    payload: data
  })
);

export const notifySuccess = createAction(
  '[Workflow List] Notify success',
  (message: string) => ({
    payload: message
  })
);

export const notifyError = createAction(
  '[Workflow List] Notify error',
  (message: string) => ({
    payload: message
  })
);

export const openDeleteWorkflowModal = createAction(
  '[Workflow List] Open delete modal',
  (workflowId: string) => ({
    payload: workflowId
  })
);

export const validateDeleteWorkflow = createAction(
  '[Workflow List] Validate delete workflow',
  (workflowId: string) => ({
    payload: workflowId
  })
);

export const cancelDeleteWorkflow = createAction(
  '[Workflow List] Cancel delete workflow',
);

export const loadPortals = createAction(
  '[Workflow List] Load portals',
  (workflowId: string) => ({
    payload: { workflowId }
  })
);

export const loadPortalsSuccess = createAction(
  '[Workflow List] Load portals success',
  (workflowId: string, workflowPortals: WorkflowPortal[]) => ({
    payload: { workflowId, workflowPortals }
  })
);

export const loadPortalsError = createAction(
  '[Workflow List] Load portals error',
  (workflowId: string) => ({
    payload: { workflowId }
  })
);

export const importWorkflow = createAction(
  '[Workflow List] Import workflow',
  (file: File) => ({
    payload: file
  })
);

export const importSuccess = createAction(
  '[Workflow List] Import success',
  (workflowId: string) => ({
    payload: workflowId,
    notification: "Workflow imported successfully"
  })
);

export const importError = createAction(
  '[Workflow List] Import error',
  (error?: HttpErrorResponse) => ({
    notification: "Workflow import failed",
    payload: error
  })
);

export const openImportWorkflowModal = createAction(
  '[Workflow List] Open workflow import modal',
);

export const closeImportWorkflowModal = createAction(
  '[Workflow List] Close workflow import modal',
);

export const closeImportSuccessWorkflowModal = createAction(
  '[Workflow List] Close workflow import success modal',
);

export const closeImportErrorWorkflowModal = createAction(
  '[Workflow List] Close workflow import error modal',
);


