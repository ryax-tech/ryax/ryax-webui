// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createAction } from '@ngrx/store';
import { WorkflowEditData } from '../../entities';

export const save = createAction(
  '[Workflow Edit] Save',
  (data: WorkflowEditData) => ({
    payload: data
  })
);

export const close = createAction(
  '[Workflow Edit] Close'
);


export const createSuccess = createAction(
  '[Workflow Edit] Create success',
  (workflowId: string) => ({
    payload: workflowId,
    notification: "Workflow created successfully"
  })
)

export const createError = createAction(
  '[Workflow Edit] Create error',
  () => ({
    notification: "Workflow creation failed"
  })
)

export const copySuccess = createAction(
  '[Workflow Edit] Copy success',
  (workflowId: string) => ({
    payload: workflowId,
    notification: "Workflow copied successfully"
  })
)

export const copyError = createAction(
  '[Workflow Edit] Copy error',
  () => ({
    notification: "Workflow copy failed"
  })
)

export const updateSuccess = createAction(
  '[Workflow Edit] Update success',
  (workflowId: string) => ({
    payload: workflowId,
    notification: "Workflow updated successfully"
  })
)

export const updateError = createAction(
  '[Workflow Edit] Update error',
  () => ({
    notification: "Workflow update failed"
  })
)

export const notifySuccess = createAction(
  '[Workflow Edit] Notify success',
  (message: string) => ({
    payload: message
  })
);

export const notifyError = createAction(
  '[Workflow Edit] Notify error',
  (message: string) => ({
    payload: message
  })
);

export const goToWorkflowDetails = createAction(
  '[Workflow Edit] Navigate to workflow details',
  (workflowId: string) => ({
    payload: workflowId
  })
);
