// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createAction } from '@ngrx/store';
import { ModuleRun, WorkflowModuleOutput, WorkflowPortal } from '@ryax/workflow/domain';

export const init = createAction(
  '[Workflow Portal] Init'
);

export const loadPortalSuccess = createAction(
  '[Workflow Portal] Load portal success',
  (workflowPortal: WorkflowPortal) => ({
    payload: workflowPortal
  })
);

export const loadPortalError = createAction(
  '[Workflow Portal] Load portal error',
  (workflowPortalId: string) => ({
    payload: workflowPortalId
  })
);

export const loadPortalOutputsSuccess = createAction(
  '[Workflow Portal] Load portal outputs success',
  (workflowPortalOutputs: WorkflowModuleOutput[]) => ({
    payload: { workflowPortalOutputs }
  })
);

export const loadPortalOutputsError = createAction(
  '[Workflow Portal] Load portal outputs error',
  (workflowPortalId: string) => ({
    payload: { workflowPortalId }
  })
);

export const send = createAction(
  '[Workflow Portal] Send',
  (data: object) => ({
    payload: data
  })
);

export const back = createAction(
  '[Workflow Portal] Back',
);

export const sendSuccess = createAction(
  '[Workflow Portal] Send success',
  (moduleRun: ModuleRun) => ({
    payload: moduleRun
  })
);

export const sendError = createAction(
  '[Workflow Portal] Send error',
  () => ({
    notification: "Error happen during data submission"
  })
);

export const openUnsupportedExtensionModal = createAction(
  '[Workflow Portal] Open unsupported extension modal',
);

export const closeUnsupportedExtensionModal = createAction(
  '[Workflow Portal] Close unsupported extension modal',
);


