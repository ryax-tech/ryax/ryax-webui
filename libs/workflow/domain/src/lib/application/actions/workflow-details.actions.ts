// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createAction } from '@ngrx/store';
import {
  AddWorkflowModuleData,
  MoveWorkflowModuleData, UpdateModuleVersionResponse,
  UpdateWorkflowModuleData,
  UpdateWorkflowModuleInputData, UpdateWorkflowModuleOutputData,
  Workflow,
  WorkflowDetailsTool, WorkflowError,
  WorkflowModule,
  WorkflowModuleInput,
  WorkflowModuleIOType,
  WorkflowModuleLink,
  WorkflowModuleOutput,
  WorkflowModuleVersion, WorkflowPortal,
} from '../../entities';

export const init = createAction(
  '[Workflow Details] Init',
);

export const initSuccess = createAction(
  '[Workflow Details] Init success',
  (workflow: Workflow, workflowModules: WorkflowModule[], workflowModuleLinks: WorkflowModuleLink[]) => ({
    payload: { workflow, workflowModules, workflowModuleLinks }
  })
);

export const initError = createAction(
  '[Workflow Details] Init error',
  (workflowId: string) => ({
    payload: workflowId
  })
);

export const refresh = createAction(
  '[Workflow Details] Refresh'
);

export const refreshSuccess = createAction(
  '[Workflow Details] Refresh success',
  (workflow: Workflow, workflowModules: WorkflowModule[], workflowModuleLinks: WorkflowModuleLink[]) => ({
    payload: { workflow, workflowModules, workflowModuleLinks }
  })
);

export const refreshError = createAction(
  '[Workflow Details] Refresh error',
  (workflowId: string) => ({
    payload: workflowId
  })
);

export const exit = createAction(
  '[Workflow Details] Exit',
);

export const copyWorkflow = createAction(
  '[Workflow Details] Copy workflow'
);

export const deployWorkflow = createAction(
  '[Workflow Details] Deploy workflow'
);

export const deployWorkflowSuccess = createAction(
  '[Workflow Details] Deploy workflow success',
  () => ({
    notification: "Workflow deployment started"
  })
);

export const deployWorkflowError = createAction(
  '[Workflow Details] Deploy workflow error',
  () => ({
    notification: "Workflow deployment start fail"
  })
);

export const stopWorkflow = createAction(
  '[Workflow Details] Stop workflow',
);

export const stopWorkflowSuccess = createAction(
  '[Workflow Details] Stop workflow success',
  () => ({
    notification: "Workflow stopped successfully"
  })
);

export const stopWorkflowError = createAction(
  '[Workflow Details] Stop workflow error',
  () => ({
    notification: "Workflow stop failed"
  })
);

export const editWorkflow = createAction(
  '[Workflow Details] Edit workflow',
);

export const openDeleteWorkflowModal = createAction(
  '[Workflow Details] Open delete modal',
);

export const validateDeleteWorkflow = createAction(
  '[Workflow Details] Validate delete workflow',
);

export const cancelDeleteWorkflow = createAction(
  '[Workflow Details] Cancel delete workflow',
);

export const deleteWorkflowSuccess = createAction(
  '[Workflow Details] Delete workflow success',
  (workflowId: string) => ({
    payload: workflowId,
    notification: "Workflow deleted successfully"
  })
);

export const deleteWorkflowError = createAction(
  '[Workflow Details] Delete workflow error',
  () => ({
    notification: "Workflow delete failed"
  })
);

export const selectModule = createAction(
  '[Workflow Details] Select module',
  (moduleId: string) => ({
    payload: moduleId
  })
);

export const unselectModule = createAction(
  '[Workflow Details] Unselect module',
);

export const addModule = createAction(
  '[Workflow Details] Add module',
  (data: AddWorkflowModuleData) => ({
    payload: data
  })
);

export const addModuleSuccess = createAction(
  '[Workflow Details] Add module success',
  () => ({
    notification: "Module added successfully"
  })
);

export const addModuleError = createAction(
  '[Workflow Details] Add module error',
  () => ({
    notification: "Module add failed"
  })
);


export const moveModule = createAction(
  '[Workflow Details] Move module',
  (data: MoveWorkflowModuleData) => ({
    payload: data
  })
);

export const moveModuleSuccess = createAction(
  '[Workflow Details] Move module success',
  () => ({
    notification: "Module updated successfully"
  })
);

export const moveModuleError = createAction(
  '[Workflow Details] Move module error',
  () => ({
    notification: "Module update failed"
  })
);

export const deleteModule = createAction(
  '[Workflow Details] Delete module',
  (moduleInstanceId: string) => ({
    payload: moduleInstanceId
  })
);

export const deleteModuleSuccess = createAction(
  '[Workflow Details] Delete module success',
  (deletedWorkflowModuleId: string) => ({
    payload: deletedWorkflowModuleId,
    notification: "Module deleted successfully"
  })
);


export const deleteModuleError = createAction(
  '[Workflow Details] Delete module error',
  () => ({
    notification: "Module delete failed"
  })
);


export const addLink = createAction(
  '[Workflow Details] Add link',
  (inputModuleId: string, outputModuleId: string) => ({
    payload: { inputModuleId, outputModuleId }
  })
);

export const addLinkSuccess = createAction(
  '[Workflow Details] Add link success',
  () => ({
    notification: "Module link added successfully"
  })
);

export const addLinkError = createAction(
  '[Workflow Details] Add link error',
  () => ({
    notification: "Module link add failed"
  })
);

export const deleteLink = createAction(
  '[Workflow Details] Delete link',
  (linkId: string) => ({
    payload: linkId
  })
);

export const deleteLinkSuccess = createAction(
  '[Workflow Details] Delete link success',
  () => ({
    notification: "Module link deleted successfully"
  })
);

export const deleteLinkError = createAction(
  '[Workflow Details] Delete link error',
  () => ({
    notification: "Module link delete failed"
  })
);

export const updateModule = createAction(
  '[Workflow Details] Update module',
  (data: UpdateWorkflowModuleData) => ({
    payload: data
  })
);

export const updateModuleSuccess = createAction(
  '[Workflow Details] Update module success',
  () => ({
    notification: "Module updated successfully"
  })
);

export const updateModuleError = createAction(
  '[Workflow Details] Update module error',
  () => ({
    notification: "Module update failed"
  })
);

export const loadModuleInputsSuccess = createAction(
  '[Workflow Details] Load module inputs success',
  (workflowModuleInputs: WorkflowModuleInput[], workflowModuleOutputs: WorkflowModuleOutput[]) => ({
    payload: { workflowModuleInputs, workflowModuleOutputs }
  })
);

export const loadModuleInputsError = createAction(
  '[Workflow Details] Load module inputs error',
  (workflowModuleId: string) => ({
    payload: workflowModuleId
  })
);

export const loadModuleOutputsSuccess = createAction(
  '[Workflow Details] Load module outputs success',
  (workflowModuleOutputs: WorkflowModuleOutput[]) => ({
    payload: workflowModuleOutputs
  })
);

export const loadModuleOutputsError = createAction(
  '[Workflow Details] Load module outputs error',
  (workflowModuleId: string) => ({
    payload: workflowModuleId
  })
);


export const updateModuleInput = createAction(
  '[Workflow Details] Update module input',
  (data: UpdateWorkflowModuleInputData) => ({
    payload: data
  })
);

export const updateModuleInputSuccess = createAction(
  '[Workflow Details] Update module input success',
  () => ({
    notification: "Module inputs setup successfully"
  })
);

export const updateModuleInputError = createAction(
  '[Workflow Details] Update module input error',
  () => ({
    notification: "Module inputs setup failed"
  })
);

export const searchModulesOutputs = createAction(
  '[Workflow Details] Search modules outputs',
  (withType: WorkflowModuleIOType) => ({
      payload: withType
  })
)

export const searchModulesOutputsSuccess = createAction(
  '[Workflow Details] Search modules outputs success',
  (workflowModuleOutputs: WorkflowModuleOutput[]) => ({
    payload: workflowModuleOutputs
  })
)

export const searchModulesOutputsError = createAction(
  '[Workflow Details] Search modules outputs error',
)


export const addWorkflowModuleOutput = createAction(
  '[Workflow Details] Add workflow module output',
);

export const addWorkflowModuleOutputSuccess = createAction(
  '[Workflow Details] Add workflow module output success',
  () => ({
    notification: "Module output added successfully"
  })
);

export const addWorkflowModuleOutputError = createAction(
  '[Workflow Details] Add workflow module output error',
  () => ({
    notification: "Module output add failed"
  })
);


export const updateWorkflowModuleOutput = createAction(
  '[Workflow Details] Update workflow module output',
  (data: UpdateWorkflowModuleOutputData) => ({
    payload: data
  })
);

export const updateWorkflowModuleOutputSuccess = createAction(
  '[Workflow Details] Update workflow module output success',
  () => ({
    notification: "Module output updated successfully"
  })
);

export const updateWorkflowModuleOutputError = createAction(
  '[Workflow Details] Update workflow module output error',
  () => ({
    notification: "Module output update failed"
  })
);

export const deleteWorkflowModuleOutput = createAction(
  '[Workflow Details] Delete workflow module output',
  (workflowModuleOutputId: string) => ({
    payload: workflowModuleOutputId
  })
);

export const deleteWorkflowModuleOutputSuccess = createAction(
  '[Workflow Details] Delete workflow module output success',
  (workflowModuleOutputId: string) => ({
    payload: workflowModuleOutputId,
    notification: "Module output deleted successfully"
  })
);

export const deleteWorkflowModuleOutputError = createAction(
  '[Workflow Details] Delete workflow module output error',
  () => ({
    notification: "Module output delete failed"
  })
);

export const activeTool = createAction(
  '[Workflow Details] Active tool',
  (tool: WorkflowDetailsTool) => ({
    payload: tool
  })
);

export const loadWorkflowErrorsSuccess = createAction(
  '[Workflow Details] Load workflow errors success',
  (workflowErrors: WorkflowError[]) => ({
    payload: workflowErrors
  })
);

export const loadWorkflowErrorsError = createAction(
  '[Workflow Details] Load workflow errors error',
);

export const openWarningsTool = createAction(
  '[Workflow Details] Open warnings tool',
);

export const exportWorkflow = createAction(
  '[Workflow Details] Export workflow',
);

export const exportSuccess = createAction(
  '[Workflow Details] Export success',
  () => ({
    notification: "Workflow successfully export"
  })
);

export const exportError = createAction(
  '[Workflow Details] Export error',
  () => ({
    notification: "Workflow failed export"
  })
);

export const deleteFile = createAction(
  '[Workflow Details] Delete input file',
  (moduleInputId: string) => ({
    payload: moduleInputId
  })
);

export const deleteFileSuccess = createAction(
  '[Workflow Details] Delete input file success',() => ({
    notification: "File input deleted successfully"
  })
);

export const deleteFileError = createAction(
  '[Workflow Details] Delete input file error',() => ({
    notification: "File input delete failed"
  })
);

export const updateModuleVersion = createAction(
  '[Workflow Details] Update module version',
  (data: string) => ({
    payload: data
  })
);

export const updateModuleVersionSuccess = createAction(
  '[Workflow Details] Update module version success',
  (moduleId: UpdateModuleVersionResponse) => ({
    payload: moduleId,
    notification: "Module version setup successfully"
  })
);

export const updateModuleVersionError = createAction(
  '[Workflow Details] Update module version error',
  () => ({
    notification: "Module version setup failed"
  })
);

export const loadModuleVersionsSuccess = createAction(
  '[Workflow Details] Load workflow module versions success',
  (versions: WorkflowModuleVersion[]) => ({
    payload: versions
  })
);

export const loadModuleVersionsError = createAction(
  '[Workflow Details] Load workflow module versions error',
);

export const loadPortals = createAction(
  '[Workflow Details] Load portals',
  (workflowId: string) => ({
    payload: { workflowId }
  })
);

export const loadPortalsSuccess = createAction(
  '[Workflow Details] Load portals success',
  (workflowId: string, workflowPortals: WorkflowPortal[]) => ({
    payload: { workflowId, workflowPortals }
  })
);

export const loadPortalsError = createAction(
  '[Workflow Details] Load portals error',
  (workflowId: string) => ({
    payload: { workflowId }
  })
);
