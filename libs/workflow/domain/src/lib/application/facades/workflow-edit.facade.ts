// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { WorkflowEditData } from '../../entities';
import { selectEditMode, selectEditWorkflow, WorkflowState } from '../reducers';
import { WorkflowEditActions } from '../actions';

@Injectable()
export class WorkflowEditFacade {
  mode$ = this.store.select(selectEditMode);
  workflow$ = this.store.select(selectEditWorkflow);

  constructor(
    private readonly store: Store<WorkflowState>
  ) {}

  save(data: WorkflowEditData) {
    this.store.dispatch(WorkflowEditActions.save(data));
  }

  close() {
    this.store.dispatch(WorkflowEditActions.close())
  }
}
