// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Workflow, WorkflowFilter } from '../../entities';
import { WorkflowListActions } from '../actions';
import {
  selectDeleteModalVisible,
  selectImportedWorkflowId,
  selectImportLoading,
  selectRetryImportLoading,
  selectImportModalVisible,
  selectImportSuccessModalVisible,
  selectImportErrorModalVisible,
  selectListFilter,
  selectListLoading,
  selectListWorkflowPortals,
  selectListWorkflows,
  selectSelectedWorkflow,
  selectImportErrorMessage,
  WorkflowState,
} from '../reducers';
import { Router } from '@angular/router';

@Injectable()
export class WorkflowListFacade {
  loading$ = this.store.select(selectListLoading);
  importLoading$ = this.store.select(selectImportLoading);
  retryImportLoading$ = this.store.select(selectRetryImportLoading);
  filter$ = this.store.select(selectListFilter);
  workflows$ = this.store.select(selectListWorkflows);
  selectedWorkflow$ = this.store.select(selectSelectedWorkflow);
  importedWorkflowId$ = this.store.select(selectImportedWorkflowId);
  deleteModalVisible$ = this.store.select(selectDeleteModalVisible);
  importModalVisible$ = this.store.select(selectImportModalVisible);
  importSuccessModalVisible$ = this.store.select(selectImportSuccessModalVisible);
  importErrorModalVisible$ = this.store.select(selectImportErrorModalVisible);
  importErrorMessage$ = this.store.select(selectImportErrorMessage);

  constructor(
    private readonly store: Store<WorkflowState>,
    private readonly router: Router
  ) {}

  getWorkflowPortals$(workflowId: string) {
    return this.store.select(selectListWorkflowPortals, workflowId);
  }

  init() {
    this.store.dispatch(WorkflowListActions.init())
  }

  exit() {
    this.store.dispatch(WorkflowListActions.exit());
  }

  refresh() {
    this.store.dispatch(WorkflowListActions.refresh());
  }

  addWorkflow() {
    this.store.dispatch(WorkflowListActions.addWorkflow());
  }

  copyWorkflow(workflow: Workflow) {
    this.store.dispatch(WorkflowListActions.copyWorkflow(workflow.id));
  }

  editWorkflow(workflow: Workflow) {
    this.store.dispatch(WorkflowListActions.editWorkflow(workflow.id));
  }

  deployWorkflow(workflow: Workflow) {
    this.store.dispatch(WorkflowListActions.deployWorkflow(workflow.id));
  }

  stopWorkflow(workflow: Workflow) {
    this.store.dispatch(WorkflowListActions.stopWorkflow(workflow.id));
  }

  filter(data: WorkflowFilter) {
    this.store.dispatch(WorkflowListActions.filter(data));
  }

  //replace deleteWorkflow
  openDeleteWorkflowModal(workflowId: string) {
    this.store.dispatch(WorkflowListActions.openDeleteWorkflowModal(workflowId));
  }

  validateDeleteWorkflow(workflowId: string) {
    this.store.dispatch(WorkflowListActions.validateDeleteWorkflow(workflowId));
  }

  cancelDeleteWorkflow() {
    this.store.dispatch(WorkflowListActions.cancelDeleteWorkflow());
  }

  loadWorkflowPortals(workflowId: string) {
    this.store.dispatch(WorkflowListActions.loadPortals(workflowId));
  }

  importWorkflow(file: File) {
    this.store.dispatch(WorkflowListActions.importWorkflow(file));
  }

  openImportModal() {
    this.store.dispatch(WorkflowListActions.openImportWorkflowModal());
  }

  closeImportModal() {
    this.store.dispatch(WorkflowListActions.closeImportWorkflowModal());
  }

  navigateToImportedWorkflow(workflowId: string) {
    this.router.navigate(['/', 'studio', 'workflows', workflowId]);
    this.closeImportSuccessModal();
  }

  closeImportSuccessModal() {
    this.store.dispatch(WorkflowListActions.closeImportSuccessWorkflowModal());
  }

  closeImportErrorModal() {
    this.store.dispatch(WorkflowListActions.closeImportErrorWorkflowModal());
  }
}
