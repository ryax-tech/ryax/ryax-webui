// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import {
  selectDetailsActiveTool,
  selectDetailsActiveWorkflowModule,
  selectDetailsActiveWorkflowModuleId,
  selectDetailsActiveWorkflowModuleInputs,
  selectDetailsActiveWorkflowModuleOutputs,
  selectDetailsDeleteModalVisible,
  selectDetailsSearchWorkflowModuleOutputsIds,
  selectDetailsWorkflow,
  selectDetailsWorkflowErrors,
  selectDetailsWorkflowModuleLinks,
  selectDetailsWorkflowModules,
  selectDetailsWorkflowModulesOutputs,
  selectLoading,
  selectLoadingInputs,
  selectLoadingOutputs,
  selectLoadingWorkflowErrors,
  selectExportLoading,
  selectExportModalVisible,
  selectDetailsActiveWorkflowModuleVersions,
  WorkflowState, selectListWorkflowPortals,
} from '../reducers';
import {
  AddWorkflowModuleData,
  MoveWorkflowModuleData,
  UpdateWorkflowModuleData,
  UpdateWorkflowModuleInputData,
  UpdateWorkflowModuleOutputData,
  WorkflowDetailsTool,
  WorkflowModuleIOType,
} from '../../entities';
import { WorkflowDetailsActions } from '../actions';

@Injectable()
export class WorkflowDetailsFacade {
  loading$ = this.store.select(selectLoading);
  exportLoading$ = this.store.select(selectExportLoading);
  loadingInputs$ = this.store.select(selectLoadingInputs);
  loadingOutputs$ = this.store.select(selectLoadingOutputs);
  loadingWorkflowErrors$ = this.store.select(selectLoadingWorkflowErrors);
  workflow$ = this.store.select(selectDetailsWorkflow);
  workflowModules$ = this.store.select(selectDetailsWorkflowModules);
  workflowModuleLinks$ = this.store.select(selectDetailsWorkflowModuleLinks);
  workflowModulesOutputs$ = this.store.select(selectDetailsWorkflowModulesOutputs);
  workflowErrors$ = this.store.select(selectDetailsWorkflowErrors);
  activeTool$ = this.store.select(selectDetailsActiveTool);
  activeWorkflowModuleId$ = this.store.select(selectDetailsActiveWorkflowModuleId);
  activeWorkflowModule$ = this.store.select(selectDetailsActiveWorkflowModule);
  activeWorkflowModuleInputs$ = this.store.select(selectDetailsActiveWorkflowModuleInputs);
  activeWorkflowModuleOutputs$ = this.store.select(selectDetailsActiveWorkflowModuleOutputs);
  activeWorkflowModuleVersions$ = this.store.select(selectDetailsActiveWorkflowModuleVersions);
  deleteModalVisible$ = this.store.select(selectDetailsDeleteModalVisible);
  searchWorkflowModuleOutputsIds$ = this.store.select(selectDetailsSearchWorkflowModuleOutputsIds);
  exportModalVisible$ = this.store.select(selectExportModalVisible);

  constructor(
    private readonly store: Store<WorkflowState>
  ) {}

  getWorkflowPortals$(workflowId: string) {
    return this.store.select(selectListWorkflowPortals, workflowId);
  }

  init() {
    this.store.dispatch(WorkflowDetailsActions.init());
  }

  exit() {
    this.store.dispatch(WorkflowDetailsActions.exit());
  }

  refresh() {
    this.store.dispatch(WorkflowDetailsActions.refresh());
  }

  copyWorkflow() {
    this.store.dispatch(WorkflowDetailsActions.copyWorkflow());
  }

  editWorkflow() {
    this.store.dispatch(WorkflowDetailsActions.editWorkflow());
  }

  deployWorkflow() {
    this.store.dispatch(WorkflowDetailsActions.deployWorkflow());
  }

  stopWorkflow() {
    this.store.dispatch(WorkflowDetailsActions.stopWorkflow());
  }

  activeTool(tool: WorkflowDetailsTool) {
    this.store.dispatch(WorkflowDetailsActions.activeTool(tool));
  }

  openWarningsTool() {}

  openedTool(data: any) {}

  selectModule(moduleId: string) {
    this.store.dispatch(WorkflowDetailsActions.selectModule(moduleId));
  }

  unselectModule() {
    this.store.dispatch(WorkflowDetailsActions.unselectModule());
  }

  addModule(data: AddWorkflowModuleData) {
    this.store.dispatch(WorkflowDetailsActions.addModule(data));
  }

  moveModule(data: MoveWorkflowModuleData) {
    this.store.dispatch(WorkflowDetailsActions.moveModule(data));
  }

  deleteModule(moduleId: string) {
    this.store.dispatch(WorkflowDetailsActions.deleteModule(moduleId));
  }

  addLink(inputModuleId: string, outputModuleId: string) {
    this.store.dispatch(WorkflowDetailsActions.addLink(inputModuleId, outputModuleId));
  }

  deleteLink(linkId: string) {
    this.store.dispatch(WorkflowDetailsActions.deleteLink(linkId));
  }

  updateModulePropertiesSetup(data: UpdateWorkflowModuleData) {
    this.store.dispatch(WorkflowDetailsActions.updateModule(data))
  }

  updateModuleInputSetup(data: UpdateWorkflowModuleInputData) {
    this.store.dispatch(WorkflowDetailsActions.updateModuleInput(data));
  }

  searchModulesOutputs(withType: WorkflowModuleIOType) {
    this.store.dispatch(WorkflowDetailsActions.searchModulesOutputs(withType));
  }

  openDeleteWorkflowModal() {
    this.store.dispatch(WorkflowDetailsActions.openDeleteWorkflowModal());
  }

  validateDeleteWorkflow() {
    this.store.dispatch(WorkflowDetailsActions.validateDeleteWorkflow());
  }

  cancelDeleteWorkflow() {
    this.store.dispatch(WorkflowDetailsActions.cancelDeleteWorkflow());
  }

  addWorkflowModuleOutput() {
    this.store.dispatch(WorkflowDetailsActions.addWorkflowModuleOutput());
  }

  updateWorkflowModuleOutput(data: UpdateWorkflowModuleOutputData) {
    this.store.dispatch(WorkflowDetailsActions.updateWorkflowModuleOutput(data));
  }

  deleteWorkflowModuleOutput(workflowModuleOutputId: string) {
    this.store.dispatch(WorkflowDetailsActions.deleteWorkflowModuleOutput(workflowModuleOutputId));
  }

  exportWorkflow() {
    this.store.dispatch(WorkflowDetailsActions.exportWorkflow());
  }

  deleteFile(moduleInputId: string) {
    this.store.dispatch(WorkflowDetailsActions.deleteFile(moduleInputId));
  }

  updateModuleVersion(moduleId: string) {
    this.store.dispatch(WorkflowDetailsActions.updateModuleVersion(moduleId));
  }

  /*

  editSetupOutput(data: WorkflowDetailsSetupOutputsItemDataView) {
    const { name, humanName, type, help } = data;
    this.store.dispatch(WorkflowDetailsActions.editSetupOutput(name, humanName, type, help));
  }

  deleteSetupOutput(data: WorkflowDetailsSetupOutputsItemDataView) {
    const { name } = data;
    this.store.dispatch(WorkflowDetailsActions.deleteSetupOutput(name));
  }
   */
}
