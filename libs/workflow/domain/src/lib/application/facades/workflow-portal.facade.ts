// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import {
  selectPortalLoading,
  selectPortalWorkflowPortal,
  selectPortalWorkflowPortalOutputs, selectPortalWorkflowPortalResult, selectUnsupportedExtensionModalVisibility,
  WorkflowState,
} from '../reducers';
import { WorkflowPortalActions } from '../actions';

@Injectable()
export class WorkflowPortalFacade {
  loading$ = this.store.select(selectPortalLoading);
  workflowPortal$ = this.store.select(selectPortalWorkflowPortal);
  workflowPortalOutputs$ = this.store.select(selectPortalWorkflowPortalOutputs);
  workflowPortalResult$ = this.store.select(selectPortalWorkflowPortalResult);
  unsupportedExtensionModalVisibility$ = this.store.select(selectUnsupportedExtensionModalVisibility);

  constructor(
    private readonly store: Store<WorkflowState>
  ) {}

  init() {
    this.store.dispatch(WorkflowPortalActions.init());
  }

  sendData(data: object) {
    this.store.dispatch(WorkflowPortalActions.send(data));
  }

  back() {
    this.store.dispatch(WorkflowPortalActions.back())
  }

  openUnsupportedExtensionModal() {
    this.store.dispatch(WorkflowPortalActions.openUnsupportedExtensionModal())
  }

  closeUnsupportedExtensionModal() {
    this.store.dispatch(WorkflowPortalActions.closeUnsupportedExtensionModal())
  }
}
