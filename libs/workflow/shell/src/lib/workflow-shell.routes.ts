// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { WorkflowListComponent } from '@ryax/workflow/feature-list';
import { CanAccessGuard, WorkflowDetailsComponent } from '@ryax/workflow/feature-details';
import { WorkflowShellComponent } from './workflow-shell/workflow-shell.component';

export const routes = [
  // Worfklows segment definition
  {
    path: 'workflows',
    component: WorkflowShellComponent,
    data: {
      breadcrumb: null
    },
    children: [
      {
        path: '',
        component: WorkflowListComponent
      },
      {
        path: ':workflowId',
        data: {
          breadcrumb: 'Editor'
        },
        component: WorkflowDetailsComponent,
        canActivate: [CanAccessGuard],
      },
    ]
  },


  // Index segment redirection
  {
    path: '',
    patchMatch: "full",
    redirectTo: "workflows"
  }
];
