// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { WorkflowFeatureListModule } from '@ryax/workflow/feature-list';
import { WorkflowFeatureDetailsModule } from '@ryax/workflow/feature-details';
import { WorkflowFeatureEditModule } from '@ryax/workflow/feature-edit';
import { WorkflowShellComponent } from './workflow-shell/workflow-shell.component';
import { routes } from './workflow-shell.routes';

@NgModule({
  imports: [
    CommonModule,
    WorkflowFeatureListModule,
    WorkflowFeatureDetailsModule,
    WorkflowFeatureEditModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    WorkflowShellComponent
  ]
})
export class WorkflowShellModule {}
