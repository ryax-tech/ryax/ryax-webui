// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WorkflowUiModule } from '@ryax/workflow/ui';
import { WorkflowDomainModule } from '@ryax/workflow/domain';
import { WorkflowListComponent } from './workflow-list/workflow-list.component';
import { WorkflowListTableComponent } from './workflow-list-table/workflow-list-table.component';
import { WorkflowListHeaderComponent } from './workflow-list-header/workflow-list-header.component';
import { WorkflowListFilterComponent } from './workflow-list-filter/workflow-list-filter.component';
import { WorkflowListTableRowComponent } from './workflow-list-table-row/workflow-list-table-row.component';

@NgModule({
  imports: [
    CommonModule,
    WorkflowUiModule,
    WorkflowDomainModule
  ],
  declarations: [
    WorkflowListComponent,
    WorkflowListHeaderComponent,
    WorkflowListTableComponent,
    WorkflowListTableRowComponent,
    WorkflowListFilterComponent,
  ],
  exports: [
    WorkflowListComponent
  ]
})
export class WorkflowFeatureListModule {}
