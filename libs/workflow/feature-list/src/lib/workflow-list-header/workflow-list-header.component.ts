// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'ryax-workflow-list-header',
  templateUrl: './workflow-list-header.component.pug'
})
export class WorkflowListHeaderComponent {
  @Output() addItem = new EventEmitter<void>();
  @Output() refresh = new EventEmitter<void>();
  @Output() openImportModal = new EventEmitter<void>();

  onAddItem() {
    this.addItem.emit();
  }

  onRefresh() {
    this.refresh.emit();
  }

  onOpenImportModal() {
    this.openImportModal.emit()
  }
}
