// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Workflow, WorkflowFilter, WorkflowListFacade } from '@ryax/workflow/domain';

@Component({
  selector: 'ryax-workflow-list',
  templateUrl: './workflow-list.component.pug'
})
export class WorkflowListComponent implements OnInit, OnDestroy {
  loading$ = this.facade.loading$;
  importLoading$ = this.facade.importLoading$;
  retryImportLoading$ = this.facade.retryImportLoading$;
  filter$ = this.facade.filter$;
  workflows$ = this.facade.workflows$;
  selectedWorkflow$ = this.facade.selectedWorkflow$;
  importedWorkflowId$ = this.facade.importedWorkflowId$;
  deleteModalVisible$ = this.facade.deleteModalVisible$;
  importModalVisible$ = this.facade.importModalVisible$;
  importSuccessModalVisible$ = this.facade.importSuccessModalVisible$;
  importErrorModalVisible$ = this.facade.importErrorModalVisible$;
  importErrorMessage$ = this.facade.importErrorMessage$;

  constructor(
    private readonly facade: WorkflowListFacade
  ) {}

  getWorkflowPortals$(workflowId: string) {
    return this.facade.getWorkflowPortals$(workflowId);
  }

  ngOnInit(): void {
    this.facade.init();
  }

  ngOnDestroy(): void {
    this.facade.exit();
  }

  onRefresh() {
    this.facade.refresh();
  }

  onAddItem() {
    this.facade.addWorkflow();
  }

  onCopyWorkflow(workflow: Workflow) {
    this.facade.copyWorkflow(workflow);
  }

  onEditWorkflow(workflow: Workflow) {
    this.facade.editWorkflow(workflow);
  }

  onDeployWorkflow(workflow: Workflow) {
    this.facade.deployWorkflow(workflow);
  }

  onStopWorkflow(workflow: Workflow) {
    this.facade.stopWorkflow(workflow);
  }

  onFilterChange(data: WorkflowFilter) {
    this.facade.filter(data);
  }

  //replace onDeleteItem
  showDeleteModal(workflow: Workflow) {
    this.facade.openDeleteWorkflowModal(workflow.id);
  }

  validateDeleteWorkflow(workflowId: string) {
    this.facade.validateDeleteWorkflow(workflowId);
  }

  cancelDeleteWorkflow() {
    this.facade.cancelDeleteWorkflow();
  }

  onLoadWorkflowPortals(workflowId: string) {
    this.facade.loadWorkflowPortals(workflowId);
  }

  onImport(file: File) {
    this.facade.importWorkflow(file);
  }

  openImportModal() {
    this.facade.openImportModal();
  }

  cancelWorkflowImport() {
    this.facade.closeImportModal();
  }

  validateImportSuccessModal(workflowId: string) {
    this.facade.navigateToImportedWorkflow(workflowId)
  }

  cancelImportSuccessModal() {
    this.facade.closeImportSuccessModal();
  }

  cancelImportErrorModal() {
    this.facade.closeImportErrorModal();
  }
}
