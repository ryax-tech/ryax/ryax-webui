// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Workflow, WorkflowDeploymentStatus, WorkflowPortal, WorkflowStatus } from '@ryax/workflow/domain';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'tr[ryax-workflow-list-table-row]',
  templateUrl: './workflow-list-table-row.component.pug',
  styleUrls: ['./workflow-list-table-row.component.scss']
})
export class WorkflowListTableRowComponent {
  @Input() workflow: Workflow;
  @Input() workflowPortals: WorkflowPortal[] = [];
  @Output() loadWorkflowPortals = new EventEmitter<string>();
  @Output() editWorkflow = new EventEmitter<Workflow>();
  @Output() copyWorkflow = new EventEmitter<Workflow>();
  @Output() deleteWorkflow = new EventEmitter<Workflow>();
  @Output() deployWorkflow = new EventEmitter<Workflow>();
  @Output() stopWorkflow = new EventEmitter<Workflow>();
  @Output() showDeleteModal = new EventEmitter<Workflow>();

  get hasWorkflowPortals() {
    return this.workflowPortals.length;
  }

  get isDeployDisabled() {
    return this.workflow.status === WorkflowStatus.INVALID ||
      this.workflow.deploymentStatus !== WorkflowDeploymentStatus.NONE;
  }

  get isStopDisabled() {
    return this.workflow.deploymentStatus === WorkflowDeploymentStatus.NONE ||
      this.workflow.deploymentStatus === WorkflowDeploymentStatus.UNDEPLOYING;
  }

  get isDeleteDisabled() {
    return this.workflow.deploymentStatus === WorkflowDeploymentStatus.DEPLOYING ||
      this.workflow.deploymentStatus === WorkflowDeploymentStatus.DEPLOYED;
  }

  get isEditDisabled() {
    return this.workflow.deploymentStatus === WorkflowDeploymentStatus.DEPLOYING ||
      this.workflow.deploymentStatus === WorkflowDeploymentStatus.DEPLOYED;
  }

  get isWorkflowStatusVisible() {
    return this.workflow.deploymentStatus === WorkflowDeploymentStatus.NONE
  }

  get isWorkflowDeployStatusVisible() {
    return this.workflow.deploymentStatus !== WorkflowDeploymentStatus.NONE
  }

  onEditItem() {
    this.editWorkflow.emit(this.workflow);
  }

  onCopyItem() {
    this.copyWorkflow.emit(this.workflow);
  }

  onDeployItem() {
    this.deployWorkflow.emit(this.workflow);
  }

  onStopItem() {
    this.stopWorkflow.emit(this.workflow);
  }

  onShowDeleteModal() {
    this.showDeleteModal.emit(this.workflow);
  }

  onPortalDropdownVisibleChange(isVisible: boolean) {
    if(isVisible) this.loadWorkflowPortals.emit(this.workflow.id);
  }
}
