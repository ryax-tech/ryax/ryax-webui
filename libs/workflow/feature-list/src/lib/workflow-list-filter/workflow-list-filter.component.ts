// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, takeUntil } from 'rxjs/operators';
import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { WorkflowFilter, WorkflowStatusFilter } from '@ryax/workflow/domain';

@Component({
  selector: 'ryax-workflow-list-filter',
  templateUrl: './workflow-list-filter.component.pug',
  styleUrls: ['./workflow-list-filter.component.scss']
})
export class WorkflowListFilterComponent implements OnInit, OnDestroy, OnChanges {
  destroy$ = new Subject<void>();
  form = this.formBuilder.group({
    term: this.formBuilder.control(null),
    status: this.formBuilder.control(WorkflowStatusFilter.All)
  });

  @Input() filter: WorkflowFilter;
  @Output() filterChange = new EventEmitter<WorkflowFilter>();

  constructor(
    private readonly formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.form.valueChanges.pipe(
      debounceTime(400),
      distinctUntilChanged(),
      takeUntil(this.destroy$),
    ).subscribe(this.filterChange);
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes.filter)
      this.form.patchValue(changes.filter.currentValue, { emitEvent: false });
  }

  onClearTerm() {
    this.form.patchValue({ term: null });
  }
}
