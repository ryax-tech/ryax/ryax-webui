// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, Input, TemplateRef } from '@angular/core';
import { Workflow } from '@ryax/workflow/domain';

@Component({
  selector: 'ryax-workflow-list-table',
  templateUrl: './workflow-list-table.component.pug',
})
export class WorkflowListTableComponent {
  @Input() loading: boolean;
  @Input() items: Workflow[];
  @Input() itemTemplate: TemplateRef<any>;

  trackItemById(index: number, item: Workflow) {
    return item.id;
  }
}
