// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'ryax-unsupported-extension-modal',
  templateUrl: './unsupported-extension-modal.component.pug'
})
export class UnsupportedExtensionModalComponent {
  @Input() visible: boolean;
  @Output() cancel = new EventEmitter<void>();

  supportedExtensions = [
    'xlsx', 'xlsm', 'xlsb', 'biff8', 'biff5', 'biff2', 'xlml', 'ods', 'fods', 'csv', 'txt', 'sylk', 'html', 'dif', 'dbf', 'rtf', 'prn', 'eth'
  ];

  handleCancel() {
    this.cancel.emit();
  }
}
