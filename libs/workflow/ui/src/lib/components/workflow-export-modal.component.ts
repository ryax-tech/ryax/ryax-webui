// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'ryax-workflow-export-modal',
  templateUrl: './workflow-export-modal.component.pug'
})
export class WorkflowExportModalComponent {
  @Input() visible: boolean;
  @Input() exportLoading: boolean;
  @Output() cancel = new EventEmitter<void>();
  @Output() valid = new EventEmitter<string>();
}
