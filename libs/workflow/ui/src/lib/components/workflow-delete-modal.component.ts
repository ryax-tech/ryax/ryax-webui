// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Workflow } from '@ryax/workflow/domain';

@Component({
  selector: 'ryax-workflow-delete-modal',
  templateUrl: './workflow-delete-modal.component.pug'
})
export class WorkflowDeleteModalComponent {
  @Input() visible: boolean;
  @Input() workflow: Workflow;
  @Output() cancel = new EventEmitter<void>();
  @Output() valid = new EventEmitter<string>();


  handleCancel() {
    this.cancel.emit();
  }

  handleValid() {
    this.valid.emit(this.workflow.id);
  }
}
