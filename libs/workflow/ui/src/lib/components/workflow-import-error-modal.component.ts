// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'ryax-workflow-import-error-modal',
  templateUrl: './workflow-import-error-modal.component.pug',
  styleUrls: ['./workflow-import-error-modal.component.scss']
})
export class WorkflowImportErrorModalComponent {
  @Input() visible: boolean;
  @Input() loading: boolean;
  @Input() error: string;
  @Output() openImportModal = new EventEmitter<void>();
  @Output() cancel = new EventEmitter<void>();

  onOpenImportModal() {
    this.openImportModal.emit()
  }

  handleCancel() {
    this.cancel.emit();
  }
}
