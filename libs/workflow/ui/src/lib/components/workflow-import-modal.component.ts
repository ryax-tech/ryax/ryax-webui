// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'ryax-workflow-import-modal',
  templateUrl: './workflow-import-modal.component.pug'
})
export class WorkflowImportModalComponent {
  @Input() visible: boolean;
  @Input() loading: boolean;
  @Output() import = new EventEmitter<File>();
  @Output() valid = new EventEmitter<void>();
  @Output() cancel = new EventEmitter<void>();

  get isLoading() {
    return this.loading;
  }

  onImport(file: File) {
    this.import.emit(file);
  }

  handleCancel() {
    if(!this.isLoading)
      this.cancel.emit();
  }
}
