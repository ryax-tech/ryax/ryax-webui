// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'ryax-workflow-import-success-modal',
  templateUrl: './workflow-import-success-modal.component.pug'
})
export class WorkflowImportSuccesModalComponent {
  @Input() visible: boolean;
  @Input() workflowId: string;
  @Output() valid = new EventEmitter<string>();
  @Output() cancel = new EventEmitter<void>();

  handleOk(workflowId: string) {
    this.valid.emit(workflowId);
  }

  handleCancel() {
    this.cancel.emit();
  }
}
