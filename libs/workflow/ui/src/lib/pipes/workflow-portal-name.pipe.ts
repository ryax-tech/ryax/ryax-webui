// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Pipe, PipeTransform } from '@angular/core';
import { WorkflowPortal } from '@ryax/workflow/domain';

@Pipe({
  name: 'ryaxWorkflowPortalName',
  pure: true
})
export class WorkflowPortalNamePipe implements PipeTransform {
  transform(value: WorkflowPortal): string {
    if(value) {
      return value.customName ? value.customName : value.name;
    } else {
      return null;
    }
  }
}
