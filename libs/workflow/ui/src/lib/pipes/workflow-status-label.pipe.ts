// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Pipe, PipeTransform } from '@angular/core';
import { WorkflowStatus } from '@ryax/workflow/domain';

@Pipe({
  name: 'ryaxWorkflowStatusLabel',
  pure: true
})
export class WorkflowStatusLabelPipe implements PipeTransform {
  private config = {
    [WorkflowStatus.INVALID]: "Invalid",
    [WorkflowStatus.VALID]: "Valid",
  };

  transform(value: WorkflowStatus): string {
    if(value in this.config) {
      return this.config[value];
    } else {
      return undefined;
    }
  }
}
