// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Pipe, PipeTransform } from '@angular/core';
import { WorkflowErrorCode } from '@ryax/workflow/domain';

@Pipe({
  name: 'ryaxWorkflowWarningMessage',
  pure: true
})
export class WorkflowWarningMessagePipe implements PipeTransform {

  private config = {
    [WorkflowErrorCode.WORKFLOW_NOT_CONNECTED]: "Some modules are not linked",
    [WorkflowErrorCode.WORKFLOW_HAS_CYCLES]: "Workflow containing loops are not allowed",
    [WorkflowErrorCode.WORKFLOW_MODULES_UPPER_LIMIT_REACHED]: "There is too much module in the workflow, limit is 128",
    [WorkflowErrorCode.WORKFLOW_SOURCES_LOWER_LIMIT_REACHED]: "There should be at least one source module",
    [WorkflowErrorCode.WORKFLOW_MODULE_LINK_INPUT_UPPER_LIMIT_REACHED]: "Module can't have so many input streams",
    [WorkflowErrorCode.WORKFLOW_MODULE_LINK_INPUT_LOWER_LIMIT_REACHED]: "Module can't have so few input streams",
    [WorkflowErrorCode.WORKFLOW_MODULE_LINK_OUTPUT_UPPER_LIMIT_REACHED]: "Module can't have so many output streams",
    [WorkflowErrorCode.WORKFLOW_MODULE_LINK_OUTPUT_LOWER_LIMIT_REACHED]: "Module need have so many output streams",
    [WorkflowErrorCode.WORKFLOW_MODULE_INPUTS_NOT_DEFINED]: "Some input values are not defined",
  };

  transform(value: WorkflowErrorCode): string {
    if(value in this.config) {
      return this.config[value];
    } else {
      return `Unknown warning (${value})`;
    }
  }
}
