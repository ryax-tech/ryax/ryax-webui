// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Pipe, PipeTransform } from '@angular/core';
import { WorkflowModuleKind } from '@ryax/workflow/domain';

@Pipe({
  name: 'ryaxWorkflowModuleKindLabel',
  pure: true
})
export class WorkflowModuleKindLabelPipe implements PipeTransform {
  transform(value: WorkflowModuleKind): string {
    switch (value) {
      case WorkflowModuleKind.SOURCE:
        return "Source";
      case WorkflowModuleKind.PROCESSOR:
        return "Processor";
      case WorkflowModuleKind.STREAM_OPERATOR:
        return "Streaming Operator";
      case WorkflowModuleKind.PUBLISHER:
        return "Publisher";
      default:
        return null;
    }
  }
}
