// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Pipe, PipeTransform } from '@angular/core';
import { WorkflowModuleKind } from '@ryax/workflow/domain';

@Pipe({
  name: 'ryaxWorkflowModuleKindColor',
  pure: true
})
export class WorkflowModuleKindColorPipe implements PipeTransform {
  transform(value: WorkflowModuleKind): string {
    switch (value) {
      case WorkflowModuleKind.SOURCE:
        return 'green';
      case WorkflowModuleKind.PROCESSOR:
        return 'blue';
      case WorkflowModuleKind.PUBLISHER:
        return 'cyan';
      case WorkflowModuleKind.STREAM_OPERATOR:
        return 'purple';
      default:
        return null;
    }
  }
}
