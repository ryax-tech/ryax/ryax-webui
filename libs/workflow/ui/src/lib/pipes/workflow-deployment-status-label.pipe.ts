// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Pipe, PipeTransform } from '@angular/core';
import { WorkflowDeploymentStatus } from '@ryax/workflow/domain';

@Pipe({
  name: 'ryaxWorkflowDeploymentStatusLabel',
  pure: true
})
export class WorkflowDeploymentStatusLabelPipe implements PipeTransform {
  private config = {
    [WorkflowDeploymentStatus.DEPLOYING]: "Deploying",
    [WorkflowDeploymentStatus.DEPLOYED]: "Deployed",
    [WorkflowDeploymentStatus.UNDEPLOYING]: "Undeploying",
  };

  transform(value: WorkflowDeploymentStatus): string {
    if(value in this.config) {
      return this.config[value];
    } else {
      return undefined;
    }
  }
}
