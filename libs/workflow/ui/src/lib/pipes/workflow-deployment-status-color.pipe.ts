// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Pipe, PipeTransform } from '@angular/core';
import { WorkflowDeploymentStatus } from '@ryax/workflow/domain';

@Pipe({
  name: 'ryaxWorkflowDeploymentStatusColor',
  pure: true
})
export class RyaxWorkflowDeploymentStatusColorPipe implements PipeTransform {
  private config = {
    [WorkflowDeploymentStatus.DEPLOYING]: "blue",
    [WorkflowDeploymentStatus.DEPLOYED]: "green",
    [WorkflowDeploymentStatus.UNDEPLOYING]: "yellow",
  };

  transform(value: WorkflowDeploymentStatus): string {
    if(value in this.config) {
      return this.config[value];
    } else {
      return undefined;
    }
  }
}
