// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedUiCommonModule } from '@ryax/shared/ui-common';
import { WorkflowStatusLabelPipe } from './pipes/workflow-status-label.pipe';
import { WorkflowStatusColorPipe } from './pipes/workflow-status-color.pipe';
import { WorkflowDeleteModalComponent } from './components/workflow-delete-modal.component';
import { WorkflowWarningMessagePipe } from './pipes/workflow-warning-message.pipe';
import { WorkflowDeploymentStatusLabelPipe } from './pipes/workflow-deployment-status-label.pipe';
import { RyaxWorkflowDeploymentStatusColorPipe } from './pipes/workflow-deployment-status-color.pipe';
import { WorkflowPortalNamePipe } from './pipes/workflow-portal-name.pipe';
import { WorkflowModuleKindColorPipe } from './pipes/workflow-module-kind-color.pipe';
import { WorkflowModuleKindLabelPipe } from './pipes/workflow-module-kind-label.pipe';
import { WorkflowImportModalComponent } from './components/workflow-import-modal.component';
import { WorkflowImportSuccesModalComponent } from './components/workflow-import-success-modal.component';
import { WorkflowImportErrorModalComponent } from './components/workflow-import-error-modal.component';
import { WorkflowExportModalComponent } from './components/workflow-export-modal.component';
import { UnsupportedExtensionModalComponent } from './components/unsupported-extension-modal.component';


@NgModule({
  imports: [
    CommonModule,
    SharedUiCommonModule
  ],
  declarations: [
    WorkflowStatusLabelPipe,
    WorkflowStatusColorPipe,
    WorkflowDeploymentStatusLabelPipe,
    RyaxWorkflowDeploymentStatusColorPipe,
    WorkflowDeleteModalComponent,
    WorkflowWarningMessagePipe,
    WorkflowPortalNamePipe,
    WorkflowModuleKindColorPipe,
    WorkflowModuleKindLabelPipe,
    WorkflowImportModalComponent,
    WorkflowImportSuccesModalComponent,
    WorkflowImportErrorModalComponent,
    WorkflowExportModalComponent,
    UnsupportedExtensionModalComponent
  ],
  exports: [
    SharedUiCommonModule,
    WorkflowStatusLabelPipe,
    WorkflowStatusColorPipe,
    WorkflowDeleteModalComponent,
    WorkflowWarningMessagePipe,
    WorkflowDeploymentStatusLabelPipe,
    RyaxWorkflowDeploymentStatusColorPipe,
    WorkflowPortalNamePipe,
    WorkflowImportModalComponent,
    WorkflowImportSuccesModalComponent,
    WorkflowImportErrorModalComponent,
    WorkflowExportModalComponent,
    WorkflowModuleKindLabelPipe,
    WorkflowModuleKindColorPipe,
    UnsupportedExtensionModalComponent
  ],
})
export class WorkflowUiModule {}
