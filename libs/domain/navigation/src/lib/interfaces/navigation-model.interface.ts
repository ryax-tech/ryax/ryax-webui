// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
export type NavigationUrl = string;

export interface NavigationParams {
  [key: string]: string
}

export interface NavigationQuery {
  [key: string]: string
}

export const NavigationUtils = {
  getIndexUrl: () => '/',
  getErrorUrl:  () => '/error',
  getForbiddenUrl:  () => '/error/forbidden',
  getLoginUrl: () => '/login',
  getExecutionListUrl: () => `/workflows/monitoring`,
  getExecutionListQuery: (workflow: string, module: string, status: string = null) => ({
    workflow: workflow || undefined,
    module: module || undefined,
    status: status || undefined
  }),
  getDeploymentListUrl: () => "/workflows/deployments",
  getDeploymentDetailUrl: (deploymentId: string) => `/workflows/deployments/${deploymentId}`
};
