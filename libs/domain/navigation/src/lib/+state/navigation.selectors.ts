// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createFeatureSelector } from '@ngrx/store';
import * as fromRouter from '@ngrx/router-store';
import { NavigationFeatureKey } from './navigation.reducer';

const selectFeatureState = createFeatureSelector<fromRouter.RouterReducerState>(NavigationFeatureKey);

const {
  selectUrl,
  selectRouteParams,
  selectQueryParams
} = fromRouter.getSelectors(selectFeatureState);

export const NavigationQueries = {
  selectUrl,
  selectParams: selectRouteParams,
  selectQuery: selectQueryParams,
};
