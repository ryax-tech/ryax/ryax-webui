// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { RouterState, StoreRouterConnectingModule } from '@ngrx/router-store';
import { NavigationFeatureKey, NavigationReducer } from './+state/navigation.reducer';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature(NavigationFeatureKey, NavigationReducer),
    StoreRouterConnectingModule.forRoot({
      routerState: RouterState.Minimal
    }),
  ],
})
export class DomainNavigationModule {}
