// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthDomainModule } from '@ryax/auth/domain';
import { SharedUiCommonModule } from '@ryax/shared/ui-common';
import { SharedUiLayoutModule } from '@ryax/shared/ui-layout';
import { AuthLoginComponent } from './auth-login/auth-login.component';
import { AuthLoginFormComponent } from './auth-login-form/auth-login-form.component';
import { AuthLoginAlertComponent } from './auth-login-alert/auth-login-alert.component';

@NgModule({
  imports: [
    CommonModule,
    AuthDomainModule,
    SharedUiCommonModule,
    SharedUiLayoutModule
  ],
  declarations: [
    AuthLoginComponent,
    AuthLoginFormComponent,
    AuthLoginAlertComponent
  ],
  exports: [AuthLoginComponent]
})
export class AuthFeatureLoginModule {}
