// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { AccountLogin } from '@ryax/auth/domain';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'ryax-auth-login-form',
  templateUrl: './auth-login-form.component.pug'
})
export class AuthLoginFormComponent {
  @Input() loading: boolean;
  @Output() login = new EventEmitter<AccountLogin>();

  form = this.formBuilder.group({
    username: this.formBuilder.control(null, Validators.required),
    password: this.formBuilder.control(null, Validators.required)
  });

  constructor(
    private readonly formBuilder: FormBuilder
  ) {}

  onLogin() {
    this.login.emit(this.form.value);
  }
}
