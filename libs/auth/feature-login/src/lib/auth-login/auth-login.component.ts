// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component } from '@angular/core';
import { AccountLogin, AuthLoginFacade } from '@ryax/auth/domain';

@Component({
  selector: 'ryax-auth-login',
  templateUrl: './auth-login.component.pug',
  styleUrls: ['./auth-login.component.scss']
})
export class AuthLoginComponent {
  loading$ = this.facade.loading$;
  error$ = this.facade.error$;

  constructor(
    private readonly facade: AuthLoginFacade
  ) {}

  onLogin(data: AccountLogin) {
    this.facade.login(data);
  }
}
