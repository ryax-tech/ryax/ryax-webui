// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, Input } from '@angular/core';
import { AccountLoginError } from '@ryax/auth/domain';

@Component({
  selector: 'ryax-auth-login-alert',
  templateUrl: './auth-login-alert.component.pug'
})
export class AuthLoginAlertComponent {
  @Input() error: AccountLoginError;

  private readonly messageMap: Object = {
    [AccountLoginError.ServiceUnavailable]: "Service unavailable",
    [AccountLoginError.AuthenticationFailed]: "Authentication failed",
  };

  get message() {
    if(this.error in this.messageMap) {
      return this.messageMap[this.error];
    }
    return "Unknown error"
  }
}
