// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
export enum AccountRole {
  Admin = "Admin",
  Developer = "Developer",
  User = "User",
  Anonymous = "Anonymous"
}

export interface AccountInfos {
  id: string;
  username: string,
  role: AccountRole,
  email: string,
  comment: string,
}

export type AccountToken = string;

export interface AccountLogin {
  username: string;
  password: string;
}

export enum AccountLoginError {
  AuthenticationFailed = "AuthenticationFailed",
  ServiceUnavailable = "ServiceUnavailable"
}
