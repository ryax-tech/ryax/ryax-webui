// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { AccountDto, LoginResponseDto } from '../dtos';
import { AccountInfos, AccountRole, AccountToken } from '@ryax/auth/domain';

@Injectable()
export class AuthApiAdapter {

  adaptLoginResponse(dto: LoginResponseDto): AccountToken {
    return dto.jwt
  }

  adaptAccount(dto: AccountDto): AccountInfos {
    return {
      id: dto.id,
      username: dto.username,
      role: dto.role as AccountRole,
      email: dto.email,
      comment: dto.comment,
    }
  }
}
