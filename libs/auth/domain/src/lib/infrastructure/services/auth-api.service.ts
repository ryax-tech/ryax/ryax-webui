// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AccountDto, LoginDto, LoginResponseDto } from '../dtos';
import { map } from 'rxjs/operators';
import { AuthApiAdapter } from '../adapters';

@Injectable()
export class AuthApi {
  private readonly baseUrl = "/api/authorization";

  constructor(
    private readonly http: HttpClient,
    private readonly adapter: AuthApiAdapter
  ) {}

  private makeUrl(...urlParts: string[]) {
    return [this.baseUrl, ...urlParts].join('/');
  }

  login(data: LoginDto) {
    const url = this.makeUrl('login');
    return this.http.post<LoginResponseDto>(url, data).pipe(
      map(dto => this.adapter.adaptLoginResponse(dto))
    );
  }

  logout() {
    const url = this.makeUrl('logout');
    return this.http.get<void>(url)
  }

  loadAccount() {
    const url = this.makeUrl('me');
    return this.http.get<AccountDto>(url).pipe(
      map(dto => this.adapter.adaptAccount(dto))
    );
  }
}
