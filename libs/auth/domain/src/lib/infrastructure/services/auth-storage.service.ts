// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';

@Injectable()
export class AuthStorage {
  private storageKey = "RYAX_TOKEN";

  getToken() {
    return localStorage.getItem(this.storageKey)
  }

  saveToken(token: string) {
    localStorage.setItem(this.storageKey, token);
  }

  clearToken() {
    localStorage.removeItem(this.storageKey);
  }
}
