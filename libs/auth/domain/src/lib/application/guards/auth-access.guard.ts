// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanActivateChild, RouterStateSnapshot } from '@angular/router';
import { Store } from '@ngrx/store';
import { first, map } from 'rxjs/operators';
import { AuthState, selectAccountInfos } from '../reducers';
import { AuthAccessActions } from '../actions';

@Injectable()
export class AuthAccessGuard implements CanActivate, CanActivateChild {
  private readonly account$ = this.store.select(selectAccountInfos);

  constructor(
    private readonly store: Store<AuthState>
  ) {}

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    this.store.dispatch(AuthAccessActions.loadAccount(state.url));
    return this.account$.pipe(
      first(account => !!account),
      map(() => true)
    );
  }

  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.canActivate(childRoute, state);
  }

}
