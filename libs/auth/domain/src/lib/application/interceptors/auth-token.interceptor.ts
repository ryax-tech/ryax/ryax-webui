// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import {
  HTTP_INTERCEPTORS,
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { catchError, flatMap, map, withLatestFrom } from 'rxjs/operators';
import { throwError as observableThrowError } from 'rxjs/internal/observable/throwError';
import { AuthState, selectAccountToken } from '../reducers';


@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  private readonly authToken$ = this.store.select(selectAccountToken);

  constructor(
    private readonly store: Store<AuthState>,
  ) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return of(req).pipe(
      withLatestFrom(this.authToken$),
      map(([request, token]) => this.handleProcess(request, token)),
      flatMap(request => next.handle(request)),
      catchError((e) => this.handleError(e))
    );
  }

  private handleProcess(request: HttpRequest<any>, token: string) {
    if(token) {
      return request.clone({
        headers: request.headers.set("Authorization", token)
      });
    } else {
      return request;
    }
  }

  private handleError(err: HttpErrorResponse): Observable<any> {
    return observableThrowError(err);
  }
}

export const AuthInterceptorProvider = {
  provide: HTTP_INTERCEPTORS,
  useClass: AuthInterceptor,
  multi: true
};
