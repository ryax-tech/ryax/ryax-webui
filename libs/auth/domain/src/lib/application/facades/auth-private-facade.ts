// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { AuthState } from '../reducers';
import { Store } from '@ngrx/store';
import { AuthPrivateActions } from '../actions';

@Injectable()
export class AuthPrivateFacade {

  constructor(
    private readonly store: Store<AuthState>
  ) {}

  logout() {
    this.store.dispatch(AuthPrivateActions.logout())
  }
}
