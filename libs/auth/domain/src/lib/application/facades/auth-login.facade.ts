// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { AuthState, selectLoginError, selectLoginLoading, selectAccountToken } from '../reducers';
import { AccountLogin } from '../../entities';
import { AuthLoginActions } from '../actions';

@Injectable()
export class AuthLoginFacade {
  loading$ = this.store.select(selectLoginLoading);
  error$ = this.store.select(selectLoginError);
  token$ = this.store.select(selectAccountToken);

  constructor(
    private readonly store: Store<AuthState>
  ) {}

  login(data: AccountLogin) {
    this.store.dispatch(AuthLoginActions.login(data))
  }
}
