// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { ActionReducer, INIT, META_REDUCERS } from '@ngrx/store';
import { AuthPrivateActions } from '../actions';

export function logout(reducer: ActionReducer<any>): ActionReducer<any> {
  return function(state, action) {
    if (!!action && !!action.type && action.type === AuthPrivateActions.logoutSuccess.type) {
      return reducer(undefined, { type: INIT });
    } else {
      return reducer(state, action);
    }
  }
}

export const AuthLogoutMetaReducerProvider = {
  provide: META_REDUCERS,
  useFactory: () => logout,
  multi: true
};
