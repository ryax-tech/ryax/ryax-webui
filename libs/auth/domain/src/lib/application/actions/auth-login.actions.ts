// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createAction } from '@ngrx/store';
import { AccountLogin, AccountLoginError } from '../../entities';

export const login = createAction(
  '[Auth Login] Login',
  (accountLogin: AccountLogin) => ({
    payload: { accountLogin }
  })
);

export const loginSuccess = createAction(
  '[Auth Login] Login success',
  (accountToken: string) => ({
    payload: { accountToken }
  })
);

export const loginError = createAction(
  '[Auth Login] Login error',
  (error: AccountLoginError) => ({
    payload: { error }
  })
);
