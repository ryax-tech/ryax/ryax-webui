// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createAction } from '@ngrx/store';
import { AccountInfos, AccountToken } from '../../entities';

export const loadAccountToken = createAction(
  '[Auth Access] Load account token'
);

export const loadAccountTokenSuccess = createAction(
  '[Auth Access] Load account token success',
  (accountToken: AccountToken) => ({
    payload: { accountToken }
  })
);

export const loadAccountTokenError = createAction(
  '[Auth Access] Load account token error'
);

export const loadAccount = createAction(
  '[Auth Access] Load account',
  (url?: string) => ({
    payload: url
  })
);

export const loadAccountSuccess = createAction(
  '[Auth Access] Load account success',
  (account: AccountInfos) => ({
    payload: { account }
  })
);

export const loadAccountError = createAction(
  '[Auth Access] Load account error',
  (url: string) => ({
    payload: url
  })
);

export const saveLoginRedirect = createAction(
  '[Auth Access] Save login redirect',
  (url: string) => ({
    payload: url
  })
);
