// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createAction } from '@ngrx/store';

export const logout = createAction(
  '[Auth Private] Logout'
);

export const logoutSuccess = createAction(
  '[Auth Private] Logout success'
);

export const logoutError = createAction(
  '[Auth Private] Logout error'
);
