// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { ModalService } from '@ryax/shared/ui-common';
import { NavigationUtils } from '@ryax/domain/navigation';
import { AuthApi, AuthStorage } from '../../infrastructure/services';
import { Router } from '@angular/router';
import { AuthLoginActions, AuthPrivateActions } from '../actions';
import { catchError, filter, map, switchMap, tap } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable()
export class AuthPrivateEffects {
  constructor(
    private readonly actions$: Actions,
    private readonly modal: ModalService,
    private readonly authApi: AuthApi,
    private readonly authStorage: AuthStorage,
    private readonly router: Router
  ) {}

  logout$ = createEffect(() => this.actions$.pipe(
    ofType(AuthPrivateActions.logout),
    switchMap(() => this.modal.confirm('Logout ?', 'Are you sure you want to logout?', "Logout")),
    filter(modalResult => !!modalResult),
    switchMap(() => this.authApi.logout().pipe(
      map(() => AuthPrivateActions.logoutSuccess()),
      catchError(() => of(AuthPrivateActions.logoutError()))
    ))
  ));

  redirectToLogin$ = createEffect(() => this.actions$.pipe(
    ofType(AuthPrivateActions.logoutSuccess),
    tap(() => this.router.navigateByUrl(NavigationUtils.getLoginUrl()))
  ), { dispatch: false });

  clearToken$ = createEffect(() => this.actions$.pipe(
    ofType(AuthPrivateActions.logoutSuccess),
    tap(()  => this.authStorage.clearToken())
  ), { dispatch: false });
}
