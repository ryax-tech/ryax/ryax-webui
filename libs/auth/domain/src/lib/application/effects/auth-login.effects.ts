// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { AuthLoginActions } from '../actions';
import { catchError, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';
import { AuthApi, AuthStorage } from '../../infrastructure/services';
import { of } from 'rxjs';
import { Router } from '@angular/router';
import { NavigationUtils } from '@ryax/domain/navigation';
import { HttpErrorResponse } from '@angular/common/http';
import { AccountLoginError } from '../../entities';
import { AuthState, selectLoginRedirect } from '../reducers';
import { Store } from '@ngrx/store';

@Injectable()
export class AuthLoginEffects {
  private redirectUrl$ = this.store.select(selectLoginRedirect);

  constructor(
    private readonly actions$: Actions,
    private readonly authApi: AuthApi,
    private readonly authStorage: AuthStorage,
    private readonly router: Router,
    private store: Store<AuthState>
  ) {}

  login$ = createEffect(() => this.actions$.pipe(
    ofType(AuthLoginActions.login),
    switchMap(({ payload }) => this.authApi.login(payload.accountLogin).pipe(
      map((token) => AuthLoginActions.loginSuccess(token)),
      catchError((err: HttpErrorResponse) => {
        if(err.status === 504) {
          return of(AuthLoginActions.loginError(AccountLoginError.ServiceUnavailable))
        }
        return of(AuthLoginActions.loginError(AccountLoginError.AuthenticationFailed))
      })
    ))
  ));

  redirectToIndex$ = createEffect(() => this.actions$.pipe(
    ofType(AuthLoginActions.loginSuccess),
    withLatestFrom(this.redirectUrl$, (_, url) => url),
    tap((url: string) => this.router.navigateByUrl( url ? url : NavigationUtils.getIndexUrl()))
  ), { dispatch: false });

  saveToken$ = createEffect(() => this.actions$.pipe(
    ofType(AuthLoginActions.loginSuccess),
    tap(({ payload }) => this.authStorage.saveToken(payload.accountToken))
  ), { dispatch: false });
}
