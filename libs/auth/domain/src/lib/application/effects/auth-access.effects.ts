// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType, OnInitEffects } from '@ngrx/effects';
import { AuthApi, AuthStorage } from '../../infrastructure/services';
import { Router } from '@angular/router';
import { AuthAccessActions, AuthLoginActions } from '../actions';
import { catchError, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';
import { of } from 'rxjs';
import { NavigationUtils } from '@ryax/domain/navigation';
import { Store } from '@ngrx/store';
import { AuthState } from '../reducers';

@Injectable()
export class AuthAccessEffects implements OnInitEffects {

  constructor(
    private readonly actions$: Actions,
    private readonly authApi: AuthApi,
    private readonly authStorage: AuthStorage,
    private readonly router: Router,
    private readonly store: Store<AuthState>
  ) {}

  loadAccountToken$ = createEffect(() => this.actions$.pipe(
    ofType(AuthAccessActions.loadAccountToken),
    switchMap(() => of(this.authStorage.getToken()).pipe(
      map(accountToken => AuthAccessActions.loadAccountTokenSuccess(accountToken)),
      catchError(() => of(AuthAccessActions.loadAccountError(null)))
    ))
  ));

  loadAccount$ = createEffect(() => this.actions$.pipe(
    ofType(AuthAccessActions.loadAccount),
    switchMap(({payload}) => this.authApi.loadAccount().pipe(
      map((account) => AuthAccessActions.loadAccountSuccess(account)),
      catchError((url) => of(AuthAccessActions.loadAccountError(payload)))
    ))
  ));

  redirectToLogin$ = createEffect(() => this.actions$.pipe(
    ofType(AuthAccessActions.loadAccountError),
    tap(({payload}) => {
      this.store.dispatch(AuthAccessActions.saveLoginRedirect(payload));
      this.router.navigateByUrl(NavigationUtils.getLoginUrl());
    })
  ), { dispatch: false });

  ngrxOnInitEffects() {
    return AuthAccessActions.loadAccountToken();
  }
}
