// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { InjectionToken } from '@angular/core';
import { ActionReducerMap, createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromLogin from './login.reducer';
import * as fromAccount from './account.reducer';

export const AuthFeatureKey = 'authDomain';

export interface AuthState {
  login: fromLogin.State,
  account: fromAccount.State
}

export const AuthReducerToken = new InjectionToken<ActionReducerMap<AuthState>>(AuthFeatureKey);

export const AuthReducerProvider = {
  provide: AuthReducerToken,
  useValue: {
    login: fromLogin.reducer,
    account: fromAccount.reducer,
  },
};

export const selectFeatureState = createFeatureSelector<AuthState>(AuthFeatureKey);
export const selectLoginState = createSelector(selectFeatureState, state => state.login);
export const selectAccountState = createSelector(selectFeatureState, state => state.account);

export const selectLoginLoading = createSelector(selectLoginState, fromLogin.selectLoading);
export const selectLoginError = createSelector(selectLoginState, fromLogin.selectError);

export const selectAccountInfos = createSelector(selectAccountState, fromAccount.selectInfos);
export const selectAccountToken = createSelector(selectAccountState, fromAccount.selectToken);
export const selectLoginRedirect = createSelector(selectAccountState, fromAccount.selectLoginRedirect);
