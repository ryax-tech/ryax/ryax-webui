// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createReducer, on } from '@ngrx/store';
import { AuthLoginActions } from '../actions';
import { AccountLoginError } from '@ryax/auth/domain';

export interface State {
  loading: boolean;
  error: AccountLoginError;
}

export const initialState: State = {
  loading: false,
  error: null
};

export const reducer = createReducer<State>(
  initialState,
  on(AuthLoginActions.login, (state) => ({
    ...state,
    loading: true,
    error: null
  })),
  on(AuthLoginActions.loginSuccess, (state, { payload }) => ({
    ...state,
    loading: false,
    error: null
  })),
  on(AuthLoginActions.loginError, (state, { payload }) => ({
    ...state,
    loading: false,
    error: payload.error
  })),
);


export const selectLoading = (state: State) => state.loading;
export const selectError = (state: State) => state.error;
