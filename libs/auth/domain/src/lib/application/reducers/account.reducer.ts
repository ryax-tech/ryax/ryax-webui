// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createReducer, on } from '@ngrx/store';
import { AuthAccessActions, AuthLoginActions, AuthPrivateActions } from '../actions';
import { AccountInfos } from '../../entities';

export interface State {
  loading: boolean;
  infos: AccountInfos;
  token: string;
  loginRedirect: string;
}

export const initialState: State = {
  loading: false,
  infos: null,
  token: null,
  loginRedirect: null
};

export const reducer = createReducer<State>(
  initialState,
  on(AuthLoginActions.loginSuccess, (state, { payload }) => ({
    ...state,
    token: payload.accountToken
  })),
  on(AuthLoginActions.loginError, (state) => ({
    ...state,
    token: null
  })),
  on(AuthPrivateActions.logoutSuccess, (state) => ({
    ...state,
    infos: null,
    token: null
  })),
  on(AuthPrivateActions.logoutError, (state) => ({
    ...state,
    account: null,
    accountToken: null
  })),
  on(AuthAccessActions.loadAccount, (state) => ({
    ...state,
    loading: true,
  })),
  on(AuthAccessActions.loadAccountSuccess, (state, { payload }) => ({
    ...state,
    loading: false,
    infos: payload.account
  })),
  on(AuthAccessActions.loadAccountError, (state ) => ({
    ...state,
    loading: false,
    infos: null
  })),
  on(AuthAccessActions.loadAccountTokenSuccess, (state, { payload }) => ({
    ...state,
    token: payload.accountToken
  })),
  on(AuthAccessActions.loadAccountTokenError, (state) => ({
    ...state,
    token: null
  })),
  on(AuthAccessActions.saveLoginRedirect, (state, { payload }) => ({
    ...state,
    loginRedirect: payload
  })),
);


export const selectLoading = (state: State) => state.loading;
export const selectInfos = (state: State) => state.infos;
export const selectToken = (state: State) => state.token;
export const selectLoginRedirect = (state: State) => state.loginRedirect;
