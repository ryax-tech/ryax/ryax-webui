// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { AuthFeatureKey, AuthReducerProvider, AuthReducerToken } from './application/reducers';
import { AuthAccessEffects, AuthLoginEffects, AuthPrivateEffects } from './application/effects';
import { AuthLoginFacade, AuthPrivateFacade } from './application/facades';
import { AuthApi, AuthStorage } from './infrastructure/services';
import { AuthAccessGuard } from './application/guards';
import { AuthInterceptorProvider } from './application/interceptors';
import { AuthLogoutMetaReducerProvider } from './application/meta-reducers';
import { AuthApiAdapter } from './infrastructure/adapters';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature(AuthFeatureKey, AuthReducerToken),
    EffectsModule.forFeature([AuthLoginEffects, AuthPrivateEffects, AuthAccessEffects])
  ],
  providers: [
    AuthReducerProvider,
    AuthLogoutMetaReducerProvider,
    AuthLoginFacade,
    AuthPrivateFacade,
    AuthAccessGuard,
    AuthInterceptorProvider,
    AuthApi,
    AuthApiAdapter,
    AuthStorage,
  ]
})
export class AuthDomainModule {}
