// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { NgModule } from '@angular/core';
import { AuthFeaturePortalModule } from '@ryax/auth/feature-portal';
import { AuthReturnComponent } from '../../../feature-portal/src/lib/auth-return/auth-return.component';

@NgModule({
  imports: [
    AuthFeaturePortalModule
  ],
  exports: [
    AuthReturnComponent
  ]
})
export class AuthApiModule {}
