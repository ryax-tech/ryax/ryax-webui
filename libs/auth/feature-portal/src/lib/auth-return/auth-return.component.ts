// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component } from '@angular/core';
import { Location } from '@angular/common';
import { AuthLoginFacade } from '@ryax/auth/domain';

@Component({
  selector: 'ryax-auth-return',
  templateUrl: './auth-return.component.pug',
  styleUrls: ['./auth-return.component.scss'],
})
export class AuthReturnComponent {
  token$ = this.authFacade.token$;

  constructor(
    private _location: Location,
    private authFacade: AuthLoginFacade
  ) {}

  goBack() {
    this._location.back();
  }
}
