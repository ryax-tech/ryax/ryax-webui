// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthDomainModule } from '@ryax/auth/domain';
import { SharedUiCommonModule } from '@ryax/shared/ui-common';
import { SharedUiLayoutModule } from '@ryax/shared/ui-layout';
import { SharedUiNavigationModule } from '@ryax/shared/ui-navigation';
import { AuthPrivateComponent } from './auth-private/auth-private.component';

@NgModule({
  imports: [
    CommonModule,
    AuthDomainModule,
    SharedUiCommonModule,
    SharedUiLayoutModule,
    SharedUiNavigationModule
  ],
  declarations: [
    AuthPrivateComponent
  ],
  exports: [
    AuthPrivateComponent
  ]
})
export class AuthFeaturePrivateModule {}
