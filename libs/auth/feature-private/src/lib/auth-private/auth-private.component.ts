// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component } from '@angular/core';
import { AuthPrivateFacade } from '@ryax/auth/domain';

@Component({
  selector: 'ryax-auth-private',
  templateUrl: './auth-private.component.pug'
})
export class AuthPrivateComponent {

  constructor(
    private readonly facade: AuthPrivateFacade
  ) {}

  onLogout() {
    this.facade.logout();
  }
}
