// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
export interface Algo {
  id: number,
  fileInputName: string;
  frameHeight: number;
  input: {
    initialStatement: string,
    descriptions?: string[],
    fileUrl: string,
    fileName: string
  },
  output: {
    initialStatement: string,
    descriptions?: string[],
    fileUrl: string,
  },
  parameters?: Array<{
    name: string,
    value: string | number,
    description: string
  }>
}

export const algoList: Algo[] = [
  {
    id: 152595,
    fileInputName: 'Historical Sales Data',
    frameHeight: 500,
    input: {
      initialStatement: 'We\'ll be using historical sales data containing the following information:',
      descriptions: [
        'InvoiceNo: ID of the actual purchase',
        'StockCode: SKU number (or SKU ID)',
        'Description: written description of the item/SKU',
        'Quantity: number of the same SKU being sold to the same client (if applicable, otherwise it will be "1")',
        'InvoiceDate: actual purchase date',
        'UnitPrice: SKU/Item unit price',
        'CustomerID: ID number of the client',
        'Country: Country where the purchase has been made'
      ],
      fileUrl: 'https://ryax.tech/wp-content/uploads/2021/12/donnees-retail-1.xls',
      fileName: 'Sales_Data.xlsx'
    },
    output: {
      initialStatement: 'Output is the list of all your customers along with their status (High/Medium/Low value)',
      fileUrl: 'https://ryax.tech/wp-content/uploads/2021/12/smart_segmentation.zip'
    }
  },
  {
    id: 149735,
    fileInputName: 'Historical Sales Data',
    frameHeight: 450,
    input: {
      initialStatement: 'We\'ll be using historical sales data containing the following information :',
      descriptions: [
        'Date : historical timestep of observation (could be daily, monthly, weekly…)',
        'Revenue : sales in quantity or revenue'
      ],
      fileUrl: 'https://ryax.tech/wp-content/uploads/2021/12/winedata.csv',
      fileName: 'Sales_Data.xlsx'
    },
    output: {
      initialStatement: 'You get two deliverables :',
      descriptions: [
        'The predicted sales',
        'A graphical presentation of the predictions'
      ],
      fileUrl: 'https://ryax.tech/wp-content/uploads/2021/12/a0360c22-bba0-462c-b2d7-c7fb641bc893-archive.zip'
    },
    parameters: [
      {
        name: 'Validation period',
        value: 12,
        description: 'period of time till the last observation where you want the model to be evaluated to verify its performance. In our we\'d like the model performance to be checked for the last 12 months of observations.'
      },
      {
        name: 'Timesteps to forward',
        value: 24,
        description: 'how long you\'d like this prediction to go. In our example, we\'d like to get a 24-month prediction of the sales.'
      }
    ]
  },
  {
    id: 149734,
    fileInputName: 'Historical Sales Data',
    frameHeight: 500,
    input: {
      initialStatement: 'We\'ll be using historical sales data containing the following information:',
      descriptions: [
        'InvoiceNo: ID of the actual purchase',
        'StockCode: SKU number (or SKU ID)',
        'Description: written description of the item/SKU',
        'Quantity: number of the same SKU being sold to the same client (if applicable, otherwise it will be "1")',
        'InvoiceDate: actual purchase date',
        'UnitPrice: SKU/Item unit price',
        'CustomerID: ID number of the client',
        'Country: Country where the purchase has been made'
      ],
      fileUrl: 'https://ryax.tech/wp-content/uploads/2021/12/donnees-retail.xls',
      fileName: 'Sales_Data.xlsx'
    },
    output: {
      initialStatement: 'You receive for every customer the top 4 product recommendations ranked by probability.',
      fileUrl: 'https://ryax.tech/wp-content/uploads/2021/12/prediction_result-2.xlsx'
    }
  },
  {
    id: 138204,
    fileInputName: 'Historical Sales Data',
    frameHeight: 400,
    input: {
      initialStatement: 'We\'ll be using historical sales data containing the following information :',
      descriptions: [
        'Date: historical timestep of observation (could be daily, monthly, weekly…)',
        'SKU: SKU ID, item identification number',
        'Revenue: sales in quantity or revenue'
      ],
      fileUrl: 'https://ryax.tech/wp-content/uploads/2021/12/multiple_sku_timeseries.csv',
      fileName: 'Sales_Data.xlsx'
    },
    output: {
      initialStatement: 'You get two deliverables :',
      descriptions: [
        'the predicted sales per SKU',
        'a graphical presentation of the predictions',
      ],
      fileUrl: 'https://ryax.tech/wp-content/uploads/2021/12/forecast.zip'
    }
  },
  {
    id: 138195,
    fileInputName: 'Historical Sales Data',
    frameHeight: 500,
    input: {
      initialStatement: 'We\'ll be using historical sales data containing the following information:',
      descriptions: [
        'InvoiceNo: ID of the actual purchase',
        'StockCode: SKU number (or SKU ID)',
        'Description: written description of the item/SKU',
        'Quantity: number of the same SKU being sold to the same client (if applicable, otherwise it will be "1")',
        'InvoiceDate: actual purchase date',
        'UnitPrice: SKU/Item unit price',
        'CustomerID: ID number of the client',
        'Country: Country where the purchase has been made'
      ],
      fileUrl: 'https://ryax.tech/wp-content/uploads/2021/12/High-potential.xlsx',
      fileName: 'Sales_Data.xlsx'
    },
    output: {
      initialStatement: 'You are delivered the list of your customer IDs along with the prediction results :',
      descriptions: [
        '"1" : the customer will come back within the defined period',
        '"0" : the customer will not come back within the defined period',
      ],
      fileUrl: 'https://ryax.tech/wp-content/uploads/2021/12/customers_will_return4.xlsx',
    }
  }
];
