// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, OnInit } from '@angular/core';
import { Algo, algoList } from './algo';
import { ActivatedRoute } from '@angular/router';

declare let gtag:Function;

@Component({
  selector: 'ryax-store-catalog',
  templateUrl: './tutorial.component.pug',
  styleUrls: ['./tutorial.component.scss']
})
export class TutorialComponent implements OnInit {

  public fileName = '';
  public loading = false;
  public stepCount = 0;
  public algo: Algo;

  constructor(
    private route: ActivatedRoute
  ) {}

  public ngOnInit() {
    this.algo = algoList.find((item) => item.id === +this.route.snapshot.params.id);
  }

  public onDragStart(event: DragEvent) {
    event.dataTransfer.effectAllowed = 'move';
    event.dataTransfer.setData('text/plain', 'File');
    if (gtag) {
      gtag('event', 'Drag', {
        eventCategory: 'Fake form',
        eventLabel: 'Started Drag',
        eventAction: 'onDragStart',
        eventValue: +this.route.snapshot.params.id
      })
    }
  }

  public beforeUpload = (): boolean => {
    return false;
  };

  public onDrop(event) {
    const check = event.dataTransfer.getData('text/plain');
    if (check === 'File') {
      this.fileName = this.algo.input.fileName;
    }
    this.stepCount++;
    if (gtag) {
      gtag('event', 'Drop', {
        eventCategory: 'Fake form',
        eventLabel: 'Drag in the right place',
        eventAction: 'onDrop',
        eventValue: +this.route.snapshot.params.id
      })
    }
  }

  public start() {
    this.loading = true;
    this.stepCount++;

    if (gtag) {
      gtag('event', 'Submit', {
        eventCategory: 'Fake form',
        eventLabel: 'Submit form',
        eventAction: 'start',
        eventValue: +this.route.snapshot.params.id
      })
    }

    setTimeout(this.stop.bind(this), 3000);
  }

  public stop() {
    this.loading = false;
    this.stepCount++;
  }

  public startDLInput() {
    if (gtag) {
      gtag('event', 'Click', {
        eventCategory: 'Fake form',
        eventLabel: 'DL input Data',
        eventAction: 'startDLInput',
        eventValue: +this.route.snapshot.params.id
      })
    }
  }

  public startDLOutput() {
    if (gtag) {
      gtag('event', 'Click', {
        eventCategory: 'Fake form',
        eventLabel: 'DL Output Data',
        eventAction: 'startDLOutput',
        eventValue: +this.route.snapshot.params.id
      })
    }
  }
}
