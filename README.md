# Ryax WebUI

## Quick start
1. Run `yarn install` to install dependencies
2. Open terminal in project and run `yarn mock-api` to start mock api server.
2. Open terminal in project and run `yarn start` to start development server.
3. Open your browser on [web interface](http://localhost:9876/)
4. Congratulations, you can now start to contributing on ryax-webui project.

## Development server
Run `yarn start` in project will start development server which server webui on [http://localhost:9876/](http://localhost:9876/)
The app will automatically reload if you change any of the source files.

### Develop using mock api server
This project embed a mock server to simulate api responses. To use this mock server run `yarn mock-api` in project.

Environments are used to define which default data used on the mock server.

Available environments are :
* default : Environment use for dev process.
* demo : Environment used for demo app accessed from ryax web site

If you want to use a specific environment (different from `default`), define it in the `MOCK_ENV` environment variable before running command.

### Using local api server
Using `yarn start` command, the development server will redirect api/* requests to http://localhost:3000 by default (Url is rewritten by development server proxy).
If you need to change target port, define it in the `TARGET_PORT` environment variable before running command.

NOTE: You can also test app with production build by using `yarn start:production` command.

### Using testing api server
Using `yarn start:testing` command, the development server will redirect api/* requests to https://testing-1.ryax.org/api by default (Url is not rewritten by development server proxy).
If you need to change target port, define it in the `TARGET_PORT` environment variable before running command (e.g https://testing-1.ryax.org:{port}/api).
If you need to change target path, define it in the `TARGET_PATH` environment variable before running command (e.g https://testing-1.ryax.org/{path}/api).

NOTE: You can also test app with production build by using `yarn start:testing-production` command.

## Unit test
Run `yarn test` command in project to execute the unit tests via [Jest](https://jestjs.io/).

## Build
Run `yarn build` command in project to build app.
Run `yarn build:production` command in project to build app with production configuration.
Run `yarn build:release` command in project to build app with release configuration (same as production configuration but with some optimizations).

Note : The build artifacts will be stored in the `dist/` directory.

## Advanced commands

### Run in a container

With Docker installed run:
```sh
docker run -ti -v ./dist/apps/dashboard:/data -p 8080:80 ryaxtech/ryax-base-webui
```

Or build the container with the app injected inside with:
```sh
nix-build
docker load < ./result
docker run -p 8080:80 ryax-webui:latest
```

With both method you can access the Ryax UI at [http://localhost:8080](http://localhost:8080).

### Test inside a Ryax deployment

Assuming you have a running Ryax deployment, you can swap the webui version
with any version built by the CI with the following process:

1. Get the tag of the image you want to deploy. Each image is tagged with the
   short version of the commit hash with the first 8 characters (for example
   `3a813a9b`). You can also use the `latest` tag but it is not recommended
   because the image pointed by this tag changes at every successful CI
   pipeline.

2. Then, we need to patch the deployment definition inside Kubernetes. To do so
   be sure that you have the `kubectl` tool installed and that you have access
   to the `kubeconfig.yaml` file which contains the cluster access credentials.
   To create a patch file, you can copy the patch example given in
   [./tools/patch-deployment.yaml] and change the image tag with your own. Or
   generate it with the following script:
   ```sh
   TAG=latest
   # Be sur that the container registry is up-to-date
   REGISTRY=0l9w8226.gra5.container-registry.ovh.net/ryax
   cat > patch-deployment.yaml <<EOF
   spec:
     template:
       spec:
         containers:
         - name: ryax-webui
           image: $REGISTRY/ryax-webui:$TAG
   EOF
   ```

2. Now you can apply a configuration patch to the Ryax webUI deployment with
   your image version. This will directly kill the previous image and replace
   it by the selected one.
   ```sh
   # Set the KUBECONFIG varaible
   KUBECONFIG=./kubeconfig.yaml
   # Apply the patch
   kubectl patch deployment -n ryaxns ryax-webui --patch "$(cat patch-deployment.yaml)"
   ```

3. Check that the deployment is over with:
   ```sh
   kubectl get pods -n ryaxns
   ```
   The ryax-webui pod should be `Running`.

## Deploy

In order to deploy mock stack for demo :

1. Export kubeconfig definition:
    ```shell script
    export KUBECONFIG=<kubeconfig_file_path>
   ```

2. Get a aws authorization token:
   ````shell script
   export REGISTRY_PASSWORD="$(aws ecr get-authorization-token --output text --query 'authorizationData[].authorizationToken' | base64 -D| cut -d: -f2)"
    ````
   
3. Deploy the mock stack to k8s cluster :
   ```shell script 
   ./tools/deploy/deploy-mock.sh
   ```
