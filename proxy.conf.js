// Target api port
const targetPort = process.env['TARGET_PORT'] ? process.env['TARGET_PORT'] : 3000;

module.exports = {
  // Redirect to fake server
  '/api': {
    target: `http://localhost:${targetPort}`,
    secure: false,
    changeOrigin: true,
    pathRewrite: {
      '^/api': ''
    },
    bypass: function(req, res, proxyOptions) {
      req.headers["X-Forwarded-Prefix"] = "/api";
    },
  }
};
