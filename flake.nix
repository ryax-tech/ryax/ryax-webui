{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-22.11";
    nix2container.url = "github:nlewo/nix2container";
    nix2container.inputs.nixpkgs.follows = "nixpkgs";
    flakeUtils.follows = "nix2container/flake-utils";
  };


  outputs = { self, nixpkgs, nix2container, flakeUtils }:
    # Change values here to support more arch
    flakeUtils.lib.eachSystem [ "aarch64-linux" "x86_64-linux" ]
      (system:
        let
          pkgs = import nixpkgs { inherit system; };
          nix2containerPkgs = nix2container.packages.${system};
          buidDir = "/tmp/ryax/ryax-webui";
        in
        {
          packages = {
            install = pkgs.writeShellApplication {
              name = "install";
              text = ''
                rm -rf ${buidDir}
                BUILD_TARGET="$1"

                echo -- Building "$BUILD_TARGET" target...
                yarn install
                echo ==========
                echo node version:
                node --version
                echo node in yarn version:
                yarn exec -- node --version
                echo ==========

                # To remove when updated to node 18
                export NODE_OPTIONS=--openssl-legacy-provider

                yarn build:"$BUILD_TARGET"
                set -e && ls dist
                cp -r ./dist/apps/dashboard ${buidDir}
              '';
              runtimeInputs = [ (pkgs.yarn.override { nodejs = pkgs.nodejs-16_x; }) pkgs.nodejs-16_x ];
            };
            image = pkgs.callPackage ./nix/docker.nix {
              appDist = (/. + buidDir);
              nix2container = nix2containerPkgs.nix2container;
            };
          };
          # Enable autoformat
          formatter = pkgs.nixpkgs-fmt;
        }
      );
}
