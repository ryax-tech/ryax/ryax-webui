module.exports = {
  // Redirect to fake server
  '/api': {
    target: `https://staging-1.ryax.io`,
    secure: false,
    changeOrigin: true,
    bypass: function(req) {
      req.headers["X-Forwarded-Prefix"] = "/api";
    }
  },
  '/studio': {
    target: `https://staging-1.ryax.io`,
    secure: false,
    changeOrigin: true
  },
  '/repository': {
    target: `https://staging-1.ryax.io`,
    secure: false,
    changeOrigin: true
  },
  '/authorization': {
    target: `https://staging-1.ryax.io`,
    secure: false,
    changeOrigin: true
  },
  '/functions': {
    target: `https://staging-1.ryax.io`,
    secure: false,
    changeOrigin: true,
    bypass: function(req) {
      req.headers["X-Forwarded-Prefix"] = "/functions";
    }
  }
};
